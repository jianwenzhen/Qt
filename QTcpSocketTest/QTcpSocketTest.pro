#-------------------------------------------------
#
# Project created by QtCreator 2017-08-16T01:19:01
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTcpSocketTest
TEMPLATE = app

LIBS += /usr/lib/aarch64-linux-gnu/libdrm.so.2.4.0

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
