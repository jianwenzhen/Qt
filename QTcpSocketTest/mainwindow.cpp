#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    socket = new QTcpSocket();
    socket->connectToHost("192.168.0.108", 8888);
    connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));
    if(socket->waitForConnected(1000))
    {
        qDebug("success");
    }
    else
    {
        qDebug("failure");
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}
