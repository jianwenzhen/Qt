#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <chrono>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    tm* t = gettm(1503137410000);
    printf("%d", t->tm_year);
}

MainWindow::~MainWindow()
{
    delete ui;
}

tm* MainWindow::gettm(int64_t timestamp)
{
    int64_t milli = timestamp + (int64_t)8*60*60*1000;
    auto mTime = std::chrono::milliseconds(milli);
    auto tp=std::chrono::time_point<std::chrono::system_clock,std::chrono::milliseconds>(mTime);
    auto tt = std::chrono::system_clock::to_time_t(tp);
    tm* now = std::gmtime(&tt);
    printf("%4d year %02d month %02d day %02d:%02d:%02d\n", now->tm_year+1900,now->tm_mon+1,now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec);
    return now;
}
