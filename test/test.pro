#-------------------------------------------------
#
# Project created by QtCreator 2017-08-16T01:26:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test
TEMPLATE = app

LIBS += /usr/lib/aarch64-linux-gnu/libdrm.so.2.4.0

SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

QMAKE_CXXFLAGS += -std=c++0x        #use C++11

FORMS    += mainwindow.ui
