#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QPainter>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QString file = "C:\\Users\\Shinelon\\Pictures\\55b0493342925.jpg";
    QPixmap pix;
    pix.load(file);
    painter.drawPixmap(0, 0, 1920, 1000, pix);
}
