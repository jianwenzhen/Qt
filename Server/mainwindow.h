#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnListen_clicked();
    void on_btnSend_clicked();
    void new_connection();
    void receive_data();

private:
    std::vector<QTcpSocket*>    m_vector_sockets;
    QTcpServer                 *m_pServer;
    QString                     m_receivedData;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
