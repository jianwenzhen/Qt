#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QAbstractSocket>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Server");

    ui->portEdit->setText(QString::number(8888));

    m_pServer = new QTcpServer();
    connect(m_pServer, SIGNAL(newConnection()), this, SLOT(new_connection()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnListen_clicked()
{
    if (ui->btnListen->text() == "Listen")
    {
        m_pServer->listen(QHostAddress::Any, ui->portEdit->text().toInt());
        ui->btnListen->setText("Cancel listen");
    }
    else
    {
        std::vector<QTcpSocket*>::iterator it;
        for(it = m_vector_sockets.begin(); it != m_vector_sockets.end(); it++)
        {
            if ((*it)->state() == QAbstractSocket::ConnectedState && (*it)->state() == QAbstractSocket::ConnectingState)
                (*it)->disconnectFromHost();
            it = m_vector_sockets.erase(it);
        }
        m_pServer->close();
        ui->btnListen->setText("Listen");
    }
}

void MainWindow::on_btnSend_clicked()
{
    QString sendData = ui->sendEdit->text();
    sendData += "\n";
    std::vector<QTcpSocket*>::iterator it;
    for(it = m_vector_sockets.begin(); it != m_vector_sockets.end();)
    {
        if ((*it)->state() != QAbstractSocket::ConnectedState && (*it)->state() != QAbstractSocket::ConnectingState)
        {
            it = m_vector_sockets.erase(it);
        }
        else
        {
            (*it)->write(sendData.toUtf8());
            // it++ 必须写在这里 因为删除一个元素后it后面的下标会全部小一
            it++;
        }
    }
    ui->sendEdit->clear();
}

void MainWindow::new_connection()
{
    QTcpSocket *newSocket = m_pServer->nextPendingConnection();
    m_vector_sockets.push_back(newSocket);
    connect(newSocket, SIGNAL(readyRead()), this, SLOT(receive_data()));
    connect(newSocket, SIGNAL(disconnected()), newSocket, SLOT(deleteLater()));
    QString socket_name = newSocket->peerName();
    socket_name += " connect success!\n";
    newSocket->write(socket_name.toUtf8());
}

void MainWindow::receive_data()
{
    std::vector<QTcpSocket*>::iterator it;
    for(it = m_vector_sockets.begin(); it != m_vector_sockets.end();)
    {
        if ((*it)->state() != QAbstractSocket::ConnectedState && (*it)->state() != QAbstractSocket::ConnectingState)
        {
            it = m_vector_sockets.erase(it);
        }
        else
        {
            m_receivedData += QString((*it)->readAll());
            it++;
        }
    }
    ui->receivedText->setText(m_receivedData);
}
