#coding:utf-8
import os
from os import listdir, getcwd
from os.path import join
#imagepath="images"
#os.system('pwd')
#files = open('scripts/valid1.txt').read().strip().split()  #打开相应类别的训练文件
os.system('chmod 755 gen.sh')
os.system('./gen.sh')         #执行脚本从指定文件夹中读取所有图片并将图片路径写入到valid1.txt中
os.system('python first.py')  #执行 first.py

fd = file('valid1.txt', "r")  #读取批量测试文件路径
lines = fd.readlines()
i=0;
out_file = open('valid.txt', 'w') #与此xml对应的转换后的txt，这个txt的保存完整路径
for line in lines:
    i=i+1;
    out_file.write(line)
    if i%1000==0 or i==len(lines):
        out_file.close()
        os.system('./darknet detector test_batch voc.data yolo-voc.2.0.cfg backup/yolo-voc_9000.weights')
        break
