/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *btnStart;
    QPushButton *btnChgNames;
    QPushButton *btnSelDir;
    QPushButton *btnChgTrainCnt;
    QProgressBar *progressBar;
    QFrame *line_2;
    QLineEdit *maxTrainCount;
    QLabel *label;
    QFrame *line_3;
    QLCDNumber *lcdNumber;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(449, 193);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        btnStart = new QPushButton(centralWidget);
        btnStart->setObjectName(QStringLiteral("btnStart"));
        btnStart->setGeometry(QRect(300, 50, 141, 61));
        btnChgNames = new QPushButton(centralWidget);
        btnChgNames->setObjectName(QStringLiteral("btnChgNames"));
        btnChgNames->setGeometry(QRect(20, 70, 111, 51));
        btnSelDir = new QPushButton(centralWidget);
        btnSelDir->setObjectName(QStringLiteral("btnSelDir"));
        btnSelDir->setGeometry(QRect(20, 10, 111, 51));
        btnChgTrainCnt = new QPushButton(centralWidget);
        btnChgTrainCnt->setObjectName(QStringLiteral("btnChgTrainCnt"));
        btnChgTrainCnt->setGeometry(QRect(160, 90, 111, 25));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setEnabled(true);
        progressBar->setGeometry(QRect(10, 150, 431, 31));
        progressBar->setAutoFillBackground(false);
        progressBar->setValue(24);
        progressBar->setTextVisible(true);
        progressBar->setTextDirection(QProgressBar::TopToBottom);
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setGeometry(QRect(0, 130, 451, 16));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        maxTrainCount = new QLineEdit(centralWidget);
        maxTrainCount->setObjectName(QStringLiteral("maxTrainCount"));
        maxTrainCount->setGeometry(QRect(160, 50, 113, 25));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(160, 10, 221, 31));
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setGeometry(QRect(135, 0, 20, 138));
        line_3->setFrameShape(QFrame::VLine);
        line_3->setFrameShadow(QFrame::Sunken);
        lcdNumber = new QLCDNumber(centralWidget);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));
        lcdNumber->setGeometry(QRect(360, 10, 71, 31));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        btnStart->setText(QApplication::translate("MainWindow", "\345\274\200\345\247\213\350\256\255\347\273\203", Q_NULLPTR));
        btnChgNames->setText(QApplication::translate("MainWindow", "\344\277\256\346\224\271\347\261\273\345\210\253(\345\217\257\351\200\211)", Q_NULLPTR));
        btnSelDir->setText(QApplication::translate("MainWindow", "\351\200\211\346\213\251\345\233\276\347\211\207\347\233\256\345\275\225", Q_NULLPTR));
        btnChgTrainCnt->setText(QApplication::translate("MainWindow", "\344\277\256\346\224\271\350\256\255\347\273\203\346\254\241\346\225\260", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "\350\256\255\347\273\203\345\233\276\347\211\207", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
