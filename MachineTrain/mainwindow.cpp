#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdio.h>
#include <QProcess>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QTimer>
#include <iostream>
#include <sstream>
#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("机器学习");
    QIcon icon(":/src/timg.jpeg");
    this->setWindowIcon(icon);
    QSize size = this->size();
    this->setFixedSize(size);

    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(true);

    // 默认训练9000次
    ui->maxTrainCount->setPlaceholderText("9000");

    ui->lcdNumber->setSegmentStyle(QLCDNumber::Flat);
    QPalette lcdpal = ui->lcdNumber->palette();
    lcdpal.setColor(QPalette::Normal, QPalette::WindowText, Qt::blue);
    ui->lcdNumber->setPalette(lcdpal);

    m_shell = new QProcess(this);
    m_timer =new QTimer();
    m_timer->setInterval(100);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(updateProgress()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);

    if (m_bIsTraining)
    {
        int ret = QMessageBox::question(this, tr("警告"), tr("训练尚未结束，确认关闭程序?"));
        if (QMessageBox::Yes != ret)
        {
            return;
        }

        std::string cmd = "ps -ef | grep darknet";
        FILE *fp = popen(cmd.c_str(), "r");
        if (!fp)
        {
            return;
        }
        /**
         * qk 33715 33699  0 14:54 ? 00:00:00 sh -c ./darknet detector train voc.data yolo-voc.2.0.cfg darknet19_448.conv.23
         * qk 33716 33715 99 14:54 ? 00:00:50 ./darknet detector train voc.data yolo-voc.2.0.cfg darknet19_448.conv.23
         */
        char temp[1024];
        fgets(temp, sizeof(temp), fp);
        std::istringstream input(temp);
        std::string rubbish;
        int id = 0;
        input >> rubbish;
        input >> id;
        id += 1;;

        char killProcess[1024];
        sprintf(killProcess, "kill %d", id);
        m_shell->execute(QString::fromStdString(killProcess));
    }
}

void MainWindow::on_btnChgNames_clicked()
{
    QString cmd;
    cmd = "gedit pasacal.names";
    m_shell->execute(cmd);
}

void MainWindow::on_btnSelDir_clicked()
{
    using namespace std;
    m_strImagePath = QFileDialog::getExistingDirectory(this, tr("Select Images Dir"));
    ifstream fis;
    string line;
    QString str;
    fis.open("gen.sh");
    if (!fis.is_open()) {
        QMessageBox::warning(this, tr("警告！"), tr("打开gen.sh失败！"), QMessageBox::Yes);
        return;
    }
    ofstream fos;
    fos.open("temp.sh");
    while(getline(fis, line)) {
        str = QString::fromStdString(line);
        if (str.startsWith("cd")) {
            str = "cd " + m_strImagePath;
        }
        fos << str.toStdString() << endl;
    }
    fis.close();
    fos.close();
    m_shell->execute("rm gen.sh");
    m_shell->execute("mv temp.sh gen.sh");

    QString labelPath = m_strImagePath;
    labelPath = labelPath.replace("images", "labels");
    fis.open("gen_train.sh");
    if (!fis.is_open()) {
        QMessageBox::warning(this, tr("警告！"), tr("打开gen_train.sh失败！"), QMessageBox::Yes);
        return;
    }
    fos.open("temp.sh");
    while(getline(fis, line)) {
        str = QString::fromStdString(line);
        if (str.startsWith("cd")) {
            str = "cd " + labelPath;
        }
        fos << str.toStdString() << endl;
    }
    fis.close();
    fos.close();
    m_shell->execute("rm gen_train.sh");
    m_shell->execute("mv temp.sh gen_train.sh");
}

void MainWindow::updateProgress()
{
    using namespace std;
    ifstream fis;
    if (!m_bIsTraining) {
        fis.open("m_nProgress");
        if(!fis.is_open()) {
            ui->progressBar->setValue(m_nProgress);
            return;
        }
        fis >> m_nProgress;
        fis.close();
        ui->progressBar->setValue(m_nProgress);
        if (m_nProgress == 100) {
            m_timer->stop();
            setBtnsEnabled(true);
            m_shell->execute("rm m_nProgress");
        }
    } else {
        int count = 0;
        fis.open("count");
        if(!fis.is_open()) {
            ui->progressBar->setValue(m_nProgress);
            return;
        }
        fis >> count;
        fis.close();
        ui->lcdNumber->display(count);
        ui->progressBar->setValue(count*100/m_nMaxCount);
        if (m_nProgress == 100) {
            m_timer->stop();
            setBtnsEnabled(true);
            m_shell->execute("rm count");
        }
    }
}

void MainWindow::setBtnsEnabled(bool flag)
{
    ui->btnChgTrainCnt->setEnabled(flag);
    ui->maxTrainCount->setEnabled(flag);
    ui->btnStart->setEnabled(flag);
}

void MainWindow::on_btnChgTrainCnt_clicked()
{
    QString strm_nMaxCount = ui->maxTrainCount->text();
    int m_nMaxCount = strm_nMaxCount.toInt();
    if (m_nMaxCount == 0) {
        m_nMaxCount = 9000;
    }
    using namespace std;
    ifstream fis;
    string line;
    QString str;
    fis.open("yolo-voc.2.0.cfg");
    if (!fis.is_open()) {
        QMessageBox::warning(this, tr("警告！"), tr("打开yolo-voc.2.0.cfg失败！"), QMessageBox::Yes);
        return;
    }
    ofstream fos;
    fos.open("temp.cfg");
    while(getline(fis, line)) {
        str = QString::fromStdString(line);
        if (str.startsWith("max_batches")) {
            str = "max_batches=" + QString::number(m_nMaxCount, 10);
        }
        fos << str.toStdString() << endl;
    }
    fis.close();
    fos.close();
    m_shell->execute("rm yolo-voc.2.0.cfg");
    m_shell->execute("mv temp.cfg yolo-voc.2.0.cfg");
    QMessageBox::warning(this, tr("Tips"), tr("设置成功"), QMessageBox::Yes);
}

void MainWindow::on_btnStart_clicked()
{
    if (m_strImagePath.isNull()) {
        QMessageBox::warning(this, tr("警告！"), tr("必须先选择图片文件夹！"), QMessageBox::Yes);
        return;
    }
    std::ifstream fis;
    fis.open("yolo-voc.2.0.cfg");
    QString str;
    std::string line;
    while(std::getline(fis, line)) {
        str = QString::fromStdString(line);
        if (str.startsWith("max_batches")) {
            QStringList list = str.split("=");
            m_nMaxCount = list[1].toInt();
            break;
        }
    }
    fis.close();

    QString cmd = "python train_start.py";
    m_shell->start(cmd);
    m_bIsTraining = true;
    m_timer->start();
    setBtnsEnabled(false);
}
