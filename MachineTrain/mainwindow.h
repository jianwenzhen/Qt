#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void closeEvent(QCloseEvent *event);

private slots:
    void on_btnStart_clicked();
    void on_btnChgNames_clicked();
    void on_btnSelDir_clicked();
    void on_btnChgTrainCnt_clicked();
    void updateProgress();

private:
    void        setBtnsEnabled(bool);

    bool        m_bIsTraining = false;
    int         m_nMaxCount = 9000;
    int         m_nProgress = 0;
    QString     m_strImagePath;
    QTimer     *m_timer;
    QProcess   *m_shell;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
