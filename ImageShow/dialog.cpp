#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QPainter>

Dialog::Dialog(QWidget *parent) :
    QWidget(parent)
    //ui(new Ui::Dialog)
{
    //ui->setupUi(this);
}

Dialog::~Dialog()
{
    //delete ui;
}

void Dialog::paintEvent(QPaintEvent *)
{
    //
    QPainter painter(this);
    if(!m_image.isNull())
    {
        painter.drawPixmap(rect(), m_image);
    }
}

void Dialog::on_btnLoad_clicked()
{
    QString strfile = QFileDialog::getOpenFileName(nullptr, tr("open file"));
    if(!strfile.isEmpty())
    {
        m_image.load(strfile);
        //repaint();
    }
}
