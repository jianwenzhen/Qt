#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QWidget
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    QPixmap    m_image;
    //Ui::Dialog *ui;

protected:
    void paintEvent(QPaintEvent *);
public:
    void on_btnLoad_clicked();
};

#endif // DIALOG_H
