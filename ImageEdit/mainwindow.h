#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include "imagewidget.h"
#include "topwidget.h"
#include "rightwidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_action_Exit_triggered();
    void on_action_Open_triggered();

private:
    ImageWidget *imageWidget;
    RightWidget *rightWidget;
    QGridLayout *gridLayout;
    TopWidget *topWidget;
    Ui::MainWindow *ui;

protected:
    virtual void paintEvent(QPaintEvent *event);

};

#endif // MAINWINDOW_H
