#include "ui_mainwindow.h"
#include "imagewidget.h"
#include "rightwidget.h"
#include "mainwindow.h"
#include "topwidget.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QGridLayout>
#include <QFocusFrame>
#include <QString>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // 注意这里的父类为ui->centralWidget 如果父类为MainWindow那么菜单栏就不能点了
    gridLayout = new QGridLayout(ui->centralWidget);
    // ui->centralWidget->setStyleSheet("background-color:black;"); // 全黑了 啥都看不着
    QPalette p = this->palette();
    p.setColor(QPalette::Window, QColor(333333));
    this->setPalette(p);
    // 设置第2列的最小宽度为200
    gridLayout->setColumnMinimumWidth(1, 200);

    imageWidget = new ImageWidget(ui->centralWidget);
    rightWidget = new RightWidget(ui->centralWidget);
    topWidget = new TopWidget(ui->centralWidget);

    rightWidget->setGeometry(width() - 400, 60, 200, height() - 60);
    imageWidget->setGeometry(0, 60, width() - 200, height() - 60);
    topWidget->setGeometry(0, 0, width(), 60);

    gridLayout->addWidget(topWidget, 0, 0, 1, 2);
    gridLayout->addWidget(imageWidget, 1, 0);
    gridLayout->addWidget(rightWidget, 1, 1);

    // 这里有个疑问 original 不用public 可不可以? 如果是private怎么获取?
    connect(topWidget->original, &QPushButton::clicked, imageWidget, &ImageWidget::original);
    connect(topWidget->adapt, &QPushButton::clicked, imageWidget, &ImageWidget::adapt);
    // 一个信号可以有多个槽一起接收
    connect(topWidget->minus, &QPushButton::clicked, topWidget, &TopWidget::sliderMinus);
    connect(topWidget->minus, &QPushButton::clicked, imageWidget, &ImageWidget::minus);
    connect(topWidget->add, &QPushButton::clicked, topWidget, &TopWidget::sliderAdd);
    connect(topWidget->add, &QPushButton::clicked, imageWidget, &ImageWidget::add);

    connect(rightWidget->btns[0], &QPushButton::clicked, imageWidget, &ImageWidget::drawPoint);
    connect(rightWidget->btns[1], &QPushButton::clicked, imageWidget, &ImageWidget::drawLine);
    connect(rightWidget->btns[2], &QPushButton::clicked, imageWidget, &ImageWidget::drawRect);
    connect(rightWidget->btns[3], &QPushButton::clicked, imageWidget, &ImageWidget::drawCircle);

//    QFocusFrame *frame = new QFocusFrame(ui->centralWidget);
//    frame->setWidget(topWidget);
//    frame->setAutoFillBackground(true);
//    frame->setWidget(rightWidget);
//    frame->setAutoFillBackground(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Exit_triggered()
{
    close();
}

void MainWindow::on_action_Open_triggered()
{
    // 打开文件
    QString filePath = QFileDialog::getOpenFileName(this, tr("select file")).toLocal8Bit();
    if(filePath.isNull()) {
        // 弹窗报错

        return;
    }
    // 设置图片
    imageWidget->setImage(filePath);
}

void MainWindow::paintEvent(QPaintEvent *)
{
    // 设置第2行的最小高度为窗口高度-150
    gridLayout->setRowMinimumHeight(1, height()-150);
}

