#include <QPainter>
#include <QWheelEvent>
#include <iostream>
#include <cstdlib>
#include <QSlider>
#include "imagewidget.h"

ImageWidget::ImageWidget(QWidget *parent) : QWidget(parent)
{

}

void ImageWidget::setImage(const QPixmap& image)
{
    m_image = image;
    m_posImage = rect();
    repaint();
}

void ImageWidget::setImage(const QString& strPath)
{
    m_image.load(strPath);
    m_posImage = rect();
    repaint();
}

void ImageWidget::paintEvent(QPaintEvent *)
{
    QPainter paint(this);

    if(!m_image.isNull())
    {
        // 计算图片相对于窗口的缩放比例 注意转换成浮点数!!!
        float fScaleImageW = m_image.width()*1.0/this->width();
        float fScaleImageH = m_image.height()*1.0/this->height();
        // 缩放比例选择大的一方
        float fScaleImage = std::max(fScaleImageW, fScaleImageH);
        // 计算图片应该显示的实际宽高
        float fImageW = m_image.width() / fScaleImage * fScale;
        float fImageH = m_image.height() / fScaleImage * fScale;
        // 计算起始坐标点
        float fLeft = (this->width() - fImageW)/2.0;
        float fTop = (this->height() - fImageH)/2.0;

//        std::cout << "iW: " << m_image.width() << std::endl;
//        std::cout << "iH: " << m_image.height() << std::endl;
//        std::cout << "width: " << this->width() << std::endl;
//        std::cout << "height: " << this->height() << std::endl;
//        std::cout << "imageW: " << fImageW << std::endl;
//        std::cout << "imageH: " << fImageH << std::endl;
//        std::cout << "-----------------------------------------" << std::endl;

        m_posImage.setLeft(fLeft);
        m_posImage.setTop(fTop);
        m_posImage.setWidth(fImageW);
        m_posImage.setHeight(fImageH);
        // 画图
        paint.drawPixmap(m_posImage, m_image);

        // 设置画笔颜色
        paint.setPen(Qt::black);
        // 判断图形类型
        if (type != DrawType::NOT) {
            switch (type) {
                case DrawType::POINT:
                    std::cout << "DrawType::POINT" << std::endl;
                    break;
                case DrawType::LINE:
                    if (!drawStart.isNull() && !drawEnd.isNull()) {
                        paint.drawLine(drawStart, drawEnd);

//                        std::cout << "startX: " << drawStart.x() << std::endl;
//                        std::cout << "startY: " << drawStart.y() << std::endl;
//                        std::cout << "endX: " << drawEnd.x() << std::endl;
//                        std::cout << "endY: " << drawEnd.y() << std::endl;
//                        std::cout << "DrawType::LINE" << std::endl;
//                        std::cout << "-------------------------" << std::endl;
                    }
                    break;
                case DrawType::CIRCLE:
                    std::cout << "DrawType::CIRCLE" << std::endl;
                    break;
                case DrawType::RECT:
                    std::cout << "DrawType::RECT" << std::endl;
                    break;
                default:
                    std::cout << "DrawType::OTHER" << std::endl;
                    break;
            }
        }
        paint.end();
    }
}

 void ImageWidget::wheelEvent(QWheelEvent *event)
{
    // 滚轮滚动度数
    QPoint numDegrees = event->angleDelta() / 8;
    // 鼠标所在位置
    mouseX = event->x();
    mouseY = event->y();

    if(!numDegrees.isNull()) {
        if(numDegrees.y() > 0) {
            // 滚轮上滚 放大
            fScale *= 1.1;
        } else {
            // 滚轮下滚 缩小
            fScale /= 1.1;
        }
    }
    update();
}

 // 原比例显示
void ImageWidget::original()
{
    // 计算图片相对于窗口的缩放比例
    float fScaleImageW = m_image.width()*1.0/this->width();
    float fScaleImageH = m_image.height()*1.0/this->height();
    // 缩放比例选择大的一方
    float fScaleImage = std::max(fScaleImageW, fScaleImageH);
    // 把缩放的比例再放大
    fScale *= fScaleImage;
    update();
}

// 适屏显示
void ImageWidget::adapt()
{
    fScale = 1.0;
    update();
}

// 按钮放大
void ImageWidget::add()
{
//    float sliderVal = slider->value();
//    sliderVal *= 1.1;
    fScale *= 1.1;
    update();
}

// 按钮缩小
void ImageWidget::minus()
{
//    float sliderVal = slider->value();
//    sliderVal /= 1.1;
    fScale /= 1.1;
    update();
}

void ImageWidget::drawPoint()
{
    type = POINT;
}

void ImageWidget::drawLine()
{
    type = LINE;
}

void ImageWidget::drawCircle()
{
    type = CIRCLE;
}

void ImageWidget::drawRect()
{
    type = RECT;
}

void ImageWidget::mousePressEvent(QMouseEvent *event)
{
    drawStart = event->pos();
}

void ImageWidget::mouseReleaseEvent(QMouseEvent *event)
{
    drawEnd = event->pos();
    update();
}

void ImageWidget::mouseMoveEvent(QMouseEvent *event)
{
    drawEnd = event->pos();
    update();
    if (type == DrawType::POINT) {
        drawStart = drawEnd;
    }
}
