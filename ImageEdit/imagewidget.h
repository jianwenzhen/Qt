#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>
#include <QSlider>

enum DrawType {NOT = -1, POINT, LINE, CIRCLE, RECT};

class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImageWidget(QWidget *parent = 0);

signals:

public slots:
    void drawCircle();
    void drawPoint();
    void drawLine();
    void drawRect();
    void original();
    void adapt();
    void minus();
    void add();

public:
    void         setImage(const QPixmap& image);
    void         setImage(const QString& strPath);

protected:
    virtual void paintEvent(QPaintEvent *event);        // 绘图事件
    virtual void wheelEvent(QWheelEvent *event);        // 滚轮事件
    virtual void mousePressEvent(QMouseEvent *e);		//--鼠标按下事件
    virtual void mouseMoveEvent(QMouseEvent *e);        //--鼠标移动事件
    virtual void mouseReleaseEvent(QMouseEvent *e);     //--鼠标释放（松开）事件
    // virtual void mouseDoubleClickEvent(QMouseEvent *e);	//--鼠标双击事件

private:
//    int     count = 0;
    // 图片坐标信息
    QRect   m_posImage;
    // 图片信息
    QPixmap m_image;
    // 缩放比例
    float   fScale = 1.0;
    // 鼠标位置信息
    int     mouseX;
    int     mouseY;
    // 画线类型(线,直线,圆,矩形)
    DrawType type = DrawType::NOT;
    // 画笔起始点
    QPoint  drawStart;
    QPoint  drawEnd;
    // Vector<QPoint> lines;

};

#endif // IMAGEWIDGET_H
