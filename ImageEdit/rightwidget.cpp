#include "rightwidget.h"
#include <QPushButton>

RightWidget::RightWidget(QWidget *parent) : QWidget(parent)
{
    QString name[5] = {tr("画线"), tr("直线"), tr("矩形"), tr("圆形"), tr("选择")};
    int btnW = 80;
    int btnH = 50;
    int posY = 10;
    int posX = 10;
    int count = 0;
    for(int i=0;i<3;i++) {
        for(int j=0;j<2;j++) {
            if(count < 5) {
                btns[count] = new QPushButton(name[count], this);
                btns[count++]->setGeometry(posX, posY, btnW, btnH);
                posX += 100;
            }
        }
        posX -= 200;
        posY += 60;
    }
}
