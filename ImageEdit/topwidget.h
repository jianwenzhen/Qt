#ifndef TOPWIDGET_H
#define TOPWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QSlider>

class TopWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TopWidget(QWidget *parent = 0);

signals:

public slots:
    void sliderMinus();
    void sliderAdd();

public:
    QPushButton *original;
    QPushButton *adapt;
    QPushButton *minus;
    QPushButton *add;
    QSlider *slider;
};



#endif // TOPWIDGET_H
