#ifndef RIGHTWIDGET_H
#define RIGHTWIDGET_H

#include <QWidget>
#include <QPushButton>

class RightWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RightWidget(QWidget *parent = 0);

signals:

public slots:

public:
    QPushButton *btns[5];
};

#endif // RIGHTWIDGET_H
