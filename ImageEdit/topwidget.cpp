#include "topwidget.h"
#include <QPushButton>
#include <QSlider>

TopWidget::TopWidget(QWidget *parent) : QWidget(parent)
{
    int btnW = 60;
    int btnH = 40;
    int posY = 10;
    original = new QPushButton(QString(tr("原大")), this);
    original->setGeometry(10, posY, btnW, btnH);
    adapt = new QPushButton(QString(tr("适屏")), this);
    adapt->setGeometry(90, posY, btnW, btnH);

    minus = new QPushButton("-", this);
    minus->setGeometry(170, 20, 20, 20);
    add = new QPushButton("+", this);
    add->setGeometry(310, 20, 20, 20);

    slider = new QSlider(Qt::Horizontal, this);
    slider->setGeometry(200, 20, 100, 20);
    slider->setRange(0, 200);
    slider->setValue(80);
}

void TopWidget::sliderMinus()
{
    this->slider->setValue(slider->value()/1.1);
}

void TopWidget::sliderAdd()
{
    this->slider->setValue(slider->value()*1.1);
}
