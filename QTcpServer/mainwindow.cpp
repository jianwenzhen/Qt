#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Server");

    ui->portEdit->setText(QString::number(8888));

    m_pServer = new QTcpServer();
    connect(m_pServer, SIGNAL(newConnection()), this, SLOT(new_connection()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnListen_clicked()
{
    if (ui->btnListen->text() == "Listen")
    {
        m_pServer->listen(QHostAddress::Any, ui->portEdit->text().toInt());
        ui->btnListen->setText("Cancle listen");
    }
    else
    {
        std::vector<QTcpSocket*>::iterator it;
        for(it = m_vector_sockets.begin(); it != m_vector_sockets.end(); it++)
        {
            (*it)->disconnectFromHost();
        }
        m_pServer->close();
        ui->btnListen->setText("Listen");
    }
}

void MainWindow::on_btnSend_clicked()
{
    QString sendData = ui->sendEdit->text();
    sendData += "\n";
    std::vector<QTcpSocket*>::iterator it;
    for(it = m_vector_sockets.begin(); it != m_vector_sockets.end(); it++)
    {
        (*it)->write(sendData.toUtf8());
    }
    ui->sendEdit->clear();
}

void MainWindow::new_connection()
{
    QTcpSocket *newSocket = m_pServer->nextPendingConnection();
    m_vector_sockets.push_back(newSocket);
    connect(newSocket, SIGNAL(readyRead()), this, SLOT(receive_data()));
    connect(newSocket, SIGNAL(disconnected()), this, SLOT(deleteLater()));
    QString socket_name = newSocket->peerName();
    socket_name += " connect success!\n";
    newSocket->write(socket_name.toUtf8());
}

//void MainWindow::receive_data()
//{
    //std::vector<QTcpSocket*>::iterator it;
    //for(it = m_vector_sockets.begin(); it != m_vector_sockets.end(); it++)
    //{
    //    m_receivedData += QString((*it)->readAll());
    //}
    //ui->receivedText->setText(m_receivedData);
//}
