#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnListen_clicked();
    void on_btnSend_clicked();
    void receive_data();
    void new_connection();

private:
    QString                 m_received = "";
    QTcpServer             *m_pServer;
    std::vector<QTcpSocket*>m_vector_sockets;
    unsigned int            m_port = 8888;
    Ui::MainWindow         *ui;
};

#endif // MAINWINDOW_H
