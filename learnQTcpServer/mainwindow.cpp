#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTcpSocket>
#include <QTcpServer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Server");

    m_pServer = new QTcpServer();

    ui->portEdit->setText(QString::number(m_port));

    connect(m_pServer, SIGNAL(newConnection()), this, SLOT(new_connection()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnListen_clicked()
{
    if (ui->btnListen->text() == "listen") {
        m_port = ui->portEdit->text().toInt();
        m_pServer->listen(QHostAddress::Any, m_port);
        ui->btnListen->setText("cancel listen");
    } else {
        std::vector<QTcpSocket*>::iterator it;
        for (it = m_vector_sockets.begin(); it != m_vector_sockets.end();) {
            if ((*it)->state() == QAbstractSocket::ConnectedState)
            {
                (*it)->disconnectFromHost();
            }
            else
                it = m_vector_sockets.erase(it);
        }
        m_pServer->close();
        ui->btnListen->setText("listen");
    }
}

void MainWindow::on_btnSend_clicked()
{
    QString sendData = ui->sendEdit->toPlainText();
    sendData += "\n";
    std::vector<QTcpSocket*>::iterator it;
    for (it = m_vector_sockets.begin(); it != m_vector_sockets.end();) {
        if ((*it)->state() == QAbstractSocket::ConnectedState)
        {
            (*it)->write(sendData.toUtf8());
            it++;
        }
        else
        {
            it = m_vector_sockets.erase(it);
        }
    }
    ui->sendEdit->clear();
}

void MainWindow::receive_data()
{
    QByteArray receiveData;
    std::vector<QTcpSocket*>::iterator it;
    for (it = m_vector_sockets.begin(); it != m_vector_sockets.end();) {
        if ((*it)->state() == QAbstractSocket::ConnectedState)
        {
            // TestData data;
            // (*it)->read((char*)&data, sizeof(data));
            // qDebug("name: %s, age: %d", data.name, data.age);
            receiveData = (*it)->readAll();
            m_received += QString(receiveData);
            // m_received += tr("\n");
            it++;
        }
        else
        {
            it = m_vector_sockets.erase(it);
        }
    }
    ui->receivedText->setText(m_received);
}

void MainWindow::new_connection()
{
    // 获取客户端连接
    QTcpSocket *socket = m_pServer->nextPendingConnection();
    m_vector_sockets.push_back(socket);
    //QHostAddress socket_address = socket->peerAddress();
    //QString socket_name = socket->peerName();
    //socket->write(socket_address.toString().toLocal8Bit());
    //socket->write(socket_name.toLocal8Bit());
    //socket->write("Connect success!\n");
    connect(socket, SIGNAL(readyRead()), this, SLOT(receive_data()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(deleteLater()));
}
