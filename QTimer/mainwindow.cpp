#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QLabel>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
     QTimer *timer = new QTimer(this);
     timer->setInterval(500);
     timer->start();
     connect(timer, SIGNAL(timeout()), this, SLOT(on_timeout()));

//    ui->centralWidget
    QButtonGroup *btnGroup = new QButtonGroup();
    btnGroup->addButton(ui->radioIn1);
    btnGroup->addButton(ui->radioIn2);

    setMouseTracking(true);
    this->centralWidget()->setMouseTracking(true);

    m_label = new QLabel(this);
    m_label->setPixmap(QPixmap(":/icon/icon/conn.png").scaled(20,20));
    ui->statusBar->setMouseTracking(true);
    m_label->setMouseTracking(true);
    ui->statusBar->addWidget(m_label);

    m_labelText = new QLabel(ui->statusBar);
    m_labelText->setText("Conn Status");
    ui->statusBar->addWidget(m_labelText);
    m_labelText->hide();
//    错误的用法:
//    QTimer timer;
//    timer.setInterval(500);
//    timer.start();
//    connect(&timer, SIGNAL(timeout()), this, SLOT(on_timeout()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    QPoint pos = event->pos();
    QPoint p = m_label->pos();
    globalP = m_label->mapTo(this, p);
    int x1 = globalP.x();
    int y1 = globalP.y();
    int x2 = x1 + 20;
    int y2 = y1 + 20;
    if(pos.x() > x1 && pos.x() < x2 && pos.y() > y1 && pos.y() < y2)
        m_labelText->show();
    else
        m_labelText->hide();
}

void MainWindow::on_timeout()
{
    // qDebug("timeout");
}

void MainWindow::on_radioIn1_clicked()
{
    qDebug("radio 1 clicked");
}

void MainWindow::on_radioIn2_clicked()
{
    qDebug("radio 2 clicked");
}
