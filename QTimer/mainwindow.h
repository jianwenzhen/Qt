#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void mouseMoveEvent(QMouseEvent *event);

private:
    Ui::MainWindow *ui;
    QRect           radioRect;
    QPoint          globalP;
    QLabel         *m_label;
    QLabel         *m_labelText;

private slots:
    void on_timeout();
    void on_radioIn1_clicked();
    void on_radioIn2_clicked();
};

#endif // MAINWINDOW_H
