#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnConn_clicked()
{
    m_tcpSocket = new QTcpSocket();
    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(receive_data()));
    // 销毁自身 防止内存泄漏
    connect(m_tcpSocket, SIGNAL(disconnected()), m_tcpSocket, SLOT(deleteLater()));
    m_tcpSocket->connectToHost("127.0.0.1", 8888);
    if (!m_tcpSocket->waitForConnected(30000)) {
        qDebug("Connect Failed!\n");
    } else {
        ui->btnConn->setText("disconnect");
    }
}

void MainWindow::on_btnSend_clicked()
{
    if (m_tcpSocket->state() == QAbstractSocket::ConnectedState)
    {
        QByteArray send = "ABCDE";
        m_tcpSocket->write(send, sizeof(send));
    }
    else
    {
        ui->btnConn->setText("connect");
    }
}

void MainWindow::receive_data()
{
    QByteArray receiveData = m_tcpSocket->readAll();
    qDebug(receiveData);
}
