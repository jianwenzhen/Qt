#ifndef WORKTHREAD_H
#define WORKTHREAD_H

#include <QThread>
#include <QTcpSocket>

class WorkThread : public QThread
{
    Q_OBJECT
    void run() Q_DECL_OVERRIDE
    {
        QString result;
        // 这里是耗时或阻塞的操作
        emit resultReady(result);
    }
signals:
    void resultReady(const QString &s);

public:
    WorkThread();

private:
    QTcpSocket      *m_tcpSocket;
};

#endif // WORKTHREAD_H
