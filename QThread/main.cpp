#include "mainwindow.h"
#include <QApplication>
#include "mysocket.h"
#include "dummy.h"
#include <QThread>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    Dummy dummy;
    MySocket mysocket;
    QThread thread;
    mysocket.moveToThread(&thread);
    QObject::connect(&dummy, SIGNAL(connSig(QString,uint)), &mysocket, SLOT(conn(QString,uint)));
    QObject::connect(&dummy, SIGNAL(sendSig(QString)), &mysocket, SLOT(sendData(QString)));
    dummy.connSig();
    dummy.sendSig("ABCDE");
    return a.exec();
}
