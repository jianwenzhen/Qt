#ifndef MYSOCKET_H
#define MYSOCKET_H

#include <QTcpSocket>

class MySocket : public QTcpSocket
{
public slots:
    int         sendData(const QString &data);
    bool        conn(const QString &ip = "127.0.0.1", unsigned int port = 8888);
    void        receive_data() { emit receive(); }

signals:
    void        receive();

public:
    MySocket();
    ~MySocket();

private:
    QString         m_strIp = "127.0.0.1";
    unsigned int    m_nPort = 8888;
};

#endif // MYSOCKET_H
