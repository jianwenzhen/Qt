#ifndef DUMMY_H
#define DUMMY_H

#include <QObject>

class Dummy : public QObject
{
    Q_OBJECT
public:
    Dummy();

public slots:
    void        emitConn(const QString &ip = "127.0.0.1", unsigned int port = 8888) { emit connSig(ip, port); }
    void        emitSend(const QString &data) { emit sendSig(data); }

signals:
    void        connSig(const QString &ip = "127.0.0.1", unsigned int port = 8888);
    void        sendSig(const QString &data);
};

#endif // DUMMY_H
