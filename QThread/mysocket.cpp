#include "mysocket.h"

MySocket::MySocket()
{

}

MySocket::~MySocket()
{
    this->deleteLater();
}

bool MySocket::conn(const QString &ip, unsigned int port)
{
    this->connectToHost(ip, port);
    if(!this->waitForConnected())
        return false;
    connect(this, SIGNAL(readyRead()), this, SLOT(receive_data()));
    return true;
}

int MySocket::sendData(const QString &data)
{
    this->write((char*)&data, sizeof(data));
}
