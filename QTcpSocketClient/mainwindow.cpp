#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("Client");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnConn_clicked()
{
    if (ui->btnConn->text() == "Connect")
    {
        m_pSocket = new QTcpSocket();
        m_pSocket->connectToHost(ui->ipEdit->text(), ui->portEdit->text().toInt());
        if (!m_pSocket->waitForConnected())
        {
            qDebug("connect failed!");
        }
        else
        {
            ui->btnConn->setText("Disonnect");
            connect(m_pSocket, SIGNAL(readyRead()), this, SLOT(receive_data()));
            connect(m_pSocket, SIGNAL(disconnected()), m_pSocket, SLOT(deleteLater()));
        }
    }
    else
    {
        if(m_pSocket->state() == QAbstractSocket::ConnectedState || m_pSocket->state() == QAbstractSocket::ConnectingState)
        {
            m_pSocket->disconnectFromHost();
        }
        ui->btnConn->setText("Connect");
    }
}

void MainWindow::on_btnSend_clicked()
{
    if (m_pSocket->state() == QAbstractSocket::ConnectedState)
    {
        QString sendData = ui->sendEdit->text();
        sendData += "\n";
        m_pSocket->write(sendData.toUtf8());
        ui->sendEdit->clear();
    }
    else
    {
        QMessageBox::warning(this, "Error!", "Socket is not connecting!", QMessageBox::Yes);
        ui->btnConn->setText("Connect");
    }
}

void MainWindow::receive_data()
{
    m_receivedData += QString(m_pSocket->readAll());
    ui->receivedText->setText(m_receivedData);
}
