#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnConn_clicked();
    void on_btnSend_clicked();
    void receive_data();

private:
    QTcpSocket     *m_pSocket;
    QString         m_receivedData;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
