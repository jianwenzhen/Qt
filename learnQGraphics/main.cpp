#include "mainwindow.h"
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // MainWindow w;
    // w.show();
    QGraphicsScene scene;
    scene.addText("Qt QGraphicsScene!", QFont("Times", 22, QFont::Bold));

    QPainterPath path;
    path.moveTo(30, 120);
    path.cubicTo(80, 0, 50, 50, 80, 80);

    scene.addPath(path, QPen(Qt::black), QBrush(Qt::green));

    QGraphicsView view(&scene);
    view.show();

    return a.exec();
}
