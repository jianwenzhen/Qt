#include <QDebug>
#include <thread>
#include <iostream>
#include "MainWindow.h"
#include "ImageWidget.h"
#include "ui_MainWindow.h"


void scanThread(MainWindow* pFrame)
{
    if(pFrame)
    {
        pFrame->scanFunction();
    }
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_bIsDetect(false)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("病害检测系统"));

    m_pShell = new QProcess();

    ui->progressBar->setValue(0);
    ui->imageWidget->setStyleSheet("border:2px solid green");

    connect(this, SIGNAL(detectResult(const QString&))
         ,this, SLOT(detectResultSlot(const QString&))
         );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setBtnsEnabled(bool flag)
{
    ui->btnStart->setEnabled(flag);
    ui->btnSelOne->setEnabled(flag);
    ui->btnSelDir->setEnabled(flag);
}

void MainWindow::updateProgress()
{
    using namespace std;
    ifstream fis;
    fis.open("progress");
    if(!fis.is_open()) {
        ui->progressBar->setValue(m_progress);
    } else {
        fis >> m_progress;
        fis.close();
        ui->progressBar->setValue(m_progress);
    }
    if (m_progress == 100)
    {
        QMessageBox::information(nullptr, tr("Tip"), tr("检测完成!"));
        m_bIsDetect = false;
        setBtnsEnabled(true);
        m_pShell->execute("rm progress");
        m_progress = 0;
    }
}

bool findFile(std::vector<QString>& vfiles, const QString& path)
{
    bool  bRet = false;
    for(const auto& iter:vfiles)
    {
        if(path == iter)
        {
            bRet = true;
            break;
        }
    }
    return bRet;
}

bool MainWindow::getNewFile(std::vector<QString>& files, QString& path)
{
    std::vector<QString> vNewfiles;
    for(int i=0; i< files.size();i++)
    {
        if(!findFile(m_histFiles, files[i]))
        {
            vNewfiles.push_back(files[i]);
        }
    }
    int nNewCount = vNewfiles.size();
    if(nNewCount)
    {
        path = vNewfiles[nNewCount - 1];
        m_histFiles = files;
    }
    return nNewCount;
}

void MainWindow::scanFunction(void)
{
    QString path = "";
    while(m_bIsDetect)
    {
        updateProgress();
        ///find files
        /// get new file
        std::vector<QString> vFiles;
        if(findFiles(m_strResultDir, "jpg", vFiles, true))
        {
            if(getNewFile(vFiles, path))
            {
                //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                emit detectResult(path);
            }
        }
        /// emit path to show
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void MainWindow::detectResultSlot(const QString& path)
{
    qDebug() << "path: " << path;
    ui->imageWidget->setImage(path);
}

bool MainWindow::findFiles(const QString& strPath, const QString& strExt, std::vector<QString>& vFinds, bool bRecursive)
{
    bool bRet =false;
    do{
       QDir dir(strPath);
       //若目录不存在则返回退出
       if (!dir.exists())
          break;

       //设置过滤器(目录，文件或非上级目录)
       dir.setFilter(QDir::Dirs|QDir::Files|QDir::NoDotAndDotDot);
       dir.setSorting(QDir::DirsFirst);
       //取得目录中文件列表(包含目录)
       QFileInfoList list = dir.entryInfoList();
       int i = 0;
       do{
           if(0 == list.size())
           {
                break;
           }
           QFileInfo fileInfo = list.at(i);

           //通知View层更新当前检索到的文件
           //判断是否为目录，如果是目录则遍历，否则当前处理文件
           if(fileInfo.isDir())
           {
               if(bRecursive)
               {
                   findFiles(fileInfo.filePath(), strExt, vFinds, bRecursive);
               }
           }
           else
           {
               //取得文件类型后缀
               if(strExt.toLower() == fileInfo.suffix().toLower())
               {
                   vFinds.push_back(fileInfo.absoluteFilePath());
               }
           }
           i++;

       }while(i < list.size());

       bRet = vFinds.size();
    }while(0);

    return bRet;
}

void MainWindow::executeScript()
{
    using namespace std;
    ifstream fis;
    string line;
    QString str;
    fis.open("gen.sh");
    if (!fis.is_open()) {
        QMessageBox::warning(this, tr("警告！"), tr("打开gen.sh失败！"), QMessageBox::Yes);
        return;
    }
    ofstream fos;
    fos.open("temp.sh");
    while(getline(fis, line)) {
        str = QString::fromStdString(line);
        if (str.startsWith("cd")) {
            str = "cd " + m_strDirPath;
        }
        fos << str.toStdString() << endl;
    }
    fis.close();
    fos.close();
    m_pShell->execute("rm gen.sh");
    m_pShell->execute("mv temp.sh gen.sh");
}

void MainWindow::on_btnStart_clicked()
{
//    if (SelectMode::NONE == m_selectMode)
//    {
//    }
//    else if (SelectMode::ONE == m_selectMode)
//    {
//    }
//    else if (SelectMode::DIR == m_selectMode)
//    {
//    }
    m_strImagePath = ui->leFilePath->text();
    m_strDirPath = ui->leDirPath->text();

    m_strResultDir = m_strDirPath;
    m_strResultDir = m_strResultDir.replace("images", "testimages");
    executeScript();

    m_progress = 0;
    updateProgress();
    setBtnsEnabled(false);

    if (m_strImagePath.isEmpty() && m_strDirPath.isEmpty())
    {
        QMessageBox::warning(this, tr("警告！"), tr("请选择图片或图片文件夹！"), QMessageBox::Yes);
        setBtnsEnabled(true);
        return;
    }
    else if (m_strDirPath.isEmpty())
    {
        QString cmd = "./darknet detector test voc.data yolo-voc.2.0.cfg backup/yolo-voc_9000.weights ";
        cmd += m_strImagePath;
        m_pShell->execute(cmd);

        // set image path
        ui->imageWidget->setImage("predictions.jpg");

        m_progress = 100;
        updateProgress();
    }
    else
    {
        m_pShell->execute("rm -rf testimages");
        m_pShell->execute("rm progress");

        m_pShell->start("python just_batch_test_upgrade.py");

        m_histFiles.clear();
        m_bIsDetect = true;

        std::thread newThread(scanThread, this);
        newThread.detach();
    }
}

void MainWindow::on_btnSelOne_clicked()
{
    m_strImagePath =  QFileDialog::getOpenFileName(this, "Select image");
    if (nullptr != m_strImagePath)
    {
        ui->leDirPath->setText("");
        ui->leFilePath->setText(m_strImagePath);
    }
}

void MainWindow::on_btnSelDir_clicked()
{

    m_strDirPath =  QFileDialog::getExistingDirectory(this, "Select images dir");

    if (nullptr != m_strDirPath)
    {
        ui->leFilePath->setText("");
        ui->leDirPath->setText(m_strDirPath);
    }
}
