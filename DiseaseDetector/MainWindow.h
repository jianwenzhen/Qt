#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QFileDialog>
#include <QMessageBox>
#include <fstream>
#include <thread>

namespace Ui {
class MainWindow;
}

enum SelectMode {
    NONE = 0,
    ONE = 1,
    DIR = 2
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void        on_btnSelDir_clicked();
    void        updateProgress();
    void        on_btnStart_clicked();
    void        on_btnSelOne_clicked();

public:
    void        setBtnsEnabled(bool);
    void        scanFunction(void);
    void        executeScript(void);
    bool        getNewFile(std::vector<QString>& files, QString& path);
    bool        findFiles(const QString&, const QString&, std::vector<QString>&, bool bRecursive = true);

signals:
    void        detectResult(const QString& path);

protected slots:
    void        detectResultSlot(const QString& path);

private:
    int                  m_progress = 0;
    QString              m_strImagePath;
    QString              m_strDirPath;
    QString              m_strResultDir;
    QString              m_scriptPath;
    QString              m_output;
    QProcess            *m_pShell;
    std::vector<QString> m_histFiles;
    bool                 m_bIsDetect;
    SelectMode           m_selectMode = NONE;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
