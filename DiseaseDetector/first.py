#coding=utf-8
import os,subprocess
from os import listdir, getcwd
from os.path import join
# 根据pasacal.names这个文件中类别数量动态更改 voc.data 和 yolo-voc.2.0.cfg
namefile = file('pasacal.names', "r") 
names = len(namefile.readlines()) 
vocfile = file('voc.data', 'r+')
voclines = vocfile.readlines()

voc = file('voc.data', 'w+')
pwd = subprocess.check_output(["pwd"])              #subprocess.check_output(["cmd", "args"])
pwd = pwd[0:len(pwd) - 1]                           #substring 

voclines[0] = 'classes='+str(names)+'\n'            #classes = 5 or ?
voclines[1] = 'train=' + pwd + '/train.txt\n'       #train = /.../train.txt
voclines[2] = 'valid=' + pwd + '/valid.txt\n'       #valid = /.../valid.txt
voclines[3] = 'names=' + pwd + '/pasacal.names\n'   #names = /.../pasacal.names
voclines[4] = 'backup=' + pwd + '/backup\n'         #backup = /.../backup
voclines[5] = 'base=' + pwd + '/results\n'          #base = /.../results
voclines[6] = 'output=' + pwd + '/progress\n'
voclines[7] = 'countpath=' + pwd + '/count\n'

voc.writelines(voclines)

cfgFile = file('yolo-voc.2.0.cfg', "r+")
cfgLines = cfgFile.readlines()
count = 0
lineCnt = 0
filterLine = 0          #filters=(classes+coords+1)*num
classLine = 0           #classes=...
coords = 0
num = 0
for line in cfgLines:
    if line.startswith('filters='):
        count = count + 1
        if count == 22:
            filterLine = lineCnt
    if filterLine != 0 and line.startswith('classes='):
        classLine = lineCnt
    if filterLine != 0 and line.startswith('coords='):
        coordLine = line
        coords = int(coordLine[7:len(coordLine)])       #得到"coords=..."这一行的值 这里有个弊端就是‘=’和左右必须没有空白
    if filterLine != 0 and line.startswith('num='):
        numLine = line
        num = int(numLine[4:len(numLine)])              #得到"num=..."这一行的值 这里有个弊端就是‘=’和左右必须没有空白  
        break
    lineCnt = lineCnt + 1
        
cfgLines[filterLine] = 'filters=' + str((names + coords + 1) * num) + '\n'
cfgLines[classLine] = 'classes=' + str(names) + '\n'
cfg = file('yolo-voc.2.0.cfg', "w+")
cfg.writelines(cfgLines)

progress = file('progress', 'w+');
progress.write(str(0))

countFile = file('count', 'w+');
countFile.write(str(0))
