#include "ImageWidget.h"

ImageWidget::ImageWidget(QWidget *parent) : QWidget(parent)
{

}

void ImageWidget::setImage(const QPixmap& image)
{
    m_image = image;
    m_posImage = rect();
    repaint();
}

void ImageWidget::setImage(const QString& strPath)
{
    QPixmap temp(strPath);
    if(temp.load(strPath))
    {
        m_lockImage.lock();
        m_image = temp.copy();
        m_lockImage.unlock();
        // m_posImage = rect();
        update();
    }
    else {
        qDebug("======= read image failed\n");
    }
}

void ImageWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    m_lockImage.lock();
    QPixmap image = m_image.copy();
    m_lockImage.unlock();
    if (!image.isNull())
    {
        float fScaleImageW = image.width()*1.0 / this->width();
        float fScaleImageH = image.height()*1.0 / this->height();
        float fScaleImage = std::max(fScaleImageH, fScaleImageW);

        float fImageH = image.height()/fScaleImage;
        float fImageW = image.width()/fScaleImage;

        float fLeft = (this->width() - fImageW)/2;
        float fTop = (this->height() - fImageH)/2;

        m_posImage.setLeft(fLeft);
        m_posImage.setTop(fTop);
        m_posImage.setWidth(fImageW);
        m_posImage.setHeight(fImageH);

        painter.drawPixmap(m_posImage, image);
    }
}
