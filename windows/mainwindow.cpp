#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->pushButton->setText(tr("新窗口")); //将界面上按钮的显示文本更改为“新窗口”
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QDialog *dlg = new QDialog(this);   // 创建了一个对话框对象，然后让其显示，创建时指定的this参数表明这个对话框的父窗口是MainWindow。
    dlg->show();
}
