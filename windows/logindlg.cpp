#include "logindlg.h"
#include "ui_logindlg.h"

LoginDlg::LoginDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDlg)
{
    ui->setupUi(this);
    ui->pushButton->setText(tr("登录"));
}

LoginDlg::~LoginDlg()
{
    delete ui;
}
