#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>
#include <QPainter>
#include <mutex>
class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImageWidget(QWidget *parent = nullptr);
    void        setImage(const QString&);
    void        setImage(const QPixmap&);

protected:
    void paintEvent(QPaintEvent *event);

signals:

public slots:

private:
    std::mutex  m_lockImage;
    QPixmap     m_image;
    QRect       m_posImage;
    float       m_scale = 1.0;
};

#endif // IMAGEWIDGET_H
