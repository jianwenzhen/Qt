#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QFileDialog>
#include <QMessageBox>
#include <fstream>
#include <thread>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void        on_btnSelImg_clicked();
    void        on_btnStartOnce_clicked();
    void        on_btnSelDir_clicked();
    void        on_btnStartAll_clicked();
    void        updateProgress();

public:
    void        setBtnsEnabled(bool);
    void        scanFunction(void);
    void        executeScript(void);
    bool        getNewFile(std::vector<QString>& files, QString& path);
    bool        findFiles(const QString&, const QString&, std::vector<QString>&, bool bRecursive = true);

signals:
    void        detectResult(const QString& path);

protected slots:
    void        detectResultSlot(const QString& path);

private:
    int                  m_maxCount = 9000;
    int                  m_progress = 0;
    // QTimer              *m_pTimer;
    QString              m_imagePath;
    QString              m_dirPath;
    QString              m_strResultDir;
    QString              m_scriptPath;
    QString              m_output;
    QProcess            *m_pShell;
    std::vector<QString> m_histFiles;
    bool                 m_bIsDetect;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
