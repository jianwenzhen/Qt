#include <time.h>
#include <QDir>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <chrono>
#include "YDCommonFunc.h"

NS_YDUSKY_START
    enuFileType getFileType(const char* szFileName)
	{
        enuFileType fileType = eFT_unkown;
		do
		{
			std::string strFileName(szFileName);
			int nIndex = strFileName.find_last_of('.');
			if (-1 == nIndex)//没有找到后缀名,认为是文件夹.暂时假设是图像序列
			{
                fileType = eFT_directory;
				break;
			}
			std::string strPostfix = strFileName.substr(nIndex + 1, strFileName.length());
			if (   "jpg" == strPostfix
				|| "JPG" == strPostfix
				|| "bmp" == strPostfix
				|| "BMP" == strPostfix
				|| "png" == strPostfix
				|| "PNG" == strPostfix
				|| "tif" == strPostfix
				|| "TIF" == strPostfix)
			{
                fileType = eFT_image;
				break;

			}
			else if ("avi" == strPostfix
				|| "AVI" == strPostfix
				|| "mp4" == strPostfix
				|| "MP4" == strPostfix
				|| "mov" == strPostfix
				|| "MOV" == strPostfix
				|| "mpg" == strPostfix
				|| "MPG" == strPostfix)
			{
                fileType = eFT_video;
				break;

			}
			else if ("txt" == strPostfix
				|| "TXT" == strPostfix
				|| "ini" == strPostfix
				|| "INI" == strPostfix
				|| "xml" == strPostfix
				|| "XML" == strPostfix
				|| "pro" == strPostfix
				|| "PRO" == strPostfix)
			{
                fileType = eFT_document;
				break;
			}
			else
			{
                fileType = eFT_directory;
				break;
			}

		} while (0);


		return fileType;
	}

bool IsExistFileOrDirectory(const char* strFileName)
{
    if (strFileName == NULL)
        return false;
    if (access(strFileName, F_OK) == 0)
        return true;
    return false;
}

std::vector<std::string> split(std::string str, std::string pattern)
{
	std::string::size_type pos;
	std::vector<std::string> result;
	str += pattern; //扩展字符串以方便操作
	int size = str.size();

	for (int i = 0; i < size; i++)
	{
		pos = str.find(pattern, i++);
		if (pos < size)
		{
			std::string s = str.substr(i - 1, pos - i + 1);
			result.push_back(s);
			i = pos + pattern.size() - 1;
		}
	}

	return result;
}

/*
std::string getFileExtName(const char* lppath)
{
	char drive[_MAX_DRIVE] = { 0 };
	char dir[_MAX_DIR] = { 0 };
	char fname[_MAX_FNAME] = { 0 };
	char ext[_MAX_EXT] = { 0 };
	_splitpath(lppath, drive, dir, fname, ext);
	return ext;
}

std::string getFileNameFromPath(const char* lppath)
{
	char drive[_MAX_DRIVE] = { 0 };
	char dir[_MAX_DIR] = { 0 };
	char fname[_MAX_FNAME] = { 0 };
	char ext[_MAX_EXT] = { 0 };
	_splitpath(lppath, drive, dir, fname, ext);
	return fname;
}

std::string getFileFullNameFromPath(const char* lppath)
{
	char drive[_MAX_DRIVE] = { 0 };
	char dir[_MAX_DIR] = { 0 };
	char fname[_MAX_FNAME] = { 0 };
	char ext[_MAX_EXT] = { 0 };
	_splitpath(lppath, drive, dir, fname, ext);
	char fullName[_MAX_PATH] = { 0 };
	sprintf(fullName, "%s%s", fname, ext);
	return fullName;
}

std::string getFilePathNameWithoutExtFromPath(const char* lppath)
{
	char drive[_MAX_DRIVE] = { 0 };
	char dir[_MAX_DIR] = { 0 };
	char fname[_MAX_FNAME] = { 0 };
	char ext[_MAX_EXT] = { 0 };
	_splitpath(lppath, drive, dir, fname, ext);
	char pathName[_MAX_PATH] = { 0 };
	sprintf(pathName, "%s%s%s", drive, dir, fname);
	return pathName;
}

std::string getDirFromPath(const char* lppath)
{
	char drive[_MAX_DRIVE] = { 0 };
	char dir[_MAX_DIR] = { 0 };
	char fname[_MAX_FNAME] = { 0 };
	char ext[_MAX_EXT] = { 0 };
	_splitpath(lppath, drive, dir, fname, ext);

    char szDir[_MAX_PATH] = { 0 };

	sprintf(szDir, ("%s%s"), drive, dir);

	return szDir;
}

std::string getCurrentTimeToStringYMDHMS()
{
	time_t t = time(0);
	char tmp[64] = { 0 };
	strftime(tmp, sizeof(tmp), ("%Y%m%d %H:%M:%S"), localtime(&t));
	return tmp;
}
*/

///求512倍数值
long  get_512_Times(long lValue)
{
	return (0 == (lValue) % 512) ? lValue : (lValue + 512 - (lValue) % 512);
}

bool  findFiles(const QString& strPath, const QString& strExt, std::vector<QString>& vFinds, bool bRecursive /*= true*/)
{
    bool bRet =false;
    do{

           QDir dir(strPath);
           //若目录不存在则返回退出
           if (!dir.exists())
              break;

           //设置过滤器(目录，文件或非上级目录)
           dir.setFilter(QDir::Dirs|QDir::Files|QDir::NoDotAndDotDot);
           dir.setSorting(QDir::DirsFirst);
           //取得目录中文件列表(包含目录)
           QFileInfoList list = dir.entryInfoList();
           int i = 0;
           do{
               QFileInfo fileInfo = list.at(i);

               //通知View层更新当前检索到的文件
               //判断是否为目录，如果是目录则遍历，否则当前处理文件
               if(fileInfo.isDir())
               {
                   if(bRecursive)
                   {
                       findFiles(fileInfo.filePath(), strExt, vFinds, bRecursive);
                   }
               }
               else
               {
                   //取得文件类型后缀
                   if(strExt.toLower() == fileInfo.suffix().toLower())
                   {
                       vFinds.push_back(fileInfo.absoluteFilePath());
                   }
               }
               i++;

           }while(i < list.size());

           bRet = vFinds.size();

    }while(0);

    return bRet;
}

void createPath(const QString& strPath)
{
	QDir dir(strPath);
	if (!dir.exists())
	{
		dir.mkpath(strPath);
    }
}

/// timestamp to linux time (struct tm)
void Int64ToSystemTime(int64_t timestamp, tm *t)
{
    int64_t milli = timestamp + (int64_t)8*60*60*1000; // to Beijing time
    auto mTime = std::chrono::milliseconds(milli);
    auto tp = std::chrono::time_point<std::chrono::system_clock,std::chrono::milliseconds>(mTime);
    auto tt = std::chrono::system_clock::to_time_t(tp);
    t = std::gmtime(&tt);
    //tm_year+1900 tm_mon+1 tm_mday tm_hour tm_min tm_sec
}

QPixmap dataToPixmap(int nW, int nH, int nC, unsigned char* data)
{
    QImage::Format format = QImage::Format_RGB888;
    switch(nC)
    {
    case 1:
        format = QImage::Format_Mono;
        break;
    case 3:
        format = QImage::Format_RGB888;
        break;
    case 4:
        format = QImage::Format_RGBA8888;
        break;
    default:
        break;
    }
    QImage qImage(data, nW, nH, nW * nC, format);
    qImage.bits();
    return QPixmap::fromImage(qImage);
}

NS_YDUSKY_END
