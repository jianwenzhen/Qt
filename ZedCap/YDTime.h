/************************************************************************
/* 时间计时器
/* 功能: 跨平台的计时工具类
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2017.03.16*/
/************************************************************************/
#ifndef _YDUSKY_TIME_FILE_HEADER_2017_03_16_15_52_33_
#define _YDUSKY_TIME_FILE_HEADER_2017_03_16_15_52_33_
#include <chrono>
class YDTimer
{
public:
	YDTimer();
	void reset();

	//默认输出毫秒  
	int64_t elapsed() const;

	//默认输出秒  
	double elapsed_second() const;
	//微秒  
	int64_t elapsed_micro() const;

	//纳秒  
	int64_t elapsed_nano() const;

	////秒  
	//int64_t elapsed_seconds() const  
	//{  
	//  return duration_cast<chrono::seconds>(high_resolution_clock::now() - m_begin).count();  
	//}  

	//分  
	int64_t elapsed_minutes() const;

	//时  
	int64_t elapsed_hours() const;

private:
	std::chrono::time_point<std::chrono::high_resolution_clock> m_begin;
};
#endif
