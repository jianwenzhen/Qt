#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QDebug>
#include "opencv.hpp"

class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImageWidget(QWidget *parent = 0);

signals:

public slots:

protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void wheelEvent(QWheelEvent* event);

public:
    void        setImage(const QString& imagePath);
    void        setImage(const QImage& image);

private:
    QImage      m_image;
    QRect       m_posImage;
    float       fScale = 1.0;
    int         m_mouseX;
    int         m_mouseY;
};

#endif // IMAGEWIDGET_H
