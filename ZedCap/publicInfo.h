#ifndef __PUBLICINFO_FILE__YDUSKY_
#define __PUBLICINFO_FILE__YDUSKY_
#include "YDLockDefine.h"
#include "YDCommonFunc.h"
#include <list>
#include <vector>
#include <opencv2/opencv.hpp>

class ImageInfo
{
public:
    long        m_nFrameIndex;	       //图像序号
    uint64       m_nFrameTime;          //图像采集时间
    cv::Mat     m_image;			   //颜色图像数据
    cv::Mat		m_depthdata;			//深度数据
    cv::Mat		m_depthimage;			//深度图像
    cv::Mat		m_confidencedata;		//置信度数据
    cv::Mat		m_confidenceimage;      //置信度图像
    // 内参数据
    cv::Mat		m_kMatrix;
    cv::Mat		m_dist;
    // 外参数据
    cv::Mat		m_matTransform;
    cv::Mat		m_XVec;
    bool	    m_bCopyData;		   //是否拷贝图像数据,赋值操作不对该属性进行修改
    std::string m_strFriendlyName;

    bool			m_bSaveRgb;
    bool            m_bSaveDepth;
public:
    ImageInfo();
    ~ImageInfo();
    ImageInfo(long nIndx, uint64 nFrameTime, const cv::Mat& image, const cv::Mat& depthdata, const cv::Mat& depthimage,
                    const cv::Mat& confidencedata, const cv::Mat& confidenceimage,
                    const cv::Mat& kMatrix, const cv::Mat& dist, const cv::Mat& matTransform, const cv::Mat& XVec,
                    const bool& bSaveRgb, const bool& bSaveDepth, const std::string& strFriendlyName);

    ImageInfo(const ImageInfo& other);

    ImageInfo& operator=(const ImageInfo& other);

};

typedef std::list<ImageInfo*> ListImageInfoBuffer;

//加载文件最大内存
#define MAX_SIZE_LOAD_FILE      (1073741824)  //1GB

#endif
