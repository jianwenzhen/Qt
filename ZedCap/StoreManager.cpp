#include "StoreManager.h"
#include "YDCommonFunc.h"
#include <iostream>
#include <fstream>
#include <time.h>

CStoreManager::CStoreManager(void)
		:m_nSingleFrameIndex(0)
{

}


CStoreManager::~CStoreManager(void)
{
	Stop();

	for (ListImageInfoBuffer::iterator iter = m_listIWPBuffer.begin(); iter != m_listIWPBuffer.end(); ++iter)
	{
		if (*iter)
		{
			delete *iter;
		}
	}
	m_listIWPBuffer.clear();
}

void CStoreManager::Start()
{
	if(!IsRun())
	{
        YDThread::Start();
        YDThread::SetData();
	}
}

void CStoreManager::run(void)
{
	int nSleepTime = 0;
	bool bWork1 = SaveOneIWPFrameToFile();
	if (bWork1)
		nSleepTime = 0;
	else
		nSleepTime = 100;
	if (IsRun())
	{
		Sleep(nSleepTime);
		SetData();
	}
}

void CStoreManager::Update(CSubject* sub)
{
	if (sub)
	{
		if (SUBJECT_CAPTUREIMAGE == sub->m_strSubjectName)
		{
			/*CCameraInfo* pData = (CCameraInfo*)sub;
			ImageInfo* imgInfo = new ImageInfo();
			if (pData->getImageInfo(*imgInfo))
			{
				m_criticalImageBuffer.Lock();
				m_listImageBuffer.push_back(imgInfo);
				m_criticalImageBuffer.Unlock();
			}*/

		} else if (SUBJECT_POINTCLOUD == sub->m_strSubjectName)
		{
			/*CStereoMatch* pData = (CStereoMatch*)sub;
			Vect3DPoint32F pointData;
			if (pData->getPointCDataByIndex(m_nGetIndex, pointData))
			{
				if (pointData.size())
				{
					AddPointCloudToBuffer(pointData);

					m_nGetIndex++;
				}
			}*/
		}
	}
}


void CStoreManager::SetSaveImagePath(const char* strPath)
{
	char szPath[256] = {0};
	strcpy(szPath, strPath);
    if (!YDusky::IsExistFileOrDirectory(szPath))
	{
        YDusky::createPath(szPath);
	}
	m_strImageSavePath = strPath;
}

bool CStoreManager::SaveOneIWPFrameToFile(void)
{
	bool bRet = false;
	m_lockIWPBuffer.AddRead();
	size_t nSize = m_listIWPBuffer.size();
	m_lockIWPBuffer.DecRead();
	if (nSize)
	{
		ImageInfo* pImgInfo = m_listIWPBuffer.front();

		char szPath[MAX_PATH] = { 0 };
		char szPath1[MAX_PATH] = { 0 };	//rgb image
		char szPath2[MAX_PATH] = { 0 };	//path
		char szPath3[MAX_PATH] = { 0 };
		char szPath4[MAX_PATH] = { 0 };
		sprintf(szPath, _T("%s\\%s"), m_strImageSavePath.c_str(), pImgInfo->m_strFriendlyName.c_str());
        if (!YDusky::IsExistFileOrDirectory(szPath))
		{
            YDusky::createPath(szPath);
		}

		if (pImgInfo->m_nFrameTime)
		{
            // override

            time_t t;
            struct tm *tm;
            time(&t);
            tm = localtime(&t);
            YDusky::Int64ToSystemTime(pImgInfo->m_nFrameTime, time);
			sprintf(szPath1, "%s\\ZED-rgb-%06d-%02d%02d%03d.jpg"
				, szPath
                , pImgInfo->m_nFrameIndex
				, time.wMinute
				, time.wSecond
				, time.wMilliseconds);
			sprintf(szPath2, "%s\\ZED-rgb-%06d-%02d%02d%03d.pose"
				, szPath
				, pImgInfo->m_nFrameIndex
				, time.wMinute
				, time.wSecond
				, time.wMilliseconds);

			sprintf(szPath3, "%s\\ZED-depth-%06d-%02d%02d%03d.dat"
				, szPath
				, pImgInfo->m_nFrameIndex
				, time.wMinute
				, time.wSecond
				, time.wMilliseconds);

			sprintf(szPath4, "%s\\ZED-depth-%06d-%02d%02d%03d.png"
				, szPath
				, pImgInfo->m_nFrameIndex
				, time.wMinute
				, time.wSecond
				, time.wMilliseconds);
		}
		else
		{
			sprintf(szPath1, "%s\\ZED-rgb-%06d.jpg", szPath, pImgInfo->m_nFrameIndex);
			sprintf(szPath2, "%s\\ZED-rgb-%06d.pose", szPath, pImgInfo->m_nFrameIndex);
			sprintf(szPath3, "%s\\ZED-depth-%06d.dat", szPath, pImgInfo->m_nFrameIndex);
			sprintf(szPath4, "%s\\ZED-depth-%06d.png", szPath, pImgInfo->m_nFrameIndex);
		}

		cv::imwrite(szPath1, pImgInfo->m_image);
		cv::imwrite(szPath4, pImgInfo->m_depthimage);
		SaveDepthData(pImgInfo->m_depthdata, szPath3);
		
		std::ofstream pose_file(szPath2);
		if (pose_file.is_open())
		{
			int nIntrinsicType = 3; // total intrinsic
			pose_file << "IntrinsicType " << nIntrinsicType << std::endl;
			pose_file << "==Intrinsic==" << std::endl;
			pose_file << "Size" << std::endl << pImgInfo->m_image.size().width << " " << pImgInfo->m_image.size().height << std::endl;
			pose_file << "KMatrix" << std::endl <<
				pImgInfo->m_kMatrix.at<double>(0, 0) << " " << pImgInfo->m_kMatrix.at<double>(0, 1) << " " << pImgInfo->m_kMatrix.at<double>(0, 2) << std::endl <<
				pImgInfo->m_kMatrix.at<double>(1, 0) << " " << pImgInfo->m_kMatrix.at<double>(1, 1) << " " << pImgInfo->m_kMatrix.at<double>(1, 2) << std::endl <<
				pImgInfo->m_kMatrix.at<double>(2, 0) << " " << pImgInfo->m_kMatrix.at<double>(2, 1) << " " << pImgInfo->m_kMatrix.at<double>(2, 2) << std::endl;
			pose_file << "==Dist==" << std::endl <<
				pImgInfo->m_dist.at<double>(0) << " " << pImgInfo->m_dist.at<double>(1) << " " <<
				pImgInfo->m_dist.at<double>(2) << " " << pImgInfo->m_dist.at<double>(3) << " " << pImgInfo->m_dist.at<double>(4) << std::endl;

			pose_file << "==Extrinsic==" << std::endl;
			pose_file << "Position" << std::endl;
			pose_file << pImgInfo->m_XVec.at<double>(0) << " " << pImgInfo->m_XVec.at<double>(1) << " " << pImgInfo->m_XVec.at<double>(2) << std::endl;
			pose_file << "Translation" << std::endl;
			pose_file << pImgInfo->m_TVec.at<double>(0) << " " << pImgInfo->m_TVec.at<double>(1) << " " << pImgInfo->m_TVec.at<double>(2) << std::endl;
			pose_file << "Rotation" << std::endl;
			pose_file << pImgInfo->m_RMatrix.at<double>(0, 0) << " " << pImgInfo->m_RMatrix.at<double>(0, 1) << " " << pImgInfo->m_RMatrix.at<double>(0, 2) << std::endl <<
				pImgInfo->m_RMatrix.at<double>(1, 0) << " " << pImgInfo->m_RMatrix.at<double>(1, 1) << " " << pImgInfo->m_RMatrix.at<double>(1, 2) << std::endl <<
				pImgInfo->m_RMatrix.at<double>(2, 0) << " " << pImgInfo->m_RMatrix.at<double>(2, 1) << " " << pImgInfo->m_RMatrix.at<double>(2, 2) << std::endl;

		}
		pose_file.close();

		delete pImgInfo;
		pImgInfo = NULL;
		m_lockIWPBuffer.EnableWrite();
		m_listIWPBuffer.pop_front();
		m_lockIWPBuffer.DisableWrite();

		bRet = true;
	}
	return bRet;
}

void CStoreManager::SaveDepthData(const cv::Mat& matDepth, const std::string& sPath)
{
	int col = matDepth.size().width;
	int row = matDepth.size().height;
	float *depthdata = new float[col*row];
	memset(depthdata, 0x00, sizeof(float)*col*row);
	float* ptr = (float*)matDepth.data;
	int size = col*row;
	for (int i = 0; i < col*row; ++i)
		depthdata[i] = *(ptr + i);
	std::ofstream fileDepthData(sPath.c_str(), std::ios::out | std::ios::binary);
	if (fileDepthData.is_open())
	{
		fileDepthData.write((char*)&col, sizeof(int));
		fileDepthData.write((char*)&row, sizeof(int));
		fileDepthData.write((char*)depthdata, sizeof(float) * col*row);
		fileDepthData.flush();
	}
	fileDepthData.close();
	delete[]depthdata;
	depthdata = nullptr;
}

void CStoreManager::AddIWPToBuffer(ImageInfo* imgInfo)
{
	if (imgInfo)
	{
		m_lockIWPBuffer.EnableWrite();
		m_listIWPBuffer.push_back(imgInfo);
		m_lockIWPBuffer.DisableWrite();
	}
}
