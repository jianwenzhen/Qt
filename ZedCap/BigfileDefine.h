#ifndef _YDUSKY_ASTRACOLLECTOR_BIG_FILE_BASEDATA_HEADER_FILE_2016_05_13_10_18_
#define _YDUSKY_ASTRACOLLECTOR_BIG_FILE_BASEDATA_HEADER_FILE_2016_05_13_10_18_
#include <assert.h>
//#include "../CoreBase/PublicInfo.h"

const int g_fileSize  = 4500;
const int g_blocksize = 15;
const int g_nFlagCount = 4;
const int g_camTypeSize = 32;
enum enuFileFlag
{
	eFF_V0 = 1302, //具有原始图像数据、机器人位姿数据、车辆数据
	eFF_V1 = 1303, //具有原始图像数据、机器人位姿数据、车辆数据、激光数据
	eFF_V2 = 1304, //具有压缩图像数据、机器人位姿数据、车辆数据、激光数据
};
const int g_fileFlag[g_nFlagCount]  = 
{
	  eFF_V0 //具有原始图像数据、机器人位姿数据、车辆数据
	, eFF_V1 //具有原始图像数据、机器人位姿数据、车辆数据、激光数据
	, eFF_V2 //具有压缩图像数据、机器人位姿数据、车辆数据、激光数据
};

typedef  unsigned long long DATA_ADDRESS;

struct tagFrameHeader
{
    unsigned long  m_lFrameTime;                   /// 帧时间

    tagFrameHeader()
        :m_lFrameTime(0)
    {

    }

    tagFrameHeader(const tagFrameHeader& other)
        :m_lFrameTime(other.m_lFrameTime)
    {

    }

    tagFrameHeader& operator=(const tagFrameHeader& other)
    {
         m_lFrameTime = other.m_lFrameTime;

         return *this;
    }
};

/// 大文件,文件头信息
struct tagFileHeader
{
	int  m_nFileFlag;                              /// 文件标识固定1302
	int  m_nFrameCount;                            /// 文件总帧数
    int  m_nFrameW;                                /// 帧宽
    int  m_nFrameH;                                /// 帧高
    int  m_nFrameC;                                /// 帧通道数
    int  m_nFrameSize;
	int  m_nRealFrameCount;                        /// 文件实际帧数
    int  m_nCameraIndex;                           /// 像机序号
    char m_szCameraType[g_camTypeSize];            /// 像机类型
	DATA_ADDRESS* m_pBlockAddress;                 /// 每块数据的偏移量地址
		

	tagFileHeader()
		: m_nFileFlag(g_fileFlag[1])
		, m_nFrameCount(0)
        , m_nFrameSize(0)
		, m_nRealFrameCount(0)
		, m_pBlockAddress(nullptr)
        , m_nCameraIndex(0)
	{
        memset(m_szCameraType, 0, sizeof(char) * g_camTypeSize);
	}

	tagFileHeader(int nFrameCount, int nFrameSize)
		: m_nFileFlag(g_fileFlag[1])
		, m_nFrameCount(nFrameCount)
		, m_nRealFrameCount(0) 
        , m_nFrameSize(nFrameSize)
		, m_pBlockAddress(nullptr)
        , m_nCameraIndex(0)
	{
        memset(m_szCameraType, 0, sizeof(char) * g_camTypeSize);
	}

	tagFileHeader& operator=(const tagFileHeader& other)
	{
		m_nFileFlag       = other.m_nFileFlag;
		m_nFrameCount     = other.m_nFrameCount;
	    m_nFrameSize      = other.m_nFrameSize;
		m_pBlockAddress   = other.m_pBlockAddress;
        m_nCameraIndex    = other.m_nCameraIndex;
		m_nRealFrameCount = other.m_nRealFrameCount;
        memcpy(m_szCameraType, other.m_szCameraType, sizeof(char) * g_camTypeSize);
		return *this;
	}
	~tagFileHeader()
	{
		if (m_pBlockAddress)
		{
			delete[] m_pBlockAddress;
			m_pBlockAddress = nullptr;
		}
	}
	bool       InitFileHeader(int nFrameCount, int nFrameSize
        , const char* lpCameraType = "unkown", int nCameraIndex = 0
        , int nW = 0, int nH = 0, int nC = 0)
	{
		assert(nFrameCount > 0);
		assert(nFrameSize >  0);

		m_nFrameCount  = nFrameCount;
		m_nFrameSize   = nFrameSize;
		m_nCameraIndex = nCameraIndex;
        m_nFrameW      = nW;
        m_nFrameH      = nH;
        m_nFrameC      = nC;
        memcpy(m_szCameraType, lpCameraType, strlen(lpCameraType));

		if (m_pBlockAddress)
		{
			delete[] m_pBlockAddress;
			m_pBlockAddress = nullptr;
		}
		m_pBlockAddress = new DATA_ADDRESS[m_nFrameCount];
        memset(m_pBlockAddress, 0, sizeof(DATA_ADDRESS) * m_nFrameCount);
		return nullptr != m_pBlockAddress;
	}

	bool      InitFileHeader(void)
	{
		assert(m_nFrameCount > 0);
		assert(m_nFrameSize > 0);

		if (m_pBlockAddress)
		{
			delete[] m_pBlockAddress;
			m_pBlockAddress = nullptr;
		}
		m_pBlockAddress = new DATA_ADDRESS[m_nFrameCount];
        memset(m_pBlockAddress, 0, sizeof(DATA_ADDRESS) * m_nFrameCount);
		return nullptr != m_pBlockAddress;
	}

	void      SetFrameAddress(int nFrameIndex, int nAddress)
	{
		if (   nullptr == m_pBlockAddress 
			|| nFrameIndex < 0
			|| nFrameIndex >= m_nFrameCount)
		{
			return;
		}

		m_pBlockAddress[nFrameIndex] = nAddress;

	}

	///      文件是否是有效的大文件
	bool    IsValidate(void)
	{
        bool bRet = false;
        for (auto i = 0; i < g_nFlagCount; i++)
        {
            if (g_fileFlag[i] == m_nFileFlag)
            {
                bRet = true;
                break;
            }
        }
		return bRet;
	}
};


////////////////////////////////////////////////////////////
////大文件数据格式定义
////////////////////////////////////////////////////////////

// |--------------------------------------------------------------------------------
// |tagFileHeader|Block Address|tagBlockInfo|BlockData|...|tagBlockInfo|BlockData|
// |--------------------------------------------------------------------------------
#endif