/************************************************************************
* Copyright (c) 2015
* All rights resverved.
*
* 文件名称：StoreManager.h
* 摘    要：存储模块
* 引入文件：
*
* 当前版本：1.0.2
* 作    者：杨玉坤
* 完成日期：2015.06.27
* 更新日志：2015.08.15
* 更新日志：2016.03.20
* 添加void AddImageToBuffer(ImageInfo* imgInfo);
************************************************************************/

//#include <afxmt.h>
#include "YDThread.h"
#include "YDLockDefine.h"
#include "publicInfo.h"
#include "time.h"

class CStoreManager 
    :public YDThread
{
public:
	CStoreManager(void);
	virtual ~CStoreManager(void);

/************************************************************************/
/* 存储图像数据                                                                     */
/************************************************************************/
private:
	int							m_nSingleFrameIndex;
	std::string					m_strImageSavePath;

/*	ListImageInfoBuffer	        m_listRGBImageBuffer;
    YDLock                      m_lockRGBImageBuffer;

	ListImageInfoBuffer	        m_listDepthImageBuffer;
    YDLock                      m_lockDepthImageBuffer;

	ListImageInfoBuffer	        m_listIrImageBuffer;
    YDLock                      m_lockIrImageBuffer;*/

	ListImageInfoBuffer			m_listIWPBuffer;
    YDLock						m_lockIWPBuffer;
public:
	void					SetSaveImagePath(const char* strPath);

/*	void					AddRGBImageToBuffer(ImageInfo* imgInfo);
	void					AddDepthImageToBuffer(ImageInfo* imgInfo);
	void					AddIrImageToBuffer(ImageInfo* imgInfo);*/

	void					AddIWPToBuffer(ImageInfo* imgInfo);
private:                
/*	bool					SaveOneRGBFrameToFile(void);
	bool					SaveOneDepthFrameToFile(void);
	
	bool					SaveOneIrFrameToFile(void);

	*/
	void					SaveDepthData(const cv::Mat& matDepth, const std::string& sPath);
	bool					SaveOneIWPFrameToFile(void);
public:
/************************************************************************/
/* 框架函数                                                                     */
/************************************************************************/
private:
	virtual void run(void);//存储工作

public:
	virtual void Update(CSubject* sub);

public:
	void Start();
	
};

