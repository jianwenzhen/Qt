#include "imagewidget.h"

ImageWidget::ImageWidget(QWidget *parent) : QWidget(parent)
{

}

void ImageWidget::setImage(const QImage& image)
{
    m_image = image;
    repaint();
}

void ImageWidget::setImage(const QString& imagePath)
{
    m_image.load(imagePath);
    repaint();
}

void ImageWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);

    painter.setPen(Qt::gray);
    painter.drawRect(0, 0, width() - 1, height() - 1);

    if(!m_image.isNull())
    {
        float fScaleImageW = m_image.width()*1.0/width();
        float fScaleImageH = m_image.height()*1.0/width();
        float fScaleImage = std::max(fScaleImageH, fScaleImageW);

        float fImageW = m_image.width() / fScaleImage *fScale;
        float fImageH = m_image.height() / fScaleImage *fScale;

        float fLeft = (width() - fImageW)/2;
        float fTop = (height() - fImageH)/2;

        m_posImage.setLeft(fLeft);
        m_posImage.setTop(fTop);
        m_posImage.setWidth(fImageW);
        m_posImage.setHeight(fImageH);

        painter.drawImage(m_posImage, m_image);
    }

}

void ImageWidget::wheelEvent(QWheelEvent *event)
{
    Q_UNUSED(event);

}
