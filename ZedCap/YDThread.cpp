#include "YDThread.h"


YDThread::YDThread()
	: _pThread(nullptr)
	, _bRun(false)
	, _bWait(true)
	, _bBusy(false)
	, _bProcess(false)
{
}


YDThread::~YDThread()
{
	if (_pThread)
	{
		delete _pThread;
		_pThread = nullptr;
	}
}

void YDThread::threadProc(void* lpParameter)
{
	YDThread* pThread = static_cast<YDThread*>(lpParameter);
	if (nullptr == pThread)
	{
		return;
	}
	pThread->threadProcessStart();
	do 
	{
		pThread->setBusy(false);

		if (pThread->needWait())
		{
			pThread->waitTrigger();
		}
		pThread->setBusy(true);

		pThread->run();

	} while (pThread->isRun());

	pThread->threadProcessEnd();
}

void    YDThread::run()
{

}

bool    YDThread::start(void)
{
	bool bRet = false;

	do 
	{
		if (_pThread)
		{
			bRet = true;
			break;
		}

		_pThread = new std::thread(threadProc, this);
		_pThread->detach();
		setRun(true);

		bRet = true;

	} while (0);

	return bRet;
}

void    YDThread::stop(void)
{
	if (!isBusy())
	{
		trigger();
	}
	setRun(false);

	waitThreadQuit();

	if(_pThread)
	{
		delete _pThread;
		_pThread = nullptr;
	}
}

void YDThread::trigger(void)
{
	std::unique_lock<std::mutex> lck(_lcMutex);
	_condition.notify_one();
}

void    YDThread::waitTrigger(void)
{
	std::unique_lock<std::mutex> lck(_lcMutex);
	_condition.wait(lck);
}

bool YDThread::isBusy(void)
{
	return _bBusy;
}

bool YDThread::isRun(void)
{
	return _bRun;
}

void YDThread::setRun(bool bTrue)
{
	_bRun = bTrue;
}

void YDThread::setBusy(bool bTrue)
{
	_bBusy = bTrue;
}

void YDThread::waitThreadQuit(void)
{
	while(_bProcess)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

void YDThread::threadProcessStart(void)
{
	_bProcess = true;
}

void YDThread::threadProcessEnd(void)
{
	_bProcess = false;
}

bool YDThread::needWait(void)
{
	return _bWait;
}

void YDThread::needWait(bool bWait)
{
	_bWait = bWait;
}
