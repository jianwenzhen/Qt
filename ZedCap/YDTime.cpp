/************************************************************************/
/* 时间计时器
/* 功能: 跨平台的计时工具类
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2017.03.16*/
/************************************************************************/
#include "YDTime.h"
YDTimer::YDTimer()
	: m_begin(std::chrono::high_resolution_clock::now()) 
{}
void YDTimer::reset()
{ 
	m_begin = std::chrono::high_resolution_clock::now(); 
}

//默认输出毫秒  
int64_t YDTimer::elapsed() const
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
}

//默认输出秒  
double YDTimer::elapsed_second() const
{
	return std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - m_begin).count();
}

//微秒  
int64_t YDTimer::elapsed_micro() const
{
	return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
}

//纳秒  
int64_t YDTimer::elapsed_nano() const
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - m_begin).count();
}

////秒  
//int64_t elapsed_seconds() const  
//{  
//  return duration_cast<chrono::seconds>(high_resolution_clock::now() - m_begin).count();  
//}  

//分  
int64_t YDTimer::elapsed_minutes() const
{
	return std::chrono::duration_cast<std::chrono::minutes>(std::chrono::high_resolution_clock::now() - m_begin).count();
}

//时  
int64_t YDTimer::elapsed_hours() const
{
	return std::chrono::duration_cast<std::chrono::hours>(std::chrono::high_resolution_clock::now() - m_begin).count();
}
