// DlgMainFrame.cpp : implementation file
//

#include "stdafx.h"
#include "KinectCap.h"
#include "DlgMainFrame.h"
#include "afxdialogex.h"
#include "BaseOptionDlg.h"
#include "PageDevices.h"
#include "PageDataManager.h"
#include "PageKinectParameter.h"
#include "../CoreBase/CommonFunc.h"
#include "KinectGeometryFun.h"
#include "DlgStoreSetting.h"

//#include <boost/cstdint.hpp>
//#include <boost/filesystem/operations.hpp>
//#include <boost/filesystem/path.hpp>
//#include <boost/filesystem/convenience.hpp>
#include <iostream>
#include <fstream>
#include "3DScenePanel.h"
#include "../YDCameraHK/YDCameraHK.h"
#include "../CoreBase/CommonFunc.h"

#define  ID_TIMER_SLIDER   1000

class ZedDataCB
	: public ZedCallback
{
public: 
	ZedDataCB(CDlgMainFrame* pFrame)
		: m_pFrame(pFrame)
	{

	}
	virtual void dataCallback(image_with_pose& ip)
	{
		if (m_pFrame)
		{
			m_pFrame->zedDataCallback(ip);
		}
	}
private:
	CDlgMainFrame* m_pFrame;
};

class CallbackHK
	: public imageCallack
{
public:
	CallbackHK(CDlgMainFrame* pFrame)
		:m_pMFrame(pFrame)
	{

	}
	virtual void         callback(const tagFrameData& imageInfo)
	{
		if (m_pMFrame)
		{
			m_pMFrame->callback(imageInfo);
		}
	}

protected:
	CDlgMainFrame*      m_pMFrame;
};

void  dataCaptureThread(CDlgMainFrame* pFrame)
{
	if (pFrame)
	{
		pFrame->dataCaptureHandle();
	}
}

enum enumMainFrameCtrlID
{
	eMFID_TITLE   = 1,  //标题栏
	eMFID_SLIDER  = 2,  //侧边栏
	eMFID_PREVIEW = 3,  //预览区
};
// CDlgMainFrame dialog

IMPLEMENT_DYNAMIC(CDlgMainFrame, CDialogEx)

CDlgMainFrame::CDlgMainFrame(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLGMAINFRAME, pParent)
	, m_nSliderWidth(0)
	, m_nKinectPCCnt(0)
	, m_bZoom(FALSE)
	, m_bShowSlider(FALSE)
	, m_bOpenDevices(FALSE)
	, m_ePipelineType(CUDA_PACKETPIPELINE)
	, m_eImageType(bmp_type)
	, m_ePointCloudType(pc_xyz)
	, m_ePointCloudUnit(pc_mm)
	, m_bEnableFilter(true)
	, m_nFilterWidth(1)
	, m_nFilterHeight(2)
	, m_fFilterTolerance(0.01)
	, m_nStoreFrameCounts(0)
	, m_nStoreInterval(0)
	, m_eStoreMode(eSM_NONE)
	, m_bInitMulti(FALSE)
	, m_bShowOverlapInfo(TRUE)
	, m_nStoreIntervalTime(1)
	, m_eStoreIntervalType(eST_FRAME)
	, m_pZedCallback(nullptr)
	, m_nStepCameraSetting(1)
{
	m_pZedCallback = new ZedDataCB(this);
	m_pCallbackHK = new CallbackHK(this);
}

CDlgMainFrame::~CDlgMainFrame()
{
	if (m_pZedCallback)
	{
		delete m_pZedCallback;
		m_pZedCallback = nullptr;
	}
}

void CDlgMainFrame::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgMainFrame, CDialogEx)
	ON_WM_CLOSE()
	ON_WM_CREATE()
	ON_WM_PAINT()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_WM_LBUTTONDOWN()
	ON_WM_GETMINMAXINFO()
	ON_MESSAGE(MSG_NETCOMMAND, OnRecvNetCommand)
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CDlgMainFrame message handlers


int CDlgMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	UINT nID = GetDlgCtrlID() + 1;
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	
	CRect rectClient;
	GetClientRect(rectClient);
	CRect rectTitle(rectClient);
	if (!::IsWindow(m_wndTitle))
	{
		rectTitle.bottom = rectTitle.top + HEIGHT_CAPTION;
		m_wndTitle.Create(_T("ZEDCapture"), dwStyle, rectTitle, this, nID + eMFID_TITLE);
	}
	CRect rectSlider(rectClient);
	if (!::IsWindow(m_wndSlider))
	{
		rectSlider.top   = rectTitle.bottom;
		rectSlider.right = rectSlider.left + m_nSliderWidth;
		m_wndSlider.Create(_T("Slider"), dwStyle, rectSlider, this, nID + eMFID_SLIDER);
		m_wndSlider.ShowWindow(SW_HIDE);
	}

	CRect rectPreview(rectClient);
	if (!::IsWindow(m_wndPreview))
	{
		rectPreview.top   = rectTitle.bottom;
		rectPreview.left  = rectSlider.right;
		
		m_wndPreview.Create(_T("preivew"), dwStyle, rectPreview, this, nID + eMFID_PREVIEW);
	}

	int nScreenW = GetSystemMetrics(SM_CXFULLSCREEN) * 0.8;
	int nScreenH = (GetSystemMetrics(SM_CYFULLSCREEN) + GetSystemMetrics(SM_CYCAPTION)) * 0.8;
	MoveWindow(0, 0, nScreenW, nScreenH);
	CenterWindow();
	//SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE, 0);

	LoadParameterFromFile();


	///// init net manager
	CString strPath;
	strPath.Format("%s\\Netcfg_C.ini", YDuskyFun::GetCurExeDir().c_str());
	if (m_netManager.InitNetManager(strPath, this) < 0)
	{
		AfxMessageBox("网络初始化失败！");
	}

	return 0;
}

void CDlgMainFrame::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: Add your message handler code here
					   // Do not call CDialogEx::OnPaint() for painting messages

	CDC memDC;
	memDC.CreateCompatibleDC(&dc);
	CRect rectClient;
	GetClientRect(rectClient);
	CBitmap bm;
	bm.CreateCompatibleBitmap(&dc, rectClient.Width(), rectClient.Height());
	memDC.SelectObject(&bm);
	memDC.FillRect(rectClient, &CBrush(COLOR_CLIENT_BK));

	dc.BitBlt(0, 0, rectClient.Width(), rectClient.Height(), &memDC, 0, 0, SRCCOPY);
}


void CDlgMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	CRect rectClient;
	GetClientRect(rectClient);
	CRect rectTitle(rectClient);
	if (::IsWindow(m_wndTitle))
	{
		rectTitle.bottom = rectTitle.top + HEIGHT_CAPTION;
		m_wndTitle.MoveWindow(rectTitle);
	}

	if (m_bShowSlider)
	{
		CRect rectSlider(rectClient);
		if (::IsWindow(m_wndSlider))
		{
			rectSlider.top = rectTitle.bottom;
			rectSlider.right = rectSlider.left + m_nSliderWidth;
			m_wndSlider.MoveWindow(rectSlider);
		}
		CRect rectPreview(rectClient);
		if (::IsWindow(m_wndPreview))
		{
			rectPreview.top = rectTitle.bottom;
			rectPreview.left = rectSlider.right;

			m_wndPreview.MoveWindow(rectPreview);
		}
	}
	else
	{
		CRect rectPreview(rectClient);
		if (::IsWindow(m_wndPreview))
		{
			rectPreview.top = rectTitle.bottom;
			rectPreview.left = m_nSliderWidth;

			m_wndPreview.MoveWindow(rectPreview);
		}

		CRect rectSlider(rectClient);
		if (::IsWindow(m_wndSlider))
		{
			rectSlider.top = rectTitle.bottom;
			rectSlider.right = rectPreview.left;
			m_wndSlider.MoveWindow(rectSlider);
		}

	}

}


void CDlgMainFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	int nScreenW = GetSystemMetrics(SM_CXSCREEN);
	int nScreenH = GetSystemMetrics(SM_CYSCREEN);

	lpMMI->ptMaxSize.x = nScreenW;
	lpMMI->ptMaxSize.y = nScreenH;
	lpMMI->ptMaxPosition.x = nScreenW;
	lpMMI->ptMaxPosition.y = nScreenH;
	lpMMI->ptMaxTrackSize.x = nScreenW;
	lpMMI->ptMaxTrackSize.y = nScreenH;

	lpMMI->ptMinTrackSize.x = nScreenW * 0.8;

	lpMMI->ptMinTrackSize.y = lpMMI->ptMinTrackSize.x * 0.618;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}


void CDlgMainFrame::OnLButtonDown(UINT nFlags, CPoint point)
{
	SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, NULL);

	CDialogEx::OnLButtonDown(nFlags, point);
}

void CDlgMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case ID_TIMER_SLIDER:
	{
		if (m_bShowSlider)
		{
			if (m_nSliderWidth <= WIDTH_SLIDER)
			{
				m_nSliderWidth += 15;
			}
			if (m_nSliderWidth > WIDTH_SLIDER)
			{
				m_nSliderWidth = WIDTH_SLIDER;
				KillTimer(nIDEvent);
			}
			OnSize(0, 0, 0);
		}
		else
		{
			if (m_nSliderWidth < 0)
			{
				m_nSliderWidth = 0;
				KillTimer(nIDEvent);
				if (::IsWindow(m_wndSlider))
					m_wndSlider.ShowWindow(SW_HIDE);
			}
			if (m_nSliderWidth >= 0)
			{
				m_nSliderWidth -= 15;
			}

			OnSize(0, 0, 0);

		}
	}
	break;
	default:
		break;
	}

	CDialogEx::OnTimer(nIDEvent);
}

void CDlgMainFrame::ClearCapturers()
{
	for (ZEDVectPtrCapturer::const_iterator conIter = m_vectCapturer.begin(); 
	conIter != m_vectCapturer.end(); ++conIter)
	{
		CZEDCapturer* pCapture = *conIter;
		if (pCapture)
		{
			pCapture->Stop();
			pCapture->SetCallback(NULL, NULL);
			delete pCapture;
			pCapture = NULL;
		}
	}
	m_vectCapturer.clear();
}

BOOL CDlgMainFrame::IsOpenDevices()
{
	return m_bOpenDevices;
}

void CDlgMainFrame::OnCaptureSingleFrame(void)
{
/*	if (m_kinect.m_vDev.size() <= 0)
		return;

	// get ir camera parameters (also depth camera)
	libfreenect2::Freenect2Device::IrCameraParams ir_camera;
	ir_camera = m_kinect.GetDefaultDepthPara();

	// get depth camera parameters
	libfreenect2::Freenect2Device::ColorCameraParams rgb_camera;
	rgb_camera = m_kinect.GetDefaultRgbPara();

	// set the save disk path
	CString strName;
	switch (m_ePointCloudType)
	{
	case pc_xyz:
		strName.Format("%s\\kinect-xyz-%d.txt", m_kinect.m_locatedisk.c_str(), m_nKinectPCCnt);
		break;
	case pc_xyz_rgb:
		strName.Format("%s\\kinect-xyzrgb-%d.txt", m_kinect.m_locatedisk.c_str(), m_nKinectPCCnt);
		break;
	default:
		break;
	}
	
	CKinectGeometryFun kgf;
	kgf.InitialLookupTable(ir_camera, rgb_camera);
	kgf.SetFilterHalfWidth(m_nFilterWidth);
	kgf.SetFilterHalfHeight(m_nFilterHeight);
	kgf.SetFilterTolerance(m_fFilterTolerance);
	kgf.SetEnableFilter(m_bEnableFilter);

	cv::Mat undistort_depth;
	m_kinect.m_depth_frame_org.copyTo(undistort_depth);
	kgf.CreateUndistortedDepth(&m_kinect.m_depth_frame_org, &undistort_depth);
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointcloud(new pcl::PointCloud<pcl::PointXYZRGB>());
	
	switch (m_ePointCloudType)
	{
	case pc_xyz:
		kgf.DepthImageToPointCloud(undistort_depth, m_ePointCloudUnit, pointcloud);
		break;
	case pc_xyz_rgb:
		kgf.GetXYZRGB(undistort_depth, m_kinect.m_rgbd_frame, m_ePointCloudUnit, pointcloud);
		//kgf.DepthImageToPointCloud_withColor(undistort_depth, m_kinect.m_rgb_frame, nUnit, pointcloud,
		//kgf.GetEnableFilter(),kgf.GetFilterHalfWidth(),kgf.GetFilterHalfHeight(),kgf.GetFilterTolerance());
		break;
	default:
		break;
	}

	kgf.SavePointCloud(strName.GetBuffer(0), pointcloud);
	++m_nKinectPCCnt;*/
}

void CDlgMainFrame::SingleCapture(void)
{
/*	char szDiskLocate[1024];
	sprintf_s(szDiskLocate, "D:\\%s", m_kinect.m_currentSerials.c_str());
	namespace fs = boost::filesystem;
	fs::path full_path(fs::initial_path());
	full_path = fs::system_complete(fs::path(szDiskLocate, fs::native));
	if (!fs::exists(full_path))
	{
		bool bRet = fs::create_directories(full_path);
		if (false == bRet)
		{
			return;
		}
	}

	m_kinect.SetLocateDisk(szDiskLocate);
	m_kinect.m_unit_type = m_ePointCloudUnit;
	m_kinect.SingleCap();
	CaptureSingleFrame();*/
}

void CDlgMainFrame::SelectDevice(int nIndex)
{
//	m_kinect.SetCurrentDevice(nIndex);
}

void CDlgMainFrame::ResetCaptureFrames(void)
{
//	m_kinect.ResetSingleCapCnt();
//	m_kinect.ResetMultipleCapCnt();
}

void CDlgMainFrame::OnCaptureMultiFrames(void)
{
/*	char szDiskLocate[1024];
	sprintf_s(szDiskLocate, "D:\\%s", m_kinect.m_currentSerials.c_str());
	namespace fs = boost::filesystem;
	fs::path full_path(fs::initial_path());
	full_path = fs::system_complete(fs::path(szDiskLocate, fs::native));
	if (!fs::exists(full_path))
	{
		bool bRet = fs::create_directories(full_path);
		if (false == bRet)
		{
			return;
		}
	}

	m_kinect.SetLocateDisk(szDiskLocate);
	m_kinect.MultipleCap();*/

//	if (m_kinect.m_vDev.size() <= 0)
//		return;
	m_eStoreMode = eSM_MULTI;
}

void CDlgMainFrame::SetLocateDisk(void)
{
	CString strFloderPath;
	UINT nFlags = BIF_RETURNONLYFSDIRS | BIF_USENEWUI;
	if (!theApp.GetShellManager()->BrowseForFolder(strFloderPath, NULL, NULL, NULL, nFlags))
	{
		return;
	}
//	m_kinect.SetLocateDisk(strFloderPath.GetBuffer(0));
}

BOOL   CDlgMainFrame::IsEnableSaveFrame(int nKinectIndex, long long nCurFrameTime)
{
	BOOL bRet = FALSE;

	switch (m_eStoreIntervalType)
	{
	case eST_FRAME:
		bRet = !(m_vStoreFrameIndex.at(nKinectIndex) % (m_nStoreInterval + 1));
		break;
	case eST_TIME:
		bRet = (nCurFrameTime - m_vStoreFrameTime.at(nKinectIndex) >= m_nStoreIntervalTime);
		break;
	default:
		break;
	}

	return bRet;
}

void  CDlgMainFrame::zedDataCallback(image_with_pose& ip)
{
	// show current rgb image
	m_wndPreview.SetPreviewImage(0, 0, ip.matLeftRgbImg);

	// show current depth image
	m_wndPreview.SetPreviewImage(0, 1, ip.matDepthImg);

	// show current path in 3d window
//	m_wndPreview.Get3DScene().GenerateCurrentPath(ip);
//	m_wndPreview.Get3DScene().InitLookAtInFixedView();
//	m_wndPreview.Get3DScene().InitLookAtCurrentPath();
//	m_wndPreview.Get3DScene().UpdateShow();

	m_wndPreview.Get3DScene().GenerateAllPath(ip);
	m_wndPreview.Get3DScene().InitLookAtInFixedView();
	m_wndPreview.Get3DScene().UpdateShow();

	switch (m_eStoreMode)
	{
	case CDlgMainFrame::eSM_NONE:
		break;
	case CDlgMainFrame::eSM_SINGLE:
	{
		// single cap
		SaveSingleIWP(ip, true, false, false);
		m_eStoreMode = CDlgMainFrame::eSM_NONE;
	}
	break;
	case CDlgMainFrame::eSM_MULTI:
	{
		if (m_vStoreMultipleIndex.at(0) < m_nStoreFrameCounts && m_storeManager.IsRun())
		{
			if (IsEnableSaveFrame(0, ip.timestamp))
			{
				m_storeManager.AddIWPToBuffer(new ImageInfo(m_vStoreMultipleIndex.at(0), ip.timestamp,ip.matLeftRgbImg, ip.matDepthData,ip.matDepthImg,
					ip.matConfidenceData,ip.matConfidenceImg,
				ip.kMatrix, ip.disto,ip.matTransform, ip.matXVec,1, 1, m_zedCamera.getSerial()));
				
				m_vStoreMultipleIndex.at(0)++;
				m_vStoreFrameTime.at(0) = ip.timestamp;

				m_wndPreview.Get3DScene().GenerateAllPath(ip);
				m_wndPreview.Get3DScene().InitLookAtAllPath();
			}
			m_vStoreFrameIndex.at(0)++;
		}
		else
		{
			m_eStoreMode = CDlgMainFrame::eSM_NONE;
			m_vStoreMultipleIndex.at(0) = 0;
			m_vStoreFrameIndex.at(0) = 0;
			m_vStoreFrameTime.at(0) = 0;
			::PostMessage(m_wndTitle.GetSafeHwnd(), MSG_UPDATEUI, NULL, NULL);
			AfxMessageBox("采集完毕");
		}
	}
	break;
	default:
		break;
	}
}

void CDlgMainFrame::CaptureSingleFrame(void)
{
	if (!IsMultiFramesCpture())
	{
		if (m_strStorePath.IsEmpty())
		{
			CString strFloderPath;
			UINT nFlags = BIF_RETURNONLYFSDIRS | BIF_USENEWUI;
			if (!theApp.GetShellManager()->BrowseForFolder(strFloderPath, NULL, NULL, NULL, nFlags))
			{
				return;
			}
			m_strStorePath = strFloderPath;
		}
		m_eStoreMode = eSM_SINGLE;
	}
	else
	{
		AfxMessageBox("正在多帧存储,请等待或者停止多帧存储!");
	}
}

void CDlgMainFrame::CaptureMultiFrames(void)
{
	if (eSM_NONE == m_eStoreMode)
	{
		for (int i = 0; i < m_vStoreMultipleIndex.size(); ++i)
		{
			m_vStoreMultipleIndex[i] = 0;
			m_vStoreFrameIndex.at(i) = 0;
			m_vStoreFrameTime.at(i) = 0;
		}
		m_eStoreMode = eSM_MULTI;
		m_storeManager.SetSaveImagePath(m_strStorePath);
		m_storeManager.Start();
	}
	else
	{
		for (int i = 0; i < m_vStoreMultipleIndex.size(); ++i)
		{
			m_vStoreMultipleIndex[i] = 0;
			m_vStoreFrameIndex.at(i) = 0;
			m_vStoreFrameTime.at(i) = 0;
		}
		m_eStoreMode = eSM_NONE;
	}
}

BOOL CDlgMainFrame::IsMultiFramesCpture(void)
{
	return eSM_MULTI == m_eStoreMode;
}

void CDlgMainFrame::SaveBinaryData(const cv::Mat& matDepth, const std::string& sPath)
{
	int col = matDepth.size().width;
	int row = matDepth.size().height;
	float *depthdata = new float[col*row];
	memset(depthdata, 0x00, sizeof(float)*col*row);
	float* ptr = (float*)matDepth.data;
	int size = col*row;
	for (int i = 0; i < col*row; ++i)
		depthdata[i] = *(ptr + i);
	std::ofstream fileDepthData(sPath.c_str(), std::ios::out | std::ios::binary);
	if (fileDepthData.is_open())
	{
		fileDepthData.write((char*)&col, sizeof(int));
		fileDepthData.write((char*)&row, sizeof(int));
		fileDepthData.write((char*)depthdata, sizeof(float) * col*row);
		fileDepthData.flush();
	}
	fileDepthData.close();
	delete[]depthdata;
	depthdata = nullptr;
}

void CDlgMainFrame::LoadBinaryData(cv::Mat& matDepth, const std::string& sPath)
{
	std::ifstream fileDepthData(sPath.c_str(), std::ios::in | std::ios::binary);
	if (fileDepthData.is_open())
	{
		int col(0), row(0);
		fileDepthData.read((char*)&col, sizeof(int));
		fileDepthData.read((char*)&row, sizeof(int));
		matDepth = cv::Mat(row, col, CV_32F);
		float* ptr = (float*)matDepth.data;
		float* depth = new float[col*row];
		memset(depth, 0x00, sizeof(float)*col*row);
		fileDepthData.read((char*)depth, sizeof(float)*col*row);
		for (int j = 0; j < row; ++j)
		{
			for (int i = 0; i < col; ++i)
			{
				*(ptr + j*col + i) = *(depth + j*col + i);
			}
		}
		delete[]depth;
		depth = nullptr;
	}
	fileDepthData.close();
}

bool  CDlgMainFrame::SaveSingleIWP(image_with_pose iwp, bool bSaveRgb /*= true*/, bool bSaveDepth /*= true*/, bool bSavePointCloud /*=true*/)
{
	bool bRet = true;
	char szPath[MAX_PATH] = { 0 };
	sprintf(szPath, "%s\\%s", m_strStorePath.GetBuffer(), m_zedCamera.getSerial().c_str());
	if (!YDuskyFun::IsExistFile(szPath))
		YDuskyFun::createPath(szPath);
	char szFilePath[MAX_PATH] = { 0 };

	// save rgb
	if (bSaveRgb)
	{
		if (m_eImageType == bmp_type)
			sprintf(szFilePath, "%s\\ZED-rgb-%06d.bmp", szPath, m_vStoreSingleIndex.at(0));
		else if (m_eImageType == jpg_type)
			sprintf(szFilePath, "%s\\ZED-rgb-%06d.jpg", szPath, m_vStoreSingleIndex.at(0));
		cv::imwrite(szFilePath, iwp.matLeftRgbImg);

		sprintf(szFilePath, "%s\\ZED-rgb-%06d.jpg", szPath, m_vStoreSingleIndex.at(0));
	}

	// save depth image
	sprintf(szFilePath, "%s\\ZED-depth-%06d.png", szPath, m_vStoreSingleIndex.at(0));
	cv::imwrite(szFilePath, iwp.matDepthImg);

	// save depth data as binary
	sprintf(szFilePath, "%s\\ZED-depth-%06d.dat", szPath, m_vStoreSingleIndex.at(0));
	SaveBinaryData(iwp.matDepthData, szFilePath);

	// save confidence image
	sprintf(szFilePath, "%s\\ZED-confidence-%06d.png", szPath, m_vStoreSingleIndex.at(0));
	cv::imwrite(szFilePath, iwp.matConfidenceImg);

	// save confidence data as binary
	sprintf(szFilePath, "%s\\ZED-confidence-%06d.dat", szPath, m_vStoreSingleIndex.at(0));
	SaveBinaryData(iwp.matConfidenceData, szFilePath);

	// save depth and depth data
	if (bSaveDepth)
	{
		int col = iwp.matDepthImg.size().width;
		int row = iwp.matDepthImg.size().height;
		// save depth point cloud as ascii
		float cx = iwp.cx();
		float cy = iwp.cy();
		float fx = iwp.fx();
		float fy = iwp.fy();
		sprintf(szFilePath, "%s\\ZED-depth-%06d.xyzrgb", szPath, m_vStoreSingleIndex.at(0));
		float *depth_data = (float*)(iwp.matDepthData.data);
		std::ofstream file(szFilePath, std::ios::out);
		if (file.is_open())
		{
			//iwp.matPointCloud.data
			for (int r = 0; r < row; ++r)
			{
				for (int c = 0; c < col; ++c)
				{
					int idx = r*col + c;
					float depthValue = *(depth_data + idx);
					if (isnan(depthValue))
						continue;
					if (fabs(depthValue) < iwp.fMinDistance || fabs(depthValue) > iwp.fMaxDistance)
						continue;

					MyRGBA rgb = m_zedCamera.GetRGBA(iwp.matLeftRgbImg, c, r);
					float x = (c - cx) / fx*depthValue;
					float y = (r - cy) / fy*depthValue;
					file << x << " " << y << " " << depthValue << " " << 0 << " " << 0 << " " << 0 << " "
						<< (int)rgb.b << " " << (int)rgb.g << " " << (int)rgb.r << std::endl;
				}
			}
		}
		file.close();
	}

	if (bSavePointCloud)
	{
		sprintf(szFilePath, "%s\\ZED-pointcloud-%06d.ply", szPath, m_vStoreSingleIndex.at(0));
		sl::savePointCloudAs(iwp.matPointCloud, sl::POINT_CLOUD_FORMAT::POINT_CLOUD_FORMAT_PLY_ASCII, szFilePath, true);
	}


	// save path as *.pose file
	sprintf(szFilePath, "%s\\ZED-rgb-%06d.pose", szPath, m_vStoreSingleIndex.at(0));
	std::ofstream pose_file(szFilePath);
	if (pose_file.is_open())
	{
		int nIntrinsicType = 3; // total intrinsic
		pose_file << "IntrinsicType " << nIntrinsicType << std::endl;
		pose_file << "==Intrinsic==" << std::endl;
		pose_file << "Size" << std::endl << iwp.matLeftRgbImg.size().width << " " << iwp.matLeftRgbImg.size().height << std::endl;
		pose_file << "KMatrix" << std::endl <<
			iwp.kMatrix.at<double>(0, 0) << " " << iwp.kMatrix.at<double>(0, 1) << " " << iwp.kMatrix.at<double>(0, 2) << std::endl <<
			iwp.kMatrix.at<double>(1, 0) << " " << iwp.kMatrix.at<double>(1, 1) << " " << iwp.kMatrix.at<double>(1, 2) << std::endl <<
			iwp.kMatrix.at<double>(2, 0) << " " << iwp.kMatrix.at<double>(2, 1) << " " << iwp.kMatrix.at<double>(2, 2) << std::endl;
		pose_file << "==Dist==" << std::endl <<
			iwp.disto.at<double>(0) << " " << iwp.disto.at<double>(1) << " " <<
			iwp.disto.at<double>(2) << " " << iwp.disto.at<double>(3) << " " << iwp.disto.at<double>(4) << std::endl;

		pose_file << "==Extrinsic==" << std::endl;
		pose_file << "Position" << std::endl;
		pose_file << iwp.matXVec.at<double>(0) << " " << iwp.matXVec.at<double>(1) << " " << iwp.matXVec.at<double>(2) << std::endl;
		pose_file << "Translation" << std::endl;
		pose_file << iwp.matTransform(cv::Rect(3, 0, 1, 3)).at<double>(0) << " " << iwp.matTransform(cv::Rect(3, 0, 1, 3)).at<double>(1) << " " << iwp.matTransform(cv::Rect(3, 0, 1, 3)).at<double>(2) << std::endl;
		pose_file << "Rotation" << std::endl;
		pose_file << iwp.matTransform.at<double>(0, 0) << " " << iwp.matTransform.at<double>(0, 1) << " " << iwp.matTransform.at<double>(0, 2) << std::endl <<
			iwp.matTransform.at<double>(1, 0) << " " << iwp.matTransform.at<double>(1, 1) << " " << iwp.matTransform.at<double>(1, 2) << std::endl <<
			iwp.matTransform.at<double>(2, 0) << " " << iwp.matTransform.at<double>(2, 1) << " " << iwp.matTransform.at<double>(2, 2) << std::endl;

	}
	pose_file.close();

	// save hk video image
	char szHkImagePath[256] = { 0 };
	sprintf(szHkImagePath, "%s\\hk-%06d.jpg", szPath, m_vStoreSingleIndex.at(0));
	cv::Mat image = getImageFromData();
	if (image.data)
	{
		cv::imwrite(szHkImagePath, getImageFromData());
	}
	
	m_vStoreSingleIndex.at(0)++;

	return bRet;
}

void CDlgMainFrame::ScanZEDDevices(void)
{
	if (!::IsWindow(m_wndPreview))
		return;

	//if (m_zedCamera.bIsOpen())
		m_zedCamera.closeCamera();
	m_zedCamera.clear();

	// open zed camera
	m_zedCamera.openCamera();

	// set parameters
	m_zedCamera.LoadCameraParameters();
	m_zedCamera.CameraControlSetting();

	// generate save disk path
	char szDiskLocate[1024];
	sprintf_s(szDiskLocate, "%s\\%s", m_strStorePath.GetBuffer(), m_zedCamera.getSerial().c_str());
	YDuskyFun::createPath(szDiskLocate);
	
	// initial device list
	m_wndSlider.AddItem(m_zedCamera.getSerial().c_str(), IDB_ICON_KINECT);
	m_wndPreview.InitKinectsCounts(1);

	if (!m_zedCamera.bIsOpen())
	{
		AfxMessageBox(_T("没有发现在线ZED相机!"));
	}
	else
	{
		if (!IsVisiableSlider())
		{
			ShowSliderArea();
			m_wndTitle.SendMessage(MSG_UPDATEUI, NULL, NULL);
		}
	}

	// set the counter
	m_vStoreSingleIndex.resize(1, 0);
	m_vStoreMultipleIndex.resize(1, 0);
	m_vStoreFrameIndex.resize(1, 0);
	m_vStoreFrameTime.resize(1, 0);
}

void CDlgMainFrame::OpenZEDDevices(void)
{
	// 1.check
	if (!m_zedCamera.bIsOpen())
	{
		AfxMessageBox("请先扫描在线设备");
		return;
	}

	// 2.run zed device and afford callback function
	m_vectFramesCounts.clear();
	if (m_vectCapturer.size())
	{
		for (int i = 0; i < m_vectCapturer.size(); ++i)
		{
			m_vectCapturer[i]->m_nIndex = i;
		//	m_vectCapturer[i]->SetCallback(ZEDCaptureCallbackFunc, this);
			m_vectCapturer[i]->Start();
			m_vectFramesCounts.push_back(0);
		}
	}
	for (int i = 0; i < m_vStoreMultipleIndex.size(); ++i)
	{
		m_vStoreMultipleIndex[i] = 0;
		m_vStoreFrameIndex[i] = 0;
		m_vStoreFrameTime[i] = 0;
	}
	for (int i = 0; i < m_vStoreSingleIndex.size(); ++i)
	{
		m_vStoreSingleIndex[i] = 0;
	}
	m_bOpenDevices = TRUE;

/*	tagUserInfo userInfo;
	userInfo.m_strUserName = m_strUserName;
	userInfo.m_strPassword = m_strPassword;
	userInfo.m_strCameraIP = m_strCameraIP;
	userInfo.m_nPort = m_nCameraPort;
	connectHK(userInfo, m_wndPreview.GetImageHK().GetSafeHwnd());*/

	m_zedCamera.setCallback(m_pZedCallback);
	m_zedCamera.startTracking();

	

	m_wndTitle.SendMessage(MSG_UPDATEUI, NULL, NULL);
}

void CDlgMainFrame::ShowOptionDlg(void)
{
	CBaseOptionDlg       optionDlg;
	CPageDevices         pageDevices;
	CPageDataManager     pageDataManager;
	CPageKinectParameter pageKinectParameter;
	const CString strOne("ZED设备");
	optionDlg.AddPage(NULL, strOne, 0);
	optionDlg.AddPage(&pageDevices,         "ZED设备", CPageDevices::IDD,         strOne);
	optionDlg.AddPage(&pageDataManager,     "ZED数据", CPageDataManager::IDD,     strOne);
	optionDlg.AddPage(&pageKinectParameter, "ZED参数", CPageKinectParameter::IDD, strOne);
	
	///加载参数
	LoadParameterFromFile();
	if (IDOK == optionDlg.DoModal())
	{
		///存储参数
		SaveParameterToFile();
	}
}

void CDlgMainFrame::OnDestroy()
{
	CDialogEx::OnDestroy();

	if (IsOpenDevices())
	{
		/////这里使用for循环打开所有设备,并未每个设备设置回调函数
		if (m_vectCapturer.size())
		{
			for (int i = 0;i < m_vectCapturer.size();++i)
			{
				m_vectCapturer[i]->Stop();
				m_vectCapturer[i]->m_nIndex = i;
				m_vectCapturer[i]->SetCallback(NULL, NULL);
			}
		}
		m_bOpenDevices = FALSE;
	}
	m_zedCamera.setCallback(nullptr);
	m_zedCamera.clear();

	ClearCapturers();

	m_storeManager.Stop();

	if (::IsWindow(m_wndPreview))
	{
		m_wndPreview.SendMessage(WM_CLOSE);
	}

	SaveParameterToFile();
}

void CDlgMainFrame::ShowSliderArea(void)
{
	KillTimer(ID_TIMER_SLIDER);
	m_bShowSlider = !::IsWindowVisible(m_wndSlider);
	if (m_bShowSlider)
	{
		m_wndSlider.ShowWindow(SW_SHOW);
	}
	SetTimer(ID_TIMER_SLIDER, 10, NULL);
}


BOOL CDlgMainFrame::IsVisiableSlider(void)
{
	return m_bShowSlider;
}



void  CDlgMainFrame::LoadParameterFromFile(void)
{
	char szPath[MAX_PATH] = { 0 };
	sprintf(szPath, "%s\\kinect_config.ini", YDuskyFun::GetCurExeDir().c_str());
	
	///devices parameter
	char szBuffer[MAX_PATH] = { 0 };
	GetPrivateProfileString(_T("Devices"), _T("PipelineType"), _T("3"), szBuffer, MAX_PATH, szPath);
	m_ePipelineType = (PacketPipelineType)atoi(szBuffer);

	///2D/3D data
	m_eImageType = (ImageType)GetPrivateProfileInt(_T("2D/3D Data"), _T("ImageType"), 0, szPath);

	m_ePointCloudType = (pointcloud_type)GetPrivateProfileInt(_T("2D/3D Data"), _T("PointCloudType"), 1, szPath);

	m_ePointCloudUnit = (pointcloud_unit)GetPrivateProfileInt(_T("2D/3D Data"), _T("PointCloudUnit"), 0, szPath);

	m_bEnableFilter   = GetPrivateProfileInt(_T("2D/3D Data"), _T("EnableFilter"), 1, szPath);

	m_nFilterWidth    = GetPrivateProfileInt(_T("2D/3D Data"), _T("FilterWidth"), 1, szPath);

	m_nFilterHeight   = GetPrivateProfileInt(_T("2D/3D Data"), _T("FilterHeight"), 1, szPath);

	GetPrivateProfileString(_T("Data"), _T("FilterTolerance"), "0.01", szBuffer, MAX_PATH, szPath);
	m_fFilterTolerance = atof(szBuffer);

	///data  path
	GetPrivateProfileString(_T("Data"), _T("StorePath"), "D:\\Kinect_capture", szBuffer, MAX_PATH, szPath);
	m_strStorePath = szBuffer;

	///data option
	m_enable_depth = GetPrivateProfileInt(_T("Data Option"), _T("EnableDepth"), 1, szPath);
	m_enable_depth_undist = GetPrivateProfileInt(_T("Data Option"), _T("EnableDepthUndist"), 1, szPath);
	m_enable_rgb = GetPrivateProfileInt(_T("Data Option"), _T("EnableRgb"), 1, szPath);
	m_enable_ir = GetPrivateProfileInt(_T("Data Option"), _T("EnableIr"), 1, szPath);
	m_enable_rgbd = GetPrivateProfileInt(_T("Data Option"), _T("EnableRgbd"), 1, szPath);
	m_enable_drgb = GetPrivateProfileInt(_T("Data Option"), _T("EnableDrgb"), 1, szPath);
	m_nSingleCapCnt = GetPrivateProfileInt(_T("Data Option"), _T("SingleCapCnt"), 1, szPath);
	m_nStoreFrameCounts = GetPrivateProfileInt(_T("Data Option"), _T("MultipleCapCnt"), 5, szPath);

	m_nStoreInterval    = GetPrivateProfileInt(_T("Data Option"), _T("StoreInterval"), 1, szPath);
	///overlap info add by yyk@20160322
	m_bShowOverlapInfo = GetPrivateProfileInt(_T("Overlap info"), _T("ShowOverlap"), 1, szPath);

	///flip
	m_nHorzFlip = GetPrivateProfileInt(_T("Data Option"), _T("HorzFlip"), 1, szPath);

	m_nShowInV1 = GetPrivateProfileInt(_T("Data Option"), _T("ShowInV1"), 0, szPath);
	m_nShowInV2 = GetPrivateProfileInt(_T("Data Option"), _T("ShowInV2"), 1, szPath);

	char szTemp[256] = { 0 };
	GetPrivateProfileString("HKCamera", "UserName", "admin", szTemp, 256, szPath);
	m_strUserName = szTemp;
	GetPrivateProfileString("HKCamera", "Password", "qiaokang2017", szTemp, 256, szPath);
	m_strPassword = szTemp;
	GetPrivateProfileString("HKCamera", "CameraIP", "192.168.1.33", szTemp, 256, szPath);
	m_strCameraIP = szTemp;
	m_nCameraPort = GetPrivateProfileInt("HKCamera", "CameraPort", 8000, szPath);
}

void  CDlgMainFrame::SaveParameterToFile(void)
{
	char szPath[MAX_PATH] = { 0 };
	sprintf(szPath, "%s\\kinect_config.ini", YDuskyFun::GetCurExeDir().c_str());

	///devices parameter
	char szBuffer[MAX_PATH] = { 0 };
	sprintf(szBuffer, "%d", m_ePipelineType);
	WritePrivateProfileString(_T("Devices"), _T("PipelineType"), szBuffer, szPath);

	WritePrivateProfileString(_T("Data"), _T("StorePath"), m_strStorePath, szPath);

	///2D/3D data
	sprintf(szBuffer, "%d", m_eImageType);
	WritePrivateProfileString(_T("2D/3D Data"), _T("ImageType"), szBuffer, szPath);

	sprintf(szBuffer, "%d", m_ePointCloudType);
	WritePrivateProfileString(_T("2D/3D Data"), _T("PointCloudType"), szBuffer, szPath);

	sprintf(szBuffer, "%d", m_ePointCloudUnit);
	WritePrivateProfileString(_T("2D/3D Data"), _T("PointCloudUnit"), szBuffer, szPath);

	sprintf(szBuffer, "%d", m_bEnableFilter);
	WritePrivateProfileString(_T("2D/3D Data"), _T("EnableFilter"), szBuffer, szPath);

	sprintf(szBuffer, "%d", m_nFilterWidth);
	WritePrivateProfileString(_T("2D/3D Data"), _T("FilterWidth"), szBuffer, szPath);

	sprintf(szBuffer, "%d", m_nFilterHeight);
	WritePrivateProfileString(_T("2D/3D Data"), _T("FilterHeight"), szBuffer, szPath);

	sprintf(szBuffer, "%0.2f", m_fFilterTolerance);
	WritePrivateProfileString(_T("2D/3D Data"), _T("FilterTolerance"), szBuffer, szPath);

	///data option
	sprintf(szBuffer, "%d", m_enable_depth);
	WritePrivateProfileString(_T("Data Option"), _T("EnableDepth"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_enable_depth_undist);
	WritePrivateProfileString(_T("Data Option"), _T("EnableDepthUndist"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_enable_rgb);
	WritePrivateProfileString(_T("Data Option"), _T("EnableRgb"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_enable_ir);
	WritePrivateProfileString(_T("Data Option"), _T("EnableIr"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_enable_rgbd);
	WritePrivateProfileString(_T("Data Option"), _T("EnableRgbd"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_enable_drgb);
	WritePrivateProfileString(_T("Data Option"), _T("EnableDrgb"), szBuffer, szPath);
	///single cap cnt
	sprintf(szBuffer, "%d", m_nSingleCapCnt);
	WritePrivateProfileString(_T("Data Option"), _T("SingleCapCnt"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_nStoreFrameCounts);
	WritePrivateProfileString(_T("Data Option"), _T("MultipleCapCnt"), szBuffer, szPath);
	
	sprintf(szBuffer, "%d", m_nStoreInterval);
	WritePrivateProfileString(_T("Data Option"), _T("StoreInterval"), szBuffer, szPath);

	///overlap info add by yyk@20160322
	sprintf(szBuffer, "%d", m_bShowOverlapInfo);
	WritePrivateProfileString(_T("Overlap info"), _T("ShowOverlap"), szBuffer, szPath);

	///flip
	sprintf(szBuffer, "%d", m_nHorzFlip);
	WritePrivateProfileString(_T("Data Option"), _T("HorzFlip"), szBuffer, szPath);

	sprintf(szBuffer, "%d", m_nShowInV1);
	WritePrivateProfileString(_T("Data Option"), _T("ShowInV1"), szBuffer, szPath);
	sprintf(szBuffer, "%d", m_nShowInV2);
	WritePrivateProfileString(_T("Data Option"), _T("ShowInV2"), szBuffer, szPath);
}

BOOL CDlgMainFrame::IsZoom()
{
	return m_bZoom;
}

void CDlgMainFrame::ZoomWindow(void)
{
	int nMaxW = GetSystemMetrics(SM_CXFULLSCREEN);
	int nMaxH = (GetSystemMetrics(SM_CYFULLSCREEN) + GetSystemMetrics(SM_CYCAPTION));
	CRect rectClient;
	GetClientRect(rectClient);
	if (m_bZoom)
	{
		nMaxW *= 0.8;
		nMaxH *= 0.8;
	}
	MoveWindow(0, 0, nMaxW, nMaxH);
	CenterWindow();
	m_bZoom = !m_bZoom;

	m_wndTitle.SendMessage(MSG_UPDATEUI, 1, NULL);
}


///overlap info add by yyk@20160322
void CDlgMainFrame::ShowOverlapInfo(BOOL bShow)
{
	m_bShowOverlapInfo = bShow;
	if (!bShow)
	{
		for (size_t i = 0; i < m_vectCapturer.size(); i++)
		{
			m_wndPreview.SetOverlapInfo(i, "");
		}
	}
}

void    CDlgMainFrame::HandleCmd(int nIndex, int nCmd)
{
	switch (nCmd)
	{
	case eOC_CAMERA_OPEN:
		{
			ScanZEDDevices();
			OpenZEDDevices();
		}
		break;
	case eOC_CAMERA_CLOSE:
		OpenZEDDevices();
		break;
	case eOC_START_STORE:
	case eOC_STOP_STORE:
		CaptureMultiFrames();
		break;
	default:
		break;
	}
	int nCmdRet = -1;
	if (IsOpenDevices())
	{
		nCmdRet = eOC_CAMERA_OPEN;
	}
	else
	{
		nCmdRet = eOC_CAMERA_CLOSE;
	}
	m_netManager.SendCommand(-1, nCmdRet);
	if (IsMultiFramesCpture())
	{
		nCmdRet = eOC_START_STORE;
	}
	else
	{
		nCmdRet = eOC_STOP_STORE;
	}
	m_netManager.SendCommand(-1, nCmdRet);
}

LRESULT CDlgMainFrame::OnRecvNetCommand(WPARAM wParam, LPARAM lParam)
{
	tagDataHeader* pHeader = (tagDataHeader*)(lParam);
	if (pHeader)
	{
		switch (pHeader->m_nType)
		{
		case eDT_ONLINE:
			break;
		case eDT_OFFLINE:
			break;
		case eDT_CTRL_CMD:
			HandleCmd(wParam, pHeader->m_nCmd);
			break;

		}
	}
	return 0;
}

BOOL CDlgMainFrame::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	Sleep(100);
	m_netManager.SendOnline(-1);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void CDlgMainFrame::OnClose()
{
	m_netManager.SendOffline(-1);

	CDialogEx::OnClose();
}

cv::Mat CDlgMainFrame::getImageFromData(void)
{
	if (m_pFrameData)
	{

		m_lockFrame.AddRead();
		if (nullptr == m_pTempData)
		{
			m_pTempData = new tagFrameData(*m_pFrameData);
		}
		else
		{
			m_pTempData->copyFrom(*m_pFrameData);
		}
		m_lockFrame.DecRead();

		m_lockStore.EnableWrite();
		YDuskyFun::yv12ToBGR24_openCV((unsigned char*)m_pTempData->_pBuffer, m_image.m_image, m_pTempData->_nWidth, m_pTempData->_nHeight);
		m_lockStore.DisableWrite();
	}


	return m_image.m_image;
}

bool CDlgMainFrame::getImageFromData(tagFrameData* data)
{
	bool bRet = false;
	if (m_pFrameData)
	{
		m_lockFrame.AddRead();
		if (nullptr == m_pFrameData)
		{
			data = new tagFrameData(*m_pFrameData);
		}
		else
		{
			data->copyFrom(*m_pFrameData);
		}
		m_lockFrame.DecRead();

		bRet = true;
	}
	return bRet;
}

void CDlgMainFrame::callback(const tagFrameData& imageInfo)
{
	//YDTimer timer;
	//m_lockFrame.EnableWrite();
	if (nullptr == m_pFrameData)
	{
		m_pFrameData = new tagFrameData(imageInfo);
	}
	else
	{
		m_pFrameData->copyFrom(imageInfo);
	}
	//m_lockFrame.DisableWrite();
	//QString strInfo = QString::asprintf("=============  BriCaptureVR::callback use :%d ms", timer.elapsed());
	//qDebug() << strInfo;
}

YDCameraHK* CDlgMainFrame::getCameraHK(void)
{
	return m_pCameraHK;
}

bool CDlgMainFrame::connectHK(const tagUserInfo& info, HWND hWnd)
{
	if (m_pCameraHK)
	{
		m_pCameraHK->unInitCameraDevice();
		delete m_pCameraHK;
		m_pCameraHK = nullptr;
	}

	m_pCameraHK = new YDCameraHK();

	//connect(m_pCameraHK, SIGNAL(imageComming()), this, SLOT(imageComming()));
	m_pCameraHK->setCallback(m_pCallbackHK);
	m_pCameraHK->setUserInfo(info);
	m_bConnected = m_pCameraHK->initCameraDevice(hWnd);

	return m_bConnected;

}

void CDlgMainFrame::dataCaptureHandle(void)
{
	//while (isStore())
	//{
	//	
	//	if (m_pCameraHK)
	//	{
	//		m_pCameraHK->grapOneFrame(m_image.m_image);
	//	}

	//	//m_lockStore.AddRead();
	//	//m_storeMger.addImageToBuffer(m_image);
	//	//m_lockStore.DecRead();
	//	//m_storeMger.addBtnCtrlDataToBuffer(m_bottomControl);

	//	m_nCaptureCount++;
	//}
}


bool CDlgMainFrame::isConnectedHK()
{
	return m_bConnected;
}

void CDlgMainFrame::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	
	CDialogEx::OnKeyDown(nChar, nRepCnt, nFlags);
}

BOOL CDlgMainFrame::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		CString strText;
		switch (pMsg->wParam)
		{
		case 'a':
		case 'A':
		{
			bool bAuto = m_zedCamera.GetEnableAutoWhiteBalance();
			m_zedCamera.SetEnableAutoWhiteBalance(!bAuto);
			strText = m_strCameraSettingText = bAuto ? "关闭自动白平衡" : "打开自动白平衡";
		}
			break;
		case 'B':
		case 'b':
			m_nStepCameraSetting = 1;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_BRIGHTNESS;
			strText = m_strCameraSettingText = "调整亮度, 步进1";
			break;
		case 'C':
		case 'c':
			m_nStepCameraSetting = 1;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_CONTRAST;
			strText = m_strCameraSettingText = "调整对比度, 步进1";
			break;
		case 'e':
		case 'E':
			m_nStepCameraSetting = 1;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_EXPOSURE;
			strText = m_strCameraSettingText = "调整曝光时间, 步进1";
			break;
		case 'H':
		case 'h':
			m_nStepCameraSetting = 1;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_HUE;
			strText = m_strCameraSettingText = "调整色调, 步进1";
			break;
		case 'S':
		case 's':
			m_nStepCameraSetting = 5;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_SATURATION;
			strText = m_strCameraSettingText = "调整饱和度, 步进1";
			break;
		case 'G':
		case 'g':
			m_nStepCameraSetting = 5;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_GAIN;
			strText = m_strCameraSettingText = "调整增益, 步进1";
			break;
		case 'W':
		case 'w':
			m_nStepCameraSetting = 100;
			m_zedCamera.m_camerasettings = CAMERA_SETTINGS_WHITEBALANCE;
			strText = m_strCameraSettingText = "调整白平衡, 步进100";
			break;
		case '+':
		case VK_UP:
		case VK_RIGHT:
		{
			int current_value = m_zedCamera.getCurrentSettingValue();
			m_zedCamera.setCurrentSettingValue(m_nStepCameraSetting + current_value);
			char szTemp[256] = { 0 };
			sprintf(szTemp, "(%d)", m_zedCamera.getCurrentSettingValue());
			strText = m_strCameraSettingText + szTemp;
		}
			break;
		case '-':
		case VK_DOWN:
		case VK_LEFT:
		{
			int current_value = m_zedCamera.getCurrentSettingValue();
			m_zedCamera.setCurrentSettingValue(current_value - m_nStepCameraSetting);
			char szTemp[256] = { 0 };
			sprintf(szTemp, "(%d)", m_zedCamera.getCurrentSettingValue());
			strText = m_strCameraSettingText + szTemp;
		}
			break;
		case 'm':
		case 'M':
		{
			m_zedCamera.switchSeningMode();
			char szTemp[256] = { 0 };
			sprintf(szTemp, "SeningMode[%S]", m_zedCamera.getSensingMode().c_str());
			strText = szTemp;
		}
			break;
		case 'f':
		case 'F':
		{
			m_zedCamera.switchMeasure3DReferenceFrame();
			char szTemp[256] = { 0 };
			sprintf(szTemp, "Measure3DReferenceFrame[%S]", m_zedCamera.getMeasure3DReferenceFrame().c_str());
			strText = szTemp;
		}
			break;
		case 'd':
		case 'D':
		{
			m_zedCamera.switchDepthImage();
			bool bEnable = m_zedCamera.isEnableDepthImage();
			strText = bEnable ? "打开深度图像" : "关闭深度图像";
		}
			break;
		case 'p':
		case 'P':
		{
			m_zedCamera.switchPointcloud();
			bool bEnable = m_zedCamera.isEnablePc();
			strText = bEnable ? "打开点云" : "关闭点云";
		}
			break; 
		default:
			break;
		}

		if (!strText.IsEmpty())
		{
			setCaption(strText);
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void CDlgMainFrame::setCaption(const CString& strTxt)
{
	CString strPrefix("ZedCapture");
	m_wndTitle.setTitle(strPrefix + "[" + strTxt + "]");
}
