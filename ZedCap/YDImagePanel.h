#ifndef YDIMAGEPANEL_H
#define YDIMAGEPANEL_H
//#include "YDUIBase.h"
#include <QWidget>
//#include "YDBaseZoom.h"
class YDImagePanel
	: public QWidget
//	, public YDBaseZoom
{
	Q_OBJECT

public:
	YDImagePanel(QWidget *parent);
	~YDImagePanel();

protected:
	bool             m_bShowInfo;
	bool             m_bShowHistogram;
	QPixmap          m_image;
	QPixmap          m_default;
	QString          m_strMousePos;
	QString          m_strMouseRGB;
	int              m_nMaxR;
	int              m_nMaxG;
	int              m_nMaxB;
	std::vector<int> m_histCountR;
	std::vector<int> m_histCountG;
	std::vector<int> m_histCountB;
	int              m_nImageW;
	int              m_nImageH;
	int              m_nStep;
	unsigned char*   m_pImageData;
public:
	void         setDefault(const QPixmap& image);
	void         setImage(const QPixmap& image);
	void         setImage(int nW, int nH, int nStep, uchar* imageData);
	void         showInfo(bool bShow);
	bool         isShowInfo(void);
	void         showHistogram(bool bShow);
	bool         isShowHistogram(void);
protected:
	void         drawInfo(QPainter& painter);
	void         drawHistogram(QPainter& painter, const QRect& rcPosi);
protected:
	void         calcHistogram(void);
	void         updateImageData(int nW, int nH, int nC, uchar* imageData);
signals :
	void         paint(QPainter* painter);

protected:
	virtual void UpdateDraw();
	virtual void resizeEvent(QResizeEvent *event);
	virtual void paintEvent(QPaintEvent *event);
	virtual void wheelEvent(QWheelEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);

};

#endif // YDIMAGEPANEL_H
