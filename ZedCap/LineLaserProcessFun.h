#pragma once

#include <string.h>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <math.h>
#include <opencv2/opencv.hpp>

#define LINELASERSIZE 1080

class CULineBasedPoseEst;

struct KeyPointOfPointcloudSection
{
	std::vector<cv::Point3d> vKeyPoints;
	bool bValid;
	float m_outbox[6];
	float distanceToTop;
	float distanceToBottom;
	float length;
	float angle;
	float translate[3];
	KeyPointOfPointcloudSection()
		: bValid(true)
		, length(0)
		, angle(0)
		, distanceToTop(0)
		, distanceToBottom(0)
	{
		vKeyPoints.clear();
		for (int i = 0; i < 6; ++i)
		{
			m_outbox[i] = 0.0;
		}
		for (int i = 0; i < 3; ++i)
		{
			translate[i] = 0.0;
		}
	}

	KeyPointOfPointcloudSection(const KeyPointOfPointcloudSection& other)
		: bValid(other.bValid)
		, angle(other.angle)
		, length(other.length)
		, distanceToTop(other.distanceToTop)
		, distanceToBottom(other.distanceToBottom)
		, vKeyPoints(other.vKeyPoints)
	{

		memcpy(m_outbox, other.m_outbox, sizeof(float) * 6);
		memcpy(translate, other.translate, sizeof(float) * 3);

	}

	KeyPointOfPointcloudSection& operator=(const KeyPointOfPointcloudSection& other)
	{
		bValid = other.bValid;
		angle = other.angle;
		length = other.length;
		distanceToTop = other.distanceToTop;
		distanceToBottom = other.distanceToBottom;
		vKeyPoints = other.vKeyPoints;
		memcpy(m_outbox, other.m_outbox, sizeof(float) * 6);
		memcpy(translate, other.translate, sizeof(float) * 3);

		return *this;
	}

	void SetOutbox(float* outbox)
	{
		for (int i = 0; i < 6; ++i)
			m_outbox[i] = outbox[i];
	}

	double CalcArea()
	{
		int nSize = vKeyPoints.size();
		if (nSize == 4)	//convex 4-lines polygon
		{
			double a = sqrt((vKeyPoints.at(0).x - vKeyPoints.at(1).x)*(vKeyPoints.at(0).x - vKeyPoints.at(1).x) +
				(vKeyPoints.at(0).y - vKeyPoints.at(1).y)*(vKeyPoints.at(0).y - vKeyPoints.at(1).y));
			
			double b = sqrt((vKeyPoints.at(1).x - vKeyPoints.at(2).x)*(vKeyPoints.at(1).x - vKeyPoints.at(2).x) +
				(vKeyPoints.at(1).y - vKeyPoints.at(2).y)*(vKeyPoints.at(1).y - vKeyPoints.at(2).y));
			
			double c = sqrt((vKeyPoints.at(2).x - vKeyPoints.at(3).x)*(vKeyPoints.at(2).x - vKeyPoints.at(3).x) +
				(vKeyPoints.at(2).y - vKeyPoints.at(3).y)*(vKeyPoints.at(2).y - vKeyPoints.at(3).y));
			
			double d = sqrt((vKeyPoints.at(3).x - vKeyPoints.at(0).x)*(vKeyPoints.at(3).x - vKeyPoints.at(0).x) +
				(vKeyPoints.at(3).y - vKeyPoints.at(0).y)*(vKeyPoints.at(3).y - vKeyPoints.at(0).y));

			double p = (a + b + c + d) / 2.0;

			return sqrt((p - a)*(p - b)*(p - c)*(p - d));
		}

		return 0.0;
	}

	bool CheckCross()
	{
		int nSize = vKeyPoints.size();
		if (nSize == 4)	//convex 4-lines polygon
		{
			// calculate cross point
			double x1 = vKeyPoints.at(0).x;
			double y1 = vKeyPoints.at(0).y;
			double x2 = vKeyPoints.at(1).x;
			double y2 = vKeyPoints.at(1).y;
			double A1 = (y1 - y2) / (x1*y2 - x2*y1);
			double B1 = (x1 - x2) / (y1*x2 - x1*y2);
			double C1 = 1.0;
			x1 = vKeyPoints.at(2).x;
			y1 = vKeyPoints.at(2).y;
			x2 = vKeyPoints.at(3).x;
			y2 = vKeyPoints.at(3).y;
			double A2 = (y1 - y2) / (x1*y2 - x2*y1);
			double B2 = (x1 - x2) / (y1*x2 - x1*y2);
			double C2 = 1.0;

			double y = (A1*C2 - C1*A2) / (B1*A2 - A1*B2);
			double x = (B1*C2 - C1*B2) / (A1*B2 - B1*A2);

			cv::Vec2d crossPoint0, crossPoint1;
			crossPoint0(0) = x - vKeyPoints.at(0).x;
			crossPoint0(1) = y - vKeyPoints.at(0).y;
			crossPoint1(0) = x - vKeyPoints.at(1).x;
			crossPoint1(1) = y - vKeyPoints.at(1).y;

			double zeroChk = crossPoint0.dot(crossPoint1);
			if (zeroChk <= 0)
				return true;
			return false;
		}
		return false;
	}

	bool IsValid()
	{
		if (vKeyPoints.size() == 0)
			return false;
		for (int i = 0; i < vKeyPoints.size(); ++i)
		{
            if (__isnan(vKeyPoints.at(i).x) || __isnan(vKeyPoints.at(i).y))
				return false;
		}
		return true;
	}
};

namespace LINE_LASER_PROCESS_FUN
{
    struct StructModel
	{
		std::vector<cv::Point2d> vKeyPoints;
		StructModel()
		{
			vKeyPoints.clear();
		}

		StructModel& operator=(const StructModel& other)
		{
			vKeyPoints = other.vKeyPoints;
			return *this;
		}

		void init()
		{
			vKeyPoints.clear();
		}

		bool	isValid()
		{
			return vKeyPoints.size() > 0;
		}

		double CalcArea()
		{
			int nSize = vKeyPoints.size();
			if (nSize == 4)	//convex 4-lines polygon
			{
				double a = sqrt((vKeyPoints.at(0).x - vKeyPoints.at(1).x)*(vKeyPoints.at(0).x - vKeyPoints.at(1).x) +
					(vKeyPoints.at(0).y - vKeyPoints.at(1).y)*(vKeyPoints.at(0).y - vKeyPoints.at(1).y));
				double b = sqrt((vKeyPoints.at(1).x - vKeyPoints.at(2).x)*(vKeyPoints.at(1).x - vKeyPoints.at(2).x) +
					(vKeyPoints.at(1).y - vKeyPoints.at(2).y)*(vKeyPoints.at(1).y - vKeyPoints.at(2).y));
				double c = sqrt((vKeyPoints.at(2).x - vKeyPoints.at(3).x)*(vKeyPoints.at(2).x - vKeyPoints.at(3).x) +
					(vKeyPoints.at(2).y - vKeyPoints.at(3).y)*(vKeyPoints.at(2).y - vKeyPoints.at(3).y));
				double d = sqrt((vKeyPoints.at(3).x - vKeyPoints.at(0).x)*(vKeyPoints.at(3).x - vKeyPoints.at(0).x) +
					(vKeyPoints.at(3).y - vKeyPoints.at(0).y)*(vKeyPoints.at(3).y - vKeyPoints.at(0).y));

				double p = (a + b + c + d) / 2.0;

				double area = sqrt((p - a)*(p - b)*(p - c)*(p - d));
				return area;
			}

			return 0.0;
		}
	};

    class CLineLaserProcessFun
	{
	public:
		CLineLaserProcessFun(const std::string& strModelDir);
		~CLineLaserProcessFun();

	public:
		float									m_fLLPOutbox[6];
        float									m_fLLPMinLen;
        float									m_fLLPMaxLen;
        int										m_nPixelDistance;
	private:

		float									m_fLLPStartAngle;
		float									m_fLLPDeltaAngle;
		float									m_fLLPDeltaZLen;


		int										m_nLLPEnableAreaCheck;
		int										m_nLLPAreaCheckThre;
		int										m_nLLPEnableConvexCheck;
		std::string                             m_strModelDir;
		LINE_LASER_PROCESS_FUN::StructModel		m_standardModel;
		CULineBasedPoseEst*						m_pbpe;
	public:

		bool        LoadBridgeStructModel(int nType);

		// FUNCTION
		void		ProcessShape1ByBruteForceMethod(const std::vector<cv::Point3d>& vInputSectionPointcloud, KeyPointOfPointcloudSection& result, std::string strSavePath = "", int nZIdx = 0);

		void		ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nZIdx = 0);
		void		ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nZIdx = 0);

		void		XXL_ProcessLineLaserFirst(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nZIdx = 0);
		void		XXL_ProcessLineLaserFirst(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nZIdx = 0);
		void		XXL_ProcessLineLaserNext(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);
		void		XXL_ProcessLineLaserNext(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);

		void		TXL_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);
		void		TXL_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);

		void		KXB_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nZIdx = 0);
		void		KXB_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nZIdx = 0);

		void		KXBSide_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);
		void		KXBSide_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);

		void		XXLSide_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);
		void		XXLSide_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);

		void		TXLSide_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);
		void		TXLSide_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx = 0, int nZIdx = 0);

		// DATA
		void		SetDecodePointcloudPara(const float& fLLPMinLen, const float& fLLPMaxLen, const float& fLLPStartAngle, const float& fLLPDeltaAngle,
			const float* fLLPOutbox, const float& fDeltaZLen)
		{
			m_fLLPMinLen = fLLPMinLen;
			m_fLLPMaxLen = fLLPMaxLen;
			m_fLLPStartAngle = fLLPStartAngle;
			m_fLLPDeltaAngle = fLLPDeltaAngle;
			for (int i = 0; i < 6; ++i)
				m_fLLPOutbox[i] = fLLPOutbox[i];
			m_fLLPDeltaZLen = fDeltaZLen;
		}
		void		SetFeaturePara(const int& nLLPEnableAreaCheck, const int& nLLPAreaCheckThre, const int& nLLPEnableConvexCheck)
		{
			m_nLLPEnableAreaCheck = nLLPEnableAreaCheck;
			m_nLLPAreaCheckThre = nLLPAreaCheckThre;
			m_nLLPEnableConvexCheck = nLLPEnableConvexCheck;
		}
		void		DecodeLaserDataToPointcloud(const std::string& strInputLaserPath, std::vector<cv::Point3d>& vPointcloud);
		void		DecodeLaserDataToPointcloud(unsigned int *pnData, std::vector<cv::Point3d>& vInputSectionPointcloud, int nZIdx = 0);

		void		FileNameTranslation(const std::string& strOrgFileName, const std::string& strExt, const std::string& strNewDir, std::string& strTransFileName);

		void		LoadStandardModel(const std::string& strPath);

		void		GetAffineTransform(KeyPointOfPointcloudSection& src, KeyPointOfPointcloudSection& dst, cv::Mat& mapMatrix);
		void		AffineTransform(KeyPointOfPointcloudSection& src, cv::Mat& mapMatrix, KeyPointOfPointcloudSection& dst);
		void		GetBaseKeyPoint(KeyPointOfPointcloudSection& base);
		void		SaveAffineTransform(const std::string& strPath, cv::Mat& mapMatrix);
		void		LoadAffineTransform(const std::string& strPath, cv::Mat& mapMatrix);

		template<typename T>
		void		SetOutbox(T* outbox);

		template<typename T>
		void		CalPrjPts2Line(const T& db1x, const T& db1y, const T& dba, const T& dbb, const T& dbc, T& db2x, T& db2y);

		template<typename T>
		void		CalLineCrossPoint(const T& A1, const T& B1, const T& C1, const T& A2, const T& B2, const T& C2, T& x, T& y);

		template<typename T>
		void		cvLineToABC(cv::Vec4f line, T& A, T& B, T& C);

		template<typename T>
		T GetAvgY(std::vector<cv::Point>& vPoints);

	private:
		void		LoadOriginalLaserData(const std::string& strFile, std::vector<unsigned int>& vBuffer);
		void		SavePointcloudData(const std::string& strFile, std::vector<cv::Point3d>& vPointcloud);
		void		ChangeLaserDataToPointcloud(const std::vector<int>& vBuffer, std::vector<cv::Point3d>& vPointcloud, int nZIdx=0);
		void		CalcPlaneOutbox(std::vector<cv::Point3d>& vInputSectionPointcloud, cv::Rect& rectOutbox);

		void		LoadProfile();
		void		SaveProfile();
	};
}


//////////////////////////////////////////////////////////////////////////
// implementation of template function
namespace LINE_LASER_PROCESS_FUN
{
	template<typename T>
	void inline CLineLaserProcessFun::SetOutbox(T* outbox)
	{
		for (int i = 0;i < 6;++i)
			m_fLLPOutbox[i] = outbox[i];
	}

	template<typename T>
	void	inline CLineLaserProcessFun::CalPrjPts2Line(const T& db1x, const T& db1y, const T& dba, const T& dbb, const T& dbc, T& db2x, T& db2y)
	{
		// check
		if (dba == 0.0 && dbb == 0.0)
			return;

		if (dba == 0.0)
		{
			db2x = db1x;
			db2y = -dbc / dbb;
		}
		else if (dbb == 0.0)
		{
			db2y = db1y;
			db2x = -dbc / dba;
		}
		else
		{
			db2x = (dbb*dbb*db1x - dba*dbb*db1y - dba*dbc) / (dba*dba + dbb*dbb);
			db2y = dbb*(db2x - db1x) / dba + db1y;
		}
	}

	template<typename T>
	void inline 	CLineLaserProcessFun::cvLineToABC(cv::Vec4f line, T& A, T& B, T& C)
	{
		A = line[1];
		B = -line[0];
		C = line[3]*line[0]-line[1]*line[2];
	}

	template<typename T>
	T CLineLaserProcessFun::GetAvgY(std::vector<cv::Point>& vPoints)
	{
		T sum = 0.0;
		auto iter = vPoints.begin();
		for (;iter != vPoints.end();++iter)
		{
			sum += iter->y;
		}
		if(vPoints.size()>0)
			sum /= vPoints.size();

		return sum;
	}

	template<typename T>
	void	inline CLineLaserProcessFun::CalLineCrossPoint(const T& A1, const T& B1, const T& C1, const T& A2, const T& B2, const T& C2, T& x, T& y)
	{
		y = (A1*C2 - C1*A2) / (B1*A2 - A1*B2);
		x = (B1*C2 - C1*B2) / (A1*B2 - B1*A2);
	}
}
