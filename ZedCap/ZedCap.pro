#-------------------------------------------------
#
# Project created by QtCreator 2017-08-10T02:14:16
#
#-------------------------------------------------

QT       += core gui network            # network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZedCap
TEMPLATE = app

INCLUDEPATH += /usr/include/opencv2 \             #
            /usr/include/opencv \                 #
            /usr/local/zed/include \              #
            /usr/local/linux \
            /usr/local/cuda-8.0/include

#cereal
INCLUDEPATH += /home/nvidia/Downloads/cereal-master/include

# OpenCV
LIBS += /usr/lib/libopencv_core.so.3.2.0            # must!
LIBS += /usr/lib/libopencv_highgui.so.3.2.0         #
LIBS += /usr/lib/libopencv_calib3d.so.3.2.0         #
LIBS += /usr/lib/libopencv_features2d.so.3.2.0      #
LIBS += /usr/lib/libopencv_cudabgsegm.so.3.2.0      #
LIBS += /usr/lib/libopencv_cudacodec.so.3.2.0
LIBS += /usr/lib/libopencv_cudev.so.3.2.0
LIBS += /usr/lib/libopencv_photo.so.3.2.0 \
        /usr/lib/libopencv_imgproc.so.3.2.0 \       #
        /usr/lib/libopencv_imgcodecs.so.3.2.0 \
        /usr/lib/aarch64-linux-gnu/libdrm.so.2.4.0  #

# ZED
LIBS += /usr/local/zed/lib/libsl_zed.so \
        /usr/local/zed/lib/libsl_core.so \
        /usr/local/zed/lib/libtbb.so


QMAKE_CXXFLAGS += -std=c++0x        #use C++11

SOURCES += main.cpp\
        mainwindow.cpp \
        imagewidget.cpp \
        zedcamera.cpp \
        publicInfo.cpp \
        YDCommonFunc.cpp \
        YDLockDefine.cpp \
        YDThread.cpp \
        YDTime.cpp \
        BriStoreMger.cpp \
        BriBigFile.cpp \
        SensorDataStructDefine.cpp \
        YDUIBase/YDBaseZoom.cpp \
        YDUIBase/YDImagePanelEx.cpp \
        YDUIBase/YDBaseLines.cpp \
        YDUIBase/YDBasePaint.cpp \
        YDUIBase/YDCaptureRect.cpp \
        YDUIBase/YDCtrlPoint.cpp \
        YDUIBase/YDCursorRect.cpp \
        YDUIBase/UICommonFunc.cpp \
    NetUnit/netmanager.cpp \
    NetUnit/Socket.cpp

HEADERS  += mainwindow.h \
        imagewidget.h \
        zedcamera.h \
    publicInfo.h \
    YDCommonFunc.h \
    YDLockDefine.h \
    YDThread.h \
    YDTime.h \
    BriStoreMger.h \
    BriBigFile.h \
    BigfileDefine.h \
    SensorDataStructDefine.h \
    YDUIBase/YDBaseZoom.h \
    YDUIBase/YDImagePanelEx.h \
    YDUIBase/YDBaseLines.h \
    YDUIBase/YDBasePaint.h \
    YDUIBase/YDCaptureRect.h \
    YDUIBase/YDCtrlPoint.h \
    YDUIBase/YDCursorRect.h \
    YDUIBase/BaseStructDefine.h \
    YDUIBase/defines.h \
    YDUIBase/UICommonFunc.h \
    NetUnit/netmanager.h \
    NetUnit/Socket.h

FORMS    += mainwindow.ui

RESOURCES += \
    src.qrc

DISTFILES +=

SUBDIRS += \
    YDUIBase/YDUIBase.pro
