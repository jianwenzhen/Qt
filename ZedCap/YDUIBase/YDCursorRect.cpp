
#include "YDCursorRect.h"


YDCursorRect::YDCursorRect(void)
	: m_nCursorWidth(60)
	, m_nCursorHeight(60)
	, m_nRectLineWidth(2)
	, m_clrRect(QColor(200, 0, 0))
	, m_bShowCursor(false)
	, m_fCursorX(0.0)
	, m_fCursorY(0.0)
{
	
}


YDCursorRect::~YDCursorRect(void)
{

}

void YDCursorRect::drawCursor(QPainter& dc)
{
	if (!isShowCursor())
	{
		return;
	}
	QPen pen(m_clrRect);
	pen.setWidth(m_nRectLineWidth);
	pen.setStyle(Qt::DotLine);
	dc.setPen(pen);
	dc.drawRect(m_rect);
}

void YDCursorRect::mouseMove(const QPoint& point)
{
	if (isShowCursor())
	{
		m_rect = QRect(point.x() - m_nCursorWidth / 2.0, point.y() - m_nCursorHeight / 2.0, m_nCursorWidth, m_nCursorWidth);
	
		updateCursorDraw();
	}
}

void YDCursorRect::updateCursorDraw(void)
{
	
}

void YDCursorRect::mouseLeave(void)
{
	m_rect = QRect();
	updateCursorDraw();
}

void      YDCursorRect::setCursorWidth(int nW)
{
	m_nCursorWidth = nW;
}

void      YDCursorRect::setCursorHeight(int nH)
{
	m_nCursorHeight = nH;

	updateCursorDraw();
}

void      YDCursorRect::setRectLineWidth(int nW)
{
	m_nRectLineWidth = nW;
}

void      YDCursorRect::showCursor(bool bShow)
{
	m_bShowCursor = bShow;
}

void      YDCursorRect::setCursorColor(QColor color)
{
	m_clrRect = color;
}

bool      YDCursorRect::isShowCursor(void)
{
	return m_bShowCursor;
}

void YDCursorRect::screenCoord2ImageCoord(const QPoint& point, double& fImageX, double& fImageY)
{
	fImageX = point.x();
	fImageY = point.y();
}

void YDCursorRect::screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY)
{
	fImageX = nX;
	fImageY = nY;
}