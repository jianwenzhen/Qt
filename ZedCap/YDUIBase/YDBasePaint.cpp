
#include "YDBasePaint.h"

QRectF penstoRect(const VPens& pnes)
{
	float fMinX = 999999;
	float fMinY = 999999;
	float fMaxX = -1;
	float fMaxY = -1;
	for (const auto& iter:pnes)
	{
		if (iter._x > fMaxX)
		{
			fMaxX = iter._x;
		}
		if (iter._y > fMaxY)
		{
			fMaxY = iter._y;
		}
		if (iter._x < fMinX)
		{
			fMinX = iter._x;
		}
		if (iter._y < fMinY)
		{
			fMinY = iter._y;
		}
	}
	return QRectF(fMinX, fMinY, fMaxX - fMinX, fMaxY - fMinY);
}

YDBasePaint::YDBasePaint()
	: m_bLBtnDown(false)
	, m_bVisiPaint(true)
	, m_Alph(150)
	, m_nR(10)
	, m_eType(eT_Line)
	, m_bEnable(false)
	, m_clrPen(QColor(255, 0, 0))
{
	srand(0);
}


YDBasePaint::~YDBasePaint()
{
}

void YDBasePaint::DrawPen(QPainter& dc, const QRect& rect)
{
	if (!m_bEnable || eT_Line == m_eType)
	{
		return;
	}
	dc.setBrush(m_clrPen);
	dc.setPen(QPen(m_clrPen, 2.0));
	QRect rcPen(rect.left() + (m_nR) / 2
		, rect.top() + (m_nR) / 2
		, 2 * m_nR
		, 2 * m_nR);
	dc.drawEllipse(rcPen);
}

void YDBasePaint::drawSomeThings(QPainter& dc, const QRect& rect)
{
	if (!m_bEnable)
	{
		return;
	}
	dc.setBrush(m_clrPen);
	dc.setPen(QPen(m_clrPen, 2.0));
	switch (m_eType)
	{
	case YDBasePaint::eT_Point:
		DrawPoints(dc);
		break;
	case YDBasePaint::eT_Line:
		DrawLines(dc, m_vRects);
		drawInfos(dc, rect);
		break;
	default:
		break;
	}
}

void  YDBasePaint::DrawLines(QPainter& dc, const VRects& rects)
{
	dc.setBrush(Qt::NoBrush);
	for (const auto& iter : rects)
	{
		double fX[2] = { 0 };
		double fY[2] = { 0 };
		imageCoord2ScreenCoord(iter.left(), iter.top(), fX[0], fY[0]);
		imageCoord2ScreenCoord(iter.right(), iter.bottom(), fX[1], fY[1]);
		dc.drawRect(QRect(QPoint(fX[0], fY[0]), QPoint(fX[1], fY[1])));
	}
	/*for (auto iter = m_vPens.begin(); iter != m_vPens.end(); ++iter)
	{
	if (iter->size() < 2)
	{
	continue;
	}
	for (auto geo = iter->begin(); geo != iter->end() - 1; ++geo)
	{
	double fX[2] = { 0 };
	double fY[2] = { 0 };
	imageCoord2ScreenCoord(geo->_x, geo->_y, fX[0], fY[0]);
	imageCoord2ScreenCoord((geo + 1)->_x, (geo + 1)->_y, fX[1], fY[1]);

	dc.drawLine(QPointF(fX[0], fY[0]), QPointF(fX[1], fY[1]));
	}
	}*/
	if (m_curPen.size() < 2)
	{
		return;
	}
	for (auto geo = m_curPen.begin(); geo != m_curPen.end() - 1; ++geo)
	{
		double fX[2] = { 0 };
		double fY[2] = { 0 };
		imageCoord2ScreenCoord(geo->_x, geo->_y, fX[0], fY[0]);
		imageCoord2ScreenCoord((geo + 1)->_x, (geo + 1)->_y, fX[1], fY[1]);

		dc.drawLine(QPointF(fX[0], fY[0]), QPointF(fX[1], fY[1]));
	}
}

void YDBasePaint::DrawPoints(QPainter& dc)
{
	for (auto iter = m_vPens.begin(); iter != m_vPens.end(); ++iter)
	{
		for (auto geo = iter->begin(); geo != iter->end(); ++geo)
		{
			double fX = 0;
			double fY = 0;
			float fR = imageToScreen(geo->_r);
			imageCoord2ScreenCoord(geo->_x, geo->_y, fX, fY);
			dc.drawEllipse(fX - fR, fY - fR, 2 * fR, 2 * fR);
		}
	}

	for (auto geo = m_curPen.begin(); geo != m_curPen.end(); ++geo)
	{
		double fX = 0;
		double fY = 0;
		float fR = imageToScreen(geo->_r);
		imageCoord2ScreenCoord(geo->_x, geo->_y, fX, fY);
		dc.drawEllipse(fX - fR, fY - fR, 2 * fR, 2 * fR);
	}

	dc.drawEllipse(m_rcMouse);
}

void YDBasePaint::drawInfos(QPainter& dc, const QRect& rect)
{
	int nLeft = rect.left();
	int nTop = rect.top() + 10;
	int nW = 40;
	int nH = 18;
	///���ƾ���
	int i = 0;
	for (const auto& iter:m_vRectInfos)
	{
		QRect pos(nLeft, nTop + i * nH, nW, nH);
		dc.fillRect(QRect(nLeft, nTop + i * nH, nW, nH), QBrush(iter.m_color));
		dc.drawText(pos.bottomRight(), iter.m_strName);

		dc.setPen(QPen(iter.m_color, 2.0));
		DrawLines(dc, iter.m_vRects);

		i++;
	}

	///����ͼ��
}

void YDBasePaint::LButtonDown(const QPointF& point)
{
	m_bLBtnDown = true;
}

void YDBasePaint::LButtonUp(const QPointF& point)
{
	m_bLBtnDown = false;

	if (!m_bEnable)
	{
		return;
	}

	switch (m_eType)
	{
	case YDBasePaint::eT_Point:
		if (m_curPen.size())
		{
			m_vPens.push_back(m_curPen);
		}
		break;
	case YDBasePaint::eT_Line:
		if (m_curPen.size() > 1)
		{
			m_vRects.push_back(penstoRect(m_curPen));
		}
		break;
	default:
		break;
	}
	

	m_curPen.clear();

	UpdateThings();
}

void YDBasePaint::RButtonDown(const QPointF& point)
{

}

void YDBasePaint::RButtonUp(const QPointF& point)
{
	if (!m_bEnable)
	{
		return;
	}
	
	tagRectInfo info;
	info.m_strName = m_strPrefix + QString::number(getNumber() + 1);
	info.m_color = QColor(rand() % 255, rand() % 255, rand() % 255);
	info.m_vRects = m_vRects;
	m_vRectInfos.push_back(info);

	m_curPen.clear();

	UpdateThings();
}

void YDBasePaint::MouseMove(const QPointF& point)
{
	if (!m_bEnable)
	{
		return;
	}
	if (m_bLBtnDown)
	{
		double fX = 0;
		double fY = 0;
		screenCoord2ImageCoord(point.x(), point.y(), fX, fY);

		m_curPen.push_back(tagPen(fX, fY, screenToImage(m_nR)));
	}

	m_rcMouse.setRect(point.x() - m_nR, point.y() - m_nR, 2 * m_nR, 2 * m_nR);

	UpdateThings();
}

void YDBasePaint::imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY)
{


}

void YDBasePaint::screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY)
{

}

double YDBasePaint::imageToScreen(double fValue)
{
	return fValue;
}

double YDBasePaint::screenToImage(double fValue)
{
	return fValue;
}

void YDBasePaint::showPaint(bool bShow)
{
	m_bVisiPaint = bShow;

	UpdateThings();
}

void YDBasePaint::setPenR(int nR)
{
	m_nR = nR;
	UpdateThings();
}

int  YDBasePaint::getPenR(void)
{
	return m_nR;
}

QColor YDBasePaint::getPenColor(void)
{
	return m_clrPen;
}

void  YDBasePaint::setPenColor(QColor clr)
{
	m_clrPen = clr;
	UpdateThings();
}

void YDBasePaint::removeLast(void)
{
	switch (m_eType)
	{
	case YDBasePaint::eT_Point:
		if (m_vPens.size())
		{
			m_vPens.pop_back();
		}
		break;
	case YDBasePaint::eT_Line:
		if (m_vRects.size())
		{
			m_vRects.pop_back();
		}
		break;
	default:
		break;
	}

	UpdateThings();
}

void YDBasePaint::clear(void)
{
	switch (m_eType)
	{
	case YDBasePaint::eT_Point:
		if (m_vPens.size())
		{
			m_vPens.clear();
		}
		break;
	case YDBasePaint::eT_Line:
		if (m_vRects.size())
		{
			m_vRects.clear();
		}
		break;
	default:
		break;
	}
	UpdateThings();
}

void YDBasePaint::clearAll(void)
{
	m_vRectInfos.clear();
	m_strPrefix = "unkown";
	clear();
}

void YDBasePaint::remove(const QString& strName)
{
    for (auto iter = m_vRectInfos.begin(); iter != m_vRectInfos.end(); ++iter)
	{
		if (strName == iter->m_strName)
		{
			m_vRectInfos.erase(iter);
			break;
		}
	}
	UpdateThings();
}

void YDBasePaint::saveToFile(const char* lpImagePath)
{

}

void YDBasePaint::loadFromFile(const char* lpImagePath)
{

}

VVPens YDBasePaint::getPens(void)
{
	return m_vPens;
}

void  YDBasePaint::enableBasePaint(bool bEnable)
{
	m_bEnable = bEnable;
	UpdateThings();
}

VRects YDBasePaint::getRects(void)
{
	return m_vRects;
}

void YDBasePaint::setPrefix(const QString& strPrefix)
{
	m_strPrefix = strPrefix;
}

int YDBasePaint::getNumber(void)
{
	int nIndex = 0;
    for (auto iter = m_vRectInfos.rbegin(); iter != m_vRectInfos.rend(); ++iter)
	{
		if (m_strPrefix == iter->m_strName.left(m_strPrefix.length()))
		{
			nIndex = iter->m_strName.right(iter->m_strName.length() - m_strPrefix.length()).toInt();
			break;
		}
	}
	return nIndex;
}

