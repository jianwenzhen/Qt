#include <vector>
#include <QPainter>
#include "defines.h"

class YDBaseLines
{
public:
	enum enuType
	{
		eT_Point = 0,
		eT_Line
	};
public:
	YDBaseLines();
	virtual ~YDBaseLines();

public:
	void               showLines(bool bShow);
	void               setInfo(const QString& strInfo);
    void			   setInfos(const std::vector<QString>& vStrInfos);
	void               enableBaseLines(bool bEnable);
    void               setSearchlines(const std::vector<searchline>& lines, bool bLeft);

protected:
	virtual void       drawInfos(QPainter& dc, const QRect& rect);
	virtual void       drawSomeThings(QPainter& dc, const QRect& rect);
	virtual void       drawLines(QPainter& dc);
    virtual void       UpdateThings(void){};
	virtual void       LButtonDown(const QPointF& point);
	virtual void       LButtonUp(const QPointF& point);
	virtual void       RButtonDown(const QPointF& point);
	virtual void       RButtonUp(const QPointF& point);
	virtual void       MouseMove(const QPointF& point);
	virtual double     imageToScreen(double fValue);
	virtual double     screenToImage(double fValue);
	virtual  void      imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY);
	virtual  void      screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY);
protected:
	bool         m_bEnable;
	bool         m_bVisiPaint;
	bool         m_bLeft;
	QColor       m_clrPen;
	int          m_Alph;
	int          m_nR;
	QRectF       m_rcMouse;
	bool         m_bLBtnDown;
	enuType      m_eType;
	QString      m_strInfo;
	std::vector<QString>	m_vStrInfos;
	std::vector<searchline> m_vSearchLine;

};

