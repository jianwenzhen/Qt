#include "UICommonFunc.h"
#include <QFile>
#include <QApplication>

QPixmap MatToPixmap(const cv::Mat& image, bool bRgb2bgr)
{
	cv::Mat temp;
	switch (image.channels())
	{
	case 1:
		cv::cvtColor(image, temp, CV_GRAY2BGR);
		break;
	case 3:
		if(bRgb2bgr)
		cv::cvtColor(image, temp, CV_RGB2BGR);
		break;
	case 4:
		break;
	default:
		break;
	}
	QImage qImage(temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	qImage.bits();
	return QPixmap::fromImage(qImage);
}

void MatToPixmap(const cv::Mat& image, QPixmap& pixmap)
{
	cv::Mat temp;
	switch (image.channels())
	{
	case 1:
		cv::cvtColor(image, temp, CV_GRAY2BGR);
		break;
	case 3:
		cv::cvtColor(image, temp, CV_BGR2RGB);
		break;
	case 4:
		break;
	default:
		break;
	}
	QImage qImage(temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	qImage.bits();
	pixmap = QPixmap::fromImage(qImage);
}

QPixmap dataToPixmap(int nW, int nH, int nC, unsigned char* data)
{
	QImage::Format format = QImage::Format_RGB888;
	switch(nC)
	{
	case 1:
		format = QImage::Format_Mono;
		break;
	case 3:
		format = QImage::Format_RGB888;
		break;
	case 4:
		format = QImage::Format_RGBA8888;
		break;
	default:
		break;
	}
	QImage qImage(data, nW, nH, nW * nC, format);
	qImage.bits();
	return QPixmap::fromImage(qImage);
}

QImage dataToQImage(int nW, int nH, int nC, unsigned char* data)
{
	QImage::Format format = QImage::Format_RGB888;
	switch (nC)
	{
	case 1:
		format = QImage::Format_Mono;
		break;
	case 3:
		format = QImage::Format_RGB888;
		break;
	case 4:
		format = QImage::Format_RGBA8888;
		break;
	default:
		break;
	}
	QImage qimage(data, nW, nH, nW * nC, format);
	qimage.bits();
	return qimage;
}

QImage MatToQImage(const cv::Mat& image)
{
	cv::Mat temp;
	switch (image.channels())
	{
	case 1:
		cv::cvtColor(image, temp, CV_GRAY2BGR);
		break;
	case 3:
		cv::cvtColor(image, temp, CV_BGR2RGB);
		break;
	case 4:
		break;
	default:
		break;
	}
	QImage qimage(temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	qimage.bits();
	return qimage;
}

void MatToQImage(const cv::Mat& image, QImage& qImage)
{
	cv::Mat temp;
	switch (image.channels())
	{
	case 1:
		cv::cvtColor(image, temp, CV_GRAY2BGR);
		break;
	case 3:
		cv::cvtColor(image, temp, CV_BGR2RGB);
		break;
	case 4:
		break;
	default:
		break;
	}
	qImage = QImage(temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
	qImage.bits();
}

bool setStyle(const QString &style)
{
	bool bRet = false;
	do 
	{
		QFile qss(style);
		if(!qss.open(QFile::ReadOnly))
			break;

		qApp->setStyleSheet(qss.readAll());
		qss.close();

		bRet = true;

	} while (0);

	return bRet;
}

void  removeDockTitlebar(QDockWidget* pDockWidget)
{
	if (pDockWidget)
	{
		QWidget* lTitleBar = pDockWidget->titleBarWidget();
		QWidget* lEmptyWidget = new QWidget();
		pDockWidget->setTitleBarWidget(lEmptyWidget);
		delete lTitleBar;
	}
}

