#include <vector>
#include <QPainter>

struct tagPen
{
	float _x;
	float _y;
	float _r;
	tagPen()
		: _x(0)
		, _y(0)
		, _r(0)
	{

	}
	tagPen(float x, float y, float r)
		: _x(x)
		, _y(y)
		, _r(r)
	{

	}
	tagPen(const tagPen& other)
		: _x(other._x)
		, _y(other._y)
		, _r(other._r)
	{

	}
	tagPen& operator=(const tagPen& other)
	{
		_x = other._x;
		_y = other._y;
		_r = other._r;
		return *this;
	}
};

typedef std::vector<tagPen> VPens;
typedef std::vector<VPens>  VVPens;
typedef std::vector<QRectF> VRects;

struct tagRectInfo
{
	QString m_strName;
	QColor  m_color;
	VRects  m_vRects;
	tagRectInfo()
		: m_strName("unkown")
		, m_color(255, 0, 0)
	{

	}
	tagRectInfo(const tagRectInfo& other)
		: m_strName(other.m_strName)
		, m_color(other.m_color)
		, m_vRects(other.m_vRects)
	{

	}
	tagRectInfo& operator=(const tagRectInfo& other)
	{
		m_strName = other.m_strName;
		m_color   = other.m_color;
		m_vRects = other.m_vRects;
		return *this;
	}
};

typedef std::vector<tagRectInfo> VRectInfos;

class YDBasePaint
{
public:
	enum enuType
	{
		eT_Point = 0,
		eT_Line
	};
public:
	YDBasePaint();
	virtual ~YDBasePaint();

public:
	VVPens             getPens(void);
	VRects             getRects(void);
	void               showPaint(bool bShow);
	void               setPenR(int nR);
	int                getPenR(void);
	void               setPenColor(QColor clr);
	QColor             getPenColor(void);
	void               removeLast(void);
	void               clear(void);
	void               clearAll(void);
	void               remove(const QString& strName);
	void               setPrefix(const QString& strPrefix);
	void               enableBasePaint(bool bEnable);
	void               saveToFile(const char* lpImagePath);
	void               loadFromFile(const char* lpImagePath);
protected:
	int                getNumber(void);
	virtual void       drawInfos(QPainter& dc, const QRect& rect);
	virtual void       DrawPen(QPainter& dc, const QRect& rect);
	virtual void       drawSomeThings(QPainter& dc, const QRect& rect);
	virtual void       DrawLines(QPainter& dc, const VRects& rects);
	virtual void       DrawPoints(QPainter& dc);
    virtual void       UpdateThings(void){};
	virtual void       LButtonDown(const QPointF& point);
	virtual void       LButtonUp(const QPointF& point);
	virtual void       RButtonDown(const QPointF& point);
	virtual void       RButtonUp(const QPointF& point);
	virtual void       MouseMove(const QPointF& point);
	virtual double     imageToScreen(double fValue);
	virtual double     screenToImage(double fValue);
	virtual  void      imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY);
	virtual  void      screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY);
protected:
	bool         m_bEnable;
	bool         m_bVisiPaint;
	QColor       m_clrPen;
	int          m_Alph;
	int          m_nR;
	VPens        m_curPen;
	VVPens       m_vPens;
	VRects       m_vRects;
	QRectF       m_rcMouse;
	bool         m_bLBtnDown;
	enuType      m_eType;
	QString      m_strPrefix;
	VRectInfos   m_vRectInfos;

};

