/************************************************************************
/* 控制点操作基类
/* 功能: 实现控制点的提取，修改，删除等功能
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2016.12.01*/
/************************************************************************/
#define ARC_2_ANGLE_UINT   57.29578049044297
#include <QPainter>
#include <QPointF>
#include "BaseStructDefine.h"
#include "defines.h"

class QMouseEvent;
class YDCtrlPoint
{
public:
	enum enOperatorType
	{
		eOT_NOOPERATOR = 0, //无操作
		eOT_GET_POINT,      //提点
		eOT_SELECT_POINT,   //选点
		eOT_MODIDY_POINT    //修改点坐标
	};

	enum enShapeType
	{
		eST_POINT = 0,
		eST_LINE
	};

public:
	YDCtrlPoint(void);
	virtual ~YDCtrlPoint(void);

protected:
	bool			    m_bLButtonDown;
	bool				m_bDraging;
	bool				m_bLButtonDbClk;
	bool				m_bSelectedModifyPoint;
	bool                m_bShowTrackLine;
    int					m_nCurCtrlPointIndex;
	QPointF				m_ptLeftDown;
	QPointF             m_ptMosuse;
	enOperatorType      m_operatorType;
	VCtrlPoints*	    m_pVCtrlPoints;		          //图像上的点
	enShapeType         m_eShapeType;
protected:
	//draw function
    virtual void        UpdateDraw(){};
	virtual void        drawCtrlPoint(QPainter& painter);
    virtual void        addPointEvent(int nPtIndex){};
	virtual void        DrawMouseCross(QPainter& painter, const QRect& rcClient);

	//coodr transfer
	virtual QPointF    Screen_2_Image(const QPointF& ScreenPoint){ return ScreenPoint; }
	virtual QPointF    Image_2_Screen(const QPointF& ImagePoint){ return ImagePoint; }
	
	//mouse function
	virtual void        LButtonDblClk(const QPointF& point);
	virtual void        LButtonDown(const QPointF&  point);
	virtual void        LButtonUp(const QPointF&  point);
	virtual void        MouseMove(const QPointF&  point);

	virtual void        mouseMoveEvent(QMouseEvent *event);
	virtual void        mousePressEvent(QMouseEvent *event);
	virtual void        mouseReleaseEvent(QMouseEvent *event);
	virtual void        mouseDoubleClickEvent(QMouseEvent *event);
	//virtual BOOL     SetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
public:
    int                 GetValidateCtrlPointCount();
    int                 GetCtrlPointSize(void);
    int                 GetCurCtrlPointIndex(void);
    int                 GetTrackLinkPointSize(void);
	void                removePoint(void);
    void                ClearCtrlPoint(void);
	enOperatorType      GetOperatorType(void);
	void                setOperatorType(enOperatorType type);
	void                setCurCtrlPointIndex(int nIndex);
    void                SetModifyCtrlPointIndex(int nIndex);
	void                ModifyCtrlPointIndex(int nIndex);				//修改选中控制点的序号
    void                SetModifyCtrlPointIndex(const QPoint& pointScreen);
	void                ModifyCtrlPointCoord(const QPointF& point);//修改选中控制点的坐标
	bool                getCtrlPoint(int nIndex, tagCtrlPoint& ctrlPointInfo);
	void                setCtrlPoints(VCtrlPoints& pVCtrlPoints);
	void                AddScreenPoint(const tagCtrlPoint& screenPoint);
	void                AddImagePoint(const tagCtrlPoint& screenPoint);
	int                 GetSelectCtrlPointIndex(const QPoint& pointScreen);
	int                 GetSelectCtrlPointIndex(const QPointF& point);
	inline VCtrlPoints* GetCtrlPointInfo(){ return m_pVCtrlPoints; }

};

