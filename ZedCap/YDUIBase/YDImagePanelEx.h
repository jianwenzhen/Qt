/************************************************************************
/* 可缩放操作控制点
/* 功能: 集缩放控制点的提取，修改，删除等功能
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2016.12.01*/
/************************************************************************/
#ifndef YDUSKY_IMAGE_PANEL_EX_CTRLPANEL_H_2017_04_17_09_30_43_
#define YDUSKY_IMAGE_PANEL_EX_CTRLPANEL_H_2017_04_17_09_30_43_
#include <QWidget>
#include "YDBasePaint.h"
#include "YDBaseZoom.h"
#include "YDCtrlPoint.h"
#include "YDCursorRect.h"
#include "YDCaptureRect.h"
#include "YDBaseLines.h"
#include "../YDLockDefine.h"
#include <opencv2/opencv.hpp>

class YDImagePanelEx
	: public QWidget
	, public YDBasePaint
	, public YDBaseLines
	, public YDBaseZoom
	, public YDCtrlPoint
	, public YDCursorRect
	, public YDCaptureRect
{
	Q_OBJECT

public:
	YDImagePanelEx(QWidget *parent);
	~YDImagePanelEx();

protected:
	bool               m_bShowInfo;
	bool               m_bShowHistogram;
	QPixmap            m_image;
	QPixmap            m_default;
	QString            m_strMousePos;
	QString            m_strMouseRGB;
	int                m_nMaxR;
	int                m_nMaxG;
	int                m_nMaxB;
	std::vector<int>   m_histCountR;
	std::vector<int>   m_histCountG;
	std::vector<int>   m_histCountB;
	YDLock             m_lockImage;
	int                m_nImageW;
	int                m_nImageH;
	int                m_nStep;
	unsigned char*     m_pImageData;
public:
	QImage             getImage(void);
	QSize              getImageSize(void);
	void               setDefault(const QPixmap& image);
	void               setImage(const QPixmap& image);
	void               setImage(const cv::Mat& image, bool bBgr2rgb = true);
	void               showInfo(bool bShow);
	bool               isShowInfo(void);
	void               showHistogram(bool bShow);
	bool               isShowHistogram(void);
	void               setCursorSize(int nWidth, int nHeight);
	void               addRectToVect(double fImageX, double fImageY);
protected:

	void               drawInfo(QPainter& painter);
	void               drawHistogram(QPainter& painter, const QRect& rcPosi);
protected:		       
	void               calcHistogram(void);
	void               updateImageData(int nW, int nH, int nC, uchar* imageData);
signals :		       
	void               paint(QPainter* painter);
	void               addPoint(const tagCtrlPoint& imagePoint);
	void               rMouseUp(void);
protected:
	virtual QSize      sizeHint() const;
	virtual void       updateDraw();
	virtual void       UpdateThings(void);
	virtual void       addPointEvent(int nPtIndex);
	virtual void       resizeEvent(QResizeEvent *event);
	virtual void       paintEvent(QPaintEvent *event);
	virtual void       wheelEvent(QWheelEvent *event);
	virtual void       mouseMoveEvent(QMouseEvent *event);
	virtual void       mousePressEvent(QMouseEvent *event);
	virtual void       mouseReleaseEvent(QMouseEvent *event);
	virtual void       mouseDoubleClickEvent(QMouseEvent *event);
	virtual void       keyPressEvent(QKeyEvent *event);
	virtual void       keyReleaseEvent(QKeyEvent *event);
	virtual double     imageToScreen(double fValue);
	virtual double     screenToImage(double fValue);
	virtual bool       isPtInImage(const QPointF& posScreen);
	//coodr transfer
	virtual QPointF    Screen_2_Image(const QPointF& ScreenPoint);
	virtual QPointF    Image_2_Screen(const QPointF& ImagePoint);

	virtual  void      updateCursorDraw(void);
	virtual  void      imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY);
	virtual  void      screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY);

};

#endif // YDIMAGEPANEL_H
