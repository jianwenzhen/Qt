#-------------------------------------------------
#
# Project created by QtCreator 2017-07-26T09:54:39
#
#-------------------------------------------------

QT       += widgets

TARGET = YDUIBase
TEMPLATE = lib

DEFINES += YDUIBASE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += D:\ThirdPart\opencv320_v14\opencv\build_minGW_x86\install\include
LIBS += -L D:\ThirdPart\opencv320_v14\opencv\build_minGW_x86\install\x86\mingw\bin\libopencv_*.dll

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += yduibase.cpp \
    YDImagePanel.cpp \
    YDBaseZoom.cpp \
    YDCollapseItem.cpp \
    YDSwitchButton.cpp \
    YDSwitchPanel.cpp

HEADERS += yduibase.h\
        yduibase_global.h \
    YDImagePanel.h \
    YDBaseZoom.h \
    YDCollapseItem.h \
    YDSwitchButton.h \
    YDSwitchPanel.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
