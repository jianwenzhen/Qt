/************************************************************************
/* 功能 用于捕获通过鼠标拖动产生的矩形
/* 时间 20150722
/* 作者 yyk
/************************************************************************/
#ifndef _YDUSKY_INTERFACE_CAPTURE_RECT_BY_MOUSE_HEADER_FILE
#define _YDUSKY_INTERFACE_CAPTURE_RECT_BY_MOUSE_HEADER_FILE

#include <vector>
#include <QPainter>
typedef std::vector<QRectF> VRectF;
typedef std::vector<VRectF> VFramesTgtPosition;

class YDCaptureRect
{
public:
	YDCaptureRect(void);
	virtual ~YDCaptureRect(void);

protected:
	int                         m_nRectWidth_Image;
	int                         m_nRectHeight_Image;
	int                         m_nGridWidth_Image;
	int                         m_nGridHeight_Image;
	int                         m_nGridLineW;
	QColor                      m_clrGrid;
	QColor                      m_clrPen;
	bool                        m_bDragRect;
	bool						m_bCaptureRect;		//是否捕获矩形
	bool						m_bDrawRect;		//是否绘制捕获的矩形
	bool						m_bLBtnDown;		//鼠标是否按下
	QPoint						m_ptLeftUp;			//矩形左上点
	QPoint						m_ptRightBottom;	//矩形右下点
	QRect						m_rcRect;
	VRectF					    m_vRects;
	bool                        m_bShowGrid;
public:
	virtual void                showGrid(bool bShow);
	virtual bool                isShowGrid(void);
	virtual QSize               getGridSize(void);
	virtual void                removeEnd(void);				//删除最后的矩形区域
	virtual void                clearRect(void);
	virtual void                drawRects(QPainter& dc);
	virtual void                drawGrid(QPainter& dc, QRectF rcImage);
	virtual QRect               getRect(void);
	virtual bool                isDrawRect(void);
	virtual bool                isCaptureRect(void);
	virtual VRectF              getRectVect(void);
	virtual void                addRect(const QRectF& rect);
	virtual void                enableDrawRect(bool bDraw);
	virtual void                enableDragRect(bool bDrag);
	virtual void                setRectVect(const VRectF& rectV);
	virtual void                enableCaptureRect(bool bCapture);
	virtual void                setGridColor(QColor clr);
	virtual void                setPenColor(QColor clr);
	virtual void                setGridLineWidth(quint16 nW);
	virtual void                setGridSize(int nWImage, int nHImage);
	virtual void                addRect(double fImageX, double fImageY, double fW, double fH);
	virtual void                updateRect(const QPoint& ptLeftUp, const QPoint& ptRightBottom);
	virtual double              imageToScreen(double fValue);
	virtual double              screenToImage(double fValue);
	virtual void                imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY);
	virtual void                screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY);
protected:
	virtual void                updateDraw(void) = 0;
	virtual void                LButtonUp(QPointF point);
	virtual void                LButtonDown(QPointF point);
	virtual void                RButtonUp(QPointF point);
	virtual void                mouseMove(QPointF point);
};

#endif
