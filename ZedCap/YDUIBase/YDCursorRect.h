/************************************************************************
/* 功能 绘制以鼠标位置为中心的矩形区域
/* 时间 20151207
/* 作者 yyk
/************************************************************************/
#ifndef _YDUSKY_INTERFACE_BASE_CURSOR_RECT_BY_MOUSE_HEADER_FILE_
#define _YDUSKY_INTERFACE_BASE_CURSOR_RECT_BY_MOUSE_HEADER_FILE_
#include <QPainter>

class YDCursorRect
{
public:
	YDCursorRect(void);
	virtual ~YDCursorRect(void);

protected:	
	int        m_nCursorWidth;		//矩形宽
	int        m_nCursorHeight;		//矩形高
	int        m_nRectLineWidth;
	bool       m_bShowCursor;       //是否显示矩形
	QRect      m_rect;
	double     m_fCursorX;
	double     m_fCursorY;
	QColor     m_clrRect;
protected:
	virtual  void mouseLeave(void);
	virtual  void drawCursor(QPainter& dc);
	virtual  void mouseMove(const QPoint& point);
	virtual  void updateCursorDraw(void);
	virtual  void screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY);
	virtual  void screenCoord2ImageCoord(const QPoint& point, double& fImageX, double& fImageY);
public:
	bool      isShowCursor(void);
	void      setCursorWidth(int nW);
	void      setCursorHeight(int nH);
	void      setRectLineWidth(int nW);
	void      showCursor(bool bShow);
	void      setCursorColor(QColor color);

};

#endif
