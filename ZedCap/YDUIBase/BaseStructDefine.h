#ifndef _YDUSKY_CALIBRATION_MODEL_BASE_STRUCT_DEFINE_HEADER_FILES_
#define _YDUSKY_CALIBRATION_MODEL_BASE_STRUCT_DEFINE_HEADER_FILES_
#include <vector>
#include <QPointF>
struct tagCtrlPoint
{
	int	        m_nIndex;	//���
	QPointF     m_point;
	tagCtrlPoint()
		:m_nIndex(0)
	{

	}
	tagCtrlPoint(double fX, double fY, int nIdex = 0)
		:m_point(fX, fY)
		,m_nIndex(nIdex)
	{

	}

	tagCtrlPoint(const QPointF& point, int nIdex)
		: m_point(point)
		, m_nIndex(nIdex)
	{

	}
};

typedef std::vector<tagCtrlPoint> VCtrlPoints;

#endif
