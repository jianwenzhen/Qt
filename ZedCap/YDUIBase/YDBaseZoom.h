/************************************************************************
/* 可缩放模块
/* 功能: 缩放，拖动画面功能
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2016.12.01*/
/************************************************************************/
#ifndef _YDUSKY_BASE_ZOOM_FILE_HEADER_2016_10_12_16_16_14_
#define _YDUSKY_BASE_ZOOM_FILE_HEADER_2016_10_12_16_16_14_
#include <QSize>
#include <QPointF>
#include <QRect>
#include <QPixmap>
#include <QPainter>

class QWheelEvent;
class QMouseEvent;
class YDBaseZoom
{
public:
	YDBaseZoom();
	virtual ~YDBaseZoom();

public:
	enum enuAction
	{
		eZA_NOACTION = 0,		//没有动作
		eZA_ZOOM,				//缩放
		eZA_DRAG,				//拖动
	};
	enum enumRectType
	{
		eRT_SHOWRECT = 0,	  //图像在主显示区位置
		eRT_THUMBNAILWND,     //缩略图窗口区
		eRT_THUMBNAIL,        //缩略图
		eRT_NAVIGATION,       //导航区
	};
protected:
	float               m_fScale;   //放缩比例
	enuAction           m_eAction; 
	bool				m_bInitPos;
	bool                m_bEnableDrag;              //是否启动拖动
	bool				m_bImageSizeWithWinSize;    //是否根据窗口大小调整图像尺寸
	QSize				m_szOrgImage;               //原始图像的尺寸
	QSize				m_szShowWnd;                //显示窗口的尺寸
	QPointF				m_ptLBtnDown;
	QRectF				m_rectPositon;             //获得图像矩形区域在窗口中的位置
	double				m_fRectPosLUX;			   //图像矩形区域在窗口中的矩形的左上角坐标
	double				m_fRectPosLUY;			   //图像矩形区域在窗口中的矩形的左上角坐标
	QRectF				m_rectThumbnailNavigation;  //缩略图显示的区域
	QRectF				m_rectThumbnailPos;         //缩略图位置,相对于
	QRectF				m_rectThumbnailWndPos;      //缩略框位置
	float				m_fThumbnailScale;          //缩略图的缩放比例，缩略图/原图
	float				m_fZoomScale;               //缩放比例，新图/原图
	float				m_fInitZoomScale;		   //在满屏显示在当前窗口时的比例
	float               m_fLasfZoomScale;
	bool				m_bLBtnDown;
	bool				m_bMouseInNavigationArea;   //鼠标是否在缩略图的导航区
	bool				m_bMouseInThumbnail;        //鼠标是否在缩略图区域
	bool				m_bShowThumbnailWnd;        //是否显示缩略图窗口区域
	bool				m_bNeedThumbnailWnd;        //是否需要缩略图窗口区域
	QPixmap             m_imgThumbnail;
protected:
	void				OffsetXY(int nX, int nY);
protected:
	float				GetZoomScale(void);
	QRect				GetThumbnailWndPos();
	QSize				GetOrgImageSize(void);
	void				SetThumbnail(const QPixmap& image);
	void				SetRectPosLeftUp(double fX, double fY);
	void				GetRectPosLeftUp(double& fX, double& fY);
	void				SetZoomScale(float fAddValue);
	void				SetRectPosition(const QPointF& ptCenter);
    void				AdjustImagePos(int nW, int nH);
    void				SetOrgImageSize(int nimgW, int imgH);
    void				SetRectPos(const QRectF& rect, enumRectType eRectType = eRT_SHOWRECT);
	void				CalcNavigationPosByShowArea(void);//计算导航区与位置
    void				CalcShowAreaByNavigation(int nX, int nY);//根据导航区域位置计算主显示区显示的图像区域
    void				InitImagePos(int nWndWidth, int nWndHeight);
	void				ZoomImage(const QPointF& point/*缩放中心*/, float fScale/*缩放比例*/);
    int					LimitRectInRect(const QRectF& rectA, QRectF& rectB);//将矩形B限制在矩形A中
    void				MoveRect(const QPointF& ptStart, const QPointF& ptEnd);
    void				MoveRectCenter(QRectF& rect, const QPointF& ptCenter);


	// mouse function
	virtual void		UpdateDraw() {};
	virtual void		MouseLeave();
	virtual void		MButtonUp(const QPointF& point);
	virtual void		MButtonDown(const QPointF& point);
	virtual void		LButtonDblClk(const QPointF& point);
	virtual void		LButtonDown(const QPointF& point);
	virtual void		LButtonUp(const QPointF& point);
	virtual void		MouseMove(const QPointF& point);
	virtual void		Resize(int cx, int cy);
	virtual void		MouseWheel(short zDelta, const QPointF& pt);

public:
	QRectF				GetRectPosition(void);

	void                NeedThumbnailWnd(bool bNeed);

	void				AdjustImagePosByWindowSize(void);

	void                enableDrag(bool bEnable);

	//获得窗口的一点在图像中的坐标
    void				Com_ScreenCoor2ImageCoor_S(const QPointF& pointScreen,  QPointF& imagePoint);
    void				Com_ScreenCoor2ImageCoor_S(const QPointF& pointScreen, double& fImageX, double& fImageY);
    void				Com_ScreenCoor2ImageCoor_S(double fScreenX, double fScreenY, double& fImageX, double& fImageY);
	
    void				Com_ImageCoor2ScreenCoor_S(const QPointF& imagePoint, QPointF& point/*输出屏幕坐标*/);
    void				Com_ImageCoor2ScreenCoor_S(double fImageX, double fImageY, QPointF& point/*输出屏幕坐标*/);
    void				Com_ImageCoor2ScreenCoor_S(double fImageX, double fImageY, double& fScreenX, double& fScreenY);

	// draw function
protected:
	virtual void        drawThumbnail(QPainter& painter);
	virtual void        drawNavigationArea(QPainter& painter);
	virtual void        wheelEvent(QWheelEvent *event);
	virtual void        mouseMoveEvent(QMouseEvent *event);
	virtual void        mousePressEvent(QMouseEvent *event);
	virtual void        mouseReleaseEvent(QMouseEvent *event);
	virtual void        mouseDoubleClickEvent(QMouseEvent *event);
};

#endif
