
#include "YDCaptureRect.h"


YDCaptureRect::YDCaptureRect(void)
	: m_bCaptureRect(false)
	, m_bDrawRect(false)
	, m_bLBtnDown(false)
	, m_bShowGrid(false)
	, m_nRectWidth_Image(100)
	, m_nRectHeight_Image(100)
	, m_nGridWidth_Image(100)
	, m_nGridHeight_Image(100)
	, m_nGridLineW(2)
	, m_bDragRect(false)
	, m_clrGrid(QColor(0, 230, 0))
	, m_clrPen(m_clrGrid)
{
	m_ptLeftUp = m_ptRightBottom;
}


YDCaptureRect::~YDCaptureRect(void)
{

}

bool YDCaptureRect::isDrawRect(void)
{
	return m_bDrawRect;
}

void YDCaptureRect::enableDrawRect(bool bDraw)
{
	m_bDrawRect = bDraw;
}

void YDCaptureRect::enableDragRect(bool bDrag)
{
	m_bDragRect = bDrag;
}

bool YDCaptureRect::isCaptureRect(void)
{
	return m_bCaptureRect;
}

void YDCaptureRect::enableCaptureRect(bool bCapture)
{
	m_bCaptureRect = bCapture;
}

void YDCaptureRect::updateRect(const QPoint& ptLeftUp, const QPoint& ptRightBottom)
{
	QRect rcTemp;

	rcTemp.setLeft(ptLeftUp.x() <= ptRightBottom.x() ? ptLeftUp.x() : ptRightBottom.x());
	rcTemp.setTop(ptLeftUp.y() <= ptRightBottom.y() ? ptLeftUp.y() : ptRightBottom.y());
	rcTemp.setRight(ptLeftUp.x() >= ptRightBottom.x() ? ptLeftUp.x() : ptRightBottom.x());
	rcTemp.setBottom(ptLeftUp.y() >= ptRightBottom.y() ? ptLeftUp.y() : ptRightBottom.y());

	if (rcTemp.width() <= 5 && rcTemp.height() <= 5)
	{
		return;
	}

	m_rcRect = rcTemp;
}

QRect YDCaptureRect::getRect(void)
{
	return m_rcRect;
}

void YDCaptureRect::LButtonUp(QPointF point)
{
	if (isCaptureRect())
	{
		if (m_bLBtnDown)
		{
			m_bLBtnDown = false;
			m_ptRightBottom = point.toPoint();
			updateRect(m_ptLeftUp, m_ptRightBottom);
		}
		else
		{
			double fImageX = 0;
			double fImageY = 0;
			screenCoord2ImageCoord(point.x(), point.y(), fImageX, fImageY);
			addRect(QRect(fImageX - m_nRectWidth_Image / 2.0, fImageY - m_nRectHeight_Image / 2.0, m_nRectWidth_Image, m_nRectHeight_Image));

			//::SendMessage(pParentWnd->m_hWnd, MSG_SELECT_TARGET_RECT, WPARAM(&fImageX), LPARAM(&fImageY));
		}

	}
}

void YDCaptureRect::RButtonUp(QPointF point)
{
	if (isCaptureRect() && !m_rcRect.isEmpty())
	{
		double fImageLX = 0;
		double fImageLY = 0;
		double fImageRX = 0;
		double fImageRY = 0;
		screenCoord2ImageCoord(m_rcRect.left(), m_rcRect.top(), fImageLX, fImageLY);
		screenCoord2ImageCoord(m_rcRect.right(), m_rcRect.bottom(), fImageRX, fImageRY);
		addRect(QRect(fImageLX, fImageLY, fImageRX - fImageLX, fImageRY - fImageLY));
		m_rcRect = QRect();
	}
}

void YDCaptureRect::LButtonDown(QPointF point)
{
	if (isCaptureRect() && m_bDragRect && !m_bLBtnDown)
	{
		m_ptLeftUp = point.toPoint();
		m_bLBtnDown = true;
	}
}

void YDCaptureRect::mouseMove(QPointF point)
{
	if (isCaptureRect() && m_bDragRect && m_bLBtnDown)
	{
		m_ptRightBottom = point.toPoint();
		updateRect(m_ptLeftUp, m_ptRightBottom);
		
		updateDraw();
	}
}

void YDCaptureRect::drawRects(QPainter& dc)
{
	if (m_bDragRect)
	{
		QPen pen(m_clrPen);
		pen.setStyle(Qt::DotLine);
		//绘制正在选择的矩形框
		dc.setPen(pen);
		dc.drawRect(getRect());
	}

	//绘制已经选择的矩形框
	QPen pen(m_clrPen, 2.0, Qt::SolidLine);
	dc.setPen(pen);
	double fL = 0;
	double fT = 0;
	double fR = 0;
	double fB = 0;
	VRectF vTemps = getRectVect();
	for (auto& iter : vTemps)
	{
		imageCoord2ScreenCoord(iter.left(), iter.top(), fL, fT);
		imageCoord2ScreenCoord(iter.right(), iter.bottom(), fR, fB);
		dc.drawRect(fL, fT, fR - fL, fB - fT);
	}
}

void YDCaptureRect::imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY)
{
	fX = fImageX;
	fY = fImageY;
}

void YDCaptureRect::screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY)
{
	fImageX = nX;
	fImageY = nY;
}

void YDCaptureRect::addRect(const QRectF& rect)
{
	m_vRects.push_back(rect);

	updateDraw();
}

void YDCaptureRect::addRect(double fImageX, double fImageY, double fW, double fH)
{
	addRect(QRectF(fImageX - fW, fImageY - fH, fW, fH));
}

VRectF YDCaptureRect::getRectVect(void)
{
	return m_vRects;
}

void YDCaptureRect::setRectVect(const VRectF& rectV)
{
	m_vRects = rectV;
}

//删除最后的矩形区域
void YDCaptureRect::removeEnd(void)
{
	if (m_vRects.size())
	{
		m_vRects.pop_back();
		updateDraw();
	}
}

void YDCaptureRect::clearRect(void)
{
	m_vRects.clear();
	m_rcRect = QRect();
	updateDraw();
}

double YDCaptureRect::imageToScreen(double fValue)
{
	return fValue;
}

double YDCaptureRect::screenToImage(double fValue)
{
	return fValue;
}

void YDCaptureRect::setGridSize(int nWImage, int nHImage)
{
	if (nWImage > 0)
	{
		m_nGridWidth_Image = nWImage;
	}
	if (nHImage > 0)
	{
		m_nGridHeight_Image = nHImage;
	}
	
	updateDraw();
}

void YDCaptureRect::showGrid(bool bShow)
{
	m_bShowGrid = bShow;
	updateDraw();
}

bool YDCaptureRect::isShowGrid(void)
{
	return m_bShowGrid;
}

void YDCaptureRect::drawGrid(QPainter& dc, QRectF rcImage)
{
	if (!isShowGrid())
	{
		return;
	}
	QPen pen(QBrush(m_clrGrid), m_nGridLineW, Qt::DotLine);
	dc.setPen(pen);
	float fWScreen = imageToScreen(m_nGridWidth_Image);
	float fHScreen = imageToScreen(m_nGridHeight_Image);
	int nCountX = 1 + rcImage.width() / fWScreen;
	int nCountY = 1 + rcImage.height() / fHScreen;
	int nLeft = rcImage.left();
	int nTop = rcImage.top();
	int nRight = rcImage.right();
	int nBottom = rcImage.bottom();
	for (int i = 1; i < nCountX; i++)
	{
		int nL = i * fWScreen + nLeft;
		dc.drawLine(QLine(QPoint(nL, nTop), QPoint(nL, nBottom)));
	}
	for (int i = 1; i < nCountY; i++)
	{
		int nT = i * fHScreen + nTop;
		dc.drawLine(QLine(QPoint(nLeft, nT), QPoint(nRight, nT)));
	}
}

void YDCaptureRect::setGridColor(QColor clr)
{
	m_clrGrid = clr;
	updateDraw();
}

void YDCaptureRect::setPenColor(QColor clr)
{
	m_clrPen = clr;
	updateDraw();
}

void YDCaptureRect::setGridLineWidth(quint16 nW)
{
	m_nGridLineW = nW;
	updateDraw();
}

QSize YDCaptureRect::getGridSize(void)
{
	return QSize(m_nGridWidth_Image, m_nGridHeight_Image);
}