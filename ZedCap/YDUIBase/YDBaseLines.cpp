
#include "YDBaseLines.h"


//这里给出100种颜色
int common_color[] = {
	255, 0, 255,
	0, 255, 255,
	116, 148, 31,
	5, 194, 10,
	209, 135, 117,
	113, 163, 222,
	157, 53, 238,
	202, 97, 67,
	235, 200, 41,
	188, 174, 223,
	45, 118, 61,
	103, 145, 165,
	239, 203, 247,
	234, 15, 170,
	105, 154, 222,
	228, 13, 3,
	15, 106, 35,
	90, 78, 209,
	207, 223, 110,
	3, 4, 227,
	35, 196, 187,
	52, 248, 175,
	51, 252, 88,
	154, 201, 42,
	69, 112, 40,
	51, 127, 49,
	4, 55, 108,
	190, 164, 218,
	113, 82, 125,
	238, 245, 208,
	119, 185, 117,
	107, 105, 117,
	216, 190, 115,
	134, 68, 105,
	52, 112, 230,
	171, 238, 1,
	214, 174, 76,
	5, 54, 13,
	174, 214, 177,
	97, 160, 166,
	212, 34, 251,
	128, 53, 141,
	181, 155, 102,
	109, 161, 51,
	78, 94, 159,
	48, 147, 187,
	49, 115, 96,
	174, 11, 3,
	77, 7, 107,
	138, 80, 192,
	38, 3, 202,
	178, 98, 235,
	96, 174, 215,
	219, 24, 94,
	218, 9, 158,
	151, 156, 186,
	127, 155, 49,
	229, 4, 231,
	210, 4, 145,
	164, 48, 161,
	209, 150, 60,
	168, 15, 140,
	87, 94, 238,
	74, 161, 85,
	87, 183, 167,
	136, 177, 100,
	185, 21, 160,
	79, 116, 178,
	214, 113, 101,
	145, 90, 105,
	94, 39, 167,
	179, 172, 214,
	139, 178, 95,
	113, 186, 108,
	177, 122, 152,
	158, 141, 144,
	203, 31, 183,
	244, 115, 130,
	133, 183, 198,
	224, 228, 125,
	44, 70, 47,
	250, 65, 179,
	69, 221, 251,
	64, 59, 206,
	223, 205, 179,
	188, 232, 124,
	35, 59, 29,
	3, 61, 170,
	228, 13, 93,
	51, 20, 36,
	76, 163, 145,
	169, 49, 210,
	73, 215, 172,
	120, 44, 255,
	17, 44, 245,
	252, 254, 15,
	255, 255, 0,
	255, 0, 0,
	0, 255, 0,
	0, 0, 255
};

#pragma execution_character_set("utf-8")
YDBaseLines::YDBaseLines()
	: m_bLBtnDown(false)
	, m_bVisiPaint(true)
	, m_Alph(150)
	, m_nR(10)
	, m_bLeft(true)
	, m_eType(eT_Line)
	, m_bEnable(true)
	, m_clrPen(QColor(255, 0, 0))
{
	
}


YDBaseLines::~YDBaseLines()
{
}

void YDBaseLines::drawSomeThings(QPainter& dc, const QRect& rect)
{
	if (!m_bEnable)
	{
		return;
	}

	//drawLines(dc);

	/*dc.setBrush(m_clrPen);
	dc.setPen(QPen(m_clrPen, 2.0));
	switch (m_eType)
	{
	case YDBaseLines::eT_Point:
	break;
	case YDBaseLines::eT_Line:
	drawInfos(dc, rect);
	break;
	default:
	break;
	}*/
}

void YDBaseLines::drawInfos(QPainter& dc, const QRect& rect)
{
	int nLeft = rect.left();
	int nTop = rect.top() + 10;
	int nW = 200;
	int nH = 40;
	int nDelta = 20;
	QFont contextfont(QObject::tr("微软雅黑"), 9);
	QFont oldFont(dc.font());
	dc.setFont(contextfont);
	int idx = 0;
	for (auto iter : m_vStrInfos)
	{
		dc.drawText(QRect(nLeft, nTop + nDelta*idx, nW, nH), iter);
		++idx;
	}
	dc.setFont(oldFont);
}

void  YDBaseLines::drawLines(QPainter& dc)
{
	int kk = 0;
    //for (const auto& iter : m_vSearchLine)
    for(auto iter = m_vSearchLine.begin();iter!=m_vSearchLine.end();++iter)
	{
		dc.setBrush(Qt::NoBrush);
		const std::vector<MPOINT2D>* pinHead = nullptr;
		const std::vector<MPOINT2D>* pinHead_error = nullptr;
		if (m_bLeft)
		{
            pinHead = &(iter->PinHeadL);
            pinHead_error = &(iter->pinHead_error_L);
		}
		else
		{
            pinHead = &(iter->PinHeadR);
            pinHead_error = &(iter->pinHead_error_R);
		}
		if (pinHead->size())
		{
			int color_flag = kk * 3;
			QPen mpen(QColor(common_color[color_flag], common_color[color_flag + 1], common_color[color_flag + 2]));
			dc.setPen(mpen);

			for (int i = 0; i < pinHead->size(); i++)
			{
				double j = 0;// (float)(imageToScreen((iter.PinHeadL[i].x) + 0.5)/* * m_pMainFrm->m_ratio*/);
				double k = 0;// (float)(imageToScreen((iter.PinHeadL[i].y) + 0.5)/* * m_pMainFrm->m_ratio*/);
				imageCoord2ScreenCoord((pinHead->at(i).x) + 0.5, (pinHead->at(i).y) + 0.5, j, k);

				dc.drawLine(QPointF(j, k - 6), QPointF(j, k + 6));
				dc.drawLine(QPointF(j - 6, k), QPointF(j + 6, k));

				dc.drawText(QPointF(j - 5, k - 25), QString::number(i + 1));
				//CString str;
				//str.Format("%d", i + 1);
				///memdc->TextOut(j - 5 * m_pMainFrm->m_ratio, k - 25 * m_pMainFrm->m_ratio, str);

				dc.drawRect(QRectF(QPointF(j - 10, k - 10), QPointF(j + 10, k + 10)));
				//CPoint pt1((j - 10 * m_pMainFrm->m_ratio), (k - 10 * m_pMainFrm->m_ratio));
				//CPoint pt2((j + 10 * m_pMainFrm->m_ratio), (k + 10 * m_pMainFrm->m_ratio));
				//memdc->Rectangle(CRect(pt1, pt2));
				//memdc->SelectObject(pOldBrush);
			}
		}
		if (pinHead_error->size())
		{
			//CPen redpen(PS_SOLID, 1, RGB(255, 0, 0));
			//pOldPen = memdc->SelectObject(&redpen);
			for (int rr = 0; rr < pinHead_error->size(); rr++)
			{
				double j = 0;// (float)(imageToScreen(pinHead_error->at(rr).x + 0.5)/**m_pMainFrm->m_ratio*/);
				double k = 0;// (float)(imageToScreen(iter.pinHead_error_L[rr].y + 0.5)/**m_pMainFrm->m_ratio*/);
				imageCoord2ScreenCoord(pinHead_error->at(rr).x + 0.5, pinHead_error->at(rr).y + 0.5, j, k);
				dc.drawLine(QPointF(j, k - 6), QPointF(j, k + 6));
				dc.drawLine(QPointF(j - 6, k), QPointF(j + 6, k));

				//CPoint pt1((j - 10 * m_pMainFrm->m_ratio), (k - 10 * m_pMainFrm->m_ratio));
				//CPoint pt2((j + 10 * m_pMainFrm->m_ratio), (k + 10 * m_pMainFrm->m_ratio));
				//memdc->Rectangle(CRect(pt1, pt2));
				dc.drawRect(QRectF(QPointF(j - 10, k - 10), QPointF(j + 10, k + 10)));
			}
		}
		kk++;
	}
}

void YDBaseLines::LButtonDown(const QPointF& point)
{
	m_bLBtnDown = true;
}

void YDBaseLines::LButtonUp(const QPointF& point)
{
	m_bLBtnDown = false;

	if (!m_bEnable)
	{
		return;
	}

	switch (m_eType)
	{
	case YDBaseLines::eT_Point:
		break;
	case YDBaseLines::eT_Line:
		break;
	default:
		break;
	}
	UpdateThings();
}

void YDBaseLines::RButtonDown(const QPointF& point)
{

}

void YDBaseLines::RButtonUp(const QPointF& point)
{
	if (!m_bEnable)
	{
		return;
	}

	UpdateThings();
}

void YDBaseLines::MouseMove(const QPointF& point)
{
	if (!m_bEnable)
	{
		return;
	}
	if (m_bLBtnDown)
	{
		double fX = 0;
		double fY = 0;
		screenCoord2ImageCoord(point.x(), point.y(), fX, fY);

		//m_curPen.push_back(tagPen(fX, fY, screenToImage(m_nR)));
	}

	m_rcMouse.setRect(point.x() - m_nR, point.y() - m_nR, 2 * m_nR, 2 * m_nR);

	UpdateThings();
}

void YDBaseLines::imageCoord2ScreenCoord(double fImageX, double fImageY, double& fX, double& fY)
{


}

void YDBaseLines::screenCoord2ImageCoord(int nX, int nY, double& fImageX, double& fImageY)
{

}

double YDBaseLines::imageToScreen(double fValue)
{
	return fValue;
}

double YDBaseLines::screenToImage(double fValue)
{
	return fValue;
}

void YDBaseLines::showLines(bool bShow)
{
	m_bVisiPaint = bShow;

	UpdateThings();
}


void  YDBaseLines::enableBaseLines(bool bEnable)
{
	m_bEnable = bEnable;
	UpdateThings();
}


void YDBaseLines::setInfo(const QString& strInfo)
{
	m_strInfo = strInfo;
	UpdateThings();
}

void	YDBaseLines::setInfos(const std::vector<QString>& vStrInfos)
{
	m_vStrInfos = vStrInfos;
	UpdateThings();
}

void YDBaseLines::setSearchlines(const std::vector<searchline>& lines, bool bLeft)
{
	m_bLeft = bLeft;
	m_vSearchLine = lines;
	UpdateThings();
}
