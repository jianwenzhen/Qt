#include "YDBaseZoom.h"
#include <QWheelEvent>
#include <QMouseEvent>
#include <algorithm>
YDBaseZoom::YDBaseZoom()
	: m_fScale(1.0)
	, m_eAction(eZA_NOACTION)
	, m_bInitPos(false)
	, m_fZoomScale(1.0)
    , m_bLBtnDown(false)
	, m_fInitZoomScale(-1.0)
	, m_fThumbnailScale(1.0)
	, m_fRectPosLUX(0.0)
	, m_fRectPosLUY(0.0)
	, m_fLasfZoomScale(0.0)
	, m_bEnableDrag(true)
	, m_bNeedThumbnailWnd(true)
	, m_bShowThumbnailWnd(false)
	, m_bMouseInThumbnail(false)
	, m_bMouseInNavigationArea(false)
	, m_bImageSizeWithWinSize(false)
{
	m_szShowWnd = m_szOrgImage = QSize(0, 0);
	m_rectThumbnailNavigation.setRect(50, 50, 100, 100);
	m_rectThumbnailWndPos.setRect(0, 0, 200, 200 * 0.618);
}


YDBaseZoom::~YDBaseZoom()
{
	
}

void YDBaseZoom::wheelEvent(QWheelEvent *event)
{
	MouseWheel(event->delta(), event->posF());
}

void YDBaseZoom::mouseMoveEvent(QMouseEvent *event)
{
	MouseMove(event->localPos());
}

void YDBaseZoom::mousePressEvent(QMouseEvent *event)
{
	switch (event->button())
	{
	case Qt::LeftButton:
		LButtonDown(event->localPos());
		break;
	case Qt::RightButton:
		break;
	case Qt::MiddleButton:
		break;
	default:
		break;
	}
}

void YDBaseZoom::mouseReleaseEvent(QMouseEvent *event)
{
	switch (event->button())
	{
	case Qt::LeftButton:
		LButtonUp(event->localPos());
		break;
	case Qt::RightButton:
		break;
	case Qt::MidButton:
		break;
	default:
		break;
	}
}

void YDBaseZoom::mouseDoubleClickEvent(QMouseEvent *event)
{
	switch (event->button())
	{
	case Qt::LeftButton:
		LButtonDblClk(event->localPos());
		break;
	case Qt::RightButton:
		break;
	case Qt::MidButton:
		break;
	default:
		break;
	}
}

void YDBaseZoom::MButtonUp(const QPointF& point)
{

	//if (m_bMBtntDown)//第一次点击中键放大
	//{
	//	m_fLasfZoomScale = GetZoomScale();

	//	ZoomImage(point, 10.0);

	//	m_bInitPos = false;

	//	if (GetZoomScale() > m_fInitZoomScale)
	//	{
	//		m_bShowThumbnailWnd = TRUE;
	//	}
	//	else
	//	{
	//		m_bShowThumbnailWnd = false;
	//	}

	//}
	//else//点击中间后，单击恢复放大前比例显示
	//{
	//	ZoomImage(point, m_fLasfZoomScale);

	//	m_bInitPos = false;

	//	if (GetZoomScale() > m_fInitZoomScale)
	//	{
	//		m_bShowThumbnailWnd = TRUE;
	//	}
	//	else
	//	{
	//		m_bShowThumbnailWnd = false;
	//	}
	//}

	//UpdateDraw();
}

void YDBaseZoom::MButtonDown(const QPointF& point)
{
	if (!m_rectThumbnailWndPos.contains(point.toPoint()))
	{
		//m_bMBtntDown = !m_bMBtntDown;
	}
}

//双击放大十倍
//单击后恢复原状
void YDBaseZoom::LButtonDblClk(const QPointF& point)
{
	if (!m_bInitPos)
	{
		//m_bMBtntDown = false;
		m_bInitPos = true;
		AdjustImagePosByWindowSize();

		UpdateDraw();
	}
}

void YDBaseZoom::LButtonDown(const QPointF& point)
{
	if (m_rectThumbnailWndPos.contains(point.toPoint()))
	{
		if (m_bShowThumbnailWnd && m_rectThumbnailNavigation.contains(point.toPoint()))
		{
			m_bMouseInNavigationArea = true;
			m_bMouseInThumbnail = false;
		}
		else
		{
			m_bMouseInThumbnail = true;
			m_bMouseInNavigationArea = false;
		}
	}
	else
	{
		m_bMouseInThumbnail = false;
	}
	m_bLBtnDown = true;
	m_ptLBtnDown = point;
}

void YDBaseZoom::LButtonUp(const QPointF& point)
{
	m_bLBtnDown = false;
	m_bMouseInThumbnail = false;
	m_bMouseInNavigationArea = false;
	m_eAction = eZA_NOACTION;
}

void YDBaseZoom::MouseMove(const QPointF& point)
{
	if (m_bEnableDrag && m_bLBtnDown && !m_bInitPos && GetRectPosition().contains(point.toPoint()))
	{
		m_eAction = eZA_DRAG;
		MoveRect(m_ptLBtnDown, point);
		m_ptLBtnDown = point;
	}
}

void YDBaseZoom::MouseWheel(short zDelta, const QPointF& pt)
{
	float fValue = 1.0;
	if (zDelta >= 80)
	{
		fValue = 1.09;
	}
	else if (zDelta <= -80)
	{
		fValue = 0.9;
	}

	m_eAction = eZA_ZOOM;

	//m_bMBtntDown = false;

	//SetCursor(nullptr, 0, 0);

	ZoomImage(pt, GetZoomScale() * fValue);

	m_bInitPos = false;

	if (GetZoomScale() > m_fInitZoomScale)
	{
		m_bShowThumbnailWnd = true;
	}
	else
	{
		m_bShowThumbnailWnd = false;
	}
}

void YDBaseZoom::MouseLeave()
{
	m_bLBtnDown = false;
	m_bMouseInThumbnail = false;
	m_bMouseInNavigationArea = false;
	m_eAction = eZA_NOACTION;
}

QSize   YDBaseZoom::GetOrgImageSize(void)
{
	return m_szOrgImage;
}

void  YDBaseZoom::SetOrgImageSize(int nW, int nH)
{
	if (GetOrgImageSize().width() != nW || GetOrgImageSize().height() != nH)
	{
		m_szOrgImage = QSize(nW, nH);

		AdjustImagePos(m_szShowWnd.width(), m_szShowWnd.height());
	}
}

void  YDBaseZoom::ZoomImage(const QPointF& point, float fScale)
{
	if (fScale < 0.002)
		return;

	QSize imageOrgSize = GetOrgImageSize();
	if (imageOrgSize.isEmpty())
		return;

	//CRect rcImage(GetRectPos());
	double fLeftUpX = 0.0;
	double fLeftUpY = 0.0;
	GetRectPosLeftUp(fLeftUpX, fLeftUpY);

	double fX = 0;
	double fY = 0;
	Com_ScreenCoor2ImageCoor_S(point, fX, fY);

	SetZoomScale(fScale);

	Com_ImageCoor2ScreenCoor_S(fX, fY, fX, fY);

	double fNewImgW = imageOrgSize.width() * fScale;
	double fNewImgH = imageOrgSize.height() * fScale;
	QRectF rectZoom(fLeftUpX, fLeftUpY, fNewImgW, fNewImgH);
	rectZoom.translate(point.x() - fX, point.y() - fY);

	SetRectPosLeftUp(rectZoom.left(), rectZoom.top());

	SetRectPos(rectZoom);

	//计算导航区域在原图中的位置
	CalcNavigationPosByShowArea();
}


void  YDBaseZoom::CalcNavigationPosByShowArea(void)
{
	double fX1 = 0;
	double fY1 = 0;
	double fX2 = 0;
	double fY2 = 0;
	QRectF rectImage(GetRectPosition());
	QPointF ptLeftUp((std::max)(0.0, rectImage.left()), (std::max)(0.0, rectImage.top()));
	QPointF ptRightBottom((std::min)(m_szShowWnd.width() * 1.0, rectImage.right()), (std::min)(m_szShowWnd.height() * 1.0, rectImage.bottom()));
	Com_ScreenCoor2ImageCoor_S(ptLeftUp, fX1, fY1);
	Com_ScreenCoor2ImageCoor_S(ptRightBottom, fX2, fY2);

	m_fThumbnailScale = (m_rectThumbnailWndPos.width() * 1.0) / (GetOrgImageSize().width() * 1.0);

	float fLeft   = fX1 * m_fThumbnailScale + m_rectThumbnailPos.left();
	float fTop    = fY1 * m_fThumbnailScale + m_rectThumbnailPos.top();
	float fRight  = fX2 * m_fThumbnailScale + m_rectThumbnailPos.left();
	float fBottom = fY2 * m_fThumbnailScale + m_rectThumbnailPos.top();
	m_rectThumbnailNavigation.setRect(fLeft, fTop, fRight - fLeft, fBottom - fTop);
}

//根据导航区域位置计算主显示区显示的图像区域
void  YDBaseZoom::CalcShowAreaByNavigation(int nX, int nY)
{
	m_fRectPosLUX += (nX / m_fThumbnailScale) * GetZoomScale();
	m_fRectPosLUY += (nY / m_fThumbnailScale) * GetZoomScale();
	m_rectPositon.translate((nX / m_fThumbnailScale) * GetZoomScale(), (nY / m_fThumbnailScale) * GetZoomScale());
}

void  YDBaseZoom::Com_ScreenCoor2ImageCoor_S(double fScreenX, double fScreenY, double& fImageX, double& fImageY)
{
	double fLeftUpX = 0.0;
	double fLeftUpY = 0.0;
	GetRectPosLeftUp(fLeftUpX, fLeftUpY);
	float fScale = GetZoomScale();
	fImageX = (fScreenX - fLeftUpX) / fScale;
	fImageY = (fScreenY - fLeftUpY) / fScale;
}

void  YDBaseZoom::Com_ScreenCoor2ImageCoor_S(const QPointF& point, double& fImageX, double& fImageY)
{
	Com_ScreenCoor2ImageCoor_S(point.x(), point.y(), fImageX, fImageY);
}

void YDBaseZoom::Com_ScreenCoor2ImageCoor_S(const QPointF& pointScreen, QPointF& imagePoint)
{
	double fX = 0;
	double fY = 0;
	Com_ScreenCoor2ImageCoor_S(pointScreen.x(), pointScreen.y(), fX, fY);
	imagePoint.setX(fX);
	imagePoint.setY(fY);
}

void  YDBaseZoom::Com_ImageCoor2ScreenCoor_S(double fImageX, double fImageY, QPointF& point)
{
	double fX = 0;
	double fY = 0;
	Com_ImageCoor2ScreenCoor_S(fImageX, fImageY, fX, fY);
	point.setX(fX);
	point.setY(fY);
}

void  YDBaseZoom::Com_ImageCoor2ScreenCoor_S(double fImageX, double fImageY, double& fScreenX, double& fScreenY)
{
	double fLeftUpX = 0.0;
	double fLeftUpY = 0.0;
	GetRectPosLeftUp(fLeftUpX, fLeftUpY);
	float fScale = GetZoomScale();
	fScreenX = fImageX * fScale + fLeftUpX;
	fScreenY = fImageY * fScale + fLeftUpY;
}

void YDBaseZoom::Com_ImageCoor2ScreenCoor_S(const QPointF& imagePoint, QPointF& point/*输出屏幕坐标*/)
{
	Com_ImageCoor2ScreenCoor_S(imagePoint.x(), imagePoint.y(), point);
}


void  YDBaseZoom::InitImagePos(int nWndWidth, int nWndHeight)
{
	if (!m_bInitPos && nWndWidth * nWndHeight)
	{
		AdjustImagePos(nWndWidth, nWndHeight);

        m_bInitPos = true;
	}
}

void  YDBaseZoom::AdjustImagePos(int cx, int cy)
{
	if (cx * cy <= 0)
		return;

	m_szShowWnd = QSize(cx, cy);

	QSize imageOrgSize = GetOrgImageSize();
	if (imageOrgSize.isEmpty())
		return;

	double fImageA = (imageOrgSize.width() * 1.0) / (imageOrgSize.height() * 1.0);

	int nNewImgW = cx;
	int nNewImgH = nNewImgW / fImageA;

	if (nNewImgH > cy)
	{
		nNewImgH = cy;
		nNewImgW = nNewImgH * fImageA;
	}

	SetZoomScale((nNewImgW * 1.0) / (imageOrgSize.width() * 1.0));

	double fLeft = (cx - nNewImgW) * 1.0 / 2;
	double fTop = (cy - nNewImgH) * 1.0 / 2;
	SetRectPos(QRectF(fLeft, fTop, nNewImgW, nNewImgH));

	SetRectPosLeftUp(fLeft, fTop);

	m_bShowThumbnailWnd = false;

	//if (m_fInitZoomScale < 0)
	m_fInitZoomScale = GetZoomScale();

	SetRectPos(QRect(0, 0, 200, 200 * 0.618), eRT_THUMBNAILWND);

}

void  YDBaseZoom::AdjustImagePosByWindowSize(void)
{
	AdjustImagePos(m_szShowWnd.width(), m_szShowWnd.height());
}

void YDBaseZoom::Resize(int cx, int cy)
{
	InitImagePos(cx, cy);

	//if (m_bImageSizeWithWinSize)
	{
		AdjustImagePos(cx, cy);
	}

}

float YDBaseZoom::GetZoomScale(void)
{
	return 	m_fZoomScale;
}

void  YDBaseZoom::SetZoomScale(float fAddValue)
{
	if (fAddValue <= 0)
	{
		fAddValue = 1.0;
	}
	else if (fAddValue >= 100)
	{
		fAddValue = 100.0;
	}
	m_fZoomScale = fAddValue;
}

//获得图像矩形在窗口中的位置
QRectF YDBaseZoom::GetRectPosition(void)
{
	return  m_rectPositon;
}

QRect YDBaseZoom::GetThumbnailWndPos()
{
	return m_rectThumbnailWndPos.toRect();
}

void  YDBaseZoom::GetRectPosLeftUp(double& fX, double& fY)
{
	fX = m_fRectPosLUX;
	fY = m_fRectPosLUY;
}

void YDBaseZoom::SetRectPosLeftUp(double fX, double fY)
{
	m_fRectPosLUX = fX;
	m_fRectPosLUY = fY;
}

void  YDBaseZoom::SetRectPos(const QRectF& rect, enumRectType eRectType/* = eRT_SHOWRECT*/)
{
	switch (eRectType)
	{
	case YDBaseZoom::eRT_SHOWRECT:
	{
		m_rectPositon = rect;
		if (m_bShowThumbnailWnd)
		{
			CalcNavigationPosByShowArea();
		}
	}
	break;
	case YDBaseZoom::eRT_THUMBNAILWND:
	{
		m_rectThumbnailWndPos = rect;
	}
	break;
	case YDBaseZoom::eRT_NAVIGATION:
	{
		m_rectThumbnailNavigation = rect;
		LimitRectInRect(m_rectThumbnailPos, m_rectThumbnailNavigation);
	}
	break;
	default:
		break;
	}
}

void  YDBaseZoom::SetRectPosition(const QPointF& ptCenter)
{
	MoveRectCenter(m_rectPositon, ptCenter);
}

void  YDBaseZoom::OffsetXY(int nX, int nY)
{
	if (m_bMouseInThumbnail)
	{
		m_rectThumbnailWndPos.translate(nX, nY);

		m_rectThumbnailNavigation.translate(nX, nY);

	}
	else if (m_bMouseInNavigationArea)
	{
		m_rectThumbnailNavigation.translate(nX, nY);
		int nLimit = LimitRectInRect(m_rectThumbnailPos, m_rectThumbnailNavigation);

		//move 缩放区域在主显示区域的位置
		int nOffsetX = -nX;
		int nOffsetY = -nY;
		switch (nLimit)
		{
		case 0:
			break;
		case 1:
			nOffsetX = 0;
			break;
		case 2:
			nOffsetY = 0;
			break;
		case 3:
			nOffsetX = 0;
			nOffsetY = 0;
			break;
		default:
			break;
		}
		CalcShowAreaByNavigation(nOffsetX, nOffsetY);

	}
	else
	{
		m_fRectPosLUX += nX;
		m_fRectPosLUY += nY;
		m_rectPositon.translate(nX, nY);

		CalcNavigationPosByShowArea();
	}
}

//将矩形B限制在矩形A中
int  YDBaseZoom::LimitRectInRect(const QRectF& rectA, QRectF& rectB)
{
	int nRet = 0;
	int nRectBW = rectB.width();
	int nRectbH = rectB.height();
	if (rectB.left() < rectA.left())
	{
		nRet = 1;
		rectB.setLeft(rectA.left());
		rectB.setRight(rectA.left() + nRectBW);
	}
	else   if (rectB.right() > rectA.right())
	{
		nRet = 1;
		rectB.setRight(rectA.right());
		rectB.setLeft(rectA.right() - nRectBW);
	}

	if (rectB.top() < rectA.top())
	{
		nRet += 2;
		rectB.setTop(rectA.top());
		rectB.setBottom(rectB.top() + nRectbH);
	}
	else if (rectB.bottom() > rectA.bottom())
	{
		nRet += 2;
		rectB.setBottom(rectA.bottom());
		rectB.setTop(rectB.bottom() - nRectbH);
		
	}
	return nRet;
}

void  YDBaseZoom::MoveRect(const QPointF& ptStart, const QPointF& ptEnd)
{
	OffsetXY(ptEnd.x() - ptStart.x(), ptEnd.y() - ptStart.y());
}

void  YDBaseZoom::MoveRectCenter(QRectF& rect, const QPointF& ptCenter)
{
	int nW = rect.width();
	int nH = rect.height();
	rect.setRect(ptCenter.x() - nW / 2, ptCenter.y() - nH / 2, nW, nH);
}

void  YDBaseZoom::SetThumbnail(const QPixmap& image)
{
	if (m_bNeedThumbnailWnd && !image.isNull())
	{
		double fScale = 0.1;
		m_imgThumbnail = image.scaled(QSize(image.width() * fScale, image.height() * fScale));
	}

}

void YDBaseZoom::NeedThumbnailWnd(bool bNeed)
{
	m_bNeedThumbnailWnd = bNeed;
}

void YDBaseZoom::drawThumbnail(QPainter& painter)
{
	if (!m_bNeedThumbnailWnd || !m_bShowThumbnailWnd)
	{
		return;
	}
	if (!m_imgThumbnail.isNull())
	{
		////绘制背景
		double fScale = (m_imgThumbnail.width() * 1.0) / (m_imgThumbnail.height() * 1.0);

		int nNewImgW = m_rectThumbnailWndPos.width();
		int nNewImgH = nNewImgW / fScale;

		if (nNewImgH > m_rectThumbnailWndPos.height())
		{
			nNewImgH = m_rectThumbnailWndPos.height();
			nNewImgW = nNewImgH * fScale;
		}

		m_fThumbnailScale = (nNewImgW * 1.0) / (GetOrgImageSize().width() * 1.0);

		m_rectThumbnailPos.setRect((m_rectThumbnailWndPos.width() - nNewImgW) * 1.0 / 2 + m_rectThumbnailWndPos.left()
			, (m_rectThumbnailWndPos.height() - nNewImgH) * 1.0 / 2 + m_rectThumbnailWndPos.top()
			, nNewImgW, nNewImgH);

		painter.drawPixmap(m_rectThumbnailPos.toRect(), m_imgThumbnail);

		///绘制导航区
		drawNavigationArea(painter);
	}
	
}

void YDBaseZoom::drawNavigationArea(QPainter& painter)
{
	QPainterPath path;
	path.addRect(m_rectThumbnailWndPos);
	path.addRect(m_rectThumbnailNavigation);
	path.setFillRule(Qt::OddEvenFill);
	painter.setBrush(Qt::BrushStyle::NoBrush);
	painter.fillPath(path, QBrush(QColor(30, 30, 30, 180)));
	painter.setPen(QPen(QColor(0, 255, 0)));
	painter.drawRect(m_rectThumbnailNavigation);
}

void YDBaseZoom::enableDrag(bool bEnbale)
{
	m_bEnableDrag = bEnbale;
}

