#ifndef _YDUSKY_UI_COMMON_FUNCTION_2016_12_05_15_36_36_
#define _YDUSKY_UI_COMMON_FUNCTION_2016_12_05_15_36_36_
#include <QPixmap>
#include <QImage>
#include <QDockWidget>
#include <opencv2/opencv.hpp>

#define YDUIBASE_API

YDUIBASE_API QPixmap MatToPixmap(const cv::Mat& image, bool bRgb2bgr);

YDUIBASE_API void MatToPixmap(const cv::Mat& image, QPixmap& pixmap);

YDUIBASE_API QImage MatToQImage(const cv::Mat& image);

YDUIBASE_API void MatToQImage(const cv::Mat& image, QImage& qImage);

YDUIBASE_API QPixmap dataToPixmap(int nW, int nH, int nC, unsigned char* data);

YDUIBASE_API QImage dataToQImage(int nW, int nH, int nC, unsigned char* data);

YDUIBASE_API bool  setStyle(const QString& style);

YDUIBASE_API void  removeDockTitlebar(QDockWidget* pDockWidget);

#endif
