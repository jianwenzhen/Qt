
#include "YDCtrlPoint.h"
#include <QMouseEvent>

YDCtrlPoint::YDCtrlPoint(void)
	: m_pVCtrlPoints(nullptr)
	, m_bLButtonDown(false)
	, m_bDraging(false)
	, m_bShowTrackLine(false)
	, m_bLButtonDbClk(false)
	, m_bSelectedModifyPoint(false)
	, m_operatorType(eOT_GET_POINT)
	, m_nCurCtrlPointIndex(0)
	, m_eShapeType(eST_POINT)
{

}


YDCtrlPoint::~YDCtrlPoint(void)
{

}

void YDCtrlPoint::drawCtrlPoint(QPainter& dc)
{
	if (nullptr == m_pVCtrlPoints )
		return;

	tagCtrlPoint tempInfo;
	QColor textColor(0, 255, 0);
	Qt::PenStyle nPenStyle = Qt::SolidLine;
    int nBlod     = 1;
	for (int i = 0; i < GetCtrlPointSize(); i++)
	{
		if(getCtrlPoint(i, tempInfo))
		{
			if (tempInfo.m_point.x() < 0 || tempInfo.m_point.y() < 0)
			{
				continue;
			}

			textColor = QColor(98, 255, 34);
			nPenStyle = Qt::SolidLine;

            switch(GetOperatorType())
            {
            case eOT_NOOPERATOR:
                break;
            case eOT_GET_POINT:
                break;
            case eOT_MODIDY_POINT:
                {
                    if (tempInfo.m_nIndex == GetCurCtrlPointIndex() - 1)
                    {
						textColor = QColor(255, 0, 0);
                        nPenStyle = Qt::DotLine;
                        nBlod     = 1;
                    } 
                }
                break;
            case eOT_SELECT_POINT:
			{
				if (tempInfo.m_nIndex == GetCurCtrlPointIndex() - 1)
				{
					textColor = QColor(200, 0, 200);
					nPenStyle = Qt::DotLine;
					nBlod = 1;
				}
			}
			break;
                break;
            default:
                break;
            }
		    
			dc.setPen(nPenStyle);
			dc.setPen(QPen(textColor));
			dc.drawLine(QPoint(tempInfo.m_point.x() - 10, tempInfo.m_point.y())
				, QPoint(tempInfo.m_point.x() + 10, tempInfo.m_point.y()));
			dc.drawLine(QPoint(tempInfo.m_point.x(), tempInfo.m_point.y() - 10)
				, QPoint(tempInfo.m_point.x(), tempInfo.m_point.y() + 10));
			QPoint ptCenter(tempInfo.m_point.x() + 30, tempInfo.m_point.y() - 18);

			
			QRectF rc(ptCenter.x() - 20, ptCenter.y() - 10, 40, 20);
			QString strInfo = QString::number(tempInfo.m_nIndex);
			QRectF pos = dc.boundingRect(rc, strInfo);

			dc.drawLine(tempInfo.m_point, pos.bottomLeft());

			dc.drawRoundRect(pos);
			dc.drawText(pos, strInfo);
		}	
	}
}

void YDCtrlPoint::DrawMouseCross(QPainter& dc, const QRect& rcClient)
{
	QPen pen(QBrush(QColor(255, 0, 0)), 1);
	dc.setPen(pen);
	dc.drawLine(QPoint(m_ptMosuse.x(), 0), QPoint(m_ptMosuse.x(), rcClient.bottom()));
	dc.drawLine(QPoint(0, m_ptMosuse.y()), QPoint(rcClient.right(), m_ptMosuse.y()));
}

int YDCtrlPoint::GetCtrlPointSize(void)
{
	int nReturn = 0;
	do 
	{
		if (nullptr == m_pVCtrlPoints)
			break;

		nReturn = m_pVCtrlPoints->size();

	} while (0);

	return nReturn;
}

int  YDCtrlPoint::GetValidateCtrlPointCount()
{
	int nReturn = 0;
	do 
	{
		if (nullptr == m_pVCtrlPoints)
			break;

		for (size_t i = 0; i < m_pVCtrlPoints->size(); i++)
		{
			if (m_pVCtrlPoints->at(i).m_point.x() >= 0 && m_pVCtrlPoints->at(i).m_point.y() >=0)
			{
				nReturn++;
			}
		}

	} while (0);

	return nReturn;
}

bool YDCtrlPoint::getCtrlPoint(int nIndex, tagCtrlPoint& ctrlPointInfo)
{
	bool bRet = false;
	do 
	{
		if (nullptr == m_pVCtrlPoints)
			break;

		if (nIndex >= 0 && nIndex < m_pVCtrlPoints->size())
		{
			ctrlPointInfo = m_pVCtrlPoints->at(nIndex);
			ctrlPointInfo.m_point = Image_2_Screen(ctrlPointInfo.m_point);

			bRet = true;
		}

	} while (0);

	return bRet;
}

void YDCtrlPoint::setCtrlPoints(VCtrlPoints& vCtrlPoints)
{
	m_pVCtrlPoints = &vCtrlPoints;
}

void YDCtrlPoint::AddScreenPoint(const tagCtrlPoint& screenPoint)
{
	AddImagePoint(tagCtrlPoint(Screen_2_Image(screenPoint.m_point), screenPoint.m_nIndex));
}

void YDCtrlPoint::AddImagePoint(const tagCtrlPoint& imagePoint)
{
	switch(m_eShapeType)
	{
	case eST_POINT:
		if (m_pVCtrlPoints)
		{
			m_pVCtrlPoints->push_back(imagePoint);
			addPointEvent(imagePoint.m_nIndex);
		}
		break;
	case eST_LINE:
		{
			
		}
		break;
	default:
		break;
	}
	UpdateDraw();
}

int  YDCtrlPoint::GetCurCtrlPointIndex(void)
{
	return m_nCurCtrlPointIndex;
}

void YDCtrlPoint::SetModifyCtrlPointIndex(int nIndex)
{
    m_bSelectedModifyPoint = true;
    setCurCtrlPointIndex(nIndex);
}

void YDCtrlPoint::setCurCtrlPointIndex(int nIndex)
{
	m_nCurCtrlPointIndex = nIndex;

    UpdateDraw();
}

void YDCtrlPoint::LButtonDblClk(const QPointF& point)
{
	
}

//mouse function
void YDCtrlPoint::LButtonDown(const QPointF& point)
{
	if (eOT_NOOPERATOR != GetOperatorType())
	{
		m_ptLeftDown = point;

		m_bLButtonDown = true;

		if (eOT_SELECT_POINT == m_operatorType)//选点
		{
			setCurCtrlPointIndex(GetSelectCtrlPointIndex(Screen_2_Image(point)));

		}else if (eOT_MODIDY_POINT == m_operatorType)
		{
			if (!m_bSelectedModifyPoint)//进入修改模式，第一次选中想要修改的点
			{
				if (!GetCurCtrlPointIndex())
				{
					int nIndex = GetSelectCtrlPointIndex(Screen_2_Image(point));
					if (nIndex)
					{
						m_bSelectedModifyPoint = true;
						setCurCtrlPointIndex(nIndex);
					}
				}
			}
			else 	//第二次进来
			{
				m_bSelectedModifyPoint = false;
			}
		}
	}
}

void YDCtrlPoint::LButtonUp(const QPointF& point)
{
	if (eOT_NOOPERATOR != GetOperatorType())
	{
		if (m_bDraging)//此时为拖动画面，不提点\不选点
		{
			m_bDraging = false;
		}
		else if (eOT_GET_POINT == m_operatorType)//提点
		{
			int nIdex = m_nCurCtrlPointIndex + 1;
			AddScreenPoint(tagCtrlPoint(point, nIdex));
			setCurCtrlPointIndex(nIdex);
		}
		else if (eOT_MODIDY_POINT == m_operatorType)
		{
			if (!m_bSelectedModifyPoint)//修改模式,第二次点击，表示用该位置更新需要修改的点
			{
				if (GetCurCtrlPointIndex())//已经选好了点
				{
					ModifyCtrlPointCoord(Screen_2_Image(point));
					//setCurCtrlPointIndex(0);
				}
			}
		}

		m_bLButtonDown = false;
		UpdateDraw();
	}

}

void YDCtrlPoint::MouseMove(const QPointF& point)
{
	if (eOT_NOOPERATOR == GetOperatorType())
		return;

	if (m_bLButtonDown && (abs(point.x() - m_ptLeftDown.x()) >= 3 || abs(point.y() - m_ptLeftDown.y()) >= 3))
	{
		m_bDraging = true;
	}
	else
	{
		m_ptMosuse = point;
	}
}

//bool YDCtrlPoint::SetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
//{
//	HCURSOR hCursor = nullptr;
//	switch (GetOperatorType())
//	{
//	case eOT_GET_POINT:
//	case eOT_MODIDY_POINT:
//       
//        hCursor = m_hCursorCross;
//
//		break;
//	case eOT_SELECT_POINT:
//		hCursor = ::LoadCursor(nullptr, IDC_HAND);
//		break;
//
//	case eOT_NOOPERATOR:
//	default:
//		break;
//	}
//
//	if(hCursor)
//		::SetCursor(hCursor);
//
//	return (nullptr != hCursor);
//}


//修改选中控制点的序号
void YDCtrlPoint::ModifyCtrlPointIndex(int nIndex)
{
	int nSelectIndex = 0;
	if ((nSelectIndex = GetCurCtrlPointIndex()) > 0)
	{
		tagCtrlPoint tempInfo;
		VCtrlPoints::iterator iter = m_pVCtrlPoints->begin();
		for (int i = 0; i < GetCtrlPointSize(); i++, ++iter)
		{
			if (getCtrlPoint(i, tempInfo))
			{
				if (nSelectIndex - 1 == tempInfo.m_nIndex)
				{
					iter->m_nIndex = nIndex;

					break;
				}
			}
		}
	}
}

//修改选中点的坐标
void YDCtrlPoint::ModifyCtrlPointCoord(const QPointF& point)
{
	int nSelectIndex = 0;
	if ((nSelectIndex = GetCurCtrlPointIndex()) > 0)
	{
		tagCtrlPoint tempInfo;
		VCtrlPoints::iterator iter = m_pVCtrlPoints->begin();
		for (int i = 0; i < GetCtrlPointSize(); i++, ++iter)
		{
			if (getCtrlPoint(i, tempInfo))
			{
				if (nSelectIndex - 1 == tempInfo.m_nIndex)
				{
					iter->m_point = point;
					break;
				}
			}
		}
	}
}

void YDCtrlPoint::removePoint(void)
{
	int nIndex = 0;
	if ((nIndex = GetCurCtrlPointIndex()) > 0)
	{
		tagCtrlPoint tempInfo;
		VCtrlPoints::iterator iter = m_pVCtrlPoints->begin();
		for (int i = 0; i < GetCtrlPointSize(); i++, ++iter)
		{
			if (getCtrlPoint(i, tempInfo))
			{
				if (nIndex - 1 == tempInfo.m_nIndex)
				{
					m_pVCtrlPoints->erase(iter);
					
					if (eOT_GET_POINT == m_operatorType)//如果当前正在提点，删除时，将会减少一次累加
					{
						int nIdex = m_nCurCtrlPointIndex - 1;
						setCurCtrlPointIndex(nIdex);
					}
					UpdateDraw();
					break;
				}
			}
		}
	}
}

void YDCtrlPoint::ClearCtrlPoint(void)
{
	do 
	{
		if (nullptr == m_pVCtrlPoints)
			break;
		
		m_pVCtrlPoints->clear();

	} while (0);
	
	setCurCtrlPointIndex(0);

	UpdateDraw();

}

void YDCtrlPoint::setOperatorType(enOperatorType type)
{
	m_operatorType = type;
    if (eOT_GET_POINT == m_operatorType)
    {
        ClearCtrlPoint();
    }
	UpdateDraw();
}

YDCtrlPoint::enOperatorType YDCtrlPoint::GetOperatorType(void)
{
	return m_operatorType;
}

int  YDCtrlPoint::GetSelectCtrlPointIndex(const QPointF& point)
{
	int nMinIndex = 0;
	if (nullptr == m_pVCtrlPoints)
		 return nMinIndex;

	double fMinDistance = 1000.0;
	for (VCtrlPoints::const_iterator constIter = m_pVCtrlPoints->begin();
		constIter != m_pVCtrlPoints->end();
		++constIter)
	{
		double fDistance = (point.x() - constIter->m_point.x()) * (point.x() - constIter->m_point.x())
			+ (point.y() - constIter->m_point.y()) * (point.y() - constIter->m_point.y());
		
		if (fMinDistance > sqrtf(fDistance))
		{
			nMinIndex = constIter->m_nIndex;
			fMinDistance = sqrtf(fDistance);
		}
	}

	if (fMinDistance >= 80.0)//大于20个像素认为没有选择点
	{
		nMinIndex = -1;
	}

	return nMinIndex + 1;
}


void YDCtrlPoint::SetModifyCtrlPointIndex(const QPoint& point)
{
    if ( eOT_MODIDY_POINT == m_operatorType)
    {
		int nIndex = GetSelectCtrlPointIndex(Screen_2_Image(point));
        if (nIndex)
        {
            m_bSelectedModifyPoint = true;
            setCurCtrlPointIndex(nIndex);
            UpdateDraw();
        }
    }
}

int YDCtrlPoint::GetSelectCtrlPointIndex(const QPoint& pointScreen)
{
	return GetSelectCtrlPointIndex(Screen_2_Image(pointScreen));
}

void YDCtrlPoint::mouseMoveEvent(QMouseEvent *event)
{
	MouseMove(event->localPos());
}

void YDCtrlPoint::mousePressEvent(QMouseEvent *event)
{
	switch (event->button())
	{
	case Qt::LeftButton:
		LButtonDown(event->localPos());
		break;
	case Qt::RightButton:
		break;
	case Qt::MiddleButton:
		break;
	default:
		break;
	}
}

void YDCtrlPoint::mouseReleaseEvent(QMouseEvent *event)
{
	switch (event->button())
	{
	case Qt::LeftButton:
		LButtonUp(event->localPos());
		break;
	case Qt::RightButton:
		break;
	case Qt::MidButton:
		break;
	default:
		break;
	}
}

void YDCtrlPoint::mouseDoubleClickEvent(QMouseEvent *event)
{
	switch (event->button())
	{
	case Qt::LeftButton:
		LButtonDblClk(event->localPos());
		break;
	case Qt::RightButton:
		break;
	case Qt::MidButton:
		break;
	default:
		break;
	}
}
