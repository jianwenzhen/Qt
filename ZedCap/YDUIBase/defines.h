#ifndef _DEFINES_H_PARACHUTE_DETECT_FILE_HEADER_
#define _DEFINES_H_PARACHUTE_DETECT_FILE_HEADER_
#include <vector>
#include <QPainter>

struct MPOINT2D
{
    int id;
    double x;
    double y;
    MPOINT2D()
    {
        x = y = 0;
        id = -1;
    }
    MPOINT2D(double xx, double yy)
    {
        x = xx;
        y = yy;
        id = -1;
    }
};
struct MPOINT3D
{
    double x;
    double y;
    double z;
    int No;
    MPOINT3D()
    {
        x = y = z = 0;
        No = -1;
    }
    MPOINT3D(double xx, double yy, double zz)
    {
        x = xx;
        y = yy;
        z = zz;
        No = -1;
    }
};
struct tagXPoint
{
    int Id;
    int nValid;
    MPOINT2D pt2D;
    MPOINT2D pt2D_R;
    MPOINT3D pt3D;
};
struct Edge
{
    std::vector<MPOINT2D> ptArr;
};

struct scanline
{
    int xmin;
    int xmax;
    int y;

    int ymin;
    int ymax;
    int x;
};

struct Conic//椭圆或圆结构体
{
    double x0;
    double y0;

    double X0;
    double Y0;
    double Z0;

    //	double *para;
    double A, B, C, D, E, F;//一般方程参数
    //x^2+y^2+Dx+Ey+F=0圆参数
    ////a*x^2+b*xy+c*y^2+dx+ey+1=0椭圆参数

    double r;//圆特征值
    double R;

    double a, b, alpha;//椭圆特征值

    double startsita;//圆弧起点
    double endsita;//圆弧终点

    int No;

    std::vector<MPOINT2D>pt2D;
    std::vector<MPOINT3D>pt3D;

    Conic()
    {
        x0 = y0 = 0;
        X0 = Y0 = Z0 = 0;
        r = R = a = b = alpha = 0;
        No = -1;
        //	para=NULL;
        pt2D.clear();
        pt3D.clear();
        A = B = C = D = E = F = 0;
    }
    ~Conic()
    {
        pt2D.clear();
        pt3D.clear();
        // 		if(para!=NULL)
        // 		{
        // 			delete para;
        // 			para=NULL;
        // 		}
    }
    void clear()
    {
        pt2D.clear();
        pt3D.clear();
        // 		if(para!=NULL)
        // 		{
        // 			delete para;
        // 			para=NULL;
        // 		}
    }

};
struct SHOW_INFO
{
    long srcx;  //左上角坐标
    long srcy;

    double x_scale;  //缩放因子
    double y_scale;

    QRect pClipRect;  //绘图区域
    SHOW_INFO()
    {
        srcx = srcy = 0;
        x_scale = y_scale = 1.0;

    }
};

struct CErrorRect// 矩形模板，从中提取直线
{

    double m_x1, m_y1, m_x2, m_y2, m_dis;
    CErrorRect()
    {
        m_x1 = m_y1 = m_x2 = m_y2 = -1;
        m_dis = 10;
    }
    void SetX1Y1(double x, double y)
    {
        m_x1 = x; m_y1 = y;
    }
    void SetX2Y2(double x, double y)
    {
        m_x2 = x; m_y2 = y;
    }
    void SetDis(double x, double y)
    {
        double len = sqrt((m_x2 - m_x1)*(m_x2 - m_x1) + (m_y2 - m_y1)*(m_y2 - m_y1));
        double S = m_x1*m_y2 - m_x2*m_y1 - x*m_y2 + y*m_x2 + x*m_y1 - y*m_x1;
        if (fabs(len)<0.01)
        {
            m_dis = 0;
        }
        m_dis = S / len;
    }
    void DrawMe(QPainter& dc, double _ratio)
    {
        double x1, x2, y1, y2, x, y;
        x1 = ((m_x1*_ratio));
        y1 = ((m_y1*_ratio));
        x2 = ((m_x2*_ratio));
        y2 = ((m_y2*_ratio));
        double len = sqrt((m_x2 - m_x1)*(m_x2 - m_x1) + (m_y2 - m_y1)*(m_y2 - m_y1));
        if (fabs(len)<0.01)
        {
            return;
        }
        x = (m_dis*(m_y2 - m_y1) / len*_ratio);
        y = (m_dis*(m_x1 - m_x2) / len*_ratio);
        if (m_x1 != -1 && m_x2 != -1)
        {
            //			pMemDC->MoveTo(x1,y1); pMemDC->LineTo(x2,y2);
            //pMemDC->MoveTo(x1-x,y1-y);
            //pMemDC->LineTo(x1+x,y1+y);
            //pMemDC->LineTo(x2+x,y2+y);
            //pMemDC->LineTo(x2-x,y2-y);
            //pMemDC->LineTo(x1-x,y1-y);
            dc.drawLine(QPointF(x1 - x, y1 - y), QPointF(x1 + x, y1 + y));
            dc.drawLine(QPointF(x1 + x, y1 + y), QPointF(x2 + x, y2 + y));
            dc.drawLine(QPointF(x2 + x, y2 + y), QPointF(x2 - x, y2 - y));
            dc.drawLine(QPointF(x2 - x, y2 - y), QPointF(x1 - x, y1 - y));
        }
    }
};

struct searchline
{
    std::vector<MPOINT2D>PinHeadL, PinHeadR;
    std::vector<MPOINT3D>PinHead3D;
    std::vector<CErrorRect> error_rects_l;
    std::vector<CErrorRect> error_rects_r;
    std::vector<MPOINT2D> pinHead_error_L, pinHead_error_R;
    double dis_total;
    double uniformity;
    double density;
    bool    b_OK;                   //检测结果是否合格
    std::vector<MPOINT2D> FPeak;//(上半)部分峰值
    std::vector<MPOINT2D> SPeak;//(下半)部分峰值
    char LineType;                  //0:直线形；1：锯齿形；2：混合形
    char LeftLineType;                   //0:直线形；1：锯齿形；2：混合形
    char RightLineType;                 //0:直线形；1：锯齿形；2：混合形
    searchline()
    {
        PinHeadL.clear();
        PinHeadR.clear();
        PinHead3D.clear();
        error_rects_l.clear();
        error_rects_r.clear();
        pinHead_error_L.clear();
        pinHead_error_R.clear();
        dis_total = 0;
        uniformity = 0;
        density = 0;
        FPeak.clear();
        SPeak.clear();
        b_OK = true;
        LineType = 0;
    }
    ~searchline()
    {
        PinHeadL.clear();
        PinHeadR.clear();
        PinHead3D.clear();
        error_rects_l.clear();
        error_rects_r.clear();
        pinHead_error_L.clear();
        pinHead_error_R.clear();
        FPeak.clear();
        SPeak.clear();
    }
    void clear()
    {
        PinHeadL.clear();
        PinHeadR.clear();
        PinHead3D.clear();
        error_rects_l.clear();
        error_rects_r.clear();
        pinHead_error_L.clear();
        pinHead_error_R.clear();
        dis_total = 0;
        FPeak.clear();
        SPeak.clear();
        b_OK = true;
    }
};

struct MCOLOR
{
    float R;
    float G;
    float B;
    float H;
    float I;
    float S;
    float gray;
    long m_tag;//0——表示该样本为黑色样本，1——表示白色样本，其他表示彩色样本
    MCOLOR()
    {
        R = G = B = 0;
        H = I = S = 0;
        gray = 0;
        m_tag = 2;
    }
    MCOLOR(float mR, float mG, float mB, float mH, float mI, float mS, float mgray)
    {
        R = mR;
        G = mG;
        B = mB;
        H = mH;
        I = mI;
        S = mS;
        gray = mgray;
    }
};

struct SelectArea
{
    QPoint left_up_pt;
    QPoint right_down_pt;
    SelectArea()
    {

    }
    SelectArea(QPoint pt1, QPoint pt2)
    {
        left_up_pt = pt1;
        right_down_pt = pt2;
    }

};

struct SampleTrainResult
{
    double vector[3];
    double matrix[9];
};

enum LINETYPE{ line_type, curve_type, line_curve_type };//
struct ObjectType
{
    QString TypeName;                                  //类型名称
    int     ThreadType;                                //线型(0:直线形 1:锯齿形 2:混合形)
    int     bgSampleNum;                               //背景样本数量
    int     threadSampleNum;                           //线样本数量
    std::vector  <SampleTrainResult> bgSample;              //背景样本训练结果
    std::vector  <SampleTrainResult> threadSample;          //线样本训练结果
    std::vector  <int> NoInBgSample;                        //每个线样本对应所在的背景样本号
    std::vector  <int> LineNumPerThreadSample;              //每个线样本包含缝纫线的条数
    std::vector  <LINETYPE> ThreadTypes;
    void clear()
    {
        TypeName.clear();
        ThreadType = 0;
        bgSampleNum = 0;
        threadSampleNum = 0;

/// error: invalid initialization of non-const reference of type 'std::vector<SampleTrainResult>&' from an rvalue of type 'std::vector<SampleTrainResult>'                                                   ^
//        bgSample.swap(std::vector<SampleTrainResult>());
//        threadSample.swap(std::vector<SampleTrainResult>());
//        LineNumPerThreadSample.swap(std::vector<int>());
//        ThreadTypes.swap(std::vector<LINETYPE>());
        NoInBgSample.assign(20, -1);
    }
    ObjectType()
    {
        ThreadType = 0;
        bgSampleNum = 0;
        threadSampleNum = 0;
        NoInBgSample.resize(20);  //每类检测对象中线样本不会超过20条
        NoInBgSample.assign(20, -1);
    }
    ~ObjectType()
    {
//        bgSample.swap(std::vector<SampleTrainResult>());
//        threadSample.swap(std::vector<SampleTrainResult>());
//        NoInBgSample.swap(std::vector<int>());
//        LineNumPerThreadSample.swap(std::vector<int>());
//        ThreadTypes.swap(std::vector<LINETYPE>());
    }
};

typedef std::vector<ObjectType>   VObjects;

struct ImgProcess
{
    int nNumOfProcessors;//CPU核数
    int nThreadID;//当前线程号
    char *Img;//影像块
    bool GrayScale;
    int h;//影像高
    int w;//影像宽
    bool Y_direction;
    int *bFinish;//线程完成标志
    std::vector<searchline>multilines_pinheads;
    int lins_num;//检测多条缝隙线标记
    SelectArea inspect_area;
    bool is_rgb_mode;
    ObjectType m_ObjectType;
    ImgProcess()
    {
        nNumOfProcessors = 0;
        nThreadID = 0;
        Img = nullptr;
        h = 0;
        w = 0;
        Y_direction = 0;
        bFinish = 0;
        GrayScale = false;
        multilines_pinheads.clear();
        lins_num = 1;
        is_rgb_mode = true;
    }
};

struct InteriorParameter//内方位元素
{
    int w, h;
    double f, fx, fy;
    double x0, y0;
    double pixelSize;
    double k1, k2, p1, p2;//畸变参数
    double k3, k4, s1, s2, s;//扩展参数
    InteriorParameter()
    {
        w = h = 0;
        f = fx = fy = 0;
        x0 = y0 = 0;
        pixelSize = 0;
        k1 = k2 = p1 = p2 = 0;
        k3 = k4 = s = s1 = s2 = 0;
    }

    void init()
    {
        InteriorParameter();
    }

    void digtalToMetric(double *x, double *y)
    {
        double tx = *x, ty = *y;
        *x = tx - x0;  *y = h - ty - y0;
    }
    void metricToDigital(double *x, double *y)
    {
        double tx = *x, ty = *y;
        *x = tx + x0;  *y = h - ty - y0;
    }
};

struct ExteriorParameter//内方位元素
{
    double Xs, Ys, Zs;
    double Tx, Ty, Tz;
    double phi, omega, kappa;
    double r[9];
    double a1, a2, a3, b1, b2, b3, c1, c2, c3;
    bool bRotMatrix;

    ExteriorParameter()
    {
        Xs = Ys = Zs = 0.0;
        Tx = Ty = Tz = 0.0;
        phi = omega = kappa = 0.0;
        a1 = a2 = a3 = b1 = b2 = b3 = c1 = c2 = c3 = 0;
        //angle2rotmatrix();
        bRotMatrix = false;
    }

    void init()
    {
        ExteriorParameter();
    }

    void angle2rotmatrix()
    {
        double  f = phi, w = omega, k = kappa;

        a1 = r[0] = cos(f)*cos(k) - sin(f)*sin(w)*sin(k);
        a2 = r[1] = -cos(f)*sin(k) - sin(f)*sin(w)*cos(k);
        a3 = r[2] = -sin(f)*cos(w);
        b1 = r[3] = cos(w)*sin(k);
        b2 = r[4] = cos(w)*cos(k);
        b3 = r[5] = -sin(w);
        c1 = r[6] = sin(f)*cos(k) + cos(f)*sin(w)*sin(k);
        c2 = r[7] = -sin(f)*sin(k) + cos(f)*sin(w)*cos(k);
        c3 = r[8] = cos(f)*cos(w);
        bRotMatrix = true;
    }

    void rotmatrix2angle()
    {
        phi = atan2(-r[2], r[8]);//Φ-ω-K转角系
        omega = asin(-r[5]);
        kappa = atan2(r[3], r[4]);
    }
};

struct StereoCameraEquipment
{
    InteriorParameter		m_leftCamIp, m_rightCamIp;
    ExteriorParameter		m_leftCamEp, m_rightCamEp;

    StereoCameraEquipment()
    {
        m_leftCamIp.init();
        m_rightCamIp.init();
    }

    void init()
    {
        StereoCameraEquipment();
    }
};

#endif
