#ifndef ZEDCAMERA_H
#define ZEDCAMERA_H

#include <sl/Camera.hpp>
#include <sl/Core.hpp>
#include <sl/defines.hpp>
#include <sl/Mesh.hpp>
#include <sl/types.hpp>
#include <opencv2/opencv.hpp>
#include <QImage>

using namespace sl;

struct zed_stereo_parameters
{
    cv::Mat RVec;
    cv::Mat TVec;

    cv::Mat RMatrix;
    cv::Mat XVec;

    void init()
    {
        RVec = cv::Mat::zeros(3, 1, CV_64F);
        TVec = cv::Mat::zeros(3, 1, CV_64F);

        RMatrix = cv::Mat::eye(3, 3, CV_64F);
        XVec = cv::Mat::zeros(3, 1, CV_64F);
    }

    void Vec2Matrix()
    {
        cv::Rodrigues(RVec, RMatrix);
    }

    void CalcX()
    {
        XVec = -RMatrix.inv()*TVec;
    }
};

struct ImageWithPose
{
    unsigned long long timestamp;
    int				   tracking_confidence;

    cv::Mat matLeftRgbImg;
    cv::Mat matDepthImg;
    cv::Mat matDepthData;
    sl::Mat	matPointCloud;
    cv::Mat matConfidenceImg;
    cv::Mat	matConfidenceData;

    // Intrinsic
    cv::Mat kMatrix;
    cv::Mat disto;

    /**
    4x4 Matrix which contains the rotation (3x3) and the translation. Orientation is extracted from this transform as well.
    */
    cv::Mat matTransform;

    // 3x1
    cv::Mat matXVec;

    // limited
    float fMinDistance;
    float fMaxDistance;

    ImageWithPose()
    {
        init();
    }

    void init()
    {
        timestamp = 0;
        tracking_confidence = 0;

        kMatrix = cv::Mat::zeros(3, 3, CV_64F);
        disto = cv::Mat::zeros(1, 5, CV_64F);

        matTransform = cv::Mat::zeros(4, 4, CV_64F);
        matXVec = cv::Mat::zeros(3, 1, CV_64F);

        fMinDistance = 0.7;
        fMaxDistance = 5;
    }


    void CalcX()
    {
        cv::Mat matR = matTransform(cv::Rect(0, 0, 3, 3));
        cv::Mat matT = matTransform(cv::Rect(3, 0, 1, 3));
        matXVec = -matR.inv()*matT;
    }

    float fx()
    {
        return (float)(kMatrix.at<double>(0, 0));
    }

    float fy()
    {
        return (float)(kMatrix.at<double>(1, 1));
    }

    float cx()
    {
        return (float)(kMatrix.at<double>(0, 2));
    }

    float cy()
    {
        return (float)(kMatrix.at<double>(1, 2));
    }

    double* r()
    {
        cv::Mat matRMatrix = matTransform(cv::Rect(0, 0, 3, 3));
        double r_[9] = { 0.0 };
        r_[0] = (matRMatrix.at<double>(0, 0)); r_[1] = (matRMatrix.at<double>(0, 1)); r_[2] = (matRMatrix.at<double>(0, 2));
        r_[3] = (matRMatrix.at<double>(1, 0)); r_[4] = (matRMatrix.at<double>(1, 1)); r_[5] = (matRMatrix.at<double>(1, 2));
        r_[6] = (matRMatrix.at<double>(2, 0)); r_[7] = (matRMatrix.at<double>(2, 1)); r_[8] = (matRMatrix.at<double>(2, 2));
        return r_;
    }

    float X()
    {
        return float(matXVec.at<double>(0));
    }

    float Y()
    {
        return float(matXVec.at<double>(1));
    }

    float Z()
    {
        return float(matXVec.at<double>(2));
    }

    int w()
    {
        return matLeftRgbImg.size().width;
    }

    int h()
    {
        return matLeftRgbImg.size().height;
    }

    std::string getTimeStamp()
    {
        char sz[256];
        sprintf(sz, "%l64u", timestamp);
        return std::string(sz);
    }
};

struct MyRGB
{
    unsigned char b, g, r;
};

struct MyRGBA
{
    unsigned char r, g, b, a;
};

class ZEDCallBack
{
public:
    virtual void dataCallback(ImageWithPose& ) = 0;
};

class ZEDCamera
{
public:
    ZEDCamera();
    virtual ~ZEDCamera();
    void        openCamera();
    void        closeCamera();
    void        setCallback(ZEDCallBack* pCallback);

    void		startTracking();
    void		runTracking();

    void		getCameraInnnerParameters();

    void		printCameraInformation(sl::Camera &zed);
    cv::Mat     slMat2cvMat(sl::Mat& input);
    MyRGB&      GetRGB(const cv::Mat& mat, const int& x, const int& y);
    MyRGBA&     GetRGBA(const cv::Mat& mat, const int& x, const int& y);

    void		grabLeftImage(cv::Mat& matImg);
    void		grabRightImage(cv::Mat& matImg);
    void		grabStereoImage(cv::Mat& leftImg, cv::Mat& rightImg);
    void		grabPointcloud(cv::Mat& matPointcloud);
    void		grabPointcloud(sl::Mat& matPointcloud);

    void		grabSidebySideStereoImage(cv::Mat& matImg);
    void		grabDepthImage(cv::Mat& matImg);

    void		grabDepthData(cv::Mat& matDepth);

    void		grabConfidenceImage(cv::Mat& matImg);
    void		grabConfidenceData(cv::Mat& matImg);

    void		transformPose(sl::Transform &pose, float tx);
    void		getCurrentPose();

    void		saveStereoCamereParameters(const std::string& sLeftPosePath, const std::string& sRightPosePath);

    void		CameraControlSetting();
    std::vector<ImageWithPose>		m_vImageWithPose;
    ImageWithPose	m_curImageWithPose;

    bool		isOpen() { return m_bIsOpen; }
    void		clear();

    std::string	getSerial() { return m_strSerial; }
    float getFPS()	{ return m_fFPS; }

    // camera control
    sl::CAMERA_SETTINGS m_camerasettings;
    void		ResetCameraSettings();
    void		SaveCameraParameters();
    void		LoadCameraParameters();

    int			m_nBrightness;	//0-8
    int			m_nContrast; //0-8
    int			m_nHue; //0-11
    int			m_nSaturation; //0-8
    int			m_nGain;	//0-100
    int			m_nExposure; //0-100
    int			m_nWhiteBalance; //2800-6500,100
    bool		m_bAutoWhiteBalance;

    // brightness 0-8
    void		SetBrightness(int value)
    {
        m_nBrightness = value;
    }

    int			GetBrightness()
    {
        return m_nBrightness;
    }

    // contrast 0-8
    void		SetContrast(int value)
    {
        m_nContrast = value;
    }
    int			GetContrast()
    {
        return m_nContrast;
    }

    // hue 0-11
    void		SetHue(int value)
    {
        m_nHue = value;
    }
    int			GetHue()
    {
        return m_nHue;
    }

    void		SetSaturation(int value)
    {
        m_nSaturation = value;
    }
    int			GetSaturation()
    {
        return m_nSaturation;
    }

    void		SetGain(int value)
    {
        m_nGain = value;
    }
    int			GetGain()
    {
        return m_nGain;
    }

    void		SetExposure(const int& value)
    {
        m_nExposure = value;
    }
    int			GetExposure()
    {
        return m_nExposure;
    }

    void		SetWhiteBalance(const int& value)
    {
        m_nWhiteBalance = value;
    }
    int			GetWhiteBalance()
    {
        return m_nWhiteBalance;
    }

    void		SetEnableAutoWhiteBalance(const bool& value)
    {
        m_bAutoWhiteBalance = value;
        m_zed_camera.setCameraSettings(CAMERA_SETTINGS_AUTO_WHITEBALANCE, value);
    }
    bool		GetEnableAutoWhiteBalance()
    {
        return m_bAutoWhiteBalance;
    }

    int  getCurrentSettingValue(void)
    {
        return m_zed_camera.getCameraSettings(m_camerasettings);
    }

    void setCurrentSettingValue(int nValue)
    {
        int nValidValue = nValue;
        switch (m_camerasettings)
        {
        case sl::CAMERA_SETTINGS_BRIGHTNESS://0-8
            nValidValue %= 9;
            m_nBrightness = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_CONTRAST://0-8
            nValidValue %= 9;
            m_nContrast = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_HUE://0-11
            nValidValue %= 12;
            m_nHue = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_SATURATION://0-8
            nValidValue %= 9;
            m_nSaturation = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_GAIN://0-100
            nValidValue %= 101;
            m_nGain = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_EXPOSURE://0-100
            nValidValue %= 101;
            m_nExposure = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_WHITEBALANCE://2800-6500,100
            nValidValue = nValidValue % 6501;
            if (nValidValue < 2800)
                nValidValue = nValidValue + 2800;
            m_nWhiteBalance = nValidValue;
            break;
        case sl::CAMERA_SETTINGS_AUTO_WHITEBALANCE:
            break;
        case sl::CAMERA_SETTINGS_LAST:
            break;
        default:
            break;
        }
        m_zed_camera.setCameraSettings(m_camerasettings, nValidValue);
    }

    bool                                isEnableDepthImage(void)
    {
        return m_runParameters.enable_depth;
    }
    void switchDepthImage(void)
    {
        m_runParameters.enable_depth = !m_runParameters.enable_depth;
    }
    bool                                isEnablePc(void)
    {
        return m_runParameters.enable_point_cloud;
    }
    void                                switchPointcloud(void)
    {
        m_runParameters.enable_point_cloud = !m_runParameters.enable_point_cloud;
    }
    std::string getSensingMode(void)
    {
        std::string strText;
        switch (m_runParameters.sensing_mode)
        {
        case sl::SENSING_MODE_STANDARD:
            strText = "Standard";
            break;
        /**< This mode outputs ZED standard depth map that preserves edges and depth accuracy.
                                   * Applications example: Obstacle detection, Automated navigation, People detection, 3D reconstruction.*/
        case SENSING_MODE_FILL:
            strText = "Fill";
            break;/**< This mode outputs a smooth and fully dense depth map.
                                                      * Applications example: AR/VR, Mixed-reality capture, Image post-processing.*/
        case SENSING_MODE_LAST:
            strText = "Last";
        break;
        default:
            break;
        }
        return strText;
    }
    void     switchSeningMode(void)
    {
        int nV = m_runParameters.sensing_mode;
        m_runParameters.sensing_mode = SENSING_MODE((++nV) % 3);
    }

    void setSeningMode(SENSING_MODE mode)
    {
        m_runParameters.sensing_mode = mode;
    }

    std::string getMeasure3DReferenceFrame()
    {
        std::string strText;
        switch (m_runParameters.measure3D_reference_frame)
        {
        case REFERENCE_FRAME_WORLD:
            strText = "World";
            break;/**< The transform of sl::Pose will contains the motion with reference to the world frame (previously called PATH).*/
        case REFERENCE_FRAME_CAMERA:
            strText = "Camera";
            break;/**< The transform of sl::Pose will contains the motion with reference to the previous camera frame (previously called POSE).*/
        case REFERENCE_FRAME_LAST:
            strText = "Last";
            break;
        default:
            break;
        }
        return strText;
    }

    void setMeasure3DReferenceFrame(REFERENCE_FRAME reference)
    {
        m_runParameters.measure3D_reference_frame = reference;
    }

    void     switchMeasure3DReferenceFrame(void)
    {
        int nV = m_runParameters.measure3D_reference_frame;
        m_runParameters.measure3D_reference_frame = REFERENCE_FRAME((++nV) % 3);
    }

    void            setStrSetting(const QString& str);
    QString&        getStrSetting();
    int getCameraSettings(CAMERA_SETTINGS setting)
    {
        return m_zed_camera.getCameraSettings(setting);
    }

    ImageWithPose           m_currentImageWithPose;

private:
    sl::Camera              m_zed_camera;
    sl::Pose                m_current_camera_pose;
    sl::InitParameters      m_initParameters;
    sl::RuntimeParameters   m_runParameters;
    sl::CameraParameters    m_left_camera_parameters;
    sl::CameraParameters    m_right_camera_parameters;
    sl::TrackingParameters  m_trackingParameters;

    zed_stereo_parameters	m_stereo_camera_parameters;
    std::thread				m_zed_callback;
    ZEDCallBack            *m_pCallback;

    bool                    m_bIsOpen;
    bool                    m_bExit;
    int                     m_nPointCloudWidth;
    int                     m_nPointCloudHeight;
    bool                    m_bEnablePointCloudExport;
    float                   m_fFPS;
    std::string             m_strSerial;
    QString                 m_strSetting;
};

#endif // ZEDCAMERA_H
