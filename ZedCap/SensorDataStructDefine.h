#ifndef _YDUSKY_SENSOR_DATA_STRUCT_DEFINE_2017_05_11_15_21_11_
#define _YDUSKY_SENSOR_DATA_STRUCT_DEFINE_2017_05_11_15_21_11_

#include <list>
#include <string.h>

union si32
{
	int  xi;
	char xc[4];
	si32()
		:xi()
	{
		memset(xc, 0, sizeof(xc));
	}
	si32(const si32& other)
		:xi(other.xi)
	{
		memcpy(xc, other.xc, sizeof(xc));
	}
	si32&operator=(const si32& other)
	{
		xi = other.xi;
		memcpy(xc, other.xc, sizeof(xc));
		return *this;
	}
};

union f32
{
	float xf;
	unsigned char xc[4];
	f32()
		:xf(0)
	{
		memset(xc, 0, sizeof(xc));
	}
	f32(const f32& other)
		:xf(other.xf)
	{
		memcpy(xc, other.xc, sizeof(xc));
	}
	f32&operator=(const f32& other)
	{
		xf = other.xf;
		memcpy(xc, other.xc, sizeof(xc));
		return *this;
	}
};

struct IMU2_Data
{
	f32 roll;
	f32 pitch;
	f32 yaw;
	f32 rollRate;
	f32 pitchRate;
	f32 yawRate;
	f32 ax;
	f32 ay;
	f32 az;
	si32 reserved;
};

bool  isTrueBit(short value, int offset);
short enableBit(short value, int offset, bool bEnable);

///底盘控制器
struct tagDataFromVehicle
{
	short int _rot1agl;
	short int _rot2agl;
	short int _arm1agl;
	short int _arm2agl;
	short int _arm3agl;
	short int _arm1leng;
	short int _arm3leng;
	short int _posX;
	short int _posY;
	short int _posZ;
	short int _res40011; //避障1
	short int _res40012;
	short int _res40013;
	short int _res40014;
	short int _res40015;
	short int _lab40016;
	short int _lab40017;
	short int _lab40018;
	short int _lab40019;
	short int _lab40020;
	short int _lab40021; 
	short int _lab40022; //2号调平油缸的水平角度

	tagDataFromVehicle()
		: _rot1agl(0)
		, _rot2agl(0)
		, _arm1agl(0)
		, _arm2agl(0)
		, _arm3agl(0)
		, _arm1leng(0)
		, _arm3leng(0)
		, _posX(0)
		, _posY(0)
		, _posZ(0)
		, _res40011(0)
		, _res40012(0)
		, _res40013(0)
		, _res40014(0)
		, _res40015(0)
		, _lab40016(0)
		, _lab40017(0)
		, _lab40018(0)
		, _lab40019(0)
		, _lab40020(0)
		, _lab40021(0)
		, _lab40022(0)//2号调平油缸的水平角度
	{

	}

	tagDataFromVehicle(const tagDataFromVehicle& other)
		: _rot1agl(other._rot1agl)
		, _rot2agl(other._rot2agl)
		, _arm1agl(other._arm1agl)
		, _arm2agl(other._arm2agl)
		, _arm3agl(other._arm3agl)
		, _arm1leng(other._arm1leng)
		, _arm3leng(other._arm3leng)
		, _posX(other._posX)
		, _posY(other._posY)
		, _posZ(other._posZ)
		, _res40011(other._res40011)
		, _res40012(other._res40012)
		, _res40013(other._res40013)
		, _res40014(other._res40014)
		, _res40015(other._res40015)
		, _lab40016(other._lab40016)
		, _lab40017(other._lab40017)
		, _lab40018(other._lab40018)
		, _lab40019(other._lab40019)
		, _lab40020(other._lab40020)
		, _lab40021(other._lab40021)
		, _lab40022(other._lab40022)//2号调平油缸的水平角度
	{

	}
	tagDataFromVehicle& operator=(const tagDataFromVehicle& other)
	{
		_rot1agl = other._rot1agl;
		_rot2agl = other._rot2agl;
		_arm1agl = other._arm1agl;
		_arm2agl = other._arm2agl;
		_arm3agl = other._arm3agl;
		_arm1leng = other._arm1leng;
		_arm3leng = other._arm3leng;
		_posX = other._posX;
		_posY = other._posY;
		_posZ = other._posZ;
		_res40011 = other._res40011;
		_res40012 = other._res40012;
		_res40013 = other._res40013;
		_res40014 = other._res40014;
		_res40015 = other._res40015;
		_lab40016 = other._lab40016;
		_lab40017 = other._lab40017;
		_lab40018 = other._lab40018;
		_lab40019 = other._lab40019;
		_lab40020 = other._lab40020;
		_lab40021 = other._lab40021;
		_lab40022 = other._lab40022;//2号调平油缸的水平角度
		return *this;
	}

	float getRot1Agl(void)
	{
		return _rot1agl / 10.0;
	}
	float getRot2Agl(void)
	{
		return _rot2agl / 10.0;
	}
	float getArm1Agl(void)
	{
		return _arm1agl / 10.0;
	}
	float getArm2Agl(void)
	{
		return _arm2agl / 10.0;
	}
	float getArm3Agl(void)
	{
		return _arm3agl / 10.0;
	}
	float getArm1Lenght(void)
	{
		return _arm1leng;
	}
	float getArm3Lenght(void)
	{
		return _arm3leng;
	}
	float getPosX(void)
	{
		return _posX;
	}
	float getPosY(void)
	{
		return _posY;
	}
	float getPosZ(void)
	{
		return _posZ;
	}
	float getAdjustH(void)
	{
		return _lab40022 / 10.0;
	}

	bool isStop_ZT1_Rotate(void)
	{
		return isTrueBit(_lab40016, 0);
	}

	bool isStop_Arm1_UpDown(void)
	{
		return isTrueBit(_lab40016, 1);
	}

	bool isStop_Arm2_UpDown(void)
	{
		return isTrueBit(_lab40016, 2);
	}

	bool isStop_Arm3_UpDown(void)
	{
		return isTrueBit(_lab40016, 3);
	}

	bool isStop_Arm1_StretchShrink(void)
	{
		return isTrueBit(_lab40016, 4);
	}

	bool isStop_Arm3_StretchShrink(void)
	{
		return isTrueBit(_lab40016, 5);
	}

	bool isStop_ZT2_UpDown(void)
	{
		return isTrueBit(_lab40016, 6);
	}

	bool isStop_ZT2_Rotate(void)
	{
		return isTrueBit(_lab40016, 7);
	}

	bool isStop_TRAVEL_ForwordReverse(void)
	{
		return isTrueBit(_lab40016, 8);
	}

	bool isSuccess_Onekey_Expand(void)
	{
		return isTrueBit(_lab40016, 9);
	}

	bool isSuccess_Onekey_Fold(void)
	{
		return isTrueBit(_lab40016, 10);
	}

	bool isSuccess_Deflection_adjust(void)
	{
		return isTrueBit(_lab40016, 11);
	}

	bool isEnbaleLevel1Stop()
	{
		return isTrueBit(_lab40017, 5);
	}

	bool isEnbaleLevel2Stop()
	{
		return isTrueBit(_lab40017, 6);
	}

	bool isEnbaleDeflectionAdjust()
	{
		return isTrueBit(_lab40017, 7);
	}

	bool isEnableVehicle()
	{
		return (2 == (_lab40017 & 0x0002));
	}
};

typedef std::list<tagDataFromVehicle>  ListBottomControlBuffer;
#endif
