#include "zedcamera.h"
#include <QDebug>
#include <QSettings>
#include <QCoreApplication>
#include "YDTime.h"
ZEDCamera::ZEDCamera():
    m_bIsOpen(false),
    m_bExit(false),
    m_pCallback(nullptr)
{
    // initial parameters
    m_initParameters.camera_resolution = sl::RESOLUTION_HD720;
    m_initParameters.camera_fps = 0;
    m_initParameters.camera_linux_id = 0;
    m_initParameters.svo_input_filename = sl::String();
    m_initParameters.svo_real_time_mode = false;
    m_initParameters.depth_mode = sl::DEPTH_MODE_PERFORMANCE;
    m_initParameters.coordinate_units = sl::UNIT_METER;
    m_initParameters.coordinate_system = sl::COORDINATE_SYSTEM_IMAGE; //used in opengl
    m_initParameters.sdk_verbose = false;
    m_initParameters.sdk_gpu_id = -1;
    m_initParameters.depth_minimum_distance = -1;
    m_initParameters.camera_disable_self_calib = true;
    m_initParameters.camera_image_flip = false;
    m_initParameters.camera_buffer_count_linux = 4;

    // run parameters
    m_runParameters.sensing_mode = sl::SENSING_MODE::SENSING_MODE_STANDARD;
    m_runParameters.enable_depth = true;
    m_runParameters.enable_point_cloud = true;
    m_runParameters.measure3D_reference_frame = sl::REFERENCE_FRAME::REFERENCE_FRAME_WORLD;

    // tracking parameters
    m_trackingParameters.initial_world_transform = sl::Transform();

    m_vImageWithPose.clear();

    m_bEnablePointCloudExport = true;
    m_nPointCloudWidth = 800;
    m_nPointCloudHeight = 600;
}

ZEDCamera::~ZEDCamera()
{
    closeCamera();
    SaveCameraParameters();
}

void ZEDCamera::clear()
{
    m_vImageWithPose.clear();
}

void ZEDCamera::setCallback(ZEDCallBack* pCallback)
{
    m_pCallback = pCallback;
}

void ZEDCamera::openCamera()
{
    sl::ERROR_CODE err = m_zed_camera.open(m_initParameters);

    if (err != sl::SUCCESS)
    {
        std::cout << errorCode2str(err) << std::endl;
        m_zed_camera.close();
        return;
    }

    m_bIsOpen = m_zed_camera.isOpened();
    char szNo[256];
    sprintf(szNo, "%d", m_zed_camera.getCameraInformation().serial_number);
    m_strSerial = szNo;

    // get camera calibration parameters
    m_left_camera_parameters = m_zed_camera.getCameraInformation().calibration_parameters.left_cam;
    m_right_camera_parameters = m_zed_camera.getCameraInformation().calibration_parameters.right_cam;

    m_curImageWithPose.kMatrix.at<double>(0, 0) = m_left_camera_parameters.fx;
    m_curImageWithPose.kMatrix.at<double>(0, 1) = 0.0;
    m_curImageWithPose.kMatrix.at<double>(0, 2) = m_left_camera_parameters.cx;
    m_curImageWithPose.kMatrix.at<double>(1, 0) = 0.0;
    m_curImageWithPose.kMatrix.at<double>(1, 1) = m_left_camera_parameters.fy;
    m_curImageWithPose.kMatrix.at<double>(1, 2) = m_left_camera_parameters.cy;
    m_curImageWithPose.kMatrix.at<double>(2, 0) = 0.0;
    m_curImageWithPose.kMatrix.at<double>(2, 1) = 0.0;
    m_curImageWithPose.kMatrix.at<double>(2, 2) = 1.0;

    for (int i = 0; i < 5;++i)
        m_curImageWithPose.disto.at<double>(i) = m_left_camera_parameters.disto[i];

    // get stereo camera parameters
    m_stereo_camera_parameters.init();
    for (int i = 0; i < 3; ++i)
    {
        m_stereo_camera_parameters.RVec.at<double>(i) = m_zed_camera.getCameraInformation().calibration_parameters.R[i];
        m_stereo_camera_parameters.TVec.at<double>(i) = m_zed_camera.getCameraInformation().calibration_parameters.T[i];
    }
    m_stereo_camera_parameters.Vec2Matrix();
    m_stereo_camera_parameters.CalcX();

    // Initialize motion tracking parameters
    sl::TrackingParameters trackingParameters;
    trackingParameters.initial_world_transform = sl::Transform::identity();
    trackingParameters.enable_spatial_memory = true;     // Enable Spatial memory
    m_zed_camera.enableTracking(trackingParameters);
}

void ZEDCamera::closeCamera()
{
    m_bExit = true;

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    if (m_bIsOpen)
    {

    //	m_zed_camera.disableTracking("./fileT1");
        m_zed_camera.close();
    }
    m_bIsOpen = false;
}

void ZEDCamera::printCameraInformation(sl::Camera &zed)
{
    printf("ZED Serial Number         : %d\n", zed.getCameraInformation().serial_number);
    printf("ZED Firmware              : %d\n", zed.getCameraInformation().firmware_version);
    printf("ZED Camera Resolution     : %dx%d\n", zed.getResolution().width, zed.getResolution().height);
    printf("ZED Camera FPS            : %d\n", (int)zed.getCameraFPS());
}

/**
* Conversion function between sl::Mat and cv::Mat
**/
cv::Mat ZEDCamera::slMat2cvMat(sl::Mat& input)
{
    // Mapping between MAT_TYPE and CV_TYPE
    int cv_type = -1;
    switch (input.getDataType())
    {
    case sl::MAT_TYPE_32F_C1: cv_type = CV_32FC1; break;
    case sl::MAT_TYPE_32F_C2: cv_type = CV_32FC2; break;
    case sl::MAT_TYPE_32F_C3: cv_type = CV_32FC3; break;
    case sl::MAT_TYPE_32F_C4: cv_type = CV_32FC4; break;
    case sl::MAT_TYPE_8U_C1: cv_type = CV_8UC1; break;
    case sl::MAT_TYPE_8U_C2: cv_type = CV_8UC2; break;
    case sl::MAT_TYPE_8U_C3: cv_type = CV_8UC3; break;
    case sl::MAT_TYPE_8U_C4: cv_type = CV_8UC4; break;
    default: break;
    }

    // Since cv::Mat data requires a uchar* pointer, we get the uchar1 pointer from sl::Mat (getPtr<T>())
    // cv::Mat and sl::Mat will share a single memory structure
    sl::ERROR_CODE code = input.updateCPUfromGPU();
    return cv::Mat(input.getHeight(), input.getWidth(), cv_type, input.getPtr<sl::uchar1>(MEM_CPU)).clone();

}

MyRGB&	ZEDCamera::GetRGB(const cv::Mat& mat, const int& x, const int& y)
{
    assert((mat.step / mat.cols) == sizeof(MyRGB));
    MyRGB *data = (MyRGB*)mat.data;
    data += y*mat.cols + x;
    return *data;
}

MyRGBA&	ZEDCamera::GetRGBA(const cv::Mat& mat, const int& x, const int& y)
{
    assert((mat.step / mat.cols) == sizeof(MyRGBA));
    MyRGBA *data = (MyRGBA*)mat.data;
    data += y*mat.cols + x;
    return *data;
}

void ZEDCamera::grabDepthData(cv::Mat& matDepth)
{
    if (!m_bIsOpen)
        return;

    sl::Mat slMat;
    m_zed_camera.retrieveMeasure(slMat, sl::MEASURE_DEPTH, sl::MEM_GPU);
    matDepth = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
}

void ZEDCamera::grabPointcloud(cv::Mat& matPointcloud)
{
    if (!m_bIsOpen)
        return;

    sl::Mat slMat;
    m_zed_camera.retrieveMeasure(slMat, sl::MEASURE_XYZRGBA, sl::MEM_GPU);
    matPointcloud = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
}

void ZEDCamera::grabPointcloud(sl::Mat& matPointcloud)
{
    if (!m_bIsOpen)
        return;

    m_zed_camera.retrieveMeasure(matPointcloud, sl::MEASURE_XYZRGBA, sl::MEM_GPU);
}

void ZEDCamera::grabLeftImage(cv::Mat& matImg)
{
    if (!m_bIsOpen)
        return;

    sl::Mat slMat;
    m_zed_camera.retrieveImage(slMat, sl::VIEW_LEFT, sl::MEM_GPU);
    matImg = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
    //matImg = cv::Mat(slMat.getHeight(), slMat.getWidth(), CV_8UC4, slMat.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();
}

void ZEDCamera::grabRightImage(cv::Mat& matImg)
{
    if (!m_bIsOpen)
        return;

    sl::Mat slMat;
    m_zed_camera.retrieveImage(slMat, sl::VIEW_RIGHT, sl::MEM_GPU);
    matImg = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
    //matImg = cv::Mat(slMat.getHeight(), slMat.getWidth(), CV_8UC4, slMat.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();
}

void ZEDCamera::grabStereoImage(cv::Mat& matLeftImg, cv::Mat& matRightImg)
{
    if (!m_bIsOpen)
        return;

    if (sl::ERROR_CODE_FAILURE == m_zed_camera.grab(m_runParameters))
        return;
    sl::Mat slMatLeft, slMatRight;
    m_zed_camera.retrieveImage(slMatLeft, sl::VIEW_LEFT, sl::MEM_GPU);
    m_zed_camera.retrieveImage(slMatRight, sl::VIEW_RIGHT, sl::MEM_GPU);
    matLeftImg = cv::Mat(slMatLeft.getHeight(), slMatLeft.getWidth(), CV_8UC4, slMatLeft.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();
    matRightImg = cv::Mat(slMatLeft.getHeight(), slMatLeft.getWidth(), CV_8UC4, slMatLeft.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();
}

void ZEDCamera::grabSidebySideStereoImage(cv::Mat& matImg)
{
    if (!m_bIsOpen)
        return;

    if (sl::ERROR_CODE_FAILURE == m_zed_camera.grab(m_runParameters))
        return;
    sl::Mat slMat;
    m_zed_camera.retrieveImage(slMat, sl::VIEW_SIDE_BY_SIDE);
    matImg = cv::Mat(slMat.getHeight(), slMat.getWidth(), CV_8UC4, slMat.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();

}

void ZEDCamera::grabDepthImage(cv::Mat& matImg)
{
    if (!m_bIsOpen)
        return;

    //if (sl::ERROR_CODE_FAILURE == m_zed_camera.grab(m_runParameters))
     //   return;
    sl::Mat slMat;
    m_zed_camera.retrieveImage(slMat, sl::VIEW_DEPTH, sl::MEM_GPU);
    //matImg = cv::Mat(slMat.getHeight(), slMat.getWidth(), CV_8UC4, slMat.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();
    matImg = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
}

void ZEDCamera::grabConfidenceImage(cv::Mat& matImg)
{
    if (!m_bIsOpen)
        return;

    //if (sl::ERROR_CODE_FAILURE == m_zed_camera.grab(m_runParameters))
   //     return;
    sl::Mat slMat;
    m_zed_camera.retrieveImage(slMat, sl::VIEW_CONFIDENCE, sl::MEM_GPU);
    //matImg = cv::Mat(slMat.getHeight(), slMat.getWidth(), CV_8UC4, slMat.getPtr<sl::uchar1>(sl::MEM_GPU)).clone();
    matImg = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
}

void ZEDCamera::grabConfidenceData(cv::Mat& matImg)
{
    if (!m_bIsOpen)
        return;

    //if (sl::ERROR_CODE_FAILURE == m_zed_camera.grab(m_runParameters))
   //     return;
    sl::Mat slMat;
    m_zed_camera.retrieveMeasure(slMat, sl::MEASURE_CONFIDENCE, sl::MEM_GPU);
    matImg = slMat2cvMat(slMat);
    slMat.free(sl::MEM_GPU);
}

void ZEDCamera::saveStereoCamereParameters(const std::string& sLeftPosePath, const std::string& sRightPosePath)
{
    std::ofstream l_file(sLeftPosePath.c_str());
    if (l_file.is_open())
    {
        // intrinsic
        int type = 3;
        l_file << "IntrinsicType" << type << std::endl;
        l_file << "==Intrinsic==" << std::endl;

        l_file << "Size" << std::endl
                << m_left_camera_parameters.image_size.width << " "
                << m_left_camera_parameters.image_size.height << std::endl;
        l_file << "KMatrix" << std::endl <<
            m_left_camera_parameters.fx << " " << 0.0 << " " << m_left_camera_parameters.cx << std::endl <<
            0.0 << " " << m_left_camera_parameters.fy << " " << m_left_camera_parameters.cy << std::endl <<
            0.0 << " " << 0.0 << " " << 1.0 << std::endl;
        l_file << "Dist" << std::endl <<
            m_left_camera_parameters.disto[0] << " " <<
            m_left_camera_parameters.disto[1] << " " <<
            m_left_camera_parameters.disto[2] << " " <<
            m_left_camera_parameters.disto[3] << " " <<
            m_left_camera_parameters.disto[4] << " " << std::endl;

        // extrinsic
        l_file << "==Extrinsic==" << std::endl;
        l_file << "Position" << std::endl;
        l_file << 0.0 << " " << 0.0 << " " << 0.0 << std::endl;
        l_file << "Translation" << std::endl;
        l_file << 0.0 << " " << 0.0 << " " << 0.0 << std::endl;
        l_file << "Rotation" << std::endl;
        l_file << 1.0 << " " << 0.0 << " " << 0.0 << std::endl
            << 0.0 << " " << 1.0 << " " << 0.0 << std::endl
            << 0.0 << " " << 0.0 << " " << 1.0 << std::endl;
    }
    l_file.close();

    std::ofstream r_file(sRightPosePath.c_str());
    if (r_file.is_open())
    {
        // intrinsic
        int type = 3;
        r_file << "IntrinsicType" << type << std::endl;
        r_file << "==Intrinsic==" << std::endl;

        r_file << "Size" << std::endl
            << m_right_camera_parameters.image_size.width << " "
            << m_right_camera_parameters.image_size.height << std::endl;
        r_file << "KMatrix" << std::endl <<
            m_right_camera_parameters.fx << " " << 0.0 << " " << m_right_camera_parameters.cx << std::endl <<
            0.0 << " " << m_right_camera_parameters.fy << " " << m_right_camera_parameters.cy << std::endl <<
            0.0 << " " << 0.0 << " " << 1.0 << std::endl;
        r_file << "Dist" << std::endl <<
            m_right_camera_parameters.disto[0] << " " <<
            m_right_camera_parameters.disto[1] << " " <<
            m_right_camera_parameters.disto[2] << " " <<
            m_right_camera_parameters.disto[3] << " " <<
            m_right_camera_parameters.disto[4] << " " << std::endl;

        // extrinsic
        r_file << "==Extrinsic==" << std::endl;
        r_file << "Position" << std::endl;
        r_file << m_stereo_camera_parameters.XVec.at<double>(0) << " " <<
                    m_stereo_camera_parameters.XVec.at<double>(0) << " " <<
                    m_stereo_camera_parameters.XVec.at<double>(0) << std::endl;
        r_file << "Translation" << std::endl;
        r_file << m_stereo_camera_parameters.TVec.at<double>(0) << " " <<
                    m_stereo_camera_parameters.TVec.at<double>(1) << " " <<
                    m_stereo_camera_parameters.TVec.at<double>(2) << std::endl;
        r_file << "Rotation" << std::endl;
        r_file << m_stereo_camera_parameters.RMatrix.at<double>(0, 0) << " " << m_stereo_camera_parameters.RMatrix.at<double>(0, 1) << " " << m_stereo_camera_parameters.RMatrix.at<double>(0, 2) << std::endl
            << m_stereo_camera_parameters.RMatrix.at<double>(1, 0) << " " << m_stereo_camera_parameters.RMatrix.at<double>(1, 1) << " " << m_stereo_camera_parameters.RMatrix.at<double>(1, 2) << std::endl
            << m_stereo_camera_parameters.RMatrix.at<double>(2, 0) << " " << m_stereo_camera_parameters.RMatrix.at<double>(2, 1) << " " << m_stereo_camera_parameters.RMatrix.at<double>(2, 2) << std::endl;
    }
    r_file.close();
}

void ZEDCamera::getCameraInnnerParameters()
{
    m_zed_camera.getCameraInformation().calibration_parameters.left_cam;
}

/**
*  This function separates the camera frame and the motion tracking frame.
*  In this sample, we move the motion tracking frame to the center of the ZED ( baseline/2 for the X translation)
*  By default, the camera frame and the motion tracking frame are at the same place: the left sensor of the ZED.
**/
void ZEDCamera::transformPose(sl::Transform &pose, float tx)
{
    sl::Transform transform_;
    transform_.setIdentity(); // Create the transformation matrix to separate camera frame and motion tracking frame
    transform_.tx = tx; // Move the tracking frame at the center of the ZED (between ZED's eyes)
    pose = sl::Transform::inverse(transform_) * pose * transform_; // apply the transformation
}

void runTrackingThread(ZEDCamera* pFun)
{
    if (pFun)
    {
        pFun->runTracking();
    }
}

void ZEDCamera::startTracking()
{
    if (!m_zed_camera.isOpened())
        return;

    m_bExit = false;
    m_zed_callback = std::thread(runTrackingThread, this);
    m_zed_callback.detach();
}

void ZEDCamera::runTracking()
{
    if (!m_bIsOpen)
        return;

    float tx, ty, tz = 0;
    float rx, ry, rz = 0;
    char text_rotation[256];
    char text_translation[256];
    // Get the translation from the left eye to the center of the camera
    float camera_left_to_center = m_zed_camera.getCameraInformation().calibration_parameters.T.x *0.5f;

    // loop until exit_ flag has been set to true
    while (!m_bExit)
    {
        YDTimer timer;
        if (!m_zed_camera.grab(m_runParameters))
        {
            m_fFPS = m_zed_camera.getCameraFPS();

            // Get camera position in World frame
            sl::TRACKING_STATE tracking_state = m_zed_camera.getPosition(m_current_camera_pose, sl::REFERENCE_FRAME_WORLD);

            // Get motion tracking confidence
            int tracking_confidence = m_current_camera_pose.pose_confidence;

            if (tracking_state == sl::TRACKING_STATE_OK)
            {
                // Grab one image
                grabLeftImage(m_curImageWithPose.matLeftRgbImg);
                grabDepthImage(m_curImageWithPose.matDepthImg);
                grabDepthData(m_curImageWithPose.matDepthData);
                grabConfidenceImage(m_curImageWithPose.matConfidenceImg);
                grabConfidenceData(m_curImageWithPose.matConfidenceData);
                if (m_bEnablePointCloudExport)
                   grabPointcloud(m_curImageWithPose.matPointCloud);

                 // Get Pose
                double r0 = m_current_camera_pose.pose_data.getRotation()(0, 0);
                double r1 = m_current_camera_pose.pose_data.getRotation()(1, 0);
                double r2 = m_current_camera_pose.pose_data.getRotation()(2, 0);

                double r3 = m_current_camera_pose.pose_data.getRotation()(0, 1);
                double r4 = m_current_camera_pose.pose_data.getRotation()(1, 1);
                double r5 = m_current_camera_pose.pose_data.getRotation()(2, 1);

                double r6 = m_current_camera_pose.pose_data.getRotation()(0, 2);
                double r7 = m_current_camera_pose.pose_data.getRotation()(1, 2);
                double r8 = m_current_camera_pose.pose_data.getRotation()(2, 2);

                double tx = m_current_camera_pose.pose_data.getTranslation()[0];
                double ty = m_current_camera_pose.pose_data.getTranslation()[1];
                double tz = m_current_camera_pose.pose_data.getTranslation()[2];

                cv::Mat tmp = (cv::Mat_<double>(4, 4) << r0, r1, r2, tx, r3, r4, r5, ty, r6, r7, r8, tz, 0, 0, 0, 1);
                tmp.copyTo(m_curImageWithPose.matTransform);
                m_curImageWithPose.CalcX();

                //test
                float xx = m_curImageWithPose.matTransform(cv::Rect(3, 0, 1, 3)).at<double>(0);
                float yy = m_curImageWithPose.matTransform(cv::Rect(3, 0, 1, 3)).at<double>(1);
                float zz = m_curImageWithPose.matTransform(cv::Rect(3, 0, 1, 3)).at<double>(2);

                m_curImageWithPose.timestamp = m_zed_camera.getCameraTimestamp();
                m_curImageWithPose.tracking_confidence = tracking_confidence;

                if (m_pCallback)
                {
                    m_pCallback->dataCallback(m_curImageWithPose);
                }
            }
        }
        else
            sl::sleep_ms(1);

        qDebug() << QString::asprintf("call back use time: %d", timer.elapsed());
    }
}

void ZEDCamera::CameraControlSetting()
{
    if (m_zed_camera.isOpened())
    {
        std::cout << "camera setting" << std::endl;

        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_BRIGHTNESS, m_nBrightness);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_CONTRAST, m_nContrast);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_HUE, m_nHue, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_SATURATION, m_nSaturation, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_GAIN, m_nGain);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_EXPOSURE, m_nExposure);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_WHITEBALANCE, m_nWhiteBalance, true);
    }
}

void ZEDCamera::ResetCameraSettings()
{
    if (m_zed_camera.isOpened())
    {
        std::cout << "Reset all settings to default" << std::endl;

        m_nBrightness = -1;
        m_nContrast = -1;
        m_nHue = -1;
        m_nSaturation = -1;
        m_nGain = -1;
        m_nExposure = -1;
        m_nWhiteBalance = -1;

        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_BRIGHTNESS, m_nBrightness, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_CONTRAST, m_nContrast, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_HUE, m_nHue, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_SATURATION, m_nSaturation, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_GAIN, m_nGain, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_EXPOSURE, m_nExposure, true);
        m_zed_camera.setCameraSettings(sl::CAMERA_SETTINGS_WHITEBALANCE, m_nWhiteBalance, true);
    }
}

void ZEDCamera::SaveCameraParameters()
{

    QSettings setting(QCoreApplication::applicationDirPath() + "/zed_config.ini", QSettings::IniFormat);

     // camera control parameters
    setting.setValue("CameraControl/Brightness", m_nBrightness);

    setting.setValue("CameraControl/Contrast", m_nContrast);

    setting.setValue("CameraControl/Hue", m_nHue);

    setting.setValue("CameraControl/Saturation", m_nSaturation);

    setting.setValue("CameraControl/Gain", m_nGain);

    setting.setValue("CameraControl/Exposure", m_nExposure);

    setting.setValue("CameraControl/EnableAutoWhiteBalance", m_bAutoWhiteBalance);

    setting.setValue("CameraControl/WhiteBalance", m_nWhiteBalance);

    setting.setValue("CameraControl/DisableSelfCalib", m_initParameters.camera_disable_self_calib);

    setting.setValue("CameraControl/CameraResolution", m_initParameters.camera_resolution);

    setting.setValue("CameraControl/CoordinateSystem", m_initParameters.coordinate_system);

    setting.setValue("CameraControl/CoordinateUnits", m_initParameters.coordinate_units);

    setting.setValue("CameraControl/DepthMinimumDistance", m_initParameters.depth_minimum_distance);

    setting.setValue("CameraControl/DepthMode", m_initParameters.depth_mode);

    // run parameters
    setting.setValue("CameraControl/EnableDepth", m_runParameters.enable_depth);

    setting.setValue("CameraControl/EnablePointCloud", m_runParameters.enable_point_cloud);

    setting.setValue("CameraControl/Measure3dReferenceFrame", m_runParameters.measure3D_reference_frame);

    setting.setValue("CameraControl/SensingMode", m_runParameters.sensing_mode);
}

void ZEDCamera::LoadCameraParameters()
{
    QSettings setting(QCoreApplication::applicationDirPath() + "/zed_config.ini", QSettings::IniFormat);

    // camera control parameters
    m_nBrightness = setting.value("CameraControl/Brightness").toInt();
    m_nContrast = setting.value("CameraControl/Contrast").toInt();
    m_nHue = setting.value("CameraControl/Hue").toInt();
    m_nSaturation = setting.value("CameraControl/Saturation").toInt();
    m_nGain = setting.value("CameraControl/Gain").toInt();
    m_nExposure = setting.value("CameraControl/Exposure").toInt();
    m_bAutoWhiteBalance = setting.value("CameraControl/EnableAutoWhiteBalance").toBool();
    m_nWhiteBalance = setting.value("CameraControl/WhiteBalance").toInt();
}

void ZEDCamera::setStrSetting(const QString &str)
{
    m_strSetting = str;
}

QString &ZEDCamera::getStrSetting()
{
    return m_strSetting;
}

