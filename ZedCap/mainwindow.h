#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <sl/Camera.hpp>
#include <QLineEdit>
#include <QPushButton>
#include <QKeyEvent>
#include <QMouseEvent>
#include <QLabel>
#include <QTimer>
#include "opencv2/opencv.hpp"
#include "zedcamera.h"
#include "NetUnit/netmanager.h"
#include "BriStoreMger.h"

enum SaveModel {
    NONE = 0,
    ONE = 1,
    MULTI = 2
};

namespace Ui {
class MainWindow;
}

class MainWindow;

class ZedDataCB : public ZEDCallBack
{
private:
    MainWindow*     m_pMainWnd;

public:
    ZedDataCB(MainWindow* mw) : m_pMainWnd(mw) { }
    virtual void dataCallback(ImageWithPose& );
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    void        initDlgMultiFrame(void);
    void        initDlgHelp(void);
    void        initWidgetDisplay(void);
    void        initLCD(void);
    void        initStatusBar(void);
    void        setIWPDefalut(void);
    void        setDepthDefalut(void);
    void        initSettingWidget(void);
    bool        changeSaveModel(SaveModel model);
    void        zedDataCallback(ImageWithPose& iwp);
    void        setBtnEnable(bool flag);
    void        showWidgetInfo(bool flag);
    void        adjustCameraParameter(bool add = true);
    void        showMessage(const QString& msg, int timeout = 2000);
    bool        SaveSingleIWP(ImageWithPose iwp, bool m_bSaveRGB /*= true*/, bool m_bSaveDepth /*= true*/);

protected:
    virtual void    keyPressEvent(QKeyEvent *event);
    virtual void    mouseMoveEvent(QMouseEvent *event);

private slots:
    void on_actionOpenCap_triggered();
    void on_actionSaveOneFrame_triggered();
    void on_actionSaveMultiFrame_triggered();
    void on_actionConnectServer_triggered();
    void on_actionSendData_triggered();
    void setMultiFrames();
    void on_storeOver();
    void check_connect_state();

    void on_actionSettings_triggered();

    void on_btnAddBright_clicked();

    void on_btnSubBright_clicked();

    void on_btnAddContrast_clicked();

    void on_btnSubContrast_clicked();

    void on_btnAddExposure_clicked();

    void on_btnSubExposure_clicked();

    void on_btnAddHue_clicked();

    void on_btnSubHue_clicked();

    void on_btnAddSaturation_clicked();

    void on_btnSubSaturation_clicked();

    void on_btnAddGain_clicked();

    void on_btnSubGain_clicked();

//    void on_btnAutoBalance_clicked();

    void on_btnDepthImage_clicked();

    void on_btnPointCloud_clicked();

    void on_radio3DWorld_clicked();

    void on_radio3DCamera_clicked();

    void on_radio3DLast_clicked();

    void on_radioSensingSTD_clicked();

    void on_radioSensingFILL_clicked();

    void on_radioSensingLAST_clicked();

    void on_btnAddWhiteBalance_clicked();

    void on_btnSubWhiteBalance_clicked();

signals:
    void storeOver();

private:
    Ui::MainWindow *ui;
    BriStoreMger    m_storeManager;
    ZEDCamera       m_zedCamera;
    NetManager      m_netManager;
    ZedDataCB      *m_pZedDataCB;
    QString         m_strStorePath;
    long            m_lSaveIndex = 0;
    bool            m_bSaveRGB = true;
    bool            m_bSaveDepth = true;
    bool            m_bShowInfo;
    bool            m_bShowSetting = false;
    bool            m_bShowConnState = true;
    int             m_nMultiCount = 0;
    int             m_nStepCameraSetting = 1;
    int             m_nMultiFrames = 50;
    QLineEdit      *m_leFrame;
    QDialog        *m_dlgMultiFrames;
    QDialog        *m_dlgHelp;
    QLabel         *m_connLabel;
    QLabel         *m_connStatusLabel;
    QLabel         *m_statusLabel;
    QTimer         *timer;
    SaveModel       m_enumSaveModel = SaveModel::NONE;
    const static unsigned int MAX_PATH = 255;
};

#endif // MAINWINDOW_H
