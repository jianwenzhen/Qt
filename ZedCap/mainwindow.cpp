#include <string.h>
#include <sl/Camera.hpp>
#include <sl/types.hpp>
#include <QMessageBox>
#include <QTextBrowser>
#include <QTimer>
#include <QLabel>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "opencv2/opencv.hpp"
#include "zedcamera.h"
#include "publicInfo.h"
#include "YDTime.h"
using namespace std;

void ZedDataCB::dataCallback(ImageWithPose& iwp)
{
    if(m_pMainWnd)
        m_pMainWnd->zedDataCallback(iwp);
    else
        return;
}

void MainWindow::zedDataCallback(ImageWithPose& iwp)
{
    YDTimer timer;
    /// Display
    ui->IWPWidget->setImage(iwp.matLeftRgbImg);
    if (m_zedCamera.isEnableDepthImage())
        ui->DepthWidget->setImage(iwp.matDepthImg);

    qDebug() << QString::asprintf("use time : %d", timer.elapsed());
    /// Store
    switch(m_enumSaveModel)
    {
    case SaveModel::NONE:
        break;
    case SaveModel::ONE:
        m_storeManager.AddIWPToBuffer(new ImageInfo(++m_lSaveIndex, iwp.timestamp, iwp.matLeftRgbImg, iwp.matDepthData, iwp.matDepthImg
                                                    ,iwp.matConfidenceData, iwp.matConfidenceImg, iwp.kMatrix, iwp.disto, iwp.matTransform
                                                    ,iwp.matXVec ,m_bSaveRGB, m_bSaveDepth, m_zedCamera.getSerial()));
        m_enumSaveModel = SaveModel::NONE;
        break;
    case SaveModel::MULTI:
        if(m_nMultiCount < m_nMultiFrames)
        {
            m_storeManager.AddIWPToBuffer(new ImageInfo(++m_lSaveIndex, iwp.timestamp, iwp.matLeftRgbImg, iwp.matDepthData, iwp.matDepthImg
                                                        ,iwp.matConfidenceData, iwp.matConfidenceImg, iwp.kMatrix, iwp.disto, iwp.matTransform
                                                        ,iwp.matXVec ,m_bSaveRGB, m_bSaveDepth, m_zedCamera.getSerial()));
            m_statusLabel->setText(tr("%1 images into buffer").arg(++m_nMultiCount));
        }
        else
        {
            m_enumSaveModel = SaveModel::NONE;
            m_nMultiCount = 0;

            emit storeOver();
        }
        break;
    }
    /// Network
    // m_netManager.addIWPToBuffer(pImageInfo);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_strStorePath("/home/nvidia/Desktop/images")
    , m_lSaveIndex(0)
    , m_bSaveRGB(true)
    , m_bSaveDepth(true)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("ZedCap");

    timer = new QTimer(this);
    timer->setInterval(1000);

    m_pZedDataCB = new ZedDataCB(this);

    m_storeManager.setSaveImagePath(m_strStorePath.toLocal8Bit());

    m_bShowInfo = false;
    showWidgetInfo(m_bShowInfo);

    setBtnEnable(false);

    initWidgetDisplay();
    initSettingWidget();
    initDlgMultiFrame();
    initStatusBar();
    initDlgHelp();

    connect(timer, SIGNAL(timeout()), this, SLOT(check_connect_state()));
    connect(this, SIGNAL(storeOver()), this, SLOT(on_storeOver()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpenCap_triggered()
{
    // static const QString s_btnTxt[2] = {"OpenCap", "CloseCap"};
    static const QIcon s_btnIcon[2] = {QIcon(":/icon/icon/on.png"), QIcon(":/icon/icon/off.png")};

    bool bNotOpen = !m_zedCamera.isOpen();
    if(bNotOpen)
    {
        m_zedCamera.setCallback(m_pZedDataCB);
        m_zedCamera.openCamera();
        // set true if havn't zed
        if(m_zedCamera.isOpen())
        {
            setBtnEnable(true);
            m_zedCamera.startTracking();
            // start store image
            m_storeManager.startStore();
        }
    }
    else
    {
        m_enumSaveModel = SaveModel::NONE;
        if (!m_storeManager.savedAll())
        {
            QMessageBox box;
            box.setIcon(QMessageBox::Warning);
            box.setWindowTitle(tr("Warning"));
            box.setText(tr("Images is saving! Close now?"));
            QPushButton *btnYes = box.addButton(tr("Yes(&Y)"), QMessageBox::YesRole);
            box.addButton(tr("No(&N)"), QMessageBox::NoRole);
            box.exec();
            if (box.clickedButton() != (QAbstractButton *)btnYes)
            {
                while(!m_storeManager.savedAll())
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }
        m_zedCamera.closeCamera();

        setIWPDefalut();
        setDepthDefalut();

        setBtnEnable(false);
        m_zedCamera.setCallback(nullptr);
        // stop store image
        m_storeManager.stopStore();
    }

    bNotOpen = !m_zedCamera.isOpen();
    // ui->actionOpenCap->setText(s_btnTxt[!bNotOpen]);
    ui->actionOpenCap->setIcon(s_btnIcon[bNotOpen]);
}

void MainWindow::setBtnEnable(bool flag)
{
    ui->actionSaveOneFrame->setEnabled(flag);
    ui->actionSaveMultiFrame->setEnabled(flag);
}

void MainWindow::showMessage(const QString &msg, int timeout)
{
    ui->statusBar->showMessage(msg, timeout);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    int key = event->key();
    QLCDNumber *lcd = nullptr;
    switch(key)
    {
    case Qt::Key_A:
    {
        bool bAuto = m_zedCamera.GetEnableAutoWhiteBalance();
        m_zedCamera.SetEnableAutoWhiteBalance(!bAuto);
        QString str = bAuto ? "打开自动白平衡" : "关闭自动白平衡";
        showMessage(str);
        break;
    }
    case Qt::Key_B:
    {
        m_nStepCameraSetting = 1;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_BRIGHTNESS;
        m_zedCamera.setStrSetting(tr("亮度"));
        showMessage("调整亮度, 步进1");
        lcd = ui->lcdBright;
        break;
    }
    case Qt::Key_C:
    {
        m_nStepCameraSetting = 1;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_CONTRAST;
        m_zedCamera.setStrSetting(tr("对比度"));
        lcd = ui->lcdContrast;
        showMessage("调整对比度, 步进1");
        break;
    }
    case Qt::Key_E:
    {
        m_nStepCameraSetting = 1;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_EXPOSURE;
        m_zedCamera.setStrSetting(tr("曝光时间"));
        lcd = ui->lcdExposure;
        showMessage("调整曝光时间, 步进1");
        break;
    }
    case Qt::Key_H:
    {
        m_nStepCameraSetting = 1;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_HUE;
        m_zedCamera.setStrSetting(tr("色调"));
        lcd = ui->lcdHue;
        showMessage("调整色调, 步进1");
        break;
    }
    case Qt::Key_S:
    {
        m_nStepCameraSetting = 1;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_SATURATION;
        m_zedCamera.setStrSetting(tr("饱和度"));
        lcd = ui->lcdSaturation;
        showMessage("调整饱和度, 步进1");
        break;
    }
    case Qt::Key_G:
    {
        m_nStepCameraSetting = 5;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_GAIN;
        m_zedCamera.setStrSetting(tr("增益"));
        lcd = ui->lcdGain;
        showMessage("调整增益, 步进5");
        break;
    }
    case Qt::Key_W:
    {
        m_nStepCameraSetting = 100;
        m_zedCamera.m_camerasettings = CAMERA_SETTINGS_WHITEBALANCE;
        m_zedCamera.setStrSetting(tr("白平衡"));
        showMessage("调整白平衡, 步进100");
        break;
    }
    case Qt::Key_Up:
    case Qt::Key_Right:
    {
//        adjustCameraParameter(false);
        int current_value = m_zedCamera.getCurrentSettingValue();
        m_zedCamera.setCurrentSettingValue(m_nStepCameraSetting + current_value);
        char szTemp[256] = { 0 };
        sprintf(szTemp, "(%d)", m_zedCamera.getCurrentSettingValue());
        showMessage(m_zedCamera.getStrSetting() + szTemp);
        break;
    }
    case Qt::Key_Down:
    case Qt::Key_Left:
    {
//        adjustCameraParameter(false);
        int current_value = m_zedCamera.getCurrentSettingValue();
        m_zedCamera.setCurrentSettingValue(current_value - m_nStepCameraSetting);
        char szTemp[256] = { 0 };
        sprintf(szTemp, "(%d)", m_zedCamera.getCurrentSettingValue());
        showMessage(m_zedCamera.getStrSetting() + szTemp);
        break;
    }
    case Qt::Key_M:
    {
        m_zedCamera.switchSeningMode();
        char szTemp[256] = { 0 };
        sprintf(szTemp, "SeningMode[%s]", m_zedCamera.getSensingMode().c_str());
        showMessage(szTemp);
        break;
    }
    case Qt::Key_F:
    {
        m_zedCamera.switchMeasure3DReferenceFrame();
        char szTemp[256] = { 0 };
        sprintf(szTemp, "Measure3DReferenceFrame[%s]", m_zedCamera.getMeasure3DReferenceFrame().c_str());
        showMessage(szTemp);
        break;
    }
    case Qt::Key_D:
    {
        m_zedCamera.switchDepthImage();
        bool bEnable = m_zedCamera.isEnableDepthImage();
        showMessage(bEnable ? "打开深度图像" : "关闭深度图像");
        break;
    }
    case Qt::Key_P:
    {
        m_zedCamera.switchPointcloud();
        bool bEnable = m_zedCamera.isEnablePc();
        showMessage(bEnable ? "打开点云" : "关闭点云");
        break;
    }
    case Qt::Key_F1:
    {
        m_dlgHelp->show();
        break;
    }
    default:
        break;
    }
    if(lcd)
        lcd->display(m_zedCamera.getCurrentSettingValue());
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    QPoint p = event->pos();
    QPoint pos = m_connLabel->pos();
    QPoint realPos = m_connLabel->mapTo(this, pos);
    int x1 = realPos.x();
    int x2 = x1 + m_connLabel->width();
    int y1 = realPos.y();
    int y2 = y1 + m_connLabel->height();
    if(p.x() > x1 && p.x() < x2 && p.y() > y1 && p.y() < y2)
        m_connStatusLabel->show();
    else
        m_connStatusLabel->hide();
}

void MainWindow::on_actionSaveOneFrame_triggered()
{
    changeSaveModel(SaveModel::ONE);
}

void MainWindow::on_actionSaveMultiFrame_triggered()
{
    changeSaveModel(SaveModel::MULTI);
}

bool MainWindow::changeSaveModel(SaveModel model)
{
    bool flag = true;
    if (m_enumSaveModel == SaveModel::NONE)
    {
        if (model == SaveModel::MULTI)
        {
            m_dlgMultiFrames->exec();
        }
        m_enumSaveModel = model;
    }
    else if(m_enumSaveModel == SaveModel::MULTI)
    {
        QMessageBox::warning(this, tr("Waring"), tr("Saving multi image!"));
        flag = false;
    }
    else
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    return flag;
}

void MainWindow::on_actionConnectServer_triggered()
{
    bool conn = m_netManager.connectState();
    if (conn)
    {
        m_netManager.disconnect();
        ui->actionConnectServer->setIcon(QIcon(":/icon/icon/disconnect.png"));
    }
    else
    {
        m_netManager.connectToServer();
        conn = m_netManager.connectState();
        if (conn)
        {
            ui->actionConnectServer->setIcon(QIcon(":/icon/icon/connect.png"));
            timer->start();
        }
    }
}

void MainWindow::on_actionSendData_triggered()
{
    m_netManager.startSend();
}

void MainWindow::setMultiFrames()
{
    int frames = m_leFrame->text().toInt();
    if (frames > 0)
        m_nMultiFrames = frames;
    m_dlgMultiFrames->close();
}

void MainWindow::on_storeOver()
{
    QMessageBox::information(nullptr, tr("Tips"), tr("Multi images grab Over"));
    m_statusLabel->clear();
}

void MainWindow::check_connect_state()
{
    bool conn = m_netManager.connectState();
    if (conn)
    {
        if (m_bShowConnState)
        {
            m_connLabel->setPixmap(QPixmap(":/icon/icon/conn.png").scaled(20, 20));
            m_connStatusLabel->setText("Connecting");
        }
        else
            m_connLabel->clear();
        m_bShowConnState = !m_bShowConnState;
    }
    else
    {
        m_connLabel->setPixmap(QPixmap(":/icon/icon/disconn.png").scaled(20, 20));
        m_connStatusLabel->setText("Disconnect");
        ui->actionConnectServer->setIcon(QIcon(":/icon/icon/disconnect.png"));
        timer->stop();
    }
}

void MainWindow::initDlgMultiFrame()
{
    m_dlgMultiFrames = new QDialog(this);
    m_dlgMultiFrames->setWindowTitle("Set Multi Frames");
    m_leFrame = new QLineEdit(m_dlgMultiFrames);
    m_leFrame->setPlaceholderText("50");
    QPushButton *btn = new QPushButton("OK", m_dlgMultiFrames);
    QVBoxLayout *layout = new QVBoxLayout(m_dlgMultiFrames);
    layout->addWidget(m_leFrame);
    layout->addWidget(btn);
    QObject::connect(btn, SIGNAL(pressed()), this, SLOT(setMultiFrames()));
}

void MainWindow::initDlgHelp()
{
    m_dlgHelp = new QDialog(this);
    m_dlgHelp->setWindowTitle("Help");
    QTextBrowser *browser = new QTextBrowser(m_dlgHelp);
    browser->setMinimumSize(400, 300);
    QString help;
    help.sprintf("%s\n", "HotKey");
    QString temp;
    help += temp.sprintf("-------------------------------------------------------------------------------------\n");
    help += temp.sprintf("%10s\t\t%s\n", "A", "关闭/打开自动白平衡");
    help += temp.sprintf("%10s\t\t%s\n", "B", "调整亮度, 步进1");
    help += temp.sprintf("%10s\t\t%s\n", "C", "调整对比度, 步进1");
    help += temp.sprintf("%10s\t\t%s\n", "E", "调整曝光时间, 步进1");
    help += temp.sprintf("%10s\t\t%s\n", "H", "调整色调, 步进1");
    help += temp.sprintf("%10s\t\t%s\n", "G", "调整增益, 步进5");
    help += temp.sprintf("%10s\t\t%s\n", "S", "调整饱和度, 步进5");
    help += temp.sprintf("%10s\t\t%s\n", "W", "调整白平衡, 步进100");
    help += temp.sprintf("%10s\t\t%s\n", "M", "switchSeningMode");
    help += temp.sprintf("%10s\t\t%s\n", "F", "switchMeasure3DReferenceFrame");
    help += temp.sprintf("%10s\t\t%s\n", "D", "关闭/打开深度图像");
    help += temp.sprintf("%10s\t\t%s\n", "P", "关闭/打开点云");
    help += temp.sprintf("%10s\t\t%s\n", "Up/Right", "向上调整");
    help += temp.sprintf("%10s\t\t%s\n", "Down/Left", "向下调整");
    browser->setPlainText(help);
}

void MainWindow::initWidgetDisplay()
{
    static const QPixmap pix(":/icon/icon/noimage.png");
    ui->IWPWidget->setDefault(pix);
    ui->DepthWidget->setDefault(pix);
    ui->TDWidget->setDefault(pix);
}

void MainWindow::initLCD()
{
    ui->lcdBright->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_BRIGHTNESS));
    ui->lcdContrast->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_CONTRAST));
    ui->lcdExposure->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_EXPOSURE));
    ui->lcdGain->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_GAIN));
    ui->lcdHue->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_HUE));
    ui->lcdSaturation->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_SATURATION));
    ui->lcdWhiteBalance->display(m_zedCamera.getCameraSettings(sl::CAMERA_SETTINGS_WHITEBALANCE));
}

void MainWindow::initStatusBar()
{
    m_connLabel = new QLabel(ui->statusBar);
    m_statusLabel = new QLabel(ui->statusBar);
    m_connLabel->setMaximumSize(20, 20);
    // set status's item no border
    ui->statusBar->setStyleSheet(QString("QStatusBar::item{border:0px}"));
    ui->statusBar->setMinimumHeight(20);
    ui->statusBar->setMaximumHeight(20);
    m_connStatusLabel = new QLabel(ui->statusBar);
    ui->statusBar->addWidget(m_connLabel);
    ui->statusBar->addWidget(m_connStatusLabel);
    ui->statusBar->addWidget(m_statusLabel);
    m_connStatusLabel->hide();

    this->setMouseTracking(true);
    this->statusBar()->setMouseTracking(true);
    m_connLabel->setMouseTracking(true);
}

void MainWindow::setIWPDefalut()
{
    static const QPixmap pix(":/icon/icon/noimage.png");
    ui->IWPWidget->setImage(pix);
}

void MainWindow::setDepthDefalut()
{
    static const QPixmap pix(":/icon/icon/noimage.png");
    ui->DepthWidget->setImage(pix);
}

void MainWindow::showWidgetInfo(bool flag)
{
    ui->IWPWidget->showInfo(flag);
    ui->IWPWidget->showHistogram(flag);

    ui->DepthWidget->showInfo(flag);
    ui->DepthWidget->showHistogram(flag);

    ui->TDWidget->showInfo(flag);
    ui->TDWidget->showHistogram(flag);

}

void MainWindow::initSettingWidget()
{
    QIcon upIcon(":/icon/icon/up.png");
    QIcon downIcon(":/icon/icon/down.png");
    ui->btnAddBright->setIcon(upIcon);
    ui->btnAddContrast->setIcon(upIcon);
    ui->btnAddExposure->setIcon(upIcon);
    ui->btnAddGain->setIcon(upIcon);
    ui->btnAddHue->setIcon(upIcon);
    ui->btnAddSaturation->setIcon(upIcon);
    ui->btnAddWhiteBalance->setIcon(upIcon);

    ui->btnSubBright->setIcon(downIcon);
    ui->btnSubContrast->setIcon(downIcon);
    ui->btnSubExposure->setIcon(downIcon);
    ui->btnSubGain->setIcon(downIcon);
    ui->btnSubHue->setIcon(downIcon);
    ui->btnSubSaturation->setIcon(downIcon);
    ui->btnSubWhiteBalance->setIcon(downIcon);

    QButtonGroup *radioGroup3D = new QButtonGroup();
    radioGroup3D->addButton(ui->radio3DCamera);
    radioGroup3D->addButton(ui->radio3DWorld);
    radioGroup3D->addButton(ui->radio3DLast);

    QButtonGroup *radioGroupSensing = new QButtonGroup();
    radioGroupSensing->addButton(ui->radioSensingFILL);
    radioGroupSensing->addButton(ui->radioSensingLAST);
    radioGroupSensing->addButton(ui->radioSensingSTD);

    ui->btnDepthImage->setIcon(QIcon(":/icon/icon/on.png"));
    ui->btnDepthImage->setIconSize(QSize(60, 20));
    ui->btnDepthImage->setFlat(true);
//    ui->btnDepthImage->setFocusPolicy(Qt::NoFocus);
    ui->btnDepthImage->setStyleSheet("background:transparent;border:none;");

    ui->btnPointCloud->setIcon(QIcon(":/icon/icon/on.png"));
    ui->btnPointCloud->setFlat(true);
    ui->btnPointCloud->setIconSize(QSize(60, 20));
//     cancel black when pressed
//    ui->btnPointCloud->setFocusPolicy(Qt::NoFocus);
    ui->btnPointCloud->setStyleSheet("background:transparent;border:none;");

    ui->radio3DWorld->setChecked(true);
    ui->radioSensingSTD->setChecked(true);

    ui->settingWidget->hide();
}

void MainWindow::on_actionSettings_triggered()
{
    m_bShowSetting = !m_bShowSetting;
    if (m_bShowSetting)
    {
        initLCD();
        ui->settingWidget->show();
    }
    else
        ui->settingWidget->hide();
}

void MainWindow::on_btnAddBright_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_BRIGHTNESS;
    m_zedCamera.setStrSetting(tr("亮度"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubBright_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_BRIGHTNESS;
    m_zedCamera.setStrSetting(tr("亮度"));
    adjustCameraParameter(false);
}

void MainWindow::on_btnAddContrast_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_CONTRAST;
    m_zedCamera.setStrSetting(tr("对比度"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubContrast_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_CONTRAST;
    m_zedCamera.setStrSetting(tr("对比度"));
    adjustCameraParameter(false);
}

void MainWindow::on_btnAddExposure_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_EXPOSURE;
    m_zedCamera.setStrSetting(tr("曝光时间"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubExposure_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_EXPOSURE;
    m_zedCamera.setStrSetting(tr("曝光时间"));
    adjustCameraParameter(false);
}

void MainWindow::on_btnAddHue_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_HUE;
    m_zedCamera.setStrSetting(tr("色调"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubHue_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_HUE;
    m_zedCamera.setStrSetting(tr("色调"));
    adjustCameraParameter(false);
}

void MainWindow::on_btnAddSaturation_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_SATURATION;
    m_zedCamera.setStrSetting(tr("饱和度"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubSaturation_clicked()
{
    m_nStepCameraSetting = 1;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_SATURATION;
    m_zedCamera.setStrSetting(tr("饱和度"));
    adjustCameraParameter(false);
}

void MainWindow::on_btnAddGain_clicked()
{
    m_nStepCameraSetting = 5;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_GAIN;
    m_zedCamera.setStrSetting(tr("增益"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubGain_clicked()
{
    m_nStepCameraSetting = 5;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_GAIN;
    m_zedCamera.setStrSetting(tr("增益"));
    adjustCameraParameter(false);
}

void MainWindow::on_btnAddWhiteBalance_clicked()
{
    m_nStepCameraSetting = 100;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_WHITEBALANCE;
    m_zedCamera.setStrSetting(tr("白平衡"));
    adjustCameraParameter(true);
}

void MainWindow::on_btnSubWhiteBalance_clicked()
{
    m_nStepCameraSetting = 100;
    m_zedCamera.m_camerasettings = CAMERA_SETTINGS_WHITEBALANCE;
    m_zedCamera.setStrSetting(tr("白平衡"));
    adjustCameraParameter(false);
}

void MainWindow::adjustCameraParameter(bool add)
{
    int current_value = m_zedCamera.getCurrentSettingValue();
    QString setting = m_zedCamera.getStrSetting();
    QLCDNumber *lcd;
    if (setting == "亮度")
        lcd = ui->lcdBright;
    else if (setting == "对比度")
        lcd = ui->lcdContrast;
    else if (setting == "曝光时间")
        lcd = ui->lcdExposure;
    else if (setting == "色调")
        lcd = ui->lcdHue;
    else if (setting == "饱和度")
        lcd = ui->lcdSaturation;
    else if (setting == "增益")
        lcd = ui->lcdGain;
    else if (setting == "白平衡")
        lcd = ui->lcdWhiteBalance;
    if (add)
    {
        m_zedCamera.setCurrentSettingValue(m_nStepCameraSetting + current_value);
    }
    else
    {
        m_zedCamera.setCurrentSettingValue(current_value - m_nStepCameraSetting);
    }
    lcd->display(m_zedCamera.getCurrentSettingValue());
    //char szTemp[256] = { 0 };
    //sprintf(szTemp, "(%d)", m_zedCamera.getCurrentSettingValue());
    //showMessage(m_zedCamera.getStrSetting() + szTemp);
}

//void MainWindow::on_btnAutoBalance_clicked()
//{
//    bool bAuto = m_zedCamera.GetEnableAutoWhiteBalance();
//    m_zedCamera.SetEnableAutoWhiteBalance(!bAuto);
//    showMessage(bAuto ? "打开自动白平衡" : "关闭自动白平衡");
//    ui->btnAutoBalance->setText(bAuto ? "关闭自动白平衡" : "打开自动白平衡");
//}

void MainWindow::on_btnDepthImage_clicked()
{
    static const QIcon iconOn(":/icon/icon/on.png");
    static const QIcon iconOff(":/icon/icon/off.png");
    m_zedCamera.switchDepthImage();
    bool bEnable = m_zedCamera.isEnableDepthImage();

    if(bEnable)
        ui->btnDepthImage->setIcon(iconOn);
    else
    {
        ui->btnDepthImage->setIcon(iconOff);
        setDepthDefalut();
    }
}

void MainWindow::on_btnPointCloud_clicked()
{
    static const QIcon iconOn(":/icon/icon/on.png");
    static const QIcon iconOff(":/icon/icon/off.png");
    m_zedCamera.switchPointcloud();
    bool bEnable = m_zedCamera.isEnablePc();
   //showMessage(bEnable ? "打开点云" : "关闭点云");
//   ui->btnPointCloud->setText(bEnable ? "关闭点云" : "打开点云");
    if(bEnable)
        ui->btnPointCloud->setIcon(iconOn);
    else
        ui->btnPointCloud->setIcon(iconOff);
}

void MainWindow::on_radio3DWorld_clicked()
{
    m_zedCamera.setMeasure3DReferenceFrame(REFERENCE_FRAME_WORLD);
}

void MainWindow::on_radio3DCamera_clicked()
{
    m_zedCamera.setMeasure3DReferenceFrame(REFERENCE_FRAME_CAMERA);
}

void MainWindow::on_radio3DLast_clicked()
{
    m_zedCamera.setMeasure3DReferenceFrame(REFERENCE_FRAME_LAST);
}

void MainWindow::on_radioSensingSTD_clicked()
{
    m_zedCamera.setSeningMode(SENSING_MODE_STANDARD);
}

void MainWindow::on_radioSensingFILL_clicked()
{
    m_zedCamera.setSeningMode(SENSING_MODE_FILL);
}

void MainWindow::on_radioSensingLAST_clicked()
{
    m_zedCamera.setSeningMode(SENSING_MODE_LAST);
}
