#include "publicInfo.h"


ImageInfo::ImageInfo()
    :m_nFrameIndex(-1)
    ,m_bCopyData(true)
{

}

ImageInfo::ImageInfo(long nIndx, uint64 nFrameTime, const cv::Mat& image, const cv::Mat& depthdata, const cv::Mat& depthimage,
                                const cv::Mat& confidencedata, const cv::Mat& confidenceimage,
                                const cv::Mat& kMatrix, const cv::Mat& dist, const cv::Mat& matTransform, const cv::Mat& XVec,
                                const bool& bSaveRgb, const bool& bSaveDepth,const std::string& strFriendlyName)
                                :m_nFrameIndex(nIndx)
                                , m_nFrameTime(nFrameTime)
                                , m_bCopyData(true)
                                , m_bSaveRgb(bSaveRgb)
                                , m_strFriendlyName(strFriendlyName)
{
    if (m_bCopyData)
    {
        image.copyTo(m_image);
        depthdata.copyTo(m_depthdata);
        depthimage.copyTo(m_depthimage);
        confidencedata.copyTo(m_confidencedata);
        confidenceimage.copyTo(m_confidenceimage);

        kMatrix.copyTo(m_kMatrix);
        dist.copyTo(m_dist);
        matTransform.copyTo(m_matTransform);
        XVec.copyTo(m_XVec);
    }
    else
    {
        m_image = image;
        m_depthdata = depthdata;
        m_depthimage = depthimage;
        m_confidencedata = confidencedata;
        m_confidenceimage = confidenceimage;

        m_kMatrix = kMatrix;
        m_dist = dist;
        m_matTransform = matTransform;
        m_XVec = XVec;
    }
}

ImageInfo::~ImageInfo()
{

}

ImageInfo::ImageInfo(const ImageInfo& other)
{
    m_nFrameIndex = other.m_nFrameIndex;
    m_nFrameTime  = other.m_nFrameTime;
    if (m_bCopyData)
    {
        other.m_image.copyTo(m_image);
        other.m_depthdata.copyTo(m_depthdata);
        other.m_depthimage.copyTo(m_depthimage);
        other.m_confidencedata.copyTo(m_confidencedata);
        other.m_confidenceimage.copyTo(m_confidenceimage);

        other.m_kMatrix.copyTo(m_kMatrix);
        other.m_dist.copyTo(m_dist);
        other.m_matTransform.copyTo(m_matTransform);
        other.m_XVec.copyTo(m_XVec);
    }
    else
    {
        m_image = other.m_image;
        m_depthdata = other.m_depthdata;
        m_depthimage = other.m_depthimage;
        m_confidencedata = other.m_confidencedata;
        m_confidenceimage = other.m_confidenceimage;

        m_kMatrix = other.m_kMatrix;
        m_dist = other.m_dist;
        m_matTransform = other.m_matTransform;
        m_XVec = other.m_XVec;
    }
}

ImageInfo& ImageInfo::operator=(const ImageInfo& other)
{
    m_nFrameIndex = other.m_nFrameIndex;
    m_nFrameTime  = other.m_nFrameTime;
    if (m_bCopyData)
    {
        other.m_image.copyTo(m_image);
        other.m_depthdata.copyTo(m_depthdata);
        other.m_depthimage.copyTo(m_depthimage);
        other.m_confidencedata.copyTo(m_confidencedata);
        other.m_confidenceimage.copyTo(m_confidenceimage);

        other.m_kMatrix.copyTo(m_kMatrix);
        other.m_dist.copyTo(m_dist);
        other.m_matTransform.copyTo(m_matTransform);
        other.m_XVec.copyTo(m_XVec);
    }
    else
    {
        m_image = other.m_image;
        m_depthdata = other.m_depthdata;
        m_depthimage = other.m_depthimage;
        m_confidencedata = other.m_confidencedata;
        m_confidenceimage = other.m_confidenceimage;

        m_kMatrix = other.m_kMatrix;
        m_dist = other.m_dist;
        m_matTransform = other.m_matTransform;
        m_XVec = other.m_XVec;
    }

    return *this;
}
