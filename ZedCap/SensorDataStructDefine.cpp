#include "SensorDataStructDefine.h"

bool isTrueBit(short value, int offset)
{
	return 1 == (0x01 & (value >> offset));
}

short enableBit(short value, int offset, bool bEnable)
{
	short ret = value;
	if (bEnable)
	{
		ret |= (1 << offset);
	}
	else
	{
		ret &= ~(1 << offset);
	}
	return ret;
}
