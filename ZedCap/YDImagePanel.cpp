
#include <QPainter>
#include <vector>
#include <QDebug>
#include <QResizeEvent>
#include "YDImagePanel.h"


static void Histogram(const QImage& img, std::vector<int>& R, std::vector<int>& G, std::vector<int>& B)
{
	R.resize(256, 0);
	G.resize(256, 0);
	B.resize(256, 0);
	memset(&R[0], 0, sizeof(int) * 256);
	memset(&G[0], 0, sizeof(int) * 256);
	memset(&B[0], 0, sizeof(int) * 256);
	for (int i = 0; i != img.width(); i++)
	{
		for (int j = 0; j != img.height(); j++)
		{
			QColor rgb = img.pixelColor(i, j);
			R[rgb.red()]++;
			G[rgb.green()]++;
			B[rgb.blue()]++;
		}
	}
}

YDImagePanel::YDImagePanel(QWidget *parent)
	: QWidget(parent)
	, m_bShowInfo(true)
	, m_bShowHistogram(true)
	, m_nMaxR(0)
	, m_nMaxG(0)
	, m_nMaxB(0)
	, m_pImageData(nullptr)
	, m_nImageW(0)
	, m_nImageH(0)
	, m_nStep(0)
{
	setMouseTracking(true);
}

YDImagePanel::~YDImagePanel()
{
	if (m_pImageData)
	{
		delete[] m_pImageData;
		m_pImageData = nullptr;
	}
}

void YDImagePanel::resizeEvent(QResizeEvent *event)
{
	YDBaseZoom::Resize(this->width(), this->height());
}

void YDImagePanel::drawInfo(QPainter& painter)
{
	if (m_bShowInfo)
	{
		painter.setPen(QPen(QColor(0, 255, 0)));
		painter.fillRect(QRect(10, height() - 100, 210, 90), QBrush(QColor(80, 80, 80, 100)));
		QString strInfo = QString::asprintf("scale: %0.2f", YDBaseZoom::GetZoomScale());
		
		painter.drawText(QPointF(20, height() - 80), strInfo);
		
		painter.drawText(QPointF(20, height() - 60), m_strMouseRGB);

		painter.drawText(QPointF(20, height() - 40), m_strMousePos);
		
		strInfo = QString::asprintf("resolution: %d * %d", m_image.width(), m_image.height());
		painter.drawText(QPointF(20, height() - 20), strInfo);
	}
}

void YDImagePanel::paintEvent(QPaintEvent *event)
{

	QPainter painter(this);
	QRect rcClient(0, 0, this->width(), this->height());
	painter.fillRect(rcClient, QBrush(Qt::black));
	painter.setFont(QFont("Consolas"));
	if (!m_image.isNull())
	{
		painter.setRenderHint(QPainter::SmoothPixmapTransform);
		painter.drawPixmap(YDBaseZoom::GetRectPosition().toRect(), m_image);
		
		drawInfo(painter);
	
		drawHistogram(painter, QRect(rcClient.right() - 300, rcClient.top() + 30, 256, 100));

		YDBaseZoom::drawThumbnail(painter);
	
	}
	else if (!m_default.isNull())
	{
		QRect position((rect().width() - m_default.width()) / 2.0
			, (rect().height() - m_default.height()) / 2.0
			, m_default.width(), m_default.height());
		painter.drawPixmap(position, m_default);
	}
	emit paint(&painter);
}

void YDImagePanel::setDefault(const QPixmap& image)
{
	m_default = image.copy();

	UpdateDraw();
}

void YDImagePanel::setImage(const QPixmap& image)
{
	m_image = image;
	
	if (isShowHistogram())
	{
		calcHistogram();
	}

	YDBaseZoom::SetOrgImageSize(m_image.width(), m_image.height());

	YDBaseZoom::SetThumbnail(m_image);

	UpdateDraw();
}

void YDImagePanel::setImage(int nW, int nH, int nC, uchar* imageData)
{

}

void YDImagePanel::wheelEvent(QWheelEvent *event)
{
	YDBaseZoom::wheelEvent(event);

	update();
}

void YDImagePanel::mouseMoveEvent(QMouseEvent *event)
{
	YDBaseZoom::mouseMoveEvent(event);
	
	////������ͼ������
	double fImageX = 0.0;
	double fImageY = 0.0;
	YDBaseZoom::Com_ScreenCoor2ImageCoor_S(event->localPos(), fImageX, fImageY);
	m_strMousePos = QString::asprintf("image:(%0.2f, %0.2f)", fImageX, fImageY);
	QColor rgb = m_image.toImage().pixelColor(fImageX, fImageY);
	m_strMouseRGB = QString::asprintf("rgb:(%03d, %03d, %03d)", rgb.red(), rgb.green(), rgb.blue());
	update();
}

void YDImagePanel::mousePressEvent(QMouseEvent *event)
{
	YDBaseZoom::mousePressEvent(event);
}

void YDImagePanel::mouseReleaseEvent(QMouseEvent *event)
{
	YDBaseZoom::mouseReleaseEvent(event);
}

void YDImagePanel::mouseDoubleClickEvent(QMouseEvent *event)
{
	YDBaseZoom::mouseDoubleClickEvent(event);
}

void YDImagePanel::UpdateDraw()
{
	update();
}

void YDImagePanel::showInfo(bool bShow)
{
	m_bShowInfo = bShow;
	update();
}

bool YDImagePanel::isShowInfo(void)
{
	return m_bShowInfo;
}

void YDImagePanel::drawHistogram(QPainter& painter, const QRect& rcPosi)
{
	if (!isShowHistogram())
	{
		return;
	}

	int maxcount = m_nMaxR;

	const int  wid = rcPosi.width();
	const int  hei = rcPosi.height();

	painter.setRenderHint(QPainter::Antialiasing, true);
	painter.drawText(QRect(rcPosi.left(), rcPosi.top() - 20, rcPosi.width(), 20),
		Qt::AlignHCenter, tr("histogram"));
	painter.fillRect(rcPosi, QBrush(QColor(100, 100, 100, 140)));
	painter.setPen(QColor(0, 0, 200));
	painter.drawLine(rcPosi.bottomLeft(), rcPosi.bottomRight());// ����  
	painter.drawLine(rcPosi.bottomLeft(), rcPosi.topLeft());//����  

	float xstep = float(wid) / 256.0;
	float ystep = float(hei) / maxcount;
	//QVector<QPointF> vPoints;
	for (int i = 0; i < 256; i++)
	{
		float fValueY = ystep * m_histCountR[i];
		painter.setPen(QColor(i, 255 - i, 0));
		painter.drawRect(rcPosi.left() + i * xstep, rcPosi.bottom() - fValueY, xstep, fValueY);
		//vPoints.push_back(QPointF(rcPosi.left() + i * xstep, rcPosi.bottom() - fValueY));
		if (i % 32 == 0 || i == 255)
		{
			painter.drawText(QPointF(rcPosi.left() + (i - 0.5)*xstep, rcPosi.bottom() + 15), QString::number(i));
		}
	}
	painter.setRenderHint(QPainter::Antialiasing, false);

	
	//painter.setPen(QColor(0, 200, 0));
	//painter.drawPoints(vPoints);
}

void YDImagePanel::calcHistogram(void)
{
	Histogram(m_image.toImage(), m_histCountR, m_histCountG, m_histCountB);

	std::vector<int> temp = m_histCountR;
	std::sort(temp.begin(), temp.end());
	m_nMaxR = temp[temp.size() - 1];

	temp = m_histCountG;
	std::sort(temp.begin(), temp.end());
	m_nMaxG = temp[temp.size() - 1];

	temp = m_histCountB;
	std::sort(temp.begin(), temp.end());
	m_nMaxB = temp[temp.size() - 1];
}

void YDImagePanel::showHistogram(bool bShow)
{

	if (bShow)
	{
		calcHistogram();
	}

	m_bShowHistogram = bShow;

	update();
}

bool YDImagePanel::isShowHistogram(void)
{
	return m_bShowHistogram;
}

void YDImagePanel::updateImageData(int nW, int nH, int nStep, uchar* imageData)
{
	if (m_nImageW != nW || m_nImageH != nH || m_nStep != nStep)
	{
		if (m_pImageData)
		{
			delete[] m_pImageData;
			m_pImageData = nullptr;
		}
	}
	int imageSize = nW * nH * 3 * nStep/8;
	if (nullptr == m_pImageData)
	{
		m_pImageData = new unsigned char[imageSize];
		m_nImageW = nW;
		m_nImageH = nH;
		m_nStep = nStep;
	}
	memcpy(m_pImageData, imageData, imageSize);
}
