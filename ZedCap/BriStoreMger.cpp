#include <QDir>
#include <QTime>
#include <thread>
#include <QApplication>
#include "BriStoreMger.h"
#include "publicInfo.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#define LINELASERSIZE 1080
#define MAX_PATH 256


BriStoreMger::BriStoreMger(void)
    : m_nSingleFrameIndex(0)
{
}


BriStoreMger::~BriStoreMger(void)
{

}

void BriStoreMger::setSaveImagePath(const QString& strPath)
{
	QDir dir(strPath);
	if (!dir.exists())
	{
		dir.mkpath(strPath);
	}
    m_strImageSavePath = strPath.toLocal8Bit().data();
}

void BriStoreMger::run(void)
{
    //while (isRun())
    //{
        if (/*!saveBtCtrlDataToFile() && */!SaveOneIWPFrameToFile())
        {
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}

		needWait(false);
//	}
//	while (saveBtCtrlDataToFile())
//	{ }
//	m_btCtrlFile.close();
}

void BriStoreMger::startStore(void)
{
	if (!YDThread::isRun())
	{
		YDThread::start();
	}
	YDThread::trigger();
}

void BriStoreMger::stopStore(void)
{
	YDThread::stop();
}

bool BriStoreMger::SaveOneIWPFrameToFile(void)
{
    bool bRet = false;
    m_lockIWPBuffer.AddRead();
    size_t nSize = m_listIWPBuffer.size();
    m_lockIWPBuffer.DecRead();
    if (nSize)
    {
        ImageInfo* pImgInfo = m_listIWPBuffer.front();

        char szPath[MAX_PATH] = { 0 };
        char szPath1[MAX_PATH] = { 0 }; //rgb image
        char szPath2[MAX_PATH] = { 0 }; //path
        char szPath3[MAX_PATH] = { 0 };
        char szPath4[MAX_PATH] = { 0 };
        char szPath5[MAX_PATH] = { 0 };
        char szPath6[MAX_PATH] = { 0 };
        sprintf(szPath, "%s/%s", m_strImageSavePath.c_str(), pImgInfo->m_strFriendlyName.c_str());
        if (!YDusky::IsExistFileOrDirectory(szPath))
        {
            YDusky::createPath(szPath);
        }

        if (pImgInfo->m_nFrameTime)
        {
            //YDuskyFun::Int64ToSystemTime(pImgInfo->m_nFrameTime, time);
            sprintf(szPath1, "%s/ZED-rgb-%06d-%d.jpg"
                , szPath
                , pImgInfo->m_nFrameIndex
                , pImgInfo->m_nFrameTime);
            sprintf(szPath2, "%s/ZED-rgb-%06d-%d.pose"
                , szPath
                , pImgInfo->m_nFrameIndex
                , pImgInfo->m_nFrameTime);
            sprintf(szPath3, "%s/ZED-depth-%06d-%d.dat"
                , szPath
                , pImgInfo->m_nFrameIndex
                , pImgInfo->m_nFrameTime);
            sprintf(szPath4, "%s/ZED-depth-%06d-%d.png"
                , szPath
                , pImgInfo->m_nFrameIndex
                , pImgInfo->m_nFrameTime);
            sprintf(szPath5, "%s/ZED-confidence-%06d-%d.dat"
                , szPath
                , pImgInfo->m_nFrameIndex
                , pImgInfo->m_nFrameTime);
            sprintf(szPath6, "%s/ZED-confidence-%06d-%d.png"
                , szPath
                , pImgInfo->m_nFrameIndex
                , pImgInfo->m_nFrameTime);
        }
        else
        {
            sprintf(szPath1, "%s/ZED-rgb-%06d.jpg", szPath, pImgInfo->m_nFrameIndex);
            sprintf(szPath2, "%s/ZED-rgb-%06d.pose", szPath, pImgInfo->m_nFrameIndex);
            sprintf(szPath3, "%s/ZED-depth-%06d.dat", szPath, pImgInfo->m_nFrameIndex);
            sprintf(szPath4, "%s/ZED-depth-%06d.png", szPath, pImgInfo->m_nFrameIndex);
            sprintf(szPath5, "%s/ZED-confidence-%06d.dat", szPath, pImgInfo->m_nFrameIndex);
            sprintf(szPath6, "%s/ZED-confidence-%06d.png", szPath, pImgInfo->m_nFrameIndex);
        }

        cv::imwrite(szPath1, pImgInfo->m_image);
        cv::imwrite(szPath4, pImgInfo->m_depthimage);
        cv::imwrite(szPath6, pImgInfo->m_confidenceimage);

        SaveBinaryData(pImgInfo->m_depthdata, szPath3);
        SaveBinaryData(pImgInfo->m_confidencedata, szPath5);

        std::ofstream pose_file(szPath2);
        if (pose_file.is_open())
        {
            int nIntrinsicType = 3; // total intrinsic
            pose_file << "IntrinsicType " << nIntrinsicType << std::endl;
            pose_file << "==Intrinsic==" << std::endl;
            pose_file << "Size" << std::endl << pImgInfo->m_image.size().width << " " << pImgInfo->m_image.size().height << std::endl;
            pose_file << "KMatrix" << std::endl <<
                pImgInfo->m_kMatrix.at<double>(0, 0) << " " << pImgInfo->m_kMatrix.at<double>(0, 1) << " " << pImgInfo->m_kMatrix.at<double>(0, 2) << std::endl <<
                pImgInfo->m_kMatrix.at<double>(1, 0) << " " << pImgInfo->m_kMatrix.at<double>(1, 1) << " " << pImgInfo->m_kMatrix.at<double>(1, 2) << std::endl <<
                pImgInfo->m_kMatrix.at<double>(2, 0) << " " << pImgInfo->m_kMatrix.at<double>(2, 1) << " " << pImgInfo->m_kMatrix.at<double>(2, 2) << std::endl;
            pose_file << "==Dist==" << std::endl <<
                pImgInfo->m_dist.at<double>(0) << " " << pImgInfo->m_dist.at<double>(1) << " " <<
                pImgInfo->m_dist.at<double>(2) << " " << pImgInfo->m_dist.at<double>(3) << " " << pImgInfo->m_dist.at<double>(4) << std::endl;

            pose_file << "==Extrinsic==" << std::endl;
            pose_file << "Position" << std::endl;
            pose_file << pImgInfo->m_XVec.at<double>(0) << " " << pImgInfo->m_XVec.at<double>(1) << " " << pImgInfo->m_XVec.at<double>(2) << std::endl;
            pose_file << "Translation" << std::endl;
            pose_file << pImgInfo->m_matTransform(cv::Rect(3, 0, 1, 3)).at<double>(0) << " " << pImgInfo->m_matTransform(cv::Rect(3, 0, 1, 3)).at<double>(1) << " " << pImgInfo->m_matTransform(cv::Rect(3, 0, 1, 3)).at<double>(2) << std::endl;
            pose_file << "Rotation" << std::endl;
            pose_file << pImgInfo->m_matTransform.at<double>(0, 0) << " " << pImgInfo->m_matTransform.at<double>(0, 1) << " " << pImgInfo->m_matTransform.at<double>(0, 2) << std::endl <<
                pImgInfo->m_matTransform.at<double>(1, 0) << " " << pImgInfo->m_matTransform.at<double>(1, 1) << " " << pImgInfo->m_matTransform.at<double>(1, 2) << std::endl <<
                pImgInfo->m_matTransform.at<double>(2, 0) << " " << pImgInfo->m_matTransform.at<double>(2, 1) << " " << pImgInfo->m_matTransform.at<double>(2, 2) << std::endl;

        }
        pose_file.close();

        delete pImgInfo;
        pImgInfo = NULL;
        m_lockIWPBuffer.EnableWrite();
        m_listIWPBuffer.pop_front();
        m_lockIWPBuffer.DisableWrite();

        bRet = true;
    }
    return bRet;
}

void BriStoreMger::SaveBinaryData(const cv::Mat& matDepth, const std::string& sPath)
{
    int col = matDepth.size().width;
    int row = matDepth.size().height;
    float *depthdata = new float[col*row];
    memset(depthdata, 0x00, sizeof(float)*col*row);
    float* ptr = (float*)matDepth.data;
    int size = col*row;
    for (int i = 0; i < col*row; ++i)
        depthdata[i] = *(ptr + i);
    std::ofstream fileDepthData(sPath.c_str(), std::ios::out | std::ios::binary);
    if (fileDepthData.is_open())
    {
        fileDepthData.write((char*)&col, sizeof(int));
        fileDepthData.write((char*)&row, sizeof(int));
        fileDepthData.write((char*)depthdata, sizeof(float) * col*row);
        fileDepthData.flush();
    }
    fileDepthData.close();
    delete[]depthdata;
    depthdata = nullptr;
}

void BriStoreMger::AddIWPToBuffer(ImageInfo* imgInfo)
{
    if (imgInfo)
    {
        m_lockIWPBuffer.EnableWrite();
        m_listIWPBuffer.push_back(imgInfo);
        m_lockIWPBuffer.DisableWrite();

        if(!YDThread::isRun())
        {
            YDThread::start();
        }
        YDThread::trigger();
    }
}

bool BriStoreMger::savedAll()
{
    if (m_listIWPBuffer.size() == 0)
        return true;
    return false;
}

