/************************************************************************
* Copyright (c) 2015
* All rights resverved.
*
* 文件名称：StoreManager.h
* 摘    要：存储模块
* 引入文件：
*
* 当前版本：1.0.2
* 作    者：杨玉坤
* 完成日期：2015.06.27
* 更新日志：2015.08.15
* 添加void AddImageToBuffer(ImageInfo* imgInfo);
************************************************************************/
#include <string>
#include <iostream>
#include <fstream>
#include "YDLockDefine.h"
#include "YDThread.h"
#include "publicInfo.h"
#include "SensorDataStructDefine.h"

#define _MAX_PATH 256
class BriStoreMger
	: public YDThread
{
public:
    BriStoreMger();
	~BriStoreMger();
	/************************************************************************/
	/* 存储图像数据                                                         */
	/************************************************************************/
private:
    int							m_nSingleFrameIndex;
    std::string					m_strImageSavePath;
    ListImageInfoBuffer			m_listIWPBuffer;
    YDLock						m_lockIWPBuffer;
public:
    void                    startStore(void);
    void                    stopStore(void);
    bool                    savedAll(void);
    void					setSaveImagePath(const QString& strPath);
    void					AddIWPToBuffer(ImageInfo* imgInfo);
    void                    SaveBinaryData(const cv::Mat& matDepth, const std::string& sPath);

private:
//    void					SaveDepthData(const cv::Mat& matDepth, const std::string& sPath);
    bool					SaveOneIWPFrameToFile(void);

protected:
	virtual    void         run(void);
};
