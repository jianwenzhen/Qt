/************************************************************************
/* 公用函数
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2016.12.05
/* */
/************************************************************************/
#ifndef _YDUSKY_COMMON_FUNCTION_HEADER_FILE_2016_12_05_15_32_35_
#define _YDUSKY_COMMON_FUNCTION_HEADER_FILE_2016_12_05_15_32_35_
#include <vector>
#include <iostream>
#include <QString>
#include <QPixmap>
//#include "YDCorebase.h"
#ifndef NS_YDUSKY_START
#define NS_YDUSKY_START  namespace YDusky{
#define NS_YDUSKY_END    }
#endif // !NS_YDUSKY_START

#define _MAX_DRIVE 3
#define _MAX_DIR 256
#define _MAX_FNAME 256
#define _MAX_EXT 256

//文件类型
enum enuFileType{
    eFT_unkown   		= -1, //未知
    eFT_image   		= 0,  //图片
    eFT_video    		= 1,  //视频
    eFT_document		= 2,  //文档
    eFT_directory       = 3   //目录
};

NS_YDUSKY_START

	///文件操作相关函数
    enuFileType getFileType(const char* szFileName);

    bool IsExistFileOrDirectory(const char* strFileName);

	/************************************************************************/
	/* 字符串分割                                                           */
	/* @param str: IN 需要分割的字符串
	/* @param pattern: IN 分隔符
	/* @param std::vector<std::string> 返回分割后的子字符串
	/************************************************************************/
    std::vector<std::string> split(std::string str, std::string pattern);

    void createPath(const QString& strPath);

	/************************************************************************/
	/* 文件操作相关                                                                     */
	/************************************************************************/
//    std::string getFileExtName(const char* lppath);
//    std::string getDirFromPath(const char* lppath);
//    std::string getFileNameFromPath(const char* lppath);
//    std::string getFileFullNameFromPath(const char* lppath);
//    std::string getFilePathNameWithoutExtFromPath(const char* lpPath);


	///获取执行文件
//    bool  findFiles(const QString& strPath, const QString& strExt, std::vector<QString>& vFinds, bool bRecursive /*= true*/);

	///求512倍数值
    long  get_512_Times(long lValue);

    ///time transform
    void Int64ToSystemTime(int64_t nTime, tm *t);

    ///Image
    QPixmap dataToPixmap(int nW, int nH, int nC, unsigned char* data);

NS_YDUSKY_END

#endif
