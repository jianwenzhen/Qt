#include <QAbstractSocket>
#include "netmanager.h"

NetManager::NetManager()
{

}

NetManager::~NetManager()
{
    if(m_pSocket)
    {
        m_pSocket->deleteLater();
        m_pSocket = nullptr;
    }
}

void NetManager::startSend()
{
    if (!YDThread::isRun())
    {
        YDThread::start();
    }
    YDThread::trigger();
}

void NetManager::stopSend()
{
    YDThread::stop();
}

bool NetManager::sentAll()
{
    return (m_listIWPBuffer.size() == 0);
}

void NetManager::addIWPToBuffer(ImageInfo *pImage)
{
    if (pImage)
    {
        m_lockIWPBuffer.EnableWrite();
        m_listIWPBuffer.push_back(pImage);
        m_lockIWPBuffer.DisableWrite();

        if(!YDThread::isRun())
        {
            YDThread::start();
        }
        YDThread::trigger();
    }
}

bool NetManager::sendOneIWPToServer()
{
    bool bRet = true;
    if(m_listIWPBuffer.size() == 0)
        bRet = false;
    else
    {
        ImageInfo * pImageInfo = m_listIWPBuffer.front();
        if(!m_pSocket->sendData(pImageInfo))
        {
            bRet = false;
        }
    }
//    if connect:
//        send ...
//    else
//        ...
    return bRet;
}

bool NetManager::connectState()
{
    if (m_pSocket && m_pSocket->state() == QAbstractSocket::ConnectedState)
        return true;
    else
        return false;
}

void NetManager::disconnect()
{
    m_pSocket->disconnectFromHost();
    m_pSocket = nullptr;
}

void NetManager::connectToServer()
{
    m_pSocket = new Socket();
    m_pSocket->connectToHost(m_strIp, m_nPort);
    if (!m_pSocket->waitForConnected(3000))
    {
        qDebug("Connect to Server Failured!");
    }
    else
    {
        m_pSocket->on_connect();
        m_bIsConnecting = true;
    }
}

void NetManager::run()
{
    if (!sendOneIWPToServer())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    needWait(false);
}
