#ifndef NETMANAGER_H
#define NETMANAGER_H

#include <QTcpSocket>
#include <QObject>
#include <string>
#include "YDLockDefine.h"
#include "YDThread.h"
#include "publicInfo.h"
#include "zedcamera.h"
#include "Socket.h"

class NetManager
        : public YDThread
//        , public QObject
{
//    Q_OBJECT
public:
    NetManager();
    ~NetManager();
//    NetManager(const QString& ip, const unsigned int port);

public:
    void            startSend();
    void            stopSend();
    bool            sentAll();
    void            addIWPToBuffer(ImageInfo *);
    bool            sendOneIWPToServer();
    bool            connectState();
    void            disconnect();
    void            connectToServer();

private:
    QString                     m_strIp = "192.168.0.108";
    unsigned int                m_nPort = 8888;
    Socket                     *m_pSocket = nullptr;
    bool                        m_bIsSending = false;
    bool                        m_bIsConnecting = false;

    ListImageInfoBuffer			m_listIWPBuffer;
    YDLock						m_lockIWPBuffer;

protected:
    virtual void    run();
};

#endif // NETMANAGER_H
