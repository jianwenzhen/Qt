#ifndef SOCKET_H
#define SOCKET_H

#include <QTcpSocket>
#include "../publicInfo.h"

class Socket : public QTcpSocket
{
    Q_OBJECT
public:
    Socket();

private slots:
    void on_disconn();
    void receive();

public:
    bool sendData(ImageInfo* image);
    void on_connect();
};

#endif // SOCKET_H
