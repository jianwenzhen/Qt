#include "Socket.h"

Socket::Socket()
{

}

void Socket::on_disconn()
{
    qDebug("disconnect");
}

void Socket::receive()
{
    qDebug("receive data");
}

void Socket::on_connect()
{
    qDebug("connect");
    connect(this, SIGNAL(readyRead()), this, SLOT(receive()));
    connect(this, SIGNAL(disconnected()), this, SLOT(deleteLater()));
}

bool Socket::sendData(ImageInfo* pImageInfo)
{
    qint64 size = this->write((char*)pImageInfo, sizeof(ImageInfo));
    if (this->waitForBytesWritten())
        return true;
    return false;
    qDebug("sent %d bytes", size);
}
