/************************************************************************/
/* 大文件存储解析模块
/* 功能: 实现图像数据流文件的连续存储,写入速度快
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2016.10.10*/
/************************************************************************/
#ifndef _YDUSKY_BRI_BIGFILE_HEADER_FILE_2017_03_17_17_39_33_
#define _YDUSKY_BRI_BIGFILE_HEADER_FILE_2017_03_17_17_39_33_
#include <iostream>
#include <fstream>
#include <string.h>
#include "BigfileDefine.h"
#include "YDLockDefine.h"

#define _USE_FILE_PTR_
//#define _USE_FILE_HANDLE_

class BriBigFile
{
public:
	enum  eErrorInfo
	{
		eEI_FAILED    = -2,  ///失败
		eEI_NOSUPPORT = -1,  ///不支持的文件类型
		eEI_SUCCESS   = 0,
	};
public:
	BriBigFile();
	virtual ~BriBigFile();

public:

    bool           isOpen(void);

	///  创建大文件
	bool           createBigFile(const char* lpPath/*文件路径*/
        , int nFrameSize /*单帧数据大小*/
        , const char* lpCameraType = "unkown" /*像机类型*/
        , int nCameraIndex = 0/*像机序号*/
        , int nW = 0, int nH = 0, int nC = 0
        , int nFrameCount = 100000/*单个文件最大帧数*/);
	
	///  打开大文件
	eErrorInfo     openBigFile(const char* lpPath);
	
	///  关闭大文件
	void           closeBigFile(void);

	std::string    getFilePath(void);
	///   获得总数据块数
	int            getFrameCount(void);

	///   获得每个数据块的帧数
	int            getFrameCountPreBlock(void);

	///   获得实际帧数
	int            getRealFrameCount(void);

    ///   获得单帧大小
    int            getFrameSize(void);

	///   根据块序号获得块数据
    uint32_t       getFrameData(int nFrameIndex, unsigned char* pFrameData);

	///   依次添加数据
    uint32_t       addFrameData(unsigned char* pFrameData, unsigned nFrameCount = 1);

    int            getFrameWidth(void);

    int            getFrameHeight(void);

    int            getFrameChannels(void);

    int            getFileFlag(void);

protected:
#ifdef _USE_FILE_HANDLE_
        HANDLE         m_hFile;
#else
    	FILE*          m_pFile;
#endif // _USE_FILE_HANDLE_
	bool           m_bWriteFile;
	std::string    m_strFilePath;          /// 文件路径
	tagFileHeader  m_fileHeader;           /// 大文件文件头
	size_t         m_nFrameIndex;          /// 帧号
	DATA_ADDRESS   m_nFrameAddress;        /// 块地址
    YDLock         m_lockFile;
};
#endif
