#include <QSettings>
#include "LineLaserProcessFun.h"
#include "YDCommonFunc.h"

namespace LINE_LASER_PROCESS_FUN
{
	CLineLaserProcessFun::CLineLaserProcessFun(const std::string& strModelDir) 
		: m_strModelDir(strModelDir)
		, m_nPixelDistance(300)
		, m_fLLPMinLen(10.0)
		, m_fLLPMaxLen(100000.0)
		, m_fLLPStartAngle(-45.0)
		, m_fLLPDeltaAngle(0.25)
		, m_fLLPDeltaZLen(100.0)
		, m_nLLPEnableAreaCheck(1)
		, m_nLLPAreaCheckThre(10)
		, m_nLLPEnableConvexCheck(1)
	{
		m_pbpe = new CULineBasedPoseEst();

		LoadProfile();
	}

	CLineLaserProcessFun::~CLineLaserProcessFun()
	{
		SaveProfile();

		if (m_pbpe)
		{
			delete m_pbpe;
			m_pbpe = nullptr;
		}
	}

	void	CLineLaserProcessFun::LoadOriginalLaserData(const std::string& strFile, std::vector<unsigned int>& vBuffer)
	{
		FILE* fp = fopen(strFile.c_str(), "r");
		if (!fp)
			return;
		while (!feof(fp))
		{
			int val = 0;
			fscanf(fp, "%d ", &val);
			vBuffer.push_back(val);
		}
		fclose(fp);
	}

	void	CLineLaserProcessFun::SavePointcloudData(const std::string& strFile, std::vector<cv::Point3d>& vPointcloud)
	{
		FILE* fp = fopen(strFile.c_str(), "w");
		if (!fp)
			return;
		auto citer = vPointcloud.cbegin();
		for (;citer != vPointcloud.cend();++citer)
			fprintf(fp, "%lf %lf %lf\n", citer->x, citer->y, citer->z);
		fclose(fp);
	}

	void	CLineLaserProcessFun::ChangeLaserDataToPointcloud(const std::vector<int>& vBuffer, std::vector<cv::Point3d>& vPointcloud, int nZIdx/*=0*/)
	{
		auto iter = vBuffer.cbegin();
		for (;iter != vBuffer.cend();++iter)
		{
			int idx = iter - vBuffer.cbegin();
			int val = *iter;
			if (val <= m_fLLPMinLen || val >= m_fLLPMaxLen)
				continue;
			float fAngle = (m_fLLPStartAngle + m_fLLPDeltaAngle*idx)*CV_PI / 180.0;
			cv::Point3d point;
			point.x = val*std::cos(fAngle);
			point.y = val*std::sin(fAngle);
			point.z = nZIdx*m_fLLPDeltaZLen;
			vPointcloud.push_back(point);
		}
	}

	void CLineLaserProcessFun::FileNameTranslation(const std::string& strOrgFileName, const std::string& strExt, const std::string& strNewDir, std::string& strTransFileName)
	{
		// 1.check
		if (strOrgFileName.length() <= 0)
			return;

		// 2.find ext and replace new ext
		int nExtPos = strOrgFileName.find_last_of('.');
		if (nExtPos < 0)
			return;
		std::string strPart = strOrgFileName.substr(0, nExtPos);
		if (strNewDir.length() <= 0)
			strTransFileName = strPart.append(strExt);
		else
		{
			int nPos = strPart.find_last_of('\\');
			std::string strFile = strPart.substr(nPos,strPart.length());
			std::string strTmp = strNewDir;
			strTransFileName = strTmp.append(strFile).append(strExt);
		}
	}
	/*
	enum enuBridgeType
	{
	eBT_XXL = 0,
	eBT_KXB,
	eBT_TXL
	};
	*/
	bool CLineLaserProcessFun::LoadBridgeStructModel(int nType)
	{
		bool bRet = false;

		do 
		{
			std::string strModelName = m_strModelDir + "\\XXLBridge.model";
			switch (nType)
			{
			case 0:////XXL
				strModelName = m_strModelDir + "\\XXLBridge.model";
				break;
			case 1:///KXB
				strModelName = m_strModelDir + "\\XXLBridge.model";
				break;
			case 2:///TXL
				strModelName = m_strModelDir + "\\TXLBridge.model";
				break;

			default:
				break;
			}
			bRet = true;

		} while (0);

		return bRet;
			 
	}

	void	CLineLaserProcessFun::LoadStandardModel(const std::string& strPath)
	{
		FILE* fp = fopen(strPath.c_str(), "r");
		if (!fp)
			return;
		int size = 0;
		fscanf(fp, "%d\n", &size);
		while (!feof(fp))
		{
			cv::Point2d point;
			fscanf(fp, "%lf %lf\n", &point.x, &point.y);
			m_standardModel.vKeyPoints.push_back(point);
		}
		fclose(fp);
	}

	bool string_cmp(std::string const &arg_a, std::string const &arg_b)
	{
		return arg_a.size() < arg_b.size() || (arg_a.size() == arg_b.size() && arg_a < arg_b);
	}
	
	void		CLineLaserProcessFun::ProcessShape1ByBruteForceMethod(const std::vector<cv::Point3d>& vInputSectionPointcloud, KeyPointOfPointcloudSection& result, std::string strSavePath /*= ""*/, int nZIdx/*=0*/)
	{
		// 1.check size
		auto iSizePointcloud = vInputSectionPointcloud.size();
		if (iSizePointcloud <= 0)
			return;

		// 2.calculate new out box
		cv::Point ptLU, ptRB;
		ptLU.x = int(m_fLLPOutbox[0]);
		ptLU.y = int(m_fLLPOutbox[1]);
		ptRB.x = int(m_fLLPOutbox[3]);
		ptRB.y = int(m_fLLPOutbox[4]);
		cv::Rect rectOutbox(ptLU, ptRB);
		if (rectOutbox.area() <= 0)
			return;
		cv::Point2i ptStart = ptLU;

		// 3.change x,y,z to cv::Mat and get rid of isolate point
		int nCol = rectOutbox.width;
		int nRow = rectOutbox.height;
		cv::Mat matPlane = cv::Mat::zeros(rectOutbox.height, rectOutbox.width, CV_8U);
		unsigned char* pData = (unsigned char*)matPlane.data;
		auto iter = vInputSectionPointcloud.cbegin();
		for (; iter != vInputSectionPointcloud.cend(); ++iter)
		{
			int x = cvRound(iter->x) - ptStart.x;
			int y = cvRound(iter->y) - ptStart.y;
			if (x < 0 || x >= nCol || y < 0 || y >= nRow)
				continue;
			//*(pData + (nRow-1-y)*nCol + x) = 255;
			*(pData + y*nCol + x) = 255;
		}
#ifdef _DEBUG
		//	cv::namedWindow("matPlane", CV_WND_PROP_ASPECTRATIO);
		//	cv::imshow("matPlane", matPlane);
		//	cv::imwrite("d:\\test.bmp", matPlane);
#endif
		std::vector<cv::Point> vBottomSideCandidatePoints, vLeftSideCandidatePoints, vRightSideCandidatePoints, vTopSideCandidatePoints;
		int i(0), j(0);

		// 4.search from bottom side
		int nBottomInitValue = -9999.0;
		for (int i = 0; i < nCol; ++i)
		{
			for (int j = nRow - 1; j >= 0; --j)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pData + idx);
				if (value == 0)
					continue;
				if (j > nBottomInitValue)
					nBottomInitValue = j;
			}
		}
		for (int i = 0; i < nCol; ++i)
		{
			for (int j = nRow - 1; j >= 0; --j)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pData + idx);
				if (value == 0)
					continue;
				int nOffset = abs(nBottomInitValue - j);
				if (nOffset > m_nPixelDistance)
					continue;
				cv::Point point;
				point.x = i + ptStart.x;
				point.y = j + ptStart.y;
				vBottomSideCandidatePoints.push_back(point);
				break;
			}
		}

		// 5.search from top
		int nTopInitValue = 9999.0;
		for (int i = 0; i < nCol; ++i)
		{
			for (int j = 0; j < nRow; ++j)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pData + idx);
				if (value == 0)
					continue;
				if (j < nTopInitValue)
					nTopInitValue = j;
			}
		}
		for (int i = 0; i < nCol; ++i)
		{
			for (int j = 0; j < nRow; ++j)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pData + idx);
				if (value == 0)
					continue;
				int nOffset = abs(nTopInitValue - j);
				if (nOffset > m_nPixelDistance)
					continue;
				cv::Point2d point;
				point.x = i + ptStart.x;
				point.y = j + ptStart.y;
				vTopSideCandidatePoints.push_back(point);
				break;
			}
		}

		// 6.fit bottom and top line
		// line is expressed as:
		// Ax+By+C=0;
		cv::Vec4f bottomLine;
		try
		{
			cv::fitLine(vBottomSideCandidatePoints, bottomLine, CV_DIST_HUBER, 0, 0.01, 0.01);
		}
		catch (cv::Exception& e)
		{
			std::cerr << e.msg << std::endl;
		}
		double botA, botB, botC;
		cvLineToABC(bottomLine, botA, botB, botC);
		int avgBottomY = GetAvgY<int>(vBottomSideCandidatePoints);
		cv::Vec4f topLine;
		try
		{
			cv::fitLine(vTopSideCandidatePoints, topLine, CV_DIST_HUBER, 0, 0.01, 0.01);
		}
		catch (cv::Exception& e)
		{
			std::cerr << e.msg << std::endl;
		}
		double topA, topB, topC;
		cvLineToABC(topLine, topA, topB, topC);
		int avgTopY = GetAvgY<int>(vTopSideCandidatePoints);

		// 7.get rid of point nearby top or bottom
		cv::Mat matPlaneRest;
		matPlane.copyTo(matPlaneRest);
		unsigned char* pDataRest = (unsigned char*)matPlaneRest.data;
		for (int j = 0; j < nRow; ++j)
		{
			for (int i = 0; i < nCol; ++i)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pDataRest + idx);
				if (value == 0)
					continue;

				int bottomChk = abs(j + ptStart.y - avgBottomY);
				int topChk = abs(j + ptStart.y - avgTopY);
				if (bottomChk <= m_nPixelDistance || topChk <= m_nPixelDistance)
					*(pDataRest + idx) = 0;
			}
		}
#ifdef _DEBUG
		//	cv::namedWindow("matPlaneRest", CV_WND_PROP_ASPECTRATIO);
		//	cv::imshow("matPlaneRest", matPlaneRest);
		//	cv::imwrite("d:\\test_rest.bmp", matPlaneRest);
#endif

		// 8.calculate center x
		int nCenterX = 0;
		int iValidSize = 0;
		for (int j = 0; j < nRow; ++j)
		{
			for (int i = 0; i < nCol; ++i)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pDataRest + idx);
				if (value == 0)
					continue;

				nCenterX += i;
				++iValidSize;
			}
		}
		if (iValidSize == 0)
			nCenterX = nCol / 2;
		else
			nCenterX /= iValidSize;

		// 9.search from left
		for (int j = 0; j < nRow; ++j)
		{
			for (int i = 0; i < nCol; ++i)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pDataRest + idx);
				if (value == 0)
					continue;

				if (i >= nCenterX)
					continue;

				cv::Point point;
				point.x = i + ptStart.x;
				point.y = j + ptStart.y;
				vLeftSideCandidatePoints.push_back(point);
				break;
			}
		}
		cv::Vec4f leftLine;
		try
		{
			cv::fitLine(vLeftSideCandidatePoints, leftLine, CV_DIST_HUBER, 0, 0.01, 0.01);
		}
		catch (cv::Exception&e)
		{
			std::cerr << e.msg << std::endl;
		}
		double leftA, leftB, leftC;
		cvLineToABC(leftLine, leftA, leftB, leftC);

		// 10.search from right
		for (int j = 0; j < nRow; ++j)
		{
			for (int i = nCol - 1; i >= 0; --i)
			{
				int idx = j*nCol + i;
				unsigned char value = *(pDataRest + idx);
				if (value == 0)
					continue;
				if (i <= nCenterX)
					continue;

				cv::Point point;
				point.x = i + ptStart.x;
				point.y = j + ptStart.y;
				vRightSideCandidatePoints.push_back(point);
				break;
			}
		}
		cv::Vec4f rightLine;
		try
		{
			cv::fitLine(vRightSideCandidatePoints, rightLine, CV_DIST_HUBER, 0, 0.01, 0.01);
		}
		catch (cv::Exception& e)
		{
			std::cerr << e.msg << std::endl;
		}
		double rightA, rightB, rightC;
		cvLineToABC(rightLine, rightA, rightB, rightC);

		// 11.cross point
		cv::Point2d pt1, pt2, pt3, pt4;
		CalLineCrossPoint(topA, topB, topC, leftA, leftB, leftC, pt1.x, pt1.y);
		CalLineCrossPoint(leftA, leftB, leftC, botA, botB, botC, pt2.x, pt2.y);
		CalLineCrossPoint(botA, botB, botC, rightA, rightB, rightC, pt3.x, pt3.y);
		CalLineCrossPoint(rightA, rightB, rightC, topA, topB, topC, pt4.x, pt4.y);
		cv::Point3d pt11, pt22, pt33, pt44;
		pt11.x = pt1.x;
		pt11.y = pt1.y;
		pt11.z = nZIdx*m_fLLPDeltaZLen;
		pt22.x = pt2.x;
		pt22.y = pt2.y;
		pt22.z = nZIdx*m_fLLPDeltaZLen;
		pt33.x = pt3.x;
		pt33.y = pt3.y;
		pt33.z = nZIdx*m_fLLPDeltaZLen;
		pt44.x = pt4.x;
		pt44.y = pt4.y;
		pt44.z = nZIdx*m_fLLPDeltaZLen;
		result.vKeyPoints.push_back(pt11);
		result.vKeyPoints.push_back(pt22);
		result.vKeyPoints.push_back(pt33);
		result.vKeyPoints.push_back(pt44);
		result.distanceToTop = (pt2.y + pt3.y) / 2.0;
		result.distanceToBottom = (pt1.y + pt4.y) / 2.0;

		// check valid
		if (_isnan(pt1.x) || _isnan(pt1.y) || _isnan(pt2.x) || _isnan(pt2.y) || _isnan(pt3.x) || _isnan(pt3.y) || _isnan(pt4.x) || _isnan(pt4.y))
		{
			result.bValid = false;
		}
		else
		{
			// check area
			if (m_nLLPEnableConvexCheck)
			{
				result.bValid = !result.CheckCross();
			}
			if (result.bValid == true)
			{
				double standardArea = m_standardModel.CalcArea();
				if (m_nLLPEnableAreaCheck&&standardArea > 0.0)
				{
					double currentArea = result.CalcArea();
					if (fabs(standardArea - currentArea) < (standardArea*m_nLLPAreaCheckThre / 100.0))
						result.bValid = true;
					else
						result.bValid = false;
				}
			}
		}

		if (strSavePath.length() > 0)
		{
			FILE *fp = fopen(strSavePath.c_str(), "w");
			if (!fp)
				return;
			fprintf(fp, "%d\n", (int)(result.bValid));
			fprintf(fp, "%lf %lf %lf\n", pt11.x, pt11.y, pt11.z);
			fprintf(fp, "%lf %lf %lf\n", pt22.x, pt22.y, pt22.z);
			fprintf(fp, "%lf %lf %lf\n", pt33.x, pt33.y, pt33.z);
			fprintf(fp, "%lf %lf %lf\n", pt44.x, pt44.y, pt44.z);
			fclose(fp);
		}
#ifdef _DEBUG
		//std::cout << "key point1: " << pt1 << std::endl;
		//std::cout << "key point2: " << pt2 << std::endl;
		//std::cout << "key point3: " << pt3 << std::endl;
		//std::cout << "key point4: " << pt4 << std::endl;
		//std::cout << "distance: " << result.distanceToTop << std::endl<< std::endl;
#endif
	}

	void	CLineLaserProcessFun::ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nZIdx/*=0*/)
	{
		// 1.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		// 2.get the keypoint
		ProcessShape1ByBruteForceMethod(vPointCloud, result, "", nZIdx);

		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}

	void	CLineLaserProcessFun::ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nZIdx/*=0*/)
	{
		// 2.get the keypoint
		ProcessShape1ByBruteForceMethod(vPointCloud, result, "", nZIdx);

		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}

	void	CLineLaserProcessFun::DecodeLaserDataToPointcloud(unsigned int *pnData, std::vector<cv::Point3d>& vInputSectionPointcloud, int nZIdx/*=0*/)
	{
		for (int i = 0;i < LINELASERSIZE;++i)
		{
			int val = pnData[i];
			if (val <= m_fLLPMinLen || val >= m_fLLPMaxLen)
				continue;
			float fAngle = (m_fLLPStartAngle + m_fLLPDeltaAngle*i)*CV_PI / 180.0;
			cv::Point3d point;
			point.x = val*std::cos(fAngle);
			point.y = val*std::sin(fAngle);
			point.z = nZIdx*m_fLLPDeltaZLen;
			vInputSectionPointcloud.push_back(point);
		}
	}
	void	CLineLaserProcessFun::DecodeLaserDataToPointcloud(const std::string& strInputLaserPath, std::vector<cv::Point3d>& vPointcloud)
	{
		// 1.check file
		std::vector<unsigned int> vBuffer;
		LoadOriginalLaserData(strInputLaserPath, vBuffer);
		DecodeLaserDataToPointcloud(&vBuffer[0], vPointcloud);
	}
	void		CLineLaserProcessFun::CalcPlaneOutbox(std::vector<cv::Point3d>& vInputSectionPointcloud, cv::Rect& rectOutbox)
	{
		// 1.check
		auto iSize = vInputSectionPointcloud.size();
		if (iSize <= 0)
		{
			rectOutbox.width = 0;
			rectOutbox.height = 0;
			return;
		}

		// 2.calculate outbox
		float fMinX = 9999.0;
		float fMaxX = -9999.0;
		float fMinY = 9999.0;
		float fMaxY = -9999.0;
		auto iter = vInputSectionPointcloud.cbegin();
		for (;iter != vInputSectionPointcloud.cend();++iter)
		{
			if (iter->x < fMinX)
				fMinX = iter->x;
			if (iter->x > fMaxX)
				fMaxX = iter->x;
			if (iter->y < fMinY)
				fMinY = iter->y;
			if (iter->y > fMaxY)
				fMaxY = iter->y;
		}

		cv::Point2i pt1, pt2;
		pt1.x = (int)fMinX;
		pt1.y = (int)fMinY;
		pt2.x = (int)fMaxX;
		pt2.y = (int)fMaxY;
		cv::Rect Tmp(pt1, pt2);
		rectOutbox = Tmp;
	}

	void	CLineLaserProcessFun::GetAffineTransform(KeyPointOfPointcloudSection& src, KeyPointOfPointcloudSection& dst, cv::Mat& mapMatrix)
	{
		// 1.check
		if (src.vKeyPoints.size() < 3 || dst.vKeyPoints.size() < 3)
			return;

		// 2.calculate map matrix
		std::vector<cv::Point2d> vSrc, vDst;
		for (int i = 0;i < src.vKeyPoints.size();++i)
		{
			cv::Point2d point;
			point.x = src.vKeyPoints.at(i).x;
			point.y = src.vKeyPoints.at(i).y;
			vSrc.push_back(point);
		}
		for (int j = 0;j < dst.vKeyPoints.size();++j)
		{
			cv::Point2d point;
			point.x = dst.vKeyPoints.at(j).x;
			point.y = dst.vKeyPoints.at(j).y;
			vDst.push_back(point);
		}
		try
		{
			mapMatrix = cv::findHomography(vSrc, vDst, true);
#ifdef _DEBUG
			std::cout << "affine transform: " << mapMatrix << std::endl;
#endif
		}
		catch (cv::Exception& e)
		{
#ifdef _DEBUG
			std::cerr << e.msg << std::endl;
#endif
		}
	}

	void	CLineLaserProcessFun::GetBaseKeyPoint(KeyPointOfPointcloudSection& base)
	{
		auto iter = m_standardModel.vKeyPoints.begin();
		for (;iter != m_standardModel.vKeyPoints.end();++iter)
		{
			cv::Point3d point;
			point.x = iter->x;
			point.y = iter->y;
			point.z = 0.0;
			base.vKeyPoints.push_back(point);
		}
	}

	void	CLineLaserProcessFun::SaveAffineTransform(const std::string& strPath, cv::Mat& mapMatrix)
	{
		cv::FileStorage fs;
		fs.open(strPath, cv::FileStorage::WRITE);
		if (fs.isOpened())
		{
			fs << "AFFINE_MATRIX" << mapMatrix;
		}
		fs.release();
	}

	void	CLineLaserProcessFun::LoadAffineTransform(const std::string& strPath, cv::Mat& mapMatrix)
	{
		cv::FileStorage fs;
		fs.open(strPath, cv::FileStorage::READ);
		if (fs.isOpened())
		{
			fs["AFFINE_MATRIX"] >> mapMatrix;
		}
		fs.release();
	}

	void	CLineLaserProcessFun::AffineTransform(KeyPointOfPointcloudSection& src, cv::Mat& mapMatrix, KeyPointOfPointcloudSection& dst)
	{
		// 1.check
		if (src.vKeyPoints.size() <= 0 || dst.vKeyPoints.size() <= 0 || src.vKeyPoints.size() != dst.vKeyPoints.size())
			return;

		// 2.affine transform
		std::vector<cv::Point3d> vSrc, vDst;
		for (int i = 0;i < src.vKeyPoints.size();++i)
		{
			cv::Point3d point;
			point.x = src.vKeyPoints.at(i).x;
			point.y = src.vKeyPoints.at(i).y;
			point.z = 1.0;
			vSrc.push_back(point);
		}

		try
		{
			cv::transform(vSrc, vDst, mapMatrix);
		}
		catch (cv::Exception& e)
		{
#ifdef _DEBUG
			std::cerr << e.msg << std::endl;
#endif
		}
		
		// 3.copy to dst
		for (int j = 0;j < dst.vKeyPoints.size();++j)
		{
			dst.vKeyPoints.at(j).x = vDst.at(j).x;
			dst.vKeyPoints.at(j).y = vDst.at(j).y;
		}
	}

	void	CLineLaserProcessFun::LoadProfile()
	{
		int nStandardModelSize = 4;
		QString  path(QString::fromLocal8Bit(m_strModelDir.c_str()) + "/LineLaserProcessProfile.ini");
		if (!YDusky::IsExistFileOrDirectory(path.toLocal8Bit().data()))
		{
			m_fLLPMinLen = 10.0;
			m_fLLPMaxLen = 10000;
			m_fLLPStartAngle = -45.0;
			m_fLLPDeltaAngle = 0.25;
			m_fLLPDeltaZLen = 100.0;
			m_nLLPEnableAreaCheck = 1;
			m_nLLPEnableConvexCheck = 1;
			m_nLLPAreaCheckThre = 40;
			// outbox
			m_fLLPOutbox[0] = 0;
			m_fLLPOutbox[1] = 0;
			m_fLLPOutbox[2] = 0;
			m_fLLPOutbox[3] = 0;
			m_fLLPOutbox[4] = 0;
			m_fLLPOutbox[5] = 0;

		}
		else
		{
			QSettings settting(QString::fromLocal8Bit(m_strModelDir.c_str()), QSettings::IniFormat);
			
			QString strAppName = "LLPParameter";
			// algorithm parameter

			m_fLLPMinLen = settting.value(strAppName + "/LLPMinLen").toFloat();
			m_fLLPMaxLen = settting.value(strAppName + "/LLPMaxLen").toFloat();
			m_fLLPStartAngle = settting.value(strAppName + "/LLPStartAngle").toFloat();
			m_fLLPDeltaAngle = settting.value(strAppName + "/LLPDeltaAngle").toFloat();

			m_fLLPDeltaZLen = settting.value(strAppName + "/LLPDeltaZLen").toFloat();
			m_nLLPEnableAreaCheck = settting.value(strAppName + "/LLPEnableAreCheck").toInt();
			m_nLLPEnableConvexCheck = settting.value(strAppName + "/LLPEnableConvexCheck").toInt();
			m_nLLPAreaCheckThre = settting.value(strAppName + "/LLPAreaCheckThre").toInt();

			// outbox
			m_fLLPOutbox[0] = settting.value(strAppName + "/LLPOutbox.minx").toFloat();
			m_fLLPOutbox[1] = settting.value(strAppName + "/LLPOutbox.miny").toFloat();
			m_fLLPOutbox[2] = settting.value(strAppName + "/LLPOutbox.minz").toFloat();
			m_fLLPOutbox[3] = settting.value(strAppName + "/LLPOutbox.maxx").toFloat();
			m_fLLPOutbox[4] = settting.value(strAppName + "/LLPOutbox.maxy").toFloat();
			m_fLLPOutbox[5] = settting.value(strAppName + "/LLPOutbox.maxz").toFloat();

			nStandardModelSize = settting.value(strAppName + "/StandardModelSize").toInt();

			// standard model
			m_standardModel.init();

			for (int i = 0; i < nStandardModelSize; ++i)
			{
				QString strKey(QString::asprintf("/LLPStandardModel.point%d.x", i + 1));
				cv::Point2d pts;
				pts.x = settting.value(strAppName + strKey).toFloat();
				strKey = (QString::asprintf("/LLPStandardModel.point%d.y", i + 1)); 
				pts.y = settting.value(strAppName + strKey).toFloat();

				m_standardModel.vKeyPoints.push_back(pts);
			}
		}
	}

	void	CLineLaserProcessFun::SaveProfile()
	{
		QSettings settting(QString::fromLocal8Bit(m_strModelDir.c_str()), QSettings::IniFormat);

		QString strAppName = "LLPParameter";

		int nPrecis = 3;

		// algorithm parameter
		settting.setValue(strAppName + "/LLPMinLen", QString::number(m_fLLPMinLen, 'f', nPrecis));
		settting.value(strAppName + "/LLPMaxLen", QString::number(m_fLLPMinLen, 'f', nPrecis));
		settting.value(strAppName + "/LLPStartAngle", QString::number(m_fLLPStartAngle, 'f', nPrecis));
		settting.value(strAppName + "/LLPDeltaAngle", QString::number(m_fLLPDeltaAngle, 'f', nPrecis));

		settting.value(strAppName + "/LLPDeltaZLen", QString::number(m_fLLPDeltaZLen));
		settting.value(strAppName + "/LLPEnableAreCheck", QString::number(m_nLLPEnableAreaCheck));
		settting.value(strAppName + "/LLPEnableConvexCheck", QString::number(m_nLLPEnableConvexCheck));
		settting.value(strAppName + "/LLPAreaCheckThre", QString::number(m_nLLPAreaCheckThre));

		// outbox
		settting.value(strAppName + "/LLPOutbox.minx", QString::number(m_fLLPOutbox[0], 'f', nPrecis));
		settting.value(strAppName + "/LLPOutbox.miny", QString::number(m_fLLPOutbox[1], 'f', nPrecis));
		settting.value(strAppName + "/LLPOutbox.minz", QString::number(m_fLLPOutbox[2], 'f', nPrecis));
		settting.value(strAppName + "/LLPOutbox.maxx", QString::number(m_fLLPOutbox[3], 'f', nPrecis));
		settting.value(strAppName + "/LLPOutbox.maxy", QString::number(m_fLLPOutbox[4], 'f', nPrecis));
		settting.value(strAppName + "/LLPOutbox.maxz", QString::number(m_fLLPOutbox[5], 'f', nPrecis));

		size_t nStandardModelSize = m_standardModel.vKeyPoints.size();
		settting.value(strAppName + "/StandardModelSize", QString::number(nStandardModelSize));

		for (int i = 0; i < nStandardModelSize; ++i)
		{
			QString strKey(QString::asprintf("/LLPStandardModel.point%d.x", i + 1));
			settting.value(strAppName + strKey, QString::number(m_standardModel.vKeyPoints.at(i).x, 'f', nPrecis));
			strKey = (QString::asprintf("/LLPStandardModel.point%d.y", i + 1));
			settting.value(strAppName + strKey, QString::number(m_standardModel.vKeyPoints.at(i).y, 'f', nPrecis));
		}
	}

	void	CLineLaserProcessFun::XXL_ProcessLineLaserFirst(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nZIdx/*=0*/)
	{
		// 1.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		XXL_ProcessLineLaserFirst(vPointCloud, result, nZIdx);
	}

	void	CLineLaserProcessFun::XXL_ProcessLineLaserFirst(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nZIdx/*=0*/)
	{

		// 2.get the struct
		std::string strModelName = m_strModelDir + "\\XXLBridge.model";
		cv::Mat transSpace2Img;
		m_pbpe->CalcSpace2ImageTrans(transSpace2Img);
		KeyPointOfPointcloudSection resultBase;
		ProcessShape1ByBruteForceMethod(vPointCloud, resultBase, "", nZIdx);
		if (result.bValid)
		{
			cv::Mat matImg;
			m_pbpe->TransPointcloud2Image(vPointCloud, transSpace2Img, matImg);
			m_pbpe->GenerateObjectModel(strModelName, resultBase.vKeyPoints, transSpace2Img);
		}
	}
	void	CLineLaserProcessFun::XXL_ProcessLineLaserNext(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx /*= 0*/)
	{
		if (0 == pnData)
		{
			return;
		}
		// 2.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		XXL_ProcessLineLaserNext(vPointCloud, result, nActiveIdx, nZIdx);
	}

	void	CLineLaserProcessFun::XXL_ProcessLineLaserNext(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx /*= 0*/)
	{
		if (0 == vPointCloud.size())
		{
			return;
		}
		// 1.check model
		std::string strModelName = m_strModelDir + "XXLBridge.model";
		if (!YDusky::IsExistFileOrDirectory(strModelName.c_str()))
			return;

		
		// 3.get the keypoint
		cv::Mat transSpace2Img;
		m_pbpe->CalcSpace2ImageTrans(transSpace2Img);
		m_pbpe->LoadObjectModel(strModelName);
		m_pbpe->SetOutbox(m_fLLPOutbox);	
		
		std::vector<CXXLKeyPoints> resultOwn;
		if (!m_pbpe->XXL_KeyPointsDetectionByLineMatch(vPointCloud, transSpace2Img, resultOwn, true))
			return;


		result.vKeyPoints = resultOwn.at(nActiveIdx).vKeyPoints;
		result.distanceToTop = resultOwn.at(nActiveIdx).distanceToTop;
		result.distanceToBottom = resultOwn.at(nActiveIdx).distanceToBottom;
		result.angle = resultOwn.at(nActiveIdx).angle;
		for (int i = 0; i < 3; ++i)
			result.translate[i] = resultOwn.at(nActiveIdx).translate[i];
		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}

	void	CLineLaserProcessFun::TXL_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 2.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		TXL_ProcessLineLaser(vPointCloud, result, nActiveIdx, nZIdx);
	}

	void	CLineLaserProcessFun::TXL_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 1.check model
		std::string strModelName = m_strModelDir + "\\TXLBridge.model";
		if (!YDusky::IsExistFileOrDirectory(strModelName.c_str()))
			return;

		// 3.get the keypoint
		cv::Mat transSpace2Img;
		m_pbpe->CalcSpace2ImageTrans(transSpace2Img);
		m_pbpe->LoadObjectModel(strModelName);
		std::vector<CTXLKeyPoints> resultOwn;
		m_pbpe->SetOutbox(m_fLLPOutbox);
		if (!m_pbpe->TXL_KeyPointsDetectionByLineMatch(vPointCloud, transSpace2Img, resultOwn, true))
			return;

		result.vKeyPoints = resultOwn.at(nActiveIdx).vKeyPoints;
		result.distanceToTop = resultOwn.at(nActiveIdx).distanceToTop;
		result.distanceToBottom = resultOwn.at(nActiveIdx).distanceToBottom;
		result.angle = resultOwn.at(nActiveIdx).angle;
		for (int i = 0; i < 3; ++i)
			result.translate[i] = resultOwn.at(nActiveIdx).translate[i];
		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}

	void CLineLaserProcessFun::KXB_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nZIdx/*=0*/)
	{
		// 1.check
		if (pnData == nullptr)
			return;

		// 2.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		KXB_ProcessLineLaser(vPointCloud, result, nZIdx);
	}


	void CLineLaserProcessFun::KXB_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nZIdx/*=0*/)
	{

		// 3.get the keypoint
		std::vector<CKXBKeyPoints> resultOwn;
		m_pbpe->SetOutbox(m_fLLPOutbox);
		if (!m_pbpe->KXB_KeyPointsDetectionByLineFit(vPointCloud, resultOwn))
			return;

		result.vKeyPoints = resultOwn.at(0).vKeyPoints;
		result.distanceToTop = resultOwn.at(0).distanceToTop;
		result.distanceToBottom = resultOwn.at(0).distanceToBottom;
		result.length = resultOwn.at(0).dbLineLength;
		result.angle = resultOwn.at(0).angle;
		for (int i = 0; i < 3; ++i)
			result.translate[i] = resultOwn.at(0).translate[i];
		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}



	void	CLineLaserProcessFun::KXBSide_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 2.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		KXBSide_ProcessLineLaser(vPointCloud, result, nActiveIdx, nZIdx);
	}

	void	CLineLaserProcessFun::KXBSide_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud , KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 1.check model
		std::string strModelName = m_strModelDir + "\\KXBBridgeSide.model";
		if (!YDusky::IsExistFileOrDirectory(strModelName.c_str()))
			return;

		// 3.get the keypoint
		cv::Mat transSpace2Img;
		m_pbpe->CalcSpace2ImageTrans(transSpace2Img);
		m_pbpe->LoadObjectModel(strModelName);
		std::vector<CXXLSideKeyPoints> resultOfXXL;
		std::vector<CKXBSideKeyPoints> resultOfKXBSide;
		m_pbpe->SetOutbox(m_fLLPOutbox);
		if (m_pbpe->XXLSide_KeyPointsDetectionByLineMatch(vPointCloud, transSpace2Img, resultOfXXL, true))
		{
			CKXBSideKeyPoints tmp;
			tmp.vKeyPoints = resultOfXXL.at(0).vKeyPoints;
			tmp.CalcDistance();
			resultOfKXBSide.push_back(tmp);
		}
		if (resultOfKXBSide.size() <= 0)
			return;
		result.vKeyPoints = resultOfKXBSide.at(nActiveIdx).vKeyPoints;
		result.distanceToTop = resultOfKXBSide.at(nActiveIdx).distanceToTop;
		result.distanceToBottom = resultOfKXBSide.at(nActiveIdx).distanceToBottom;
		result.angle = resultOfKXBSide.at(nActiveIdx).angle;
		for (int i = 0; i < 3; ++i)
			result.translate[i] = resultOfKXBSide.at(nActiveIdx).translate[i];
		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}


	void	CLineLaserProcessFun::XXLSide_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 2.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		XXLSide_ProcessLineLaser(vPointCloud, result, nActiveIdx, nZIdx);
	}

	void	CLineLaserProcessFun::XXLSide_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 1.check model
		std::string strModelName = m_strModelDir + "\\TXLBridgeSide.model";
		if (!YDusky::IsExistFileOrDirectory(strModelName.c_str()))
			return;

		// 3.get the keypoint
		cv::Mat transSpace2Img;
		m_pbpe->CalcSpace2ImageTrans(transSpace2Img);
		m_pbpe->LoadObjectModel(strModelName);
		std::vector<CXXLSideKeyPoints> resultOwn;
		m_pbpe->SetOutbox(m_fLLPOutbox);
		if (!m_pbpe->XXLSide_KeyPointsDetectionByLineMatch(vPointCloud, transSpace2Img, resultOwn, true))
			return;

		result.vKeyPoints = resultOwn.at(nActiveIdx).vKeyPoints;
		result.distanceToTop = resultOwn.at(nActiveIdx).distanceToTop;
		result.distanceToBottom = resultOwn.at(nActiveIdx).distanceToBottom;
		result.angle = resultOwn.at(nActiveIdx).angle;
		for (int i = 0; i < 3; ++i)
			result.translate[i] = resultOwn.at(nActiveIdx).translate[i];
		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}


	void	CLineLaserProcessFun::TXLSide_ProcessLineLaser(unsigned int* pnData, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 2.get the 1080 data to X Y Z data
		std::vector<cv::Point3d> vPointCloud;
		DecodeLaserDataToPointcloud(pnData, vPointCloud, nZIdx);

		TXLSide_ProcessLineLaser(vPointCloud, result, nActiveIdx, nZIdx);
	}

	void	CLineLaserProcessFun::TXLSide_ProcessLineLaser(const std::vector<cv::Point3d>& vPointCloud, KeyPointOfPointcloudSection& result, int nActiveIdx/* = 0*/, int nZIdx/*=0*/)
	{
		// 1.check model
		std::string strModelName = m_strModelDir + "\\TXLBridgeSide.model";
		if (!YDusky::IsExistFileOrDirectory(strModelName.c_str()))
			return;

		// 3.get the keypoint
		cv::Mat transSpace2Img;
		m_pbpe->CalcSpace2ImageTrans(transSpace2Img);
		m_pbpe->LoadObjectModel(strModelName);
		std::vector<CTXLSideKeyPoints> resultOwn;
		std::vector<CTXLKeyPoints> resultOfTXL;
		m_pbpe->SetOutbox(m_fLLPOutbox);
		if (!m_pbpe->TXLSide_KeyPointsDetectionByLineMatch(vPointCloud, transSpace2Img, resultOwn, true))
		{
			//			if(m_bpe.TXL_KeyPointsDetectionByLineMatch(vPointCloud, transSpace2Img,resultOfTXL,true))
			{
				// special edit
				double deltaY = resultOfTXL.at(0).vKeyPoints.at(0).y - resultOfTXL.at(0).vKeyPoints.at(3).y;
				if (deltaY < 0)
				{
					std::vector<cv::Point3d> vTmp;
					vTmp.push_back(resultOfTXL.at(0).vKeyPoints.at(1));
					vTmp.push_back(resultOfTXL.at(0).vKeyPoints.at(2));
					vTmp.push_back(resultOfTXL.at(0).vKeyPoints.at(3));
					vTmp.push_back(resultOfTXL.at(0).vKeyPoints.at(0));
					resultOfTXL.at(0).vKeyPoints = vTmp;
				}

				CTXLSideKeyPoints ptTmp;
				for (int i = 0; i < resultOfTXL.at(0).vKeyPoints.size() - 1; ++i)
				{
					ptTmp.vKeyPoints.push_back(resultOfTXL.at(0).vKeyPoints.at(i));
				}
				resultOwn.push_back(ptTmp);
				auto it2 = resultOwn.begin();
				for (; it2 != resultOwn.end(); ++it2)
				{
					it2->CalcDistance();
				}
			}
			if (resultOwn.size() <= 0)
				return;
		}
		result.vKeyPoints = resultOwn.at(nActiveIdx).vKeyPoints;
		result.distanceToTop = resultOwn.at(nActiveIdx).distanceToTop;
		result.distanceToBottom = resultOwn.at(nActiveIdx).distanceToBottom;
		result.angle = resultOwn.at(nActiveIdx).angle;
		for (int i = 0; i < 3; ++i)
			result.translate[i] = resultOwn.at(nActiveIdx).translate[i];
		for (int i = 0; i < 6; ++i)
			result.m_outbox[i] = m_fLLPOutbox[i];
	}
}

