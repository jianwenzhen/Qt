#include <thread>
#include "YDLockDefine.h"

const unsigned YDSWMRLockSize = sizeof(YDSWMRLock);

//创建单写多读锁
void SWMRLock_Create(YDSWMRLock* pLock)
{
	pLock->m_nReadCount = 0;
	pLock->m_bIsWriting = false;
}

//销毁单写多读锁
void SWMRLock_Destroy(YDSWMRLock* pLock)
{

}

//获取写状态
bool SWMRLock_GetWriteFlag(YDSWMRLock* pLock)
{
	bool bRet = false;
    pLock->m_lock.lock();
	{
		bRet = pLock->m_bIsWriting;
	}
    pLock->m_lock.unlock();
	return bRet;
}

//获取读操作计数器
int SWMRLock_GetReadCount(YDSWMRLock* pLock)
{
	int nRet = 0;
    pLock->m_lock.lock();
	{
		nRet = pLock->m_nReadCount;
	}
    pLock->m_lock.unlock();
	return nRet;
}

//“进入写”操作
void SWMRLock_EnableWrite(YDSWMRLock* pLock)
{
	while (1)
	{
		//第一重循环，检测"写"标示是否为“假”,尝试抢夺"写"权
        pLock->m_lock.lock();	//进入锁域
		{
			//在此，其他访问本单写多读锁的线程
			//会因为内部锁的作用，被挂住
			//判断是否可以抢夺"写"权利
			if (!pLock->m_bIsWriting)
			{
				//立刻将写标示置为真
				pLock->m_bIsWriting = true;

				//抢到"写"权后，本轮逻辑ok,解除内部锁后，可以退出
				//
                pLock->m_lock.unlock();
				goto SWMRLock_EnableWrite_Wait_Read_Clean;
			}

			//此处,“写”标示为真,表示其他线程已经抢到"写"权
			//本线程必须悬挂等待
		}
		//等待睡眠时,应该及时是否内部锁，避免其他线程被连锁挂死
        pLock->m_lock.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

	//"写"权已经被本线程抢到
SWMRLock_EnableWrite_Wait_Read_Clean:
	//等待其他"读操作完毕
	while(SWMRLock_GetReadCount(pLock))
	{
		//第二次循环,等待所有"读"操作结束
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}

//退出写
void SWMRLock_DisableWrite(YDSWMRLock* pLock)
{
    pLock->m_lock.lock();
	{
		pLock->m_bIsWriting = false;
	}
    pLock->m_lock.unlock();
}

//进入读
int SWMRLock_AddRead(YDSWMRLock* pLock)
{
	while(1)
	{
        pLock->m_lock.lock();
		{
			if (!pLock->m_bIsWriting)
			{
				pLock->m_nReadCount++;
                pLock->m_lock.unlock();
				return SWMRLock_GetReadCount(pLock);
			}
		}
        pLock->m_lock.unlock();
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}

}

//退出读
int SWMRLock_DecRead(YDSWMRLock* pLock)
{
	int nRet = 0;
    pLock->m_lock.lock();
	{
		if (0 < pLock->m_nReadCount)
		{
			pLock->m_nReadCount--;
		}
		nRet = pLock->m_nReadCount;
	}
     pLock->m_lock.unlock();
	return nRet;
}


YDLock::YDLock()
{
	SWMRLock_Create(&m_lock);
}

YDLock::YDLock(const YDLock& other)
{

}

YDLock& YDLock::operator=(const YDLock& other)
{
	return *this;
}

YDLock::~YDLock()
{
	SWMRLock_Destroy(&m_lock);
}

void YDLock::AddRead()
{
	SWMRLock_AddRead(&m_lock);
}

void YDLock::DecRead()
{
	SWMRLock_DecRead(&m_lock);
}

void YDLock::EnableWrite()
{
	SWMRLock_EnableWrite(&m_lock);
}

void YDLock::DisableWrite()
{
	SWMRLock_DisableWrite(&m_lock);
}
