#include "BriBigFile.h"
#include "YDCommonFunc.h"


BriBigFile::BriBigFile()
	: 
#ifdef _USE_FILE_HANDLE_
m_hFile(NULL)
#else
m_pFile(nullptr)
#endif
	, m_bWriteFile(false)
	, m_nFrameIndex(0)
	, m_nFrameAddress(0)
{
	
}


BriBigFile::~BriBigFile()
{
    m_lockFile.EnableWrite();
#ifdef _USE_FILE_HANDLE_
    if (m_hFile)
    {
        CloseHandle(m_hFile);
        m_hFile = NULL;
    }
#else
	if (m_pFile)
	{
        fclose(m_pFile);
        m_pFile = NULL;
	}
#endif // _USE_FILE_HANDLE_
    m_lockFile.DisableWrite();
}

bool  BriBigFile::isOpen(void)
{
#ifdef _USE_FILE_HANDLE_
    return m_hFile;
#else
    return m_pFile;
#endif
}

///  创建大文件
bool  BriBigFile::createBigFile(const char* lpPath/*文件路径*/
    , int nFrameSize                              /*单帧数据大小*/
    , const char* lpCameraType                    /*= "unkown" 像机类型*/
    , int nCameraIndex                            /* = 0像机序号*/
    , int nW                                      /* = 0*/
    , int nH                                      /* = 0*/
    , int nC                                      /* = 0*/
    , int nFrameCount                             /* = 100000单个文件最大帧数*/
    )
{
	bool bRet = false;

	do 
	{
		if (   nullptr == lpPath
            || nFrameSize <= 0
			|| nFrameCount <= 0)
		{
			break;
		}
		m_nFrameIndex   = 0;             /// 帧号
		m_nFrameAddress = 0;

        m_lockFile.EnableWrite();
#ifdef _USE_FILE_HANDLE_
        if (m_hFile)
        {
            CloseHandle(m_hFile);
            m_hFile = NULL;
        }
        m_hFile = (HANDLE)CreateFileA(lpPath
            , GENERIC_WRITE | GENERIC_READ
            , FILE_SHARE_WRITE | FILE_SHARE_READ
            , NULL
            , CREATE_ALWAYS
            , FILE_FLAG_NO_BUFFERING
            , NULL);
       
#else
		if (m_pFile)
		{
			fclose(m_pFile);
			m_pFile = NULL;
		}
      
		m_pFile = fopen(lpPath, "wb+");
#endif
        m_lockFile.DisableWrite();
#ifdef _USE_FILE_HANDLE_
        if (INVALID_HANDLE_VALUE == m_hFile)
        {
            //FILE_FLAG_OVERLAPPED
            m_hFile = NULL;
            break;
        }
#else
		if (NULL == m_pFile)
		{
			break;
		}
#endif
		m_strFilePath = lpPath;
        nFrameSize = YDusky::get_512_Times(nFrameSize);
		if (!m_fileHeader.InitFileHeader(nFrameCount, nFrameSize, lpCameraType, nCameraIndex, nW, nH, nC))
			break;

		/// 把文件头写入
        m_lockFile.AddRead();
#ifdef _USE_FILE_HANDLE_
        DWORD dwWriteNum = 0;
        SetFilePointer(m_hFile, 0, 0, FILE_BEGIN);
		long lBufferSize = YDusky::get_512_Times(sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*) + sizeof(DATA_ADDRESS) * getFrameCount());
        
        char* pBuffer = new char[lBufferSize];
        if (pBuffer)
        {
            memcpy(pBuffer, &m_fileHeader, sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*));
            memcpy(pBuffer + sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*), m_fileHeader.m_pBlockAddress, sizeof(DATA_ADDRESS) * getFrameCount());
            WriteFile(m_hFile, pBuffer, lBufferSize, &dwWriteNum, NULL);
            delete[] pBuffer;
            pBuffer = NULL;
        }
#else
        fseek(m_pFile, 0, 0);
        fwrite(&m_fileHeader, 1, sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*), m_pFile);
        fwrite(m_fileHeader.m_pBlockAddress, 1, sizeof(DATA_ADDRESS) * getFrameCount(), m_pFile);
        long lBufferSize = sizeof(tagFileHeader) + sizeof(DATA_ADDRESS) * getFrameCount();
#endif
		m_lockFile.DecRead();
		m_nFrameAddress = lBufferSize;
		bRet            = true;
		m_bWriteFile    = true;

	} while (0);

	return bRet;
}

///  打开大文件
BriBigFile::eErrorInfo  BriBigFile::openBigFile(const char* lpPath)
{
	eErrorInfo bRet = eEI_FAILED;

	do
	{
		m_nFrameIndex   = 0;          /// 帧号
		m_nFrameAddress = 0;

		m_lockFile.EnableWrite();
#ifdef _USE_FILE_HANDLE_
		if (m_hFile)
		{
			CloseHandle(m_hFile);
			m_hFile = NULL;
		}

		m_hFile = (HANDLE)CreateFileA(lpPath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING
			, FILE_FLAG_NO_BUFFERING
			, NULL);
#else
		if (m_pFile)
		{
			fclose(m_pFile);
			m_pFile = NULL;
		}
		std::locale::global(std::locale(""));
		m_pFile = fopen(lpPath, "rb");
#endif
		m_lockFile.DisableWrite();
#ifdef _USE_FILE_HANDLE_
		if (INVALID_HANDLE_VALUE == m_hFile)
		{
			CloseHandle(m_hFile);
			m_hFile = NULL;
			break;
		}
		DWORD dwReadNum = 0;
		long lBufferSize0 = sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*);
		long lBufferSize1 = YDusky::get_512_Times(lBufferSize0);
		char* pBuffer = new char[lBufferSize1];
		if (pBuffer)
		{
			int nReadSize = ReadFile(m_hFile, pBuffer, lBufferSize1, &dwReadNum, NULL);
			if (!nReadSize)
			{
				delete[] pBuffer;
				pBuffer = NULL;
				break;
			}
			memcpy(&m_fileHeader, pBuffer, lBufferSize0);
			delete[] pBuffer;
			pBuffer = NULL;
		}
#else
		if (NULL == m_pFile)
		{
			break;
		}
		/// 读取文件头数据
		fread(&m_fileHeader, 1, sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*), m_pFile);
#endif

		m_strFilePath = lpPath;

		/// 检查是否是有效格式文件
		if (!m_fileHeader.IsValidate())
		{
			bRet = eEI_NOSUPPORT;
			break;
		}

		if (!m_fileHeader.InitFileHeader())
			break;

#ifdef _USE_FILE_HANDLE_
		lBufferSize0 = sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*) + m_fileHeader.m_nFrameCount * sizeof(DATA_ADDRESS);
		lBufferSize1 = YDusky::get_512_Times(lBufferSize0);
		pBuffer = new char[lBufferSize1];
		if (pBuffer)
		{
			SetFilePointer(m_hFile, 0, 0, FILE_BEGIN);
			int nReadSize = ReadFile(m_hFile, pBuffer, lBufferSize1, &dwReadNum, NULL);
			if (!nReadSize)
			{
				delete[] pBuffer;
				pBuffer = NULL;
				break;
			}
			memcpy(m_fileHeader.m_pBlockAddress, pBuffer + sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*), m_fileHeader.m_nFrameCount * sizeof(DATA_ADDRESS));
			delete[] pBuffer;
			pBuffer = NULL;
		}
#else
		fread(m_fileHeader.m_pBlockAddress, 1, m_fileHeader.m_nFrameCount * sizeof(DATA_ADDRESS), m_pFile);
#endif


		bRet         = eEI_SUCCESS;
		m_bWriteFile = false;

	} while (0);

	return bRet;
}

///  关闭大文件
void  BriBigFile::closeBigFile(void)
{
	m_nFrameIndex   = 0;          /// 帧号
	m_nFrameAddress = 0;
	m_lockFile.EnableWrite();
#ifdef _USE_FILE_HANDLE_
	if (m_hFile)
	{
		if (m_bWriteFile)
		{
			m_bWriteFile = false;

			DWORD dwWriteNum = 0;
			SetFilePointer(m_hFile, 0, 0, FILE_BEGIN);
			long lBufferSize0 = sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*) + sizeof(DATA_ADDRESS) * getFrameCount();
			long lBufferSize1 = YDusky::get_512_Times(lBufferSize0);
			char* pBuffer = new char[lBufferSize1];
			if (pBuffer)
			{
				memset(pBuffer, 0, sizeof(char) * lBufferSize1);
				memcpy(pBuffer, &m_fileHeader, sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*));
				memcpy(pBuffer + sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*), m_fileHeader.m_pBlockAddress, sizeof(DATA_ADDRESS) * getFrameCount());
				WriteFile(m_hFile, pBuffer, lBufferSize1, &dwWriteNum, NULL);
				m_fileHeader.m_nRealFrameCount = 0;
				delete[] pBuffer;
				pBuffer = NULL;
			}
		}

		CloseHandle(m_hFile);
		m_hFile = NULL;
	}
#else
	if (m_pFile)
	{
		/// 把文件头写入
		if (m_bWriteFile)
		{
			m_bWriteFile = false;

			fseek(m_pFile, 0, 0);
			fwrite(&m_fileHeader, 1, sizeof(tagFileHeader) - sizeof(DATA_ADDRESS*), m_pFile);
			fwrite(m_fileHeader.m_pBlockAddress, 1, sizeof(DATA_ADDRESS) * getFrameCount(), m_pFile);
			m_fileHeader.m_nRealFrameCount = 0;
		}

		fclose(m_pFile);
		m_pFile = NULL;
	}
#endif

	m_lockFile.DisableWrite();
}

std::string BriBigFile::getFilePath(void)
{
	return m_strFilePath;
}

///   获得总数据块数
int   BriBigFile::getFrameCount(void)
{
	return m_fileHeader.m_nFrameCount;
}


///   获取实际帧数
int   BriBigFile::getRealFrameCount(void)
{
	return  m_fileHeader.m_nRealFrameCount;
}


///   根据块序号获得块数据
uint32_t   BriBigFile::getFrameData(int nFrameIndex, unsigned char* pFrameData)
{
    uint32_t nReadSize = -1;
    m_lockFile.AddRead();
	do 
	{
		if (   nullptr == pFrameData
			|| nFrameIndex < 0 
			|| nFrameIndex >= getRealFrameCount())
		{
			break;
		}
#ifdef _USE_FILE_HANDLE_
		LARGE_INTEGER liDistanceToMove;
		liDistanceToMove.QuadPart = m_fileHeader.m_pBlockAddress[nFrameIndex];
		//设置文件指针
		SetFilePointerEx(m_hFile, liDistanceToMove, NULL, FILE_BEGIN);
        ReadFile(m_hFile, pFrameData, m_fileHeader.m_nFrameSize, &nReadSize, NULL);
#else
        fseek(m_pFile, m_fileHeader.m_pBlockAddress[nFrameIndex], 0);

        nReadSize = fread(pFrameData, 1, m_fileHeader.m_nFrameSize, m_pFile);
#endif


	} while (0);
     m_lockFile.DecRead();
	return nReadSize;
}

 int  BriBigFile::getFrameSize(void)
 {
     return m_fileHeader.m_nFrameSize;
 }

 
///   依次添加数据
uint32_t  BriBigFile::addFrameData(unsigned char* pFrameData, unsigned nFrameCount/* = 1*/)
{
    m_lockFile.EnableWrite();
    uint32_t nWriteSize = -1;
	do
	{
		if (   nullptr == pFrameData)
		{
			break;
		}
		if (m_nFrameIndex + nFrameCount>= m_fileHeader.m_nFrameCount)
		{
			break;
		}

#ifdef _USE_FILE_HANDLE_
        //// 将压缩后的数据进行存储
        WriteFile(m_hFile, pFrameData, m_fileHeader.m_nFrameSize * nFrameCount, &nWriteSize, NULL);
#else
        //// 将压缩后的数据进行存储
        nWriteSize += fwrite(pFrameData, 1, m_fileHeader.m_nFrameSize * nFrameCount, m_pFile);
#endif
		for (size_t i = 0; i < nFrameCount; i++)
		{
			m_fileHeader.m_pBlockAddress[m_nFrameIndex] = m_nFrameAddress + i * m_fileHeader.m_nFrameSize;
			m_nFrameIndex++;
		}
		
		m_nFrameAddress = m_nFrameAddress + m_fileHeader.m_nFrameSize * nFrameCount;

		m_fileHeader.m_nRealFrameCount += nFrameCount;

	} while (0);

    m_lockFile.DisableWrite();

	return nWriteSize;
}


int  BriBigFile::getFrameWidth(void)
{
    return m_fileHeader.m_nFrameW;
}

int  BriBigFile::getFrameHeight(void)
{
    return m_fileHeader.m_nFrameH;
}

int  BriBigFile::getFrameChannels(void)
{
    return m_fileHeader.m_nFrameC;
}

int BriBigFile::getFileFlag(void)
{
    return m_fileHeader.m_nFileFlag;
}
