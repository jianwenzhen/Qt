/************************************************************************
/* 线程基类
/* 功能: 实现跨平台的线程基类
/* 作者: YDusky@126.com
/* 创建: 2017.02.20*/
/************************************************************************/
#ifndef _YDUSKY_COREBASE_THREAD_HEADER_FILE_2017_02_20_15_27_
#define _YDUSKY_COREBASE_THREAD_HEADER_FILE_2017_02_20_15_27_
#include <thread>
#include <atomic>
#include <condition_variable>
#include <YDCommonFunc.h>

class YDThread
{
public:
	YDThread();
	virtual ~YDThread();

public:
	virtual void run();
public:
	bool        start(void);
	void        stop(void);
	void        trigger(void);
	bool        isBusy(void);
	bool        isRun(void);
public:
	void        waitTrigger(void);

	void        waitThreadQuit(void);
	void        threadProcessStart(void);
	void        threadProcessEnd(void);
protected:
	bool        needWait(void);
	void        needWait(bool bWait);
	void        setRun(bool bTrue);
	void        setBusy(bool bTrue);
	static void threadProc(void* lpParameter);


private:
	std::atomic<bool>            _bRun;
	std::atomic<bool>            _bBusy;
	std::atomic<bool>            _bProcess;
	std::atomic<bool>            _bWait;////解决c++的条件变量只能由另外一个线程激活而导致线程无法自循环运行
	std::thread*                 _pThread;
	std::mutex                   _lcMutex;
	std::condition_variable      _condition;
};

#endif
