/************************************************************************
/* 同步锁
/* 功能: 单写多读锁的封装
/* 作者: YDusky (YDusky@126.com)
/* 创建: 2016.05.06
/************************************************************************/
#ifndef _YDLOCKDEFINE_FILE_2017_07_25_14_51_01_
#define _YDLOCKDEFINE_FILE_2017_07_25_14_51_01_

#include <mutex>
#include <YDCommonFunc.h>

struct YDSWMRLock
{
	bool m_bIsWriting;	//是否正在写操作
	int  m_nReadCount;	//读操作计数器
    std::mutex m_lock;		//

};

extern const unsigned YDSWMRLockSize;


//创建单写多读锁
void SWMRLock_Create(YDSWMRLock* pLock);

//销毁单写多读锁
void SWMRLock_Destroy(YDSWMRLock* pLock);

//获取写状态
bool SWMRLock_GetWriteFlag(YDSWMRLock* pLock);

//获取读操作计数器
int SWMRLock_GetReadCount(YDSWMRLock* pLock);

//“进入写”操作
void SWMRLock_EnableWrite(YDSWMRLock* pLock);

//退出写
void SWMRLock_DisableWrite(YDSWMRLock* pLock);

//进入读
int SWMRLock_AddRead(YDSWMRLock* pLock);

//退出读
int SWMRLock_DecRead(YDSWMRLock* pLock);


/************************************************************************/
/* 单写多读锁                                                                     */
/************************************************************************/
class YDLock
{
private:
	YDSWMRLock m_lock;
	
public:
	YDLock();
	~YDLock();
	YDLock(const YDLock& other);
	YDLock& operator=(const YDLock& other);
public:
	void AddRead();
	void DecRead();
	void EnableWrite();
	void DisableWrite();
};

#endif
