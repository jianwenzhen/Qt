#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include "net.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnConn_clicked();
    void on_btnSend_clicked();
    void receive_data();

private:
    Net             _net;
    unsigned int    port = 8888;
    QString         ip = "127.0.0.1";
    QString         received = "";
    QTcpSocket     *socket;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
