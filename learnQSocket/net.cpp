#include "net.h"
#include <QMessageBox>
Net::Net(QObject* pParent )
    :QObject(pParent)
{

}
Net::~Net()
{

}

void runSendThread(Net *net, TestData &data)
{
    if(net)
    {
        net->sendData(data);
    }
}

void Net::startSendThread(TestData& data)
{
    std::thread run(runSendThread, this, std::ref(data));
    run.detach();
}

void Net::sendData(TestData &data)
{
     if (m_pTcpSocket == 0)
        m_pTcpSocket = new QTcpSocket();
    if (m_pTcpSocket->state() != QAbstractSocket::ConnectedState)
    {
        m_pTcpSocket->connectToHost("127.0.0.1", 8888);
        // m_pTcpSocket->setReadBufferSize();
        if (m_pTcpSocket->waitForConnected(500))
        {
            connect(m_pTcpSocket, SIGNAL(readyRead()), this, SLOT(receive_data()), Qt::DirectConnection);
            connect(m_pTcpSocket, SIGNAL(disconnected()), m_pTcpSocket, SLOT(deleteLater()));
        }
        else
        {
            qDebug("connect server failure!");
        }
    }
    qDebug("name: %s, age: %d", data.name, data.age);
//    while(m_bIsSend)
//    {
//        _sleep(1);
//    }
//    m_bIsSend = true;
    m_pTcpSocket->write((char *)&data, sizeof(TestData));
//    m_bIsSend = false;
//    return;
}

void Net::receive_data()
{
    QByteArray receive = m_pTcpSocket->readAll();
    QMessageBox::information(nullptr, tr("hello"), QString(receive));
    qDebug(receive);
    qDebug("receive_data.............");
}
