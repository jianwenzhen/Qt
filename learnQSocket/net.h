#ifndef NET_H
#define NET_H

#include <QObject>
#include <QTcpSocket>
#include <thread>
#include <mutex>

struct TestData
{
    char name[20];
    int age;
};

class Net: public QObject
{
    Q_OBJECT
public:
    Net(QObject* pParent = Q_NULLPTR);
    ~Net();
protected slots:
    void receive_data();

public:
    void startSendThread(TestData& data);
    void sendData(TestData&);

private:
    QTcpSocket     *m_pTcpSocket = nullptr;
    bool            m_bIsSend = false;
};

#endif // NET_H
