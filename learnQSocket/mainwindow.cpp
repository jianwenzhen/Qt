#include <QTcpSocket>
#include <QMessageBox>
#include <QDebug>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "net.h"
#include <time.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Client");

    ui->ipEdit->setText(ip);
    ui->portEdit->setText(QString::number(port));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnConn_clicked()
{
    ip = ui->ipEdit->text();
    port = ui->portEdit->text().toInt();
    if (ui->btnConn->text() == "connect") {
        socket = new QTcpSocket();
        connect(socket, SIGNAL(readyRead()), this, SLOT(receive_data()));
        // 销毁自身 防止内存泄漏
        connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));
        socket->connectToHost(ip, port);
        if (!socket->waitForConnected(30000)) {
            qDebug("Connect Failed!\n");
            QMessageBox::warning(this, "Error!", "Connect Failed!", QMessageBox::Yes);
        } else {
            ui->btnConn->setText("disconnect");
        }
    } else {
        if (socket->state() == QAbstractSocket::ConnectedState || socket->state() == QAbstractSocket::ConnectingState)
        {
            // 与服务器断开连接
            socket->disconnectFromHost();
        }

        ui->btnConn->setText("connect");
    }
}

void MainWindow::on_btnSend_clicked()
{
    //if (socket->state() == QAbstractSocket::ConnectedState)
    //{
//        QString sendMsg = ui->sendEdit->toPlainText();
//        sendMsg += "\n";
//        socket->write(sendMsg.toLatin1());
//        ui->sendEdit->clear();

//        TestData send {"Tom", 20};
//        socket->write((char *)&send, sizeof(TestData));
        static int i = 0;
        //while(1)
        //{
            TestData send {"Tom", i++};
            _net.startSendThread(send);
            _sleep(1000);
        //}
//    }
//    else
//    {
//        if (ui->btnConn->text() == "connect")
//        {
//            QMessageBox::warning(this, "Error!", "Connect to Host First!", QMessageBox::Yes);

//        }
//        else
//        {
//            QMessageBox::warning(this, "Error!", "Connection is abort!", QMessageBox::Yes);
//            ui->btnConn->setText("connect");
//        }
//    }
}

void MainWindow::receive_data()
{
    QByteArray receiveData = socket->readAll();
    received += QString(receiveData);
    ui->receiveEdit->setText(QString(received));
}
