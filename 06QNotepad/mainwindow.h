#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QLineEdit>
#include <QDialog>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class QDialog;
class QLineEdit;
class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public:
    void            newFile();
    bool            needSave();
    bool            save();
    bool            saveAs();
    bool            saveFile(const QString& fileName);
    bool            loadFile(const QString& filename);

protected:
    void            closeEvent(QCloseEvent *event);

private slots:
    void on_action_New_triggered();

    void on_action_Save_triggered();

    void on_action_Save_As_triggered();

    void on_action_Find_triggered();

    void on_action_Undo_triggered();

    void on_action_Cut_triggered();

    void on_action_Paste_triggered();

    void on_action_Cop_y_triggered();

    void on_action_Exit_triggered();

    void on_actionLoad_triggered();

    void show_find_text();

    void on_action_Find_2_triggered();

private:
    Ui::MainWindow *ui;
    bool            m_bIsUntitled;
    QString         m_strCurrentFile;
    QLineEdit      *m_findLineEdit;
    QDialog        *m_findDlg;
    QLabel         *m_statusLabel;
    QLabel         *m_permanentLabel;

};

#endif // MAINWINDOW_H
