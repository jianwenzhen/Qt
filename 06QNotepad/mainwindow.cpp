#include <QMessageBox>
#include <QPushButton>
#include <QFileDialog>
#include <QTextStream>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_bIsUntitled(true),
    m_strCurrentFile(tr("未命名.txt"))
{
    ui->setupUi(this);
    setWindowTitle(m_strCurrentFile);

    m_findDlg = new QDialog(this);
    m_findDlg->setWindowTitle("查找");
    m_findLineEdit = new QLineEdit(m_findDlg);
    QPushButton *btnFindNext = new QPushButton(tr("查找下一个"), m_findDlg);
    QVBoxLayout *layout = new QVBoxLayout(m_findDlg);
    layout->addWidget(m_findLineEdit);
    layout->addWidget(btnFindNext);
    connect(btnFindNext, SIGNAL(clicked(bool)), this, SLOT(show_find_text()));

    m_statusLabel = new QLabel(this);
    m_statusLabel->setMinimumSize(150, 20);
    // 设置标签形状
    m_statusLabel->setFrameShape(QFrame::WinPanel);
    // 设置标签阴影
    m_statusLabel->setFrameShadow(QFrame::Sunken);
    m_statusLabel->setText(tr("SW简易文本编辑器"));
    ui->statusBar->addWidget(m_statusLabel);

    // 左下角显示2秒 提示文字
    ui->statusBar->showMessage("欢迎使用SW文本编辑器", 2000);

    m_permanentLabel = new QLabel(this);
    // 另一种方式设置形状和阴影
    m_permanentLabel->setFrameStyle(QFrame::Box | QFrame::Sunken);
    m_permanentLabel->setText("<a href='http://www.scoder.xyz'>scoder.xyz</a>");
    // 开启富文本模式
    m_permanentLabel->setTextFormat(Qt::RichText);
    // 可以打开链接
    m_permanentLabel->setOpenExternalLinks(true);
    ui->statusBar->addPermanentWidget(m_permanentLabel);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newFile()
{
    if (needSave())
    {
        m_bIsUntitled = true;
        m_strCurrentFile = tr("未命名.txt");
        setWindowTitle(m_strCurrentFile);
        ui->textEdit->clear();
        ui->textEdit->setFocus();
    }
}

bool MainWindow::needSave()
{
    // 内容被修改了
    if (ui->textEdit->document()->isModified())
    {
        QMessageBox box;
        box.setWindowTitle(tr("警告"));
        box.setIcon(QMessageBox::Warning);
        box.setText(tr("尚未保存, 是否保存?"));
        QPushButton *btnYes = box.addButton(tr("是(&Y)"), QMessageBox::YesRole);
        box.addButton(tr("否(&N)"), QMessageBox::NoRole);
        QPushButton *btnCancel = box.addButton(tr("取消(&C)"), QMessageBox::RejectRole);
        box.exec();
        if(box.clickedButton() == btnYes)
        {
            return save();
        }
        else if (box.clickedButton() == btnCancel)
            return false;
        else
            return true;
    }
    return true;
}

bool MainWindow::save()
{
    if(m_bIsUntitled)
    {
        return saveAs();
    }
    else
        return saveFile(m_strCurrentFile);
}

bool MainWindow::saveAs()
{
    QString filename = QFileDialog::getSaveFileName(this, tr("另存为"), m_strCurrentFile);
    if (filename.isEmpty())
        return false;
    return saveFile(filename);
}

bool MainWindow::saveFile(const QString& fileName)
{
    QFile file(fileName);

    if(!file.open(QFile::WriteOnly | QFile::Text | QFile::Truncate))
    {
        QMessageBox::warning(this, tr("QNotepad"), tr("无法写入文件 %1: \n %2").arg(fileName).arg(file.errorString()));
        return false;
    }
    QTextStream out(&file);
    // 鼠标变为等待状态
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << ui->textEdit->toPlainText();
    // 鼠标状态变回来
    QApplication::restoreOverrideCursor();
    m_bIsUntitled = false;
    // 获取文件标准路径
    m_strCurrentFile = QFileInfo(file).canonicalFilePath();
    return true;
}

void MainWindow::on_action_New_triggered()
{
    newFile();
}

void MainWindow::on_action_Save_triggered()
{
    save();
}

void MainWindow::on_action_Save_As_triggered()
{
    saveAs();
}

void MainWindow::on_action_Undo_triggered()
{
    ui->textEdit->undo();
}

void MainWindow::on_action_Cut_triggered()
{
    ui->textEdit->cut();
}

void MainWindow::on_action_Paste_triggered()
{
    ui->textEdit->paste();
}

void MainWindow::on_action_Cop_y_triggered()
{
    ui->textEdit->copy();
}

void MainWindow::on_action_Exit_triggered()
{
    // qApp 是指向程序的全局指针
    if(needSave())
        qApp->exit();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(needSave())
        event->accept();
    else
        event->ignore();
}

void MainWindow::on_actionLoad_triggered()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("打开文件"));
    if(needSave())
    {
        loadFile(filename);
    }
}

void MainWindow::show_find_text()
{
    QString word = m_findLineEdit->text();
    // QTextDocument::FindFlags 默认向前查找
    bool bFind = ui->textEdit->find(word, QTextDocument::FindBackward);
    if(!bFind)
    {
        QMessageBox::warning(this, tr("查找"), tr("未找到%1").arg(word));
    }
}

bool MainWindow::loadFile(const QString& filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, tr("QNotepad"), tr("读取文件失败 %1: \n %2").arg(filename).arg(file.errorString()));
        return false;
    }
    QTextStream in(&file);

    QApplication::setOverrideCursor(Qt::WaitCursor);
    ui->textEdit->setText(in.readAll());
    // ui->textEdit->setPlainText(in.readAll());
    QApplication::restoreOverrideCursor();

    m_strCurrentFile = QFileInfo(file).canonicalFilePath();
    setWindowTitle(m_strCurrentFile);
    return true;
}

void MainWindow::on_action_Find_triggered()
{
    m_findDlg->show();
    m_findLineEdit->setFocus();
}

