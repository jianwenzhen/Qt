#ifndef LASERFUN_H
#define LASERFUN_H

#include <vector>
#include "Urg_driver.h"
#include "Connection_information.h"

class LaserFun
{
public:
    LaserFun();

public:
    Connection_information  connInfo = Connection_information();

    bool connect();
    void startMeasurement();
    bool getDistance(std::vector<long>& data, long& timestamp);

private:
    Urg_driver              urg;
};

#endif // LASERFUN_H
