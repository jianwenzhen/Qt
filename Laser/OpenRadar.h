#ifndef OPENRADAR_H
#define OPENRADAR_H

#include <iostream>
#include <cmath>
#include<cv.h>
#include<highgui.h>
using namespace std;
#include <vector>

static int RadarImageWdith  = 600;
static int RadarImageHeight = 600;

class OpenRadar
{
public:
    OpenRadar(void);
    ~OpenRadar(void);

    vector<long> RadarRho;
    //数据读取
    void setData(const vector<long>& data);
    void CreateRadarImage(IplImage* RadarImage);

};
#endif // OPENRADAR_H
