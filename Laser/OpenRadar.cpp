#include "OpenRadar.h"

#define pi 3.141592653
OpenRadar::OpenRadar(void)
{
}
OpenRadar::~OpenRadar(void)
{
}
void OpenRadar::setData(const vector<long>& data)
{
    RadarRho.clear();
    RadarRho = data;
}

void OpenRadar::CreateRadarImage(IplImage* RadarImage){
    //RadarImage = cvCreateImage(cvSize(RadarImageWdith,RadarImageHeight),IPL_DEPTH_8U,1);
    cvZero(RadarImage);
    //在中心加上一个圆心
    cvCircle(RadarImage, cvPoint(RadarImageWdith/2,RadarImageHeight/2),3, CV_RGB(0,255,255), -1, 8,0);

    int x,y;
    double theta,rho;
    unsigned char * pPixel = 0;
    int halfWidth  = RadarImageWdith/2;
    int halfHeight = RadarImageHeight/2;
    for (int i = 0; i < RadarRho.size();i++)
    {
        theta = (i/4.0 - 45)*pi/180;
        rho = RadarRho.at(i);

        x = (int)(rho*cos(theta)/5) + halfWidth;
        y = (int)(-rho*sin(theta)/5)+ halfHeight;

        if (x >= 0 && x < RadarImageWdith && y >= 0 && y < RadarImageHeight)
        {
            pPixel = (unsigned char*)RadarImage->imageData + y*RadarImage->widthStep + 3*x+2;
            *pPixel = 255;
        }else{
            //cout<<"x: "<<x<<"  y: "<<y<<endl;
        }
    }
}
