#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "OpenRadar.h"
#include "Urg_driver.h"
#include "Connection_information.h"
#include "math_utilities.h"
#include <iostream>

using namespace qrk;
using namespace std;

namespace
{
    void print_data(const Urg_driver& urg,
                    const vector<long>& data, long time_stamp)
    {
#if 1
        /*
        int front_index = urg.step2index(0);
        cout << data[front_index] << " [mm], ("
             << time_stamp << " [msec])" << endl;
        */
        for(int i=0; i<data.size(); ++i) {
            cout << data[i] << "[mm] ";
        }
        cout << "------------------------------------------------------------\n";
#else
        // Prints the X-Y coordinates for all the measurement points
        long min_distance = urg.min_distance();
        long max_distance = urg.max_distance();
        size_t data_n = data.size();
        for (size_t i = 0; i < data_n; ++i) {
            long l = data[i];
            if ((l <= min_distance) || (l >= max_distance)) {
                continue;
            }

            double radian = urg.index2rad(i);
            long x = static_cast<long>(l * cos(radian));
            long y = static_cast<long>(l * sin(radian));
            cout << "(" << x << ", " << y << ")" << endl;
        }
        cout << endl;
#endif
    }
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    // urg.set_scanning_parameter(urg.deg2step(-90), urg.deg2step(+90), 0);
    laser.connect();
    laser.startMeasurement();

    OpenRadar openRadar;
    char key;
    IplImage* RadarImage = cvCreateImage(cvSize(RadarImageWdith,RadarImageHeight),IPL_DEPTH_8U,3);
    vector<long> data;
    long timestamp = 0;

    while (1)
    {
        laser.getDistance(data, timestamp);
        print_data(urg, data, time_stamp);
        openRadar.setData(data);
        openRadar.CreateRadarImage(RadarImage);
        cvNamedWindow("Radar",1);
        cvShowImage("Radar",RadarImage);
        key = cvWaitKey(30);
        if (key == 27)//esc退出
            break;
    }
    cvReleaseImage(&RadarImage);
    cvDestroyWindow("Radar");
}
