#include "LaserFun.h"

LaserFun::LaserFun()
{

}

bool LaserFun::connect()
{
    if (!urg.open(connInfo.device_or_ip_name(),
                  connInfo.baudrate_or_port_number(),
                  connInfo.connection_type())) {
        cout << "Urg_driver::open(): "
             << connInfo.device_or_ip_name() << ": " << urg.what() << endl;
        return false;
    }
    return true;
}

void LaserFun::startMeasurement()
{
    urg.start_measurement(Urg_driver::Distance, Urg_driver::Infinity_times, 0);
}

bool LaserFun::getDistance(std::vector<long> &data, long &timestamp)
{
    if (!urg.get_distance(data, &time_stamp)) {
        cout << "Urg_driver::get_distance(): " << urg.what() << endl;
        return false;
    }
    return true;
}
