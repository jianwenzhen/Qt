#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "imagewidget.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QString>
#include <QDir>
#include <QThread>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // ui->widget->setImage(QString::fromLocal8Bit("C:/Users/Public/Pictures/Sample Pictures/2.jpg"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Exit_triggered()
{
    exit(0);
}

void MainWindow::on_action_Open_triggered()
{
    // 打开文件夹 得到所有jpg文件列表
    QString path = QFileDialog::getExistingDirectory(this, tr("open dir"));
    QDir dir(path);
    if(!dir.exists())
    {
        QMessageBox::information(this,
                            tr("打开文件夹失败"),
                            tr("打开文件夹失败!"));
        return ;
    }
    // 设置过滤条件 只保留文件
    dir.setFilter(QDir::Files);
    QStringList nameFilter;
    nameFilter.append("*.jpg");
    dir.setNameFilters(nameFilter);
    // 得到文件列表
    QFileInfoList infoList = dir.entryInfoList();
//    QFileInfoList suffixInfoList;
//    foreach (QFileInfo info, infoList) {
//        // 把jpg文件添加到 suffixInfoList
//        if(info.suffix().compare(".jpg") == 0 || info.suffix().compare(".JPG") == 0) // 这个有问题 比较结果不对
//        {
//            suffixInfoList.append(info);
//        }
//    }
    foreach (QFileInfo fileInfo, infoList) {
        ui->widget->setImage(fileInfo.filePath());
        // 延时
        QThread::msleep(40);
    }

    return;
}
