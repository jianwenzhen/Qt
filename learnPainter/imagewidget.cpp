#include <QPainter>
#include <QWheelEvent>
#include <iostream>
#include <cstdlib>
#include "imagewidget.h"

ImageWidget::ImageWidget(QWidget *parent) : QWidget(parent)
{

}

void ImageWidget::setImage(const QPixmap& image)
{
    m_image = image;
    m_posImage = rect();
    repaint();
}

void ImageWidget::setImage(const QString& strPath)
{
    m_image.load(strPath);
    m_posImage = rect();
    repaint();
}

void ImageWidget::paintEvent(QPaintEvent *)
{
    QPainter paint(this);

    if(!m_image.isNull())
    {
        // 计算图片相对于窗口的缩放比例
        float fScaleImageW = m_image.width()/rect().width();
        float fScaleImageH = m_image.height()/rect().height();
        // 缩放比例选择大的一方
        float fScaleImage = std::max(fScaleImageW, fScaleImageH);
        // 计算图片应该显示的实际宽高
        float fImageW = m_image.width() / fScaleImage * fScale;
        float fImageH = m_image.height() / fScaleImage * fScale;
        // 计算起始坐标点
        float fLeft =(width() - fImageW)/2.0;
        float fTop =(height() - fImageH)/2.0;

        m_posImage.setLeft(fLeft);
        m_posImage.setTop(fTop);
        m_posImage.setWidth(fImageW);
        m_posImage.setHeight(fImageH);

        paint.drawPixmap(m_posImage, m_image);
    }
}

 void ImageWidget::wheelEvent(QWheelEvent *event)
{
    // 滚轮滚动度数
    QPoint numDegrees = event->angleDelta() / 8;
    // 鼠标所在位置
    int mouseX = event->x();
    int mouseY = event->y();

    if(!numDegrees.isNull()) {
        if(numDegrees.y() > 0) {
            // 滚轮上滚 放大
            fScale *= 1.1;
        } else {
            // 滚轮下滚 缩小
            fScale *= 0.9;
        }
    }
    update();
}
