#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>

class ImageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ImageWidget(QWidget *parent = 0);

signals:

public:
    void         setImage(const QPixmap& image);
    void         setImage(const QString& strPath);
protected:
    virtual void paintEvent(QPaintEvent *event);
    virtual void wheelEvent(QWheelEvent *event);
public slots:

private:
int     count = 0;
QRect   m_posImage;
QPixmap m_image;
float   fScale = 1.0;
};

#endif // IMAGEWIDGET_H
