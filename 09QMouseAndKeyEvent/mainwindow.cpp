#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    int x = event->x();
    int y = event->y();
    ui->pushButton->setText(tr("%1, %2").arg(x).arg(y));
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    int x = ui->pushButton->x();
    int y = ui->pushButton->y();
    int key = event->key();
    switch(key)
    {
    case Qt::Key_W:
        ui->pushButton->move(x, y-10);
        break;
    case Qt::Key_S:
        ui->pushButton->move(x, y+10);
        break;
    case Qt::Key_A:
        ui->pushButton->move(x-10, y);
        break;
    case Qt::Key_D:
        ui->pushButton->move(x+10, y);
        break;
    }
}
