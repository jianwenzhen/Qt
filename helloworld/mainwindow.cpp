#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <chrono>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    tm *t = gettm(1503136680123);
}

MainWindow::~MainWindow()
{
    delete ui;
    // qDebug(tr("%1 year, %2 month, %3 day, %4 hour, %5 minutes, %6 second, %7 mill").arg(t->tm_year+1900).arg(t->tm_mon+1).arg(t->tm_mday).arg(t->tm_hour).arg(t->tm_min).arg(t->tm_sec));
}

tm* MainWindow::gettm(int64_t timestamp)
{
    int64_t milli = timestamp + (int64_t)8*60*60*1000;//此处转化为东八区北京时间，如果是其它时区需要按需求修改
    auto mTime = std::chrono::milliseconds(milli);
    auto tp=std::chrono::time_point<std::chrono::system_clock,std::chrono::milliseconds>(mTime);
    auto tt = std::chrono::system_clock::to_time_t(tp);
    tm* now = std::gmtime(&tt);
    fprintf(stderr, "%4d/%02d/%02d %02d:%02d:%02d %03d\n", now->tm_year+1900,now->tm_mon+1,now->tm_mday,now->tm_hour,now->tm_min,now->tm_sec, timestamp%1000);
    return now;
}
